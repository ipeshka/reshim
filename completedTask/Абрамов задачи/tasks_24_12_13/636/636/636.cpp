// 636.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>

/* ���������� ��� ������� */
void Join(int A[], int B[], int *left, int *right,
		  int *kl, int *kr, int Step){
			  // ����������� ������� �������� ������� ������ A:
			  //   ������, ������� � *left � �������, ������� � *right
			  // ( �� ���� ���� *left, *right ����������)
			  // ��������� ������� ���������� � ������ B, ������� �
			  // *kl ��� *kr ���������� � ����� Step. Step=1,
			  // ���� ��������� ������� ���������� � b ����� ������� �
			  // Step=-1, ���� ������ ������
			  bool l=true,r=true; // �������� ����, ��� �������
			  // ��������������� (left,right) ��� �� ��������
			  int v;

			  while(l || r){
				  // ������ v - ������� ���� � ��������� �������
				  if(l && (!r || A[*left] <= A[*right])){
					  v=A[(*left)++];
					  l=(*left<*right && A[*left] >= A[*left-1]);
				  } else {
					  v=A[(*right)--];
					  r=(*right>*left && A[*right] >= A[*right+1]);
				  }
				  if(Step==1){
					  B[(*kl)++]=v;
				  } else {
					  B[(*kr)--]=v;
				  }
			  }
}

//------------------------------------------------
int Prohod(int A[], int B[], int N){
	// ������� ��������� ��������� �� ������� A � ������� B
	// � ���������� ����� �������� ��������������� � ������� B
	int left,right,kl,kr;
	int Count,step;

	left=0; right=N-1; kl=0; kr=N-1; step=1;Count=0;

	while(left<right){
		Join(A,B,&left,&right,&kl,&kr,step);
		if(right==left){
			B[kl]=A[left];
			Count++;
		}
		Count++;
		step=-step;
	}
	return Count;
}

//----------------------------------------------
void Neumann(int N, int A[]){
	int *B;
	bool flag=false; // ���� flag=false, �� ���������
	// ��������� �� A � B
	int *from,*to;

	B=new int[N];
	do {
		if(flag){
			from=B;
			to=A;
		} else {
			from=A;
			to=B;
		}
		flag=!flag;
	} while(Prohod(from,to,N) > 1);
	if(flag){
		memcpy(A,B,N*sizeof(int));
	}
	delete [] B;
}


int main() {
	const int N =10;
	int a[N];
	for(int i=0;i<N;i++) {
		a[i] = rand() % 100;
		printf("%4d", a[i]);
	}
	printf("\n");

	Neumann(10, a);

	for (int i=0; i<N; i++)
		printf("%4d", a[i]);

	return 0;
}