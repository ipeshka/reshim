#include "stdafx.h"
#include <string.h>

int _tmain(int argc, _TCHAR* argv[])
{
	char s[1024];
	char buffer[256];
	printf("Input string: ");
	gets(s); // записываем строку
	for (int i=0; i < strlen(s); i++) // цикл по символам строки
		if ((s[i]=='.')) { // если нашли точку
			// копируем из буфера строку, которая идёт за точкой
			strncpy(buffer, s + i + 1, strlen(s));
			// добавляем точек
			s[i + 1] = '.';
			s[i + 2] = '.';
			// конец строки ставим
			s[i + 3] = '\0';
			// контектируем две строки
			strcat(s, buffer);
			// передвигаем счётчик
			i+=2;
		}

	printf("%s", s);
	return 0;
}