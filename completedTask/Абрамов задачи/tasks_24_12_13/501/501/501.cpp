#include "stdafx.h"
#include <stdio.h>
#include <string.h>

#define COUNT_FIO 100
int main(){
	FILE *f;
	FILE *g;
	char buffer[128 * 3] = "";

	struct FIO { //структура для хранения имени
		char f[128]; // фамилия
		char i[128]; // имя
		char o[128]; // отчество
	};


	struct FIO fios[COUNT_FIO];
	char i = 0;
 
	// котрываем файл с записями
	f = fopen("D://f.txt", "r+");
	while (fscanf (f, "%s,", buffer) != EOF) {
		// прочитав записываем во массив структур
		strcpy(fios[i].f, strtok(buffer, "_"));
		strcpy(fios[i].i, strtok('\0', "_"));
		strcpy(fios[i].o, strtok('\0', ","));
		i++;
	}

	// открываем файл на запись
	g = fopen("D://g.txt", "w+");
	for(int j = 0; j < i; j++) {
		// записываме в другом порядке
		fprintf(g, "%s %s %s,", fios[j].i, fios[j].o, fios[j].f);
	}

	fclose(f);
	fclose(g);
	return 0;
}
