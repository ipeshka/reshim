#include "stdafx.h"
# include <stdio.h>
# include <conio.h>
# include <math.h>
# define N 10

void main()
{
	double a = 0;
	printf("Input a: ");
	scanf("%f", &a);
	double result;
	if(a <= 2) {
		result = pow(a, 2) + 4*a+5;
	}else{
		result = pow(pow(a, 2) + 4*a+5, -1);
	}
	printf("%f", result);
	_getch();
}