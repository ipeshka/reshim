// 632.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <stdlib.h>


int main() {
	const int N = 10;
	int a[N];
	int p = 0;
	int k = 0;
	for(int i=0; i<N; i++) {
		printf("%d - ", i + 1);
		scanf("%d", &a[i]);
	}

	printf("Vvedite k - ");
	scanf("%d", &k);

	printf("Vvedite p - ");
	scanf("%d", &p);

	for(int i=k; i<N - 1; i++) {
		a[i] = a[i + 1];
	}

	for(int i=0; i<N; i++) {
		if(a[i] > p) {
			for(int j=N; j>i; j--) {
				a[j] = a[j - 1];
			}
			a[i] = p;
		}
	}


	for(int i=0; i<N; i++) {
		printf("%d ", a[i]);
	}

	return 0;
}

