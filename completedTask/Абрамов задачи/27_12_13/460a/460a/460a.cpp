// 460a.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string.h>
#include <ctype.h>


int string_to_real(char s[]) {
    bool flag = false; int i=0; int sum=0;
    if (s[0]=='-') { flag = true; i=1; }
    for(i; i<strlen(s); i++)
        if (!isdigit(s[i])) break;
        else switch(s[i]) {
            case '0': { sum=sum*10; break; }
            case '1':   { sum=sum*10+1; break; }
            case '2': { sum=sum*10+2; break; }
            case '3': { sum=sum*10+3; break; }
            case '4':{ sum=sum*10+4; break; }
            case '5':{ sum=sum*10+5; break; }
            case '6':{ sum=sum*10+6; break; }
            case '7':{ sum=sum*10+7; break; }
            case '8':{ sum=sum*10+8; break; }
            case '9':{ sum=sum*10+9; break; }}
    if (flag) return -sum; else return sum;
}


int main()
{
	printf("%d", string_to_real("10") + string_to_real("20"));
	return 0;
}

