// 6_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
const int N = 10;


// ������������ ������� �������
float MaxElement(float array1[]) {
	float max = array1[0]; // ��������� �������
	for (int counter = 0; counter < N; counter++) {
		if(array1[counter] > max) {
			max = array1[counter];
		}
	}
	return max;
}
// ����� ��������� �������, ������������� �� ���������� �������������� ��������
float SumPos(float array1[]) {
	int k = N;
	float sum = 0;

	while(array1[k] < 0) k--;
	for(int counter = 0; counter < k; counter++) sum += array1[counter];

	return sum;
}

void compression(int a, int b, float array1[]) // ������� ������
{
	int i, j;
	for (i = 0, j = 0; i < N; ++i) {
		float t = abs(array1[i]);
		if (t < a || t > b) array1[j++] = array1[i];
	}
	while (j < N) array1[j++] = 0;
}

int _tmain(int argc, _TCHAR* argv[]) {

	float array1[N] = { 3.24, -7.16, 2.18, -0.16, -3.22, 7.14, 2.88, -3.20, -0.99, -4.15 }; // ���������� � ������������� ����������� �������
	cout << "max" << "\t\t" << "element massiva" << endl; // ������ ����������
	cout << "max element = " << MaxElement(array1) << endl;
	cout << "summa elements = " << SumPos(array1) << endl;

	compression(2.2, 5.5, array1);

	for (int counter = 0; counter < N; counter++) {
		cout << " " << array1[counter];
	}

	system("pause");
	return 0;
}



