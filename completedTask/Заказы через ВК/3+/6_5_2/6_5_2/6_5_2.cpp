#include "stdafx.h"
#include <iostream>
using namespace std;
const int N = 10;

// ����� ��������� � ��� ��������, ������� �� �������� ������������� ���������
void SummaElementov(int array[N][N]) {

	int i = 0, j = 0;
	for(j = 0; j < N; ++j)
	{
		int sum = 0;
		for(i = 0; i < N; ++i)
		{  
			if(array[i][j] < 0) 
				break;

			sum += array[i][j];
		}

		if(i == N) cout << "sum " << j + 1 << " = " << sum << "\n"; 
	}
}

int sumDiagonal(int array[N][N], int currentDiagonal)
{
    int sum = 0;
    for(int i = 0, j = currentDiagonal; i < N; ++i)
    {
        sum += abs(array[j][i]);
        --j;
        if(j < 0)
            j = N - 1;
    }

    return sum;
}
int MinDiagonal(int matrix[N][N])
{
    int min = sumDiagonal(matrix, 0);
    for(int i = 1, tmp; i < N; ++i)
    {
        tmp = sumDiagonal(matrix, i);
        if(tmp < min)
            min = tmp;
    }
    
	return min;
}



int _tmain(int argc, _TCHAR* argv[])
{
	// ������������� ���������� �������
	int mas[N][N] = 
	{ { 16, 78, 0, 6, -29, 19, -52, 65, -88, 51 }, 
	{ -79, -22, 32, -25, -62, -69, -2, -59, -75, 89 },
	{ -87, 95, -22, 85, -49, -75, 76, 73, -59, -52 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 },
	{ 30, 49, -28, -48, 0, 57, -6, -85, 0, -18 } };


	SummaElementov(mas);
	cout << "min diagonal " << MinDiagonal(mas) << "\n"; 
	system("pause");

	return 0;
}

