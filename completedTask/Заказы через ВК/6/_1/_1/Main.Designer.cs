﻿namespace _1
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonHand = new System.Windows.Forms.RadioButton();
            this.radioButtonRandom = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxN = new System.Windows.Forms.TextBox();
            this.textBoxM = new System.Windows.Forms.TextBox();
            this.log = new System.Windows.Forms.RichTextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.arr1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.arr2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonHand);
            this.groupBox1.Controls.Add(this.radioButtonRandom);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(239, 70);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Инициализация массива";
            // 
            // radioButtonHand
            // 
            this.radioButtonHand.AutoSize = true;
            this.radioButtonHand.Location = new System.Drawing.Point(6, 42);
            this.radioButtonHand.Name = "radioButtonHand";
            this.radioButtonHand.Size = new System.Drawing.Size(67, 17);
            this.radioButtonHand.TabIndex = 1;
            this.radioButtonHand.TabStop = true;
            this.radioButtonHand.Text = "Вручную";
            this.radioButtonHand.UseVisualStyleBackColor = true;
            // 
            // radioButtonRandom
            // 
            this.radioButtonRandom.AutoSize = true;
            this.radioButtonRandom.Location = new System.Drawing.Point(6, 19);
            this.radioButtonRandom.Name = "radioButtonRandom";
            this.radioButtonRandom.Size = new System.Drawing.Size(72, 17);
            this.radioButtonRandom.TabIndex = 0;
            this.radioButtonRandom.TabStop = true;
            this.radioButtonRandom.Text = "Случайно";
            this.radioButtonRandom.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Размерность 1-ого массива:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 203);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Размерность 2-ого массива:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // textBoxN
            // 
            this.textBoxN.Location = new System.Drawing.Point(165, 172);
            this.textBoxN.Name = "textBoxN";
            this.textBoxN.Size = new System.Drawing.Size(86, 20);
            this.textBoxN.TabIndex = 3;
            this.textBoxN.Text = "10";
            // 
            // textBoxM
            // 
            this.textBoxM.Location = new System.Drawing.Point(165, 200);
            this.textBoxM.Name = "textBoxM";
            this.textBoxM.Size = new System.Drawing.Size(86, 20);
            this.textBoxM.TabIndex = 4;
            this.textBoxM.Text = "10";
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(12, 262);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(239, 89);
            this.log.TabIndex = 5;
            this.log.Text = "";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 224);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(239, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Запуск";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // arr1
            // 
            this.arr1.Location = new System.Drawing.Point(70, 93);
            this.arr1.Name = "arr1";
            this.arr1.Size = new System.Drawing.Size(181, 20);
            this.arr1.TabIndex = 8;
            this.arr1.Text = "10 11 3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Массив 1:";
            // 
            // arr2
            // 
            this.arr2.Location = new System.Drawing.Point(70, 118);
            this.arr2.Name = "arr2";
            this.arr2.Size = new System.Drawing.Size(181, 20);
            this.arr2.TabIndex = 10;
            this.arr2.Text = "2 3 6";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 121);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Массив 2:";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(263, 363);
            this.Controls.Add(this.arr2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.arr1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.log);
            this.Controls.Add(this.textBoxM);
            this.Controls.Add(this.textBoxN);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "Main";
            this.Text = "Задание 1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonHand;
        private System.Windows.Forms.RadioButton radioButtonRandom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxN;
        private System.Windows.Forms.TextBox textBoxM;
        private System.Windows.Forms.RichTextBox log;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox arr1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox arr2;
        private System.Windows.Forms.Label label4;
    }
}

