﻿using System;
using System.Windows.Forms;

namespace _1
{
    public partial class Main : Form
    {
        

        public Main()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();

            if (!radioButtonRandom.Checked && !radioButtonHand.Checked)
            {
                MessageBox.Show("Выберите способ заполнения массивов", "Ошибка запуска");
                return;
            }

            int N = Int32.Parse(textBoxN.Text);
            int M = Int32.Parse(textBoxM.Text);

            int[] array_1 = new int[N];
            int[] array_2 = new int[M];


            if (radioButtonRandom.Checked)
            {
                for (int i = 0; i < N; i++)
                {
                    array_1[i] = rnd.Next(10);
                }

                for (int i = 0; i < M; i++)
                {
                    array_2[i] = rnd.Next(10);
                }
            }
            else
            {
                var tmp1 = arr1.Text.Split(' ');
                for (int i = 0; i < N; i++)
                {
                    array_1[i] = Int32.Parse(tmp1[i]);
                }

                var tmp2 = arr2.Text.Split(' ');
                for (int i = 0; i < M; i++)
                {
                    array_2[i] = Int32.Parse(tmp2[i]);
                }
            }

            log.AppendText("Первый массив: ");
            for (int i = 0; i < N; i++)
            {
                log.AppendText(array_1[i].ToString() + " ");
            }
            log.AppendText("\nВторой массив массив: ");
            for (int i = 0; i < M; i++)
            {
                log.AppendText(array_2[i].ToString() + " ");
            }
            log.AppendText("\n");
            log.AppendText(Multiplication(array_1, array_2).ToString());
        }


        private int Multiplication(int[] array_1, int[] array_2)
        {
            int result = 1;
            if (array_1[0] > array_2[0])
            {
                for (int i = 0; i < array_1.Length; i++)
                {
                    result *= array_1[i];
                }
            }
            else if (array_1[0] < array_2[0])
            {
                for (int i = 0; i < array_2.Length; i++)
                {
                    result *= array_2[i];
                }
            }
            else
            {
                int min = array_2.Length;
                if (array_1.Length < array_2.Length) min = array_1.Length;

                for (int i = 0; i < min; i++)
                {
                    result *= array_2[i];
                }
            }

            return result;
        }
    }
}
