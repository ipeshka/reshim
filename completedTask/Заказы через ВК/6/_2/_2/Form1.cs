﻿using System;
using System.Windows.Forms;

namespace _2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random rnd = new Random();
            int n, m;
            int.TryParse(textBoxN.Text, out n);
            int.TryParse(textBoxN.Text, out m);

            int[,] a = new int[n, m];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    int.TryParse(rnd.Next(10).ToString(), out a[i, j]);
                }
            }
            dataGridView1.RowCount = n;
            dataGridView1.ColumnCount = m;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    dataGridView1.Rows[i].Cells[j].Value = a[i, j].ToString();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int n, m;
            int.TryParse(textBoxN.Text, out n);
            int.TryParse(textBoxN.Text, out m);

            int[,] a = new int[n, m];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    a[i, j] = Int32.Parse(dataGridView1.Rows[i].Cells[j].Value.ToString());
                }
            }

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (i == j)
                    {
                        dataGridView1.Rows[i].Cells[j].Value = Max(a, j).ToString();
                    }
                    
                }
            }
        }

        private int Max(int[,] arr, int numberStolb)
        {
            int max = arr[0, numberStolb];
            for (int i = 1; i < Math.Pow(arr.Length, 0.5); i++)
            {
                if (max < arr[i, numberStolb])
                {
                    max = arr[i, numberStolb];
                }
            }

            return max;
        }
    }
}
