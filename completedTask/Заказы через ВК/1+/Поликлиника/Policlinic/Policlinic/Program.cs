﻿using System;
using Policlinic.Data;

namespace Policlinic
{
    class Program
    {
        static void Main(string[] args)
        {
            Hospital hospital = new Hospital();

            // заводим посетителя Ивана
            Visitor ivan = new Visitor(626364, "Иван", "Фёдорович", "Анатолиевич", 26, "Московская", 3, 2);
            hospital.RegisteredVisitor(ivan);


            // заводим псетителя игоря с болезнью
            MedicalHistory disease = new MedicalHistory(1, new DateTime(2014, 12, 3), "Стреляет правое ухо", "Вазинатомия", "Капли в уши 2 раза в день", "Месковец А.В");
            Visitor igar = new Visitor(23234, "Игорь", "Некрашев", "Иванович", 23, "Телефонная", 23, 22, disease);
            hospital.RegisteredVisitor(igar);

            Console.WriteLine(hospital.ToString());// выводим всех посетителей

            // получим одного посетителя из госпиталя
            Visitor visitor = hospital.GetVisitor(23234);
            // добавим ещё одну болезнь
            MedicalHistory disease2 = new MedicalHistory(2, new DateTime(2014, 12, 10), "Стреляет левое ухо", "Вазинатомия", "Капли в уши 4 раза в день", "Месковец А.В");
            visitor.AddDisease(disease2);
            // выведем всю историю болезней о данном посетителе
            Console.WriteLine(visitor.GetMedicalHistory);

            // ивана исключим из больницы
            hospital.UnRegisteredVisitor(23234);
            // выведем на экран всех посетителей
            Console.WriteLine(hospital.ToString());// выводим всех посетителей

            Console.ReadLine(); // что юы не закрывалась консольное коно
        }
    }
}
