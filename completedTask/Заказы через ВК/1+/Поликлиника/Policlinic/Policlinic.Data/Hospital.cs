﻿using System.Collections.Generic;

namespace Policlinic.Data
{
    /// <summary>
    /// класс описывающий больницу, где можно ставить и снимать больных с учёта
    /// </summary>
    public class Hospital
    {
        private List<Visitor> Visitors = new List<Visitor>(); // посетители, которые просто привязаны к больнице

        /// <summary>
        /// ставим на учёт посетителя
        /// </summary>
        /// <param name="visitor"></param>
        public bool RegisteredVisitor(Visitor visitor) {
            var vis = Visitors.Find(x => x.PassportNumber == visitor.PassportNumber);
            if (vis != null) return false;
            // добавлям посетителя
            Visitors.Add(visitor);
            return true;
        }

        /// <summary>
        /// снимаем с учёта посетителя
        /// </summary>
        /// <param name="visitor"></param>yt
        /// <returns></returns>
        public bool UnRegisteredVisitor(int number)
        {
            // находим пользователя
            var visitor = Visitors.Find(x => x.PassportNumber == number);
            if(visitor != null) { // если нашли в посетителях
                Visitors.Remove(visitor); // переносим просто в посетители
                return true;
            }

            return false;
        }

        public Visitor GetVisitor(int numberPassport)
        {
            return Visitors.Find(x => x.PassportNumber == numberPassport);
        }

        public override string ToString()
        {
            string tmp = "";
            foreach (Visitor visitor in Visitors)
            {
                tmp += visitor.ToString() + "\n";
            }

            return tmp;
        }
    }
}
