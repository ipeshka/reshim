﻿using System;

namespace Policlinic.Data
{
    public class MedicalHistory
    {
        private int Number;
        private DateTime DateStartDisease; // когда выявили болезнь
        private DateTime DateEndDisease; // когда вылечили болезнь
        private string Symptoms; // симптомы
        private string Disease; // навазние болезни
        private string Treatment; // описание лечения (лекарственные стредсва)
        private string Doctor; // фио доктора

        // конструктор для заполнения истории
        public MedicalHistory(int number, DateTime dateStartDisease, string symptoms, string disease, string treatment, string doctor)
        {
            Number = number;
            DateStartDisease = dateStartDisease;
            Symptoms = symptoms;
            Disease = disease;
            Treatment = treatment;
            Doctor = doctor;
        }

        public void SetDateEndDisease(DateTime dateEndDisease) {
            DateEndDisease = dateEndDisease;
        }

        // переопределение вывода информации о истории
        public override string ToString()
        {
            return string.Format(
                "Номер - {0}\n" +
                "Дата выявления болезни - {1}\n" +
                "Дата выздоровления - {2}\n" + 
                "Симптомы - {3}\n" +
                "Название болезни - {4}\n" +
                "Описание лечения - {5}\n" +
                "ФИО врача - {6}\n"
                , Number, DateStartDisease, DateEndDisease, Symptoms, Disease, Treatment, Doctor);
        }
    }
}
