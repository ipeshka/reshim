﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Policlinic.Data
{
    /// <summary>
    /// класс описывающий посетителя (больного)
    /// </summary>
    public class Visitor
    {
        private int NumberPassport; // паспорт пользователя, служит идентификатором
        private string FirstName; // имя посетителя
        private string LastName; // фамилия
        private string MiddleName; // отчество

        private int Age; // полных лет

        private string Street; // улица
        private int NumberHouse; // номер дома
        private int NumberRoom; // номер квартиры

        private List<MedicalHistory> MedicalHistory = new List<MedicalHistory>();


        // конструктор для заполнения поситителя
        public Visitor(int numberPassport, string firstName, string lastName, string middleName, int age, string street, int numberHouser, int numberRoom)
        {
            NumberPassport = numberPassport;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            Age = age;
            Street = street;
            NumberHouse = numberHouser;
            NumberRoom = numberRoom;
        }

        // конструктор для заполнения поситителя и добавление записи о болезни
        public Visitor(int numberPassport, string firstName, string lastName, string middleName, int age, string street, int numberHouser, int numberRoom, MedicalHistory disease)
        {
            NumberPassport = numberPassport;
            FirstName = firstName;
            LastName = lastName;
            MiddleName = middleName;
            Age = age;
            Street = street;
            NumberHouse = numberHouser;
            NumberRoom = numberRoom;
            MedicalHistory.Add(disease);
        }


        public int PassportNumber { get { return NumberPassport; } }

        // получения ФИО
        public string FIO
        {
            get
            {
                return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
            }
        }

        public string _LastName
        {
            set { LastName = value; }
        }

        public string _FirstName
        {
            set { FirstName = value; }
        }

        public string _MiddleName
        {
            set { MiddleName = value; }
        }

        // получение места жительсва
        public string PlaceResidence
        {
            get
            {
                return string.Format("{0} {1}/{2}", Street, NumberHouse, NumberRoom);
            }
        }

        /// <summary>
        /// вывод истории болезни
        /// </summary>
        public string GetMedicalHistory {
            get {
                string tmp = "";
                foreach (MedicalHistory disease in MedicalHistory)
                {
                    tmp += disease.ToString() + "\n";
                }

                return tmp;
            }
        }

        /// <summary>
        /// добавление болезни
        /// </summary>
        public void AddDisease(MedicalHistory disease)
        {
            MedicalHistory.Add(disease);
        }

        // переопределение вывода информации о посетителе
        public override string ToString()
        {
            return string.Format(
                "ФИО - {0}\n" +
                "Полных лет - {1}\n" +
                "Место жительства - {2}\n"
                , FIO, Age, PlaceResidence);
        }
    }
}
