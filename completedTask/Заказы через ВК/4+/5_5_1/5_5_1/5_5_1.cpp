// 5_5_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cmath>
#include <iostream>
using namespace std;

bool Compare(double a, double b) {
	return abs(abs(a) - abs(b)) < 0.1;
}


int _tmain(int argc, _TCHAR* argv[])
{
	cout<<(Compare(10.5, 11.5) == 0 ? "false" : "true")<<endl;
	cout<<(Compare(10.53, 10.51) == 0 ? "false" : "true")<<endl;
	system("pause");

	return 0;
}

