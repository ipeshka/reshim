﻿#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"



int main(int argc, char* argv[])
{
	char s, s2; // переменные для чтения символов
	int str = 0; // переменная для хранения номера строки

	// октрываем файлы
	FILE *pFile_1 = fopen(argv[0], "rb");
	FILE *pFile_2 = fopen(argv[1], "rb");
	if (pFile_1 == NULL || pFile_2 == NULL) // если файлы не открылись
	{
		printf("File opening error\n"); // вывод ошибки
		system("pause");
		return -1;
	}


	while (fscanf(pFile_1, "%c", &s) != EOF) // чтение символа пока не конец файла
	{
		fscanf(pFile_2, "%c", &s2); // чтение симвоа из второго файла
		if (s == '\n') str++; // если конец строки, увеличиваем номер строки
		if (s2 != s) { // если расхождение символов
			printf("Files are different. Line number is %d", str + 1);
			system("pause");
			return 1;
		}
	}

	printf("Files are equal");
	system("pause");
	return 0;
}

