﻿#define _CRT_SECURE_NO_WARNINGS
#include "stdafx.h"



int main(int argc, char* argv[])
{
	float a = atoi(argv[1]);
	float b = atoi(argv[2]);
	float c = atoi(argv[3]);
	float D, x1, x2;

	if (a == 0) {
		printf("Error a = 0!");
		system("pause");
		return -1;
	}
	else
	{
		D = pow(b, 2) - 4 * a * c;
		if (D > 0) {
			x1 = -(b + sqrt(D)) / 2 * a;
			x2 = -(b - sqrt(D)) / 2 * a;
			printf("%.4f %.4f", x1, x2);
			system("pause");
			return 0;
		}
		else if (D == 0) {
			x1 = -(b + sqrt(D)) / 2 * a;
			printf("%.4f", x1);
			system("pause");
			return 0;
		}
		else
		{
			printf("There is no real root!");
			system("pause");
			return -1;
		}
	}
}

