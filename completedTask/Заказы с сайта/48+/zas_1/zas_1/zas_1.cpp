#include "stdafx.h"

// ����� ������ � ������� ������
class Rus {
	public:
		Rus(const char* text) : data(new char [strlen(text)+1]) {
			CharToOemA( text, data);
		}

		~Rus() {
			delete [] data;
		};

		operator const char* () const {
			return data;
		}

	private:
		char* data;
};


// ������������� ��������
class Operation  {
	public:
		virtual ~Operation() {}
		virtual float Eval () const = 0;
};

// ������ �����
class Number : public Operation  {    
	public:
		Number (float f) : value (f) {}
		virtual float Eval () const { return value; }

	private:
		float value;
};

// ���� �������
class Unary : public Operation  {
	public:
		Unary (Operation *op) : one (op) {}
		~Unary() { delete one; }

	protected:
		const Operation *one;
};

// ��� ��������
class Binary : public Operation  {
	public:
		Binary (Operation *l, Operation *r) : left (l), right (r) {}
		~Binary() { delete left; delete right; }

	protected:
		const Operation *left, *right;
};

// �������� �����
class Negation : public Unary  {    
	public:
		Negation (Operation *n) : Unary (n) {}
		virtual float Eval() const { return - one->Eval(); }
};

// ��������
class Plus : public Binary  {
	public:
		Plus (Operation *l, Operation *r) : Binary (l, r) {}
		virtual float Eval() const { return left->Eval() + right->Eval(); }    
};

// ���������
class Minus : public Binary  {
	public:
		Minus (Operation *l, Operation *r) : Binary (l, r) {}
		virtual float Eval() const { return left->Eval() - right->Eval(); }
};

// ���������
class Multiply : public Binary  {
	public:
		Multiply (Operation *l, Operation *r) : Binary (l, r) {}
		virtual float Eval() const { return left->Eval() * right->Eval(); }        
};

// �������
class Divide : public Binary  {
	public:
		Divide (Operation *l, Operation *r) : Binary (l, r) {}
		virtual float Eval () const 
		{ 
			const float right_eval = right->Eval();
			return (right_eval != 0.0f) ? (left->Eval() / right_eval) : (printf ("Devide by zero\n"), FLT_MAX);
		}    
};


// ����� ���������� �� ����������
class Expression {
	public:
		Expression (const char * s) : source (s) {}

		float Calc () {
			pos = 0;
			const Operation * const root = Parse0 ();
			return (root) ? root->Eval () : 0.0f;
		}

	private:
		// ������ ���������: ��������, ���������
		Operation * Parse0 () {
			Operation * result = Parse1 ();
			for (;;) {
				if (Match ('+')) 
					result = new Plus(result, Parse1());
				else if (Match ('-'))
					result = new Minus (result, Parse1());
				else
					return result;
			}
		}

		// ������� ���������: ���������, �������
		Operation * Parse1() {
			Operation * result = Parse2();
			for (;;) {
				if (Match ('*')) 
					result = new Multiply(result, Parse2());
				else if (Match ('/'))
					result = new Divide(result, Parse2());
				else
					return result;
			}
		}

		// ������� ���������: �������� �����, ������, �����
		Operation * Parse2() {
			Operation * result = NULL;
			if (Match ('-')) {
				result = new Negation(Parse0());
			}
			else if(Match ('(')) {
				result = Parse0();
				if (!Match(')'))
					printf("Missing ')'\n");
			}
			else {
				// ��������� �����
				float val = 0.0f;
				if (sscanf_s(source + pos, "%f", & val) == 0)
					printf("Can't parse '%s'\n", source + pos); 
				result = new Number(val);
				while(isdigit(source[pos]) || source[pos] == '.' || source[pos] == 'e') ++pos;
			}
			return result;
		}

		// ������ ������ � ������
		bool Match (char ch)  {
			while (source [pos] == ' ') ++pos; // ��������� �������
			return (source [pos] == ch) ? (++pos, true) : false; // ����� ��� ������?
		}

	private:
		const char* source; // ������ � ����������
		int pos; // ������� ������� � ������
};

int main() 
{
	char buf[256];
	buf[0] = 0;
	printf(Rus("������� ���������"));
	gets_s(buf, 255);

	Expression expr(buf);
	printf(Rus("��������� = "));
	printf("%g\n", expr.Calc());
	system("pause");
	return 0;
}