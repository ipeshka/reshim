// xor.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

// ��������� �������
int GetNumber(int imin, int imax);
void PrintMenu();
void Coder(bool coder);


BOOL SetConsoleAttrib(HANDLE _ConsoleOut,WORD wAttrib)
{
	SetConsoleTextAttribute(_ConsoleOut, wAttrib);
	CONSOLE_SCREEN_BUFFER_INFO csbi = {0};
	GetConsoleScreenBufferInfo(_ConsoleOut, &csbi);
	DWORD dw(0);
	COORD cr = {0, 0};
	return FillConsoleOutputAttribute(_ConsoleOut, wAttrib, csbi.dwSize.X * csbi.dwSize.Y, cr, &dw);
}

int _tmain(int argc, _TCHAR* argv[])
{
	bool exit = false;
	while(true) {
		PrintMenu(); // ������ ����
		
		HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE); // ��������� ����������� ����
		SetConsoleAttrib(H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY); // ��������� ������ ����

		switch(GetNumber(1, 3)) { // ����� ������ ����
			case 1: Coder(true); break;
			case 2: Coder(false); break;
			case 3: exit = true; break;
		}
		if(exit) break;
	}
	return 0;
}

