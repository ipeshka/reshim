#include "stdafx.h"
using namespace std;

// ����� ������ � ������� ������
class Rus {
	public:
		Rus(const char* text) : data(new char [strlen(text)+1]) {
			CharToOemA( text, data);
		}

		~Rus() {
			delete [] data;
		};

		operator const char* () const {
			return data;
		}

	private:
		char* data;
};

string XOR(string value, string key)
{
    string retval(value); // �������������� ������
    short unsigned int klen = key.length();
    short unsigned int vlen = value.length();
    short unsigned int k = 0;
    short unsigned int v = 0;
     
    for(v; v < vlen; v++) {
        retval[v] = value[v] ^ key[k];
        k = (++k < klen ? k : 0);
    }
     
    return retval;
}


// ������� ������ ����
void PrintMenu() {
	cout<<endl;
	std::cout <<Rus("�������� ��������")<<std::endl;
	std::cout <<Rus("1 - ���������")<<std::endl;
	std::cout <<Rus("2 - �����������")<<std::endl;
	std::cout <<Rus("3 - �����")<<std::endl;
	std::cout <<"-------------------------------------------------" << std::endl;
}

// ���������, ����� �� ���������� ����� ����
int GetNumber(int imin, int imax) {
	int number= imin - 1;
	while(true) { // ���� �� �� ����� ������ �����
		cin>>number; // ������ ����� � �������
		if((number >= imin) && (number <= imax)) break; // ���� ����� � ����������, ����� ��� ����
		else { // ����� ������ ������ ��� ���
			cout<<Rus("��������� ����")<<endl;
			cin.clear();
			while(cin.get()!='\n') { }
		}
	}

	return number; // ���������� ������� �����
}


string GetKeyWord() {
	string key;
	cout<<Rus("������� ��������� �����:\n");
	do {
		cin>>key;
	} while (key.empty());
	return key; // ���������� ��������� �����
}

string GetFilename() {
	string filename;
	cout<<Rus("������� �������� �����:\n");
	do {
		cin>>filename;
	} while (filename.empty());
	return filename; // ���������� ��������� �����
}

// ������� �����������/�������������
void Coder(bool coder) {
	char message[1024];
	// ������ ����
	ifstream file(GetFilename());
	if(file.is_open()) {
		file.getline(message, 1024); // ��������� ����
	}
	file.close();

	string newStr = XOR(message, GetKeyWord());

	// ���� ������� ����, ���������� � ���� �����������, ����� � �������������
	ofstream fout(coder ? "CD.txt" : "DC.txt"); // ������ ������ ������ ofstream ��� ������ � ��������� ��� � ������ CD.txt
    fout << newStr; // ������ ������ � ����
    fout.close(); // ��������� ����
}