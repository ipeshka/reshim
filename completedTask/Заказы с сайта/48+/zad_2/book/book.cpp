#include <time.h>
#include <iostream>
#include <conio.h>
#include <string>
#include <stdio.h>
#include <windows.h>
#include <fstream>
#include <vector>
#include <algorithm>
#include <functional>

using namespace std;


// описание структуру записи записной книги
struct Persone
{
	int Number; // номер
	char FirstName[20]; // имя
	char LastName[20]; // фамилия
	char MiddleName[20]; // отчество
	char Phone[11]; // телефон
	char DateOfBirth[56]; // дата рождения
};


// класс записи в записной книге
class Person {
public:
	// конструктор и десктурктор
	Person() {}
	~Person() {}

	// методы для получения полей класса
	string GetFirstName() { return FirstName; }
	string GetLastName() { return LastName; }
	string GetMiddleName() { return MiddleName; }
	string GetPhone() { return Phone; }
	string GetDateOfBirth() { return DateOfBirth; }
	int GetNumber() { return Number; }

	// методы для устоновки полей класса
	void SetFirstName(string firstName) { FirstName = firstName; }
	void SetLastName(string lastName) { LastName = lastName; }
	void SetMiddleName(string middleName) { MiddleName = middleName; }
	void SetPhone(string phone) { Phone = phone; }
	void SetDateOfBirth(string dateOfBirth) { DateOfBirth = dateOfBirth; }
	void SetNumber(int number) { Number = number; }

	// прототип вывода записи н консоль
	void Print();

	// прототип записи в файл
	void toFile(Persone *sf);
	// чтения записи из файла
	void fromFile(Persone *sf);

	// переопределние сходсва записей
	bool operator==(Person a)
	{
		return (FirstName == a.FirstName) &&
			(LastName == a.LastName) &&
			(MiddleName == a.MiddleName) &&
			(Phone == a.Phone);
	}


// поля записи
private:
	int Number;
	string FirstName;
	string LastName;
	string MiddleName;
	string Phone;
	string DateOfBirth;
};

vector<Person> pers; // список записей

// описание функции класса для конвертирование из класса в структуру
// структуру мы потом сохнаняем к файл
void Person::toFile(Persone *sf)
{	
	size_t size = FirstName.size(); // оппределяем размер занимаемыей полем
	if (size > 20) size = 20; // если превышает, обрезаем
	memcpy((void*)sf->FirstName, (void*)FirstName.c_str(), size + 1); // копируем значение в структуру

	size = LastName.size();
	if (size > 20) size = 20;
	memcpy((void *)sf->LastName, (void*)LastName.c_str(), size + 1);

	size = MiddleName.size();
	if (size > 20) size = 20;
	memcpy((void*)sf->MiddleName, (void*)MiddleName.c_str(), size + 1);

	size = Phone.size();
	if (size > 11) size = 11;
	memcpy((void*)sf->Phone, (void*)Phone.c_str(), size + 1);

	size = DateOfBirth.size();
	if (size > 56) size = 56;
	memcpy((void*)sf->DateOfBirth, (void*)DateOfBirth.c_str(), size + 1);

	sf->Number = Number;
}

// чтение структуру и конвертирование к поля класса
void Person::fromFile(Persone *sf)
{
	FirstName = sf->FirstName;
	LastName = sf->LastName;
	MiddleName = sf->MiddleName;
	Phone = sf->Phone;
	DateOfBirth = sf->DateOfBirth;
	Number = sf->Number;
}


// класс для вывода русских символов в консоли
class Rus {
public:
	Rus(const char* text) : data(new char [strlen(text)+1]) {
		CharToOemA( text, data);
	}

	~Rus() {
		delete [] data;
	};

	operator const char* () const {
		return data;
	}

private:
	char* data;
};

// вывод полей класса на консоль
void Person::Print() {	

	// получаем дескриптор консоли (окна)
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	// устанавливаем атрибуты для отображниея
	SetConsoleTextAttribute (H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY );
	// выводим с форматированим полей
	printf("| %-9d | %-20.20s | %-12.12s | %-11.11s | %-11.11s | %-11.11s\n", Number, FirstName.c_str(), LastName.c_str(), MiddleName.c_str(), Phone.c_str(), DateOfBirth.c_str());
}

// выводим пункты меню
void PrintMenu() {
	cout<<endl;
	std::cout <<Rus("Выбирите действие")<<std::endl;
	std::cout <<Rus("1 - Добавить")<<std::endl;
	std::cout <<Rus("2 - Удалить")<<std::endl;
	std::cout <<Rus("3 - Поиск")<<std::endl;
	std::cout <<Rus("4 - Показать всех")<<std::endl;
	std::cout <<Rus("5 - Выход")<<std::endl;
	std::cout <<"-------------------------------------------------" << std::endl;
}

// печатаме все записи
void PrintAll() {
	system("cls"); // очиска консоли
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute ( H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY );

	cout<<Rus("***---------  Записная книжка  --------------***\n");	
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<Rus("|  Номер    |    Фамилия           | Имя         |Дата рождения |  Tелефон    | ")<<std::endl;
	cout<<"----------------------------------------------------------------"<<endl;
	cout<<"|           |                      |             |              |             | "<<endl;

	for(unsigned int n = 0; n < pers.size(); n++) {  // циклом пробегаемся по всему вектору
		pers[n].Print(); // каждому экземрляру класса, вызваем метод вывода на экран
	}

	cout<<"-----------------------------------------------------------------\n";
}

// проверяет, ввели ли правильный пункт меню
int GetNumber(int imin, int imax) {
	int number= imin - 1;
	while(true) { // пока мы не ввели верной число
		cin>>number; // читаем число с консоли
		if((number >= imin) && (number <= imax)) break; // если входи в промежуток, тогда все окей
		else { // иначе просим ввести ещё раз
			cout<<Rus("Повторите ввод")<<endl;
			cin.clear();
			while(cin.get()!='\n') { }
		}
	}

	return number; // возвращаем введёное число
}

// функция посика записи
void FindPerson() {
	system("cls");
	HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute (H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY);

	// выводим менб, по какому полю будем искать
	cout<<Rus("--------------------- поиск записи -------------------------\n");
	cout<<Rus("Выбирите критерий поиска\n");
	cout<<Rus(" 1 - Номер\n");
	cout<<Rus(" 2 - Фамилия\n");
	cout<<Rus(" 3 - Имя\n");
	cout<<Rus(" 4 - Дата рождения\n");
	cout<<Rus(" 5 - Телефон\n");
	cout<<Rus(" 6 - Выход\n");
	int vybor = GetNumber(1, 6);

	if (vybor < 6)
	{
		string key;
		string vkey;
		bool found = false;
		switch(vybor)
		{
			case 1: cout<<Rus("Введите номер: "); break;
			case 2: cout<<Rus("Введите фамилию: "); break;
			case 3: cout<<Rus("Введите имя: "); break;
			case 4: cout<<Rus("Введите дату рождения: "); break;
			case 5: cout<<Rus("Введите телефон: "); break;
			case 6: system("cls"); break;
		}
		cin>>key;

		cout<<"----------------------------------------------------------------"<<endl;
		cout<<Rus("|  Номер    |    Фамилия          | Имя         |Дата рождения|  Tелефон    | ")<<std::endl;
		cout<<"----------------------------------------------------------------"<<endl;
		cout<<"|             |                     |             |             |             | "<<endl;

		unsigned int n=0;
		// пробегаемся по всем записям
		while (n < pers.size()) {	
			switch(vybor) { // смотрим какое поле выбрали и извлекаем то значение
				case 1: vkey=pers[n].GetNumber(); break;
				case 2: vkey=pers[n].GetFirstName(); break;
				case 3: vkey=pers[n].GetLastName(); break;
				case 4: vkey=pers[n].GetMiddleName(); break;
				case 5: vkey=pers[n].GetPhone(); break;
			}

			if (key == vkey) { // если значения совпадают, тогда выводим на экран
				found = true;
				pers[n].Print();		           
			}
			n++;
		}

		cout<<"----------------------------------------------------------------"<<endl;

		if (!found)
			cout<<Rus("Нет такой записи\n");

	}
}


// функция добавления персоны
void AddPerson(){
	system("cls");
	HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute ( H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY );

	string sbuf;
	Person bufPers;

	// чтение из консоли всех данных требуемых для заполнения экземпляра класса записи
	cout<<endl;
	cout<<Rus("----------------  Добавление записи ----------------\n");
	cout<<Rus("Фамилия: ");
	cin>>sbuf;
	bufPers.SetLastName(sbuf);

	cout<<endl;
	cout<<Rus("Имя: ");
	cin>>sbuf;
	bufPers.SetFirstName(sbuf);

	cout<<endl;
	cout<<Rus("Отчество: ");
	cin>>sbuf;
	bufPers.SetMiddleName(sbuf);

	cout<<endl;
	cout<<Rus("Телефон:");
	cin>>sbuf;
	bufPers.SetPhone(sbuf);

	bufPers.SetNumber(pers.size() + 1);

	pers.push_back(bufPers); // добавляем запись в наш вектор
	cout<<"----------------------------------------------------------------"<<endl;
}

// функция удаления записи
void DeletePerson() {
	string sbuf;
	unsigned int n = 0;
	bool find = false;

	system("cls");

	cout<<Rus("----------------  Удаление записи ---------------\n");
	cout<<Rus("Выбирите критерий выбора записей на удаление\n");
	cout<<Rus(" 1 - Номер\n");
	cout<<Rus(" 2 - Фамилия\n");
	cout<<Rus(" 3 - Имя\n");
	cout<<Rus(" 4 - Выход\n");
	int vybor = GetNumber(1, 4);
	// логика обьработки аналогична поиску
	if (vybor < 4)
	{
		string key;
		string vkey;
		bool found = false;
		switch(vybor)
		{
			case 1: cout<<Rus("Введите номер: ");break;
			case 2: cout<<Rus("Введите фамилию: "); break;
			case 3: cout<<Rus("Введите имя: ");break;
			case 4: system("cls"); break;
		}
		cin>>key;
		cout<<Rus("Удалены следующие записи:")<<endl;
		cout<<"----------------------------------------------------------------"<<endl;
		cout<<Rus("|  Номер    |    Фамилия          | Имя         |Дата рождения|  Tелефон    | ")<<std::endl;
		cout<<"----------------------------------------------------------------"<<endl;
		cout<<"|             |                     |             |             |             | "<<endl;
		unsigned int n = 0;
		while (n < pers.size()) {	
			switch(vybor) {
				case 1: vkey=pers[n].GetFirstName(); break;
				case 2: vkey=pers[n].GetLastName(); break;
				case 3: vkey=pers[n].GetMiddleName(); break;
				case 4: vkey=pers[n].GetPhone(); break;
			}

			if (key == vkey) {
				found=true;
				pers[n].Print();
				// удаляем из вектора
				pers.erase(pers.begin() + n);		  		 
			}

			else
				n++;
		}
		cout<<"----------------------------------------------------------------"<<endl;
		if (!found)	 	  
			cout<<Rus("Не найдено ни одной записи\n");

	}
}

// сохранения ветора в файл
bool DataToFile(string filename) {
	Persone sf;
	ofstream fout(filename.c_str(),ios_base::out | ios_base::binary);// открываем файл на запись
	if(fout.is_open()) {
		size_t size = pers.size();  // смотрим количество пямитя требуемой для записи структуры персона
		fout.write((char *)&size, sizeof(int));
		for(size_t i=0; i < size; i++) { //
			memset(&sf, 0, sizeof(Persone));
			pers[i].toFile(&sf);
			fout.write((char *)&sf, sizeof(Persone));
		}

		fout.close();
		return true;
	}
	return false;
}

bool DataFromFile(string filename)
{
	int size;
	Persone sf;

	ifstream fin(filename.c_str());
	if( fin.is_open()) {
		pers.clear();
		fin.read((char*)&size, sizeof(int));

		for(int i=0; i<size; i++) {
			Person tmp;
			fin.read((char*)&sf, sizeof(Persone));
			tmp.fromFile(&sf);
			pers.push_back(tmp); // добавляем запись в вектор
		}

		fin.close();

		return true;
	}
	return false;
}



