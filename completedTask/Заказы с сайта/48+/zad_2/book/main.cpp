#include <iostream>
#include <string>
#include <vector>
#include <fstream> 
#include <windows.h> 
#include <algorithm>
#include <functional>

using namespace std;

// �������� ����������
void PrintMenu();
void PrintAll(); 
void FindPerson(); 
void AddPerson(); 
void DeletePerson(); 
int GetNumber(int imin, int imax);
bool DataToFile(string filename);
bool DataFromFile(string filename);


BOOL SetConsoleAttrib(HANDLE _ConsoleOut,WORD wAttrib)
{
	SetConsoleTextAttribute(_ConsoleOut, wAttrib);
	CONSOLE_SCREEN_BUFFER_INFO csbi = {0};
	GetConsoleScreenBufferInfo(_ConsoleOut, &csbi);
	DWORD dw(0);
	COORD cr = {0, 0};
	return FillConsoleOutputAttribute(_ConsoleOut, wAttrib, csbi.dwSize.X * csbi.dwSize.Y, cr, &dw);
}

int main() {
	bool exit = false;
	DataFromFile("phone.txt"); // ������ ������ �� ������
	while(true) {
		PrintMenu(); // ������ ����
		
		HANDLE H = GetStdHandle(STD_OUTPUT_HANDLE); // ��������� ����������� ����
		SetConsoleAttrib(H, BACKGROUND_RED | BACKGROUND_GREEN | BACKGROUND_BLUE| BACKGROUND_INTENSITY); // ��������� ������ ����

		switch(GetNumber(1, 5)) { // ����� ������ ����
			case 1: AddPerson(); break;
			case 2: DeletePerson(); break;
			case 3: FindPerson(); break;
			case 4: PrintAll(); break;
			case 5: exit = true; break;
		}
		if(exit) break;
	}
	DataToFile("phone.txt"); // ���������� ������� � ����
	return 0;
}