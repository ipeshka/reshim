﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fromFile_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rnd = new Random();
            int itemsCount;
            string str;
            int[] Mas;

            int firstId = 0, lastId = 0;
            bool foundFirstelement = false;

            int Sum = 0;

            //проверяем правильность ввода
            do
            {
                Console.Clear();
                Console.Write("Введите размерность массива: ");
                str = Console.ReadLine();
            } while (!int.TryParse(str, out itemsCount));

            //создаем массив
            Mas = new int[itemsCount];

            //забиваем массив элементами и выводим их на экран
            for (int i = 0; i < itemsCount; i++) {
                Mas[i] = rnd.Next(-10, 10);
                Console.Write(Mas[i].ToString() + "  ");
            }

            // ищем максимальный по модулю элемент
            int max = Mas[0];
            for (int i = 0; i < itemsCount; i++) {
                if (Math.Abs(max) < Math.Abs(Mas[i]))
                    max = Mas[i];
            }
            Console.WriteLine("Максимальный по модулю элемент: {0}", max);



            //ищем позиции первого и второго положительных элементов
            for (int i = 0; i < itemsCount; i++) {
                if (Mas[i] > 0) {
                    if (!foundFirstelement) {
                        firstId = lastId = i;
                        foundFirstelement = true;
                    }
                    else {
                        lastId = i;
                    }
                }
            }

            //находим сумму от первого до последнего ВКЛЮЧИТЕЛЬНО
            for (int i = firstId; i < lastId + 1; i++) {
                Sum += Mas[i];
            }
            //выводим результат рассчета
            Console.WriteLine("Сумма = " + Sum.ToString());


            // преобразовываем массив
            for (int i = 0, j = itemsCount; i < itemsCount; i++) {
                if (Mas[i] == 0) {
                    Mas[i] = Mas[j - 1];
                    Mas[j - 1] = 0;
                    j--;
                }
                
            }
            // вывод преобразованный массив
            for (int i = 0; i < itemsCount; i++)
            {
                Console.Write("{0} ", Mas[i]);
            }


            Console.ReadKey();
        }
    }
}
