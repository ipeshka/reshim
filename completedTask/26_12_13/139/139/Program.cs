﻿using System;

namespace _139
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            double[] b = new double[n];

            for (int i = 1; i <= n; i++) {
                b[i - 1] = Math.Pow(2, i) + Math.Pow(3, i + 1);
            }

            for (int i = 1; i <= n; i++)
            {
                Console.WriteLine("{0}", b[i - 1]);
            }
        }
    }
}
