﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fromFile_1
{
    class Program
    {
        static void Main(string[] args)
        {
            const int Iter = 1000; //Ограничитель кол-ва операций
            double ch, yn, y, x1 = 0, x2 = 1, dx = 0.1, eps = 1e-5;
            Console.WriteLine("|      X\t|      Y\t|      Кол-во\t|");
            Console.WriteLine("+---------------+---------------+---------------+---------------+");
            int b;
            for (double x = x1; x <= x2; x += dx)
            {
                b = 0;
                yn = ch = x;
                y = x;
                for (int n = 1; n <= Iter; n++)
                {
                    ch = Math.Pow(-1, n) * Math.Pow(x, 2 * n + 1) / (2 * n + 1);
                    yn = ch + y;
                    if (Math.Abs(ch) < eps)
                        break;
                    y = yn;
                    b++;
                }
                Console.WriteLine("| {0,7}\t| {1,7}\t| {2,7}\t|", Math.Round(x, 3), Math.Round(yn, 7), b);
            }
            Console.ReadLine();
        }

    }
}
