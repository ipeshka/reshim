﻿using System;

namespace _57
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            int a = Int32.Parse(Console.ReadLine());

            double result = 0;
            if (a <= 2) {
                result = Math.Pow(a, 2) + 4*a + 5;
            } else {
                result = Math.Pow(Math.Pow(a, 2) + 4 * a + 5, -1);
            }

            Console.WriteLine("Result = {0}", result);
        }
    }
}
