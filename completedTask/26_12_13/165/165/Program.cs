﻿using System;


namespace _165
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            int[] a = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = rnd.Next(99) - 50;
            }

            int sum = 0;
            for (int i = 0; i < n; i++)
            {
                sum += a[i];
            }

            Console.WriteLine("{0}", sum);
        }
    }
}
