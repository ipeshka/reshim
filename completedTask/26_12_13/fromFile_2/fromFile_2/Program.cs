﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fromFile_2
{
    class TimeExcepts : Exception
    {
        public override string Message
        {
            get
            {
                return "Ошибка изменения значений времени.";
            }
        }
    }
    class HoursExcept : TimeExcepts
    {
        public override string Message
        {
            get
            {
                return base.Message + " Вы ввели недопустимое значение для переменной Часы.";
            }
        }
    }
    class MinutesExcept : TimeExcepts
    {
        public override string Message
        {
            get
            {
                return base.Message + " Вы ввели недопустимое значение для переменной Минуты.";
            }
        }
    }
    class SecondsExcept : TimeExcepts
    {
        public override string Message
        {
            get
            {
                return base.Message + " Вы ввели недопустимое значение для переменной Секунды.";
            }
        }
    }
    class Time
    {
        private int hours = 0;
        private int minuts = 0;
        private int seconds = 0;
        public Time(int hours, int minuts, int seconds)
        {
            SetTime(hours, minuts, seconds);
        }
        public Time() { }
        public int Hours
        {
            get { return hours; }
            set
            {
                if (value > 59 || value < 0)
                    throw new HoursExcept();
                else hours = value;
            }
        }
        public int Minutes
        {
            get { return minuts; }
            set
            {
                if (value > 59 || value < 0)
                    throw new MinutesExcept();
                else minuts = value;
            }
        }
        public int Seconds
        {
            get { return seconds; }
            set
            {
                if (value > 59 || value < 0)
                    throw new SecondsExcept();
                else seconds = value;
            }
        }
        public void SetTime(int hours, int minuts, int seconds)
        {
            Hours = hours;
            Minutes = minuts;
            Seconds = seconds;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Time t = new Time(24, 56, 34);
            try
            {
                t.SetTime(61, 10, 5);
            }
            catch (TimeExcepts e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
