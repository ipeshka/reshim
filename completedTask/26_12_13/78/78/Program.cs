﻿using System;


namespace _78
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            int a = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            int result = 0;
            for (int i = 1; i <= n; i++) {
                result += a*(a + n - 1);
            }

            Console.WriteLine("Result {0}:", result);
        }
    }
}
