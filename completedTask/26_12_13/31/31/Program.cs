﻿using System;

namespace _31
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите a:");
            int a = Int32.Parse(Console.ReadLine());

            int b = a * a;
            int c = b * b;
            int d = c * c;

            Console.WriteLine("Result: {0}", d);
        }
    }
}
