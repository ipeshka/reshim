﻿using System;


namespace _119
{
    class Program
    {
        static void Main(string[] args)
        {
            double eps = 0.0001;
            double sum = 0;
            double next = 0;
            int i = 1;
            do
            {
                sum += (double)1 / (i * (i + 1)); // сумма
                i++; // счётчик
                next = (double)1 / (i * (i + 1)); // след элемент
            } while (Math.Abs(next - sum) < eps); // проверка

            Console.WriteLine("{0}", sum);
        }
    }
}
