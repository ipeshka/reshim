﻿using System;

namespace _116
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n: ");
            int n = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите x: ");
            int x = Int32.Parse(Console.ReadLine());

            double result = 0;
            for (int i = 1; i <= n; i++) {
                result += (x + Math.Cos(i*x))/(Math.Pow(2, i));
            }

            Console.WriteLine("{0}", result);
        }
    }
}
