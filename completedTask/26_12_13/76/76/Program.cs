﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _76
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите k:");
            int k = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите l:");
            int l = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите m:");
            int m = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            if (m == k || // если в одном строке
                n == l || // или если в одном столбце
                m - n == k - l || // или если по диагонали с левого верхнего угла до правого нижнего
                m + n == k + l) //или если по диагонали с правого верхнего угла до левого нижнего
            {

                Console.WriteLine("угрожает!");
            }
            else
            {
                Console.WriteLine("не угрожает!");
            }
        }

    }
}
