﻿using System;
using System.Collections.Generic;

namespace _203
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            KeyValuePair<int, int>[] a = new KeyValuePair<int, int>[n];
            KeyValuePair<int, int>[] c = new KeyValuePair<int, int>[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = new KeyValuePair<int, int>(i, rnd.Next(10));
                Console.WriteLine("{0}", a[i]);
            }

            // определяем минимального покупателя
            KeyValuePair<int, int> min = a[0];
            int summPrev = 0;
            for (int i = 0; i < n; i++) {
                if (a[i].Value < min.Value) {
                    min = a[i];
                }

                summPrev += a[i].Value;
                c[i] = new KeyValuePair<int, int>(i, summPrev);
            }

            Console.WriteLine("Min - {0}", min.Key);

            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("{0}", c[i]);
            }
        }
    }
}
