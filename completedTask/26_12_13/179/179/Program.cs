﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _179
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            double[] a = new double[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] =  rnd.Next(99) - 50;
            }

            for (int i = 0; i < n; i++) {
                double x = Math.Pow(5 - 3*a[i], 1/2);

                for (int j = 0; j < n; j++) {
                    if (Math.Abs(x) == Math.Abs(a[j])) {
                        Console.WriteLine("{0}", a[j]);
                    }
                }
            }
        }
    }
}
