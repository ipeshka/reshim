﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _136
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] mass = { 1, 2, 5, 2, 7, 8, 1, 9, 4, 7 };
            double sum = 0;
            for (int i = 0; i < mass.Length; i++)
            {
                sum += mass[i] / fact(0);
            }

            Console.WriteLine("{0}", sum);
        }
        static int fact(int num)
        {
            return (num == 0) ? 1 : num * fact(num - 1);
        }
    }
}
