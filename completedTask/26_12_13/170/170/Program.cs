﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _170
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            KeyValuePair<int, double>[] a = new KeyValuePair<int, double>[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = new KeyValuePair<int, double>(i, (double)rnd.Next(99) / 100);
                Console.WriteLine("{0}", a[i]);
            }
            // сортируем

            for (int i = 0; i < n; i++)
                for (int j = i; j < n; j++) {
                    if (a[i].Value > a[j].Value) {
                        KeyValuePair<int, double> tmp = a[i];
                        a[i] = a[j];
                        a[j] = tmp;
                    }
                }

            for (int i = 0; i < 4; i++)
            {
                Console.WriteLine("{0}", a[i].Key);
            }
        }
    }
}
