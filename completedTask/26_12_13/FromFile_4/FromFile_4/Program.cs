﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FromFile_4
{
    class Program
    {
        static void Main(string[] args)
        {
            const int N = 10;
            const int M = 10;
            Random rand = new Random();
            double[,] A = new double[N, M];

            // заполнение массива случайными числами в пределах 10-100
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < M; j++)
                {
                    A[i, j] = rand.Next(10, 100);
                    Console.Write(" {0}", A[i, j]);
                }
                Console.WriteLine();
            }

            double[,] B = new double[A.GetLength(0), A.GetLength(1)];
            double sum = 0;
            Console.WriteLine("--------------------------------");

            //  создание нового массива "newmas", с исходного массива 
            //  "mas" путем "сглаживаниия"     
            for (int i = 0; i < 10; i++)
            {
                for (int j = 0; j < 10; j++)
                {
                    if (i == 0 && j == 0) // вверх левый угол
                    {
                        B[i, j] = (A[i, j + 1] + A[i + 1, j] + A[i + 1, j + 1]) / 3;
                    }
                    if (i == 0 && j == M - 1) // вверх правый угол
                    {
                        B[i, j] = (A[i, M - 2] + A[i + 1, M - 1] + A[i + 1, M - 2]) / 3;
                    }
                    if (i == M - 1 && j == 0) // низ левый угол
                    {
                        B[i, j] = (A[M - 1, j + 1] + A[M - 2, j] + A[M - 2, j + 1]) / 3;
                    }
                    if (i == M - 1 && j == M - 1) // низ правый левый угол
                    {
                        B[i, j] = (A[M - 1, M - 2] + A[M - 2, M - 1] + A[M - 2, M - 2]) / 3;
                    }
                    if (i == 0 && j != 0 && j != M - 1) // вверх
                    {
                        B[i, j] = (A[i, j - 1] + A[i, j + 1] + A[i + 1, j - 1] + A[i + 1, j] + A[i + 1, j + 1]) / 5;
                    }
                    if (i == M - 1 && j != 0 && j != M - 1) // низ
                    {
                        B[i, j] = (A[i, j - 1] + A[i, j + 1] + A[i - 1, j - 1] + A[i - 1, j] + A[i - 1, j + 1]) / 5;
                    }
                    if (i != 0 && i != M - 1 && j == 0) // левый край
                    {
                        B[i, j] = (A[i - 1, j] + A[i - 1, j + 1] + A[i, j + 1] + A[i + 1, j] + A[i + 1, j + 1]) / 5;
                    }
                    if (i != 0 && i != M - 1 && j == M - 1) // правый край
                    {
                        B[i, j] = (A[i - 1, j] + A[i - 1, j - 1] + A[i, j - 1] + A[i + 1, j] + A[i + 1, j - 1]) / 5;
                    }
                    if (i != 0 && i != M - 1 && j != 0 && j != M - 1) // общий случай
                    {
                        B[i, j] = (A[i - 1, j - 1] + A[i - 1, j] + A[i - 1, j + 1] + A[i, j - 1] + A[i, j] + A[i, j + 1] + A[i + 1, j - 1] + A[i + 1, j] + A[i + 1, j + 1]) / 9;
                    }

                    Console.Write("{0,6:F2}  ", B[i, j]);
                }
                Console.WriteLine();
            }

            // поиск элементов под главной диагональю матрицы и подсчета их суммы
            for (int i = 1; i < 10; i++) {
                for (int j = 1; j < 10; j++) {
                    if (i == j)
                        sum += Math.Abs(B[i, j - 1]);
                }
            }

            Console.WriteLine("Сумма = {0}", sum);
            Console.ReadLine();
        }
    }
}
