﻿using System;

namespace _149
{
    class Program
    {
        static void Main(string[] args) {
            for (double x = -3; x < 1; x += 0.1){
                Console.WriteLine("x = {0}, y = {1}", x, 4 * Math.Pow(x, 3) - 2 * Math.Pow(x, 2) + 5);
            }
        }
    }
}
