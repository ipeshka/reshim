﻿using System;

namespace _194
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            int[] a = new int[n];
            Random rnd = new Random();
            for (int i = 0; i < n; i++)
            {
                a[i] = Int32.Parse(Console.ReadLine());
            }
            int sum = -10;
            for (int i = 1; i < n; i++)
            {
                if (a[0] == a[i]) {
                    for (; i < n; i++) {
                        sum += a[i];
                    }
                }
            }
            Console.Write("{0} ", sum);
        }
    }
}
