﻿using System;

namespace _9
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите R1:");
            int r1 = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите R2:");
            int r2 = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите R3:");
            int r3 = Int32.Parse(Console.ReadLine());

            double r = Math.Pow((double)1 / r1 + (double)1 / r2 + (double)1 / r3, -1);

            Console.WriteLine("R: {0}", r);
        }
    }
}
