﻿using System;

namespace _99
{
    class Program
    {
        // не решено!
        static void Main(string[] args)
        {
            Console.WriteLine("Введите u:");
            int u = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите v:");
            int v = Int32.Parse(Console.ReadLine());

            Console.WriteLine("Введите n:");
            int n = Int32.Parse(Console.ReadLine());

            double[] a = new double[n];
            double[] b = new double[n];

            a[0] = u;
            b[0] = v;
            double sum = 0;
            for (int k = 1; k < n; k++) {
                a[k] = 2*b[k - 1] + a[k - 1];
                b[k] = 2*Math.Pow(a[k - 1], 2) + b[k - 1];
                sum += a[k]*b[k]/fact(k + 2);

            }

            Console.WriteLine("{0}", sum);
        }

        static int fact(int num)
        {
            return (num == 0) ? 1 : num * fact(num - 1);
        }
    }
}
