﻿using System;

namespace _114
{
    class Program
    {
        static void Main(string[] args) {
            double result = 0;
            for (int i = 1; i <= 100; i++) {
                result += (double)1/(i*i);
            }

            Console.WriteLine("{0}", result);
        }
    }
}
