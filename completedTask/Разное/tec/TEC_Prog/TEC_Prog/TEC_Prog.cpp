#include "stdafx.h"
#include "iostream"
#include <complex>
#include<ccomplex>
#include <conio.h>
#include <math.h>

using namespace std;
using std::endl;
//���������
const int N = 12, W = 6, p1=1000;
float rad=3.1415926/180;
float omega=2*50*3.1415926;
//������������� ������
int Sopr(float a[][7],complex<float>*Z98){
	float XL,XC;
	for(int i=0;i<N;i++){
		XL=omega*a[i][2]/p1;
		if(a[i][3]>0)
			XC=(p1*p1)/(omega*a[i][3]);
		else{
			XC=0;
		}
		complex<float>Z(a[i][1],XL-XC);
		Z98[i]=Z;
	}
	return 0;
}
//���
int Eds(float a[][7],complex<float>*E){
	for(int i=0;i<N;i++){
		int a1 = (int(a[i][0]) / 10) - 1;
		int a2 = (int(a[i][0]) % 10) - 1;
		float E1=a[i][5]*cos(a[i][6]*rad);
		float E2=a[i][5]*sin(a[i][6]*rad);
		complex<float>E3(E1,E2);
		E[i]=E3;
	}
	return 0;
}
//������� ����������� ���������
int fun(float a[][7], complex<float>pr[][7],complex<float>*Z,complex<float>*E){
	for(int i=0;i<W;i++){
		for(int j=0;j<W+1;j++){
			pr[i][j]=0;
		}
	}
	for(int i=0;i<N;i++){
		complex<float>Y=complex<float>(1,0)/Z[i];
		int a1 = (int(a[i][0]) / 10)-1;
		int a2 = (int(a[i][0]) % 10)-1;
		if(a2!=W){
			pr[a1][a2] = -Y;
			pr[a2][a1] = -Y;
		}
	}
	for(int i=0;i<W;i++){
		for(int j=0;j<W+1;j++){
			if(i!=j)
				pr[i][i]-=pr[i][j];
		}
	}
	//��� � ����������� �����
	for (int i = 0; i < N; i++){
		if (a[i][5] > 0){
			int a1 = (int(a[i][0]) / 10) - 1;
			int a2 = (int(a[i][0]) % 10) - 1;
			if(a2!=W){
				complex<float>s=-E[i]*pr[a1][a2];
				if (a[i][0] == a[i][4]){
					pr[a1][W]-= s;
					pr[a2][W]+= s;
				}
				else{
					pr[a1][W]+= s;
					pr[a2][W]-= s;
				}
			}
		}
	}
	return 0;
}
//����� ������
int gaus(complex<float>pr[][7],complex<float>xx[]){
	for (int i = 0; i<W; i++){
		complex<float> tmp = pr[i][i];
		for (int j = W; j >= i; j--)
			pr[i][j] /= tmp;
		pr[i][i]=complex<float>(1,0);
		for (int j = i + 1; j<W; j++){
			tmp = pr[j][i];
			for (int k = W; k >= i; k--){
				pr[j][k] -= tmp*pr[i][k];
			}
		}
	}
	xx[W - 1] = pr[W - 1][W];
	for (int i = W - 2; i >= 0; i--){
		xx[i] = pr[i][W];
		for (int j = i + 1; j < W; j++)
			xx[i] -= pr[i][j] * xx[j];
	}
	return 0;
}
//���� � ������
int toki(float a[][7],complex<float>*xx,complex<float>*Iv,complex<float>*Z,complex<float>*E){
	for (int i = 0; i < N; i++){
		Iv[i] = 0;
	}
	for(int i=0;i<N;i++){
		int a1 = (int(a[i][0]) / 10) - 1;
		int a2 = (int(a[i][0]) % 10) - 1;
		Iv[i]=(xx[W-a1]-xx[W-a2]+E[i])/Z[i];
	}
	for (int i = 0; i < N; i++){
		cout<<Iv[i]<<"\n";
	}
	return 0;
}
//��������
int Prover(complex<float>*E,complex<float>*Iv,complex<float>*Z){
	complex<float>f1(0,0);
	complex<float>f2(0,0);
	for(int i=0;i<N;i++){
		f1+=E[i]*Iv[i];
		f2+=Iv[i]*Iv[i]*Z[i];
	}
	cout<<"\n"<<f1<<"="<<f2;
	return 0;
}
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	static float a[N][W+1] = {
		12, 24, 18, 0, 0, 0, 0,
		13, 30, 0, 91, 0, 0, 0,
		14, 20, 0, 0, 41, 36, 350,
		23, 18, 22, 91, 0, 0, 0,
		25, 33, 10, 68, 0, 0, 0,
		27, 22, 0, 75, 0, 0, 0,
		34, 51, 0, 0, 34, 80, 15,
		46, 24, 12, 68, 0, 0, 0,
		47, 18, 15, 82, 0, 0, 0,
		56, 27, 0, 0, 56, 80, 40,
		57, 10, 0, 0, 75, 60, 335,
		67, 12, 15, 91, 0, 0, 0
	};
	complex<float>E[N];
	complex<float>Z[N];
	complex<float>pr[W][W+1];
	complex<float>xx[W+1];
	complex<float>Iv[N];
	Sopr(a,Z);
	Eds(a,E);
	fun(a,pr,Z,E);
	gaus(pr,xx);
	toki(a,xx,Iv,Z,E);
	Prover(E,Iv,Z);
	_getch();
	return 0;
}