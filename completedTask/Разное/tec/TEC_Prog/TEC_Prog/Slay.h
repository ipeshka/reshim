#pragma once
#include "Matrix.h"
#include <iostream>
using namespace std;

template <class T> class Slau
{
protected:
	int m;     // ���������� ���������
	int n;     // ���������� ����������
	Matrix<T> a;  // ������� ������������� 
	// ����������� ����
	Matrix<T> b;  // ������ ��������� ������ �����
	// ����������� ����
	Matrix<T> x;  // ������� ������� �������� ���������
	bool isSolved;// ����������, �������� �� 
	// ������� ���������� 
	int* reoder; // ������ ������������ ����������
	int rang;    // ���� �������public:
	// ����������� ���� � ����������� � 
	// ���������� ��������� � ����������
	Slau(int, int);
	// ������� ������ ������ ������� ����
	void Solve();
	// �������, ����������� ����� �������
	void Kramer();
	// �������, ����������� ������� ���� 
	// � ������� �������� �������
	void InverseMatrix();
	// ������� ��������� ������ ������� ���� 
	// ������� �������-������
	void JordanGauss();
	// ������������� ������� �������, 
	// ���������������� �������� �����/������ ����
	template <class T> friend ostream& operator <<(ostream&, Slau<T>&);
	template <class T> friend istream& operator >>(istream&, Slau<T>&);
	// ����� ������ ���������� ��� ������� ����
	void PrintSolution(ostream&);
};

