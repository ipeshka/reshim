#pragma once

class Complex {
protected:
	double re, im;
public:
	Complex(void);
	Complex (double r);
	Complex (double r, double i);
	Complex (const Complex &c);
	~Complex(void);

	double Abs();
	double Argument();
	Complex& operator = (Complex &c);
	Complex& operator += (Complex &c);
	Complex operator + (const Complex &c);
	Complex operator - (const Complex &c);
	Complex operator * (const Complex &c);
	Complex operator / (const Complex &c);
};

