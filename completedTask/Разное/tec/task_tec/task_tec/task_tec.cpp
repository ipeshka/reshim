#include "stdafx.h"
#include "iostream"
#include <complex>
#include<ccomplex>
#include <conio.h>
#include <math.h>
#include <iomanip>

using namespace std;
using std::endl;
//���������
const int p1=1000; // ��� ������������ ���������
const int N = 12; // ���������� ������
const int W = 6;
float rad=3.1415926 / 180; // �������
float f = 32; // �������
float omega=2*f*3.1415926;

//������� ������������� ������
int Sopr(float a[][7],complex<float>Z98[N]){
	float XL, XC; // ���������� ��� ������������� � �������
	for(int i=0;i<N;i++){
		XL=omega*a[i][2]/p1; // ������������ �������������
		if(a[i][3]>0) // ���� ���� �������
			XC=(p1*p1)/(omega*a[i][3]); // ������������ �������
		else
			XC=0;

		complex<float>Z(a[i][1],XL-XC); // ������ ������������� �����
		Z98[i]=Z; // ���������� � ������
	}
	return 0;
}
// ������� ��� � �����
int Eds(float a[][7],complex<float>*E){
	for(int i=0;i<N;i++){
		// ������������� � �������������� �����
		float E1=a[i][5]*cos(a[i][6]*rad);
		float E2=a[i][5]*sin(a[i][6]*rad);
		complex<float>E3(E1,E2);
		E[i]=E3; // ���������� � ������
	}
	return 0;
}
//������� ����������� ���������
int fun(float a[][7], complex<float>pr[][7],complex<float>Z[N],complex<float>*E){
	// ��������� ������
	for(int i=0;i<W;i++){
		for(int j=0;j<W+1;j++){
			pr[i][j]=0;
		}
	}
	// 
	for(int i=0;i<N;i++){
		// ������������ ������������ �����
		complex<float>Y=complex<float>(1,0)/Z[i];
		// ������� ����������� �����
		int a1 = (int(a[i][0]) / 10) - 1; // ���� ������ �����
		int a2 = (int(a[i][0]) % 10) - 1; // ���� ��������� �����
		// ���������� ������������ � ��������� ������
		pr[a1][a2] = -Y;
		pr[a2][a1] = -Y;

	}

	for(int i=0;i<W;i++){
		for(int j=0;j<W+1;j++){
			if(i!=j) {
				pr[i][i] -=pr[i][j];
			}
		}
	}

	// ��������� ���
	for(int i=0;i<W;i++){
		pr[i][W]=0;
	}

	//��� � ����������� �����
	for (int i = 0; i < N; i++){
		if (a[i][5] > 0){
			// ������� ����������� �����
			int a1 = (int(a[i][4]) / 10) - 1; // ���� ������ �����
			int a2 = (int(a[i][4]) % 10) - 1; // ���� ��������� �����

			// ������������ ���
			complex<float>s=-E[i]*pr[a1][a2];
			pr[a1][W]-= s;
			pr[a2][W]+= s;
		}
	}
	return 0;
}
//����� ������
int gaus(complex<float>pr[][7],complex<float>xx[]){
	for (int i = 0; i<W; i++){
		complex<float> tmp = pr[i][i];
		for (int j = W; j >= i; j--)
			pr[i][j] /= tmp;
		pr[i][i]=complex<float>(1,0);
		for (int j = i + 1; j<W; j++){
			tmp = pr[j][i];
			for (int k = W; k >= i; k--){
				pr[j][k] -= tmp*pr[i][k];
			}
		}
	}
	xx[W - 1] = pr[W - 1][W];
	for (int i = W - 2; i >= 0; i--){
		xx[i] = pr[i][W];
		for (int j = i + 1; j < W; j++)
			xx[i] -= pr[i][j] * xx[j];
	}
	return 0;
}
//���� � ������
int toki(float a[][7],complex<float>*xx,complex<float>*Iv,complex<float>Z[N],complex<float>*E){
	// �������� ����
	for (int i = 0; i < N; i++){
		Iv[i] = 0;
	}
	// �������
	cout<<"����:"<<"\n";
	for(int i=0;i<N;i++){
		// ������ ����
		int a1 = (int(a[i][0]) / 10) - 1;
		int b1 = (int(a[i][4]) / 10) - 1;
		// ������ ����
		int a2 = (int(a[i][0]) % 10) - 1;
		int b2 = (int(a[i][4]) % 10) - 1;

		if(a1 == b2 && a2 == b1) // ���� ����������� ��� �� ��������� �� �������� ���
			Iv[i]=(xx[a1]-xx[a2] - E[i]) / Z[i];
		else
			Iv[i]=(xx[a1]-xx[a2] + E[i]) / Z[i];
		cout<<"I"<<i+1<<Iv[i]<<"\n";
	}
	return 0;
}
//��������
int Prover(float a[][7],complex<float>*E,complex<float>*Iv,complex<float>Z[N]){
	complex<float>f1(0,0);
	complex<float>f2(0,0);
	for(int i=0;i<N;i++){
		// ������ ����
		int a1 = (int(a[i][0]) / 10) - 1;
		int b1 = (int(a[i][4]) / 10) - 1;
		// ������ ����
		int a2 = (int(a[i][0]) % 10) - 1;
		int b2 = (int(a[i][4]) % 10) - 1;
		if(a1 == b2 && a2 == b1) // ���� ����������� ��� �� ��������� �� �������� ���
			f1-=E[i]*Iv[i];
		else
			f1+=E[i]*Iv[i];

		f2+=Iv[i]*Iv[i]*Z[i];
	}
	cout<<"������� ���������:";
	cout<<"\n"<<f1<<"="<<f2<<"\n";
	return 0;
}

// 0 - ����������� �����
// 1 - �������������
// 2 - �������������
// 3 - �������
// 4 - ����������� ���
// 5 - ���
// 6 - ���� ���
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");
	static float a[N][W+1] = {
		12, 33, 10, 62, 0, 0, 0, //1
		14, 47, 0, 0, 14, 80, 26, //2
		15, 36, 22, 43, 0, 0, 0, //3
		23, 15, 12, 43, 0, 0, 0, //4
		25, 82, 18, 51, 0, 0, 0, //5
		35, 43, 15, 39, 0, 0, 0, //6
		36, 100, 33, 82, 0, 0, 0, //7
		45, 27, 0, 0, 45, 80, -10, //8
		47, 20, 47, 0, 0, 0, 0, //9
		56, 10, 0, 16, 0, 0, 0, //10
		57, 51, 0, 0, 75, 60, 22, //11
		67, 12, 0, 0, 76, 36, -14 //12
	};
	complex<float>E[N];
	complex<float>Z[N];
	complex<float>pr[W][W+1];
	complex<float>xx[W+1];
	complex<float>Iv[N];
	Sopr(a,Z);
	Eds(a,E);
	fun(a,pr,Z,E);
	gaus(pr,xx);
	Sopr(a,Z);
	toki(a,xx,Iv,Z,E);
	Prover(a,E,Iv,Z);
	_getch();
	return 0;
}
