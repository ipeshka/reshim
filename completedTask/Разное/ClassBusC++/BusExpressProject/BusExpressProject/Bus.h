#pragma once


#include <iostream>
#include <ctime>

#ifndef BUS_H
#define BUS_H


using namespace std ;

///������������ - ��� �������� 
enum TypeOfBus 
{
	AutoBus = 1 ,
	Marchrutka =2,
	Minivan =3
};
class Bus
{
protected :

	//����� ���������� 
	char  * town;

	//����� ����� 
	int	FlightNumber;

	//��� �������� 
	TypeOfBus BusType;

	// ���������� �������
	int TicketNumber;
	
	//���� ������ 
    float TicketCost;

	//����� ����������� 
	tm *TimeArrived;

	//����� �������� 
	tm *TimeDelivery;

	void timeToString(tm *Time);


public:

	//����������� ��� ���������� 
	Bus(void);

	//����������� � ����������� 
	Bus(char  * ptown,int pFlightNumber,TypeOfBus pBusType,int pTicketNumber, float pTicketCost,tm  *pTimeArrived,tm *pTimeDelivery);

	//����������� ����������� - �������� ����� ������� 
	Bus(const Bus &obj);

	//����� ��������� ������ 
	void Show ();

	friend void GetTownInfo();

	//���������� �� ������� ����������� 
	friend void GetDerrivedTimeInfo();
	

	//������� - ������ ��������� ����� 
	void set_Town(char * ptown){town =ptown;}
	void set_FlightNumber(int pFlightNumber){FlightNumber = pFlightNumber;}
	void set_BusType(TypeOfBus pBusType){BusType = pBusType;}
	void set_TicketNumber(int pTicketNumber){TicketNumber = pTicketNumber;}
	void set_TicketCost(float pTicketCost){TicketCost = pTicketCost;}
	void set_TimeArrived(tm* pTimeArrived){TimeArrived = pTimeArrived;}	
	void set_TimeDelivery(tm *pTimeDelivery){TimeDelivery = pTimeDelivery;}


	char *  get_Town(){if(town){return town ;}else{ return NULL;}}
	int  get_FlightNumber(){return FlightNumber;}
	TypeOfBus get_BusType(){return BusType ;}
	int get_TicketNumber(){return TicketNumber;}
	
	float get_TicketCost(){return TicketCost;}
	tm*  get_TimeArrived(){return TimeArrived ;}	
	tm*  get_TimeDelivery(){return TimeDelivery;}

	char * getTown() {return town;}
	

	//������� ��� ����������� ����������� ���� / ������� 
	void ShowDateTime(tm * ptimeinfo);
	tm * CreateDateTime();


	void Bus::Set_first(Bus * b)
	{
		first = b;
	}

	Bus * Next;
	
	//������ �������� ��������� ����� 
	float GetSummaryFlightCost();

	~Bus(void);

	static Bus *first; 
	
	//��������� ������� ����������� �������������
	void set_TimeArrived();

	// ��������� ������� �������� ������������� 
	void set_TimeDelivery();
	
};


#endif BUS_H
