// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>



#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED
 
 
typedef struct
{
    unsigned int    bfType;
    unsigned long   bfSize;
    unsigned int    bfReserved1;
    unsigned int    bfReserved2;
    unsigned long   bfOffBits;
} BITMAPFILEHEADER;
 
typedef struct
{
    unsigned int    biSize;
    int             biWidth;
    int             biHeight;
    unsigned short  biPlanes;
    unsigned short  biBitCount;
    unsigned int    biCompression;
    unsigned int    biSizeImage;
    int             biXPelsPerMeter;
    int             biYPelsPerMeter;
    unsigned int    biClrUsed;
    unsigned int    biClrImportant;
} BITMAPINFOHEADER;
 
typedef struct
{
    int   rgbBlue;
    int   rgbGreen;
    int   rgbRed;
    int   rgbReserved;
} RGBQUAD;


static bool IsDigit(int digital[][10], RGBQUAD **rgb, int left, int right, int top, int bottom);
static unsigned int GetPixel(RGBQUAD **rgb, int x, int y);
static unsigned int GetTop(RGBQUAD **rgb, int left, int right, int top, int bottom);
static unsigned int GetBottom(RGBQUAD **rgb, int left, int right, int top, int bottom);
static unsigned int GetRight(RGBQUAD **rgb, int left, int right, int top, int bottom);
static unsigned int GetLeft(RGBQUAD **rgb, int left, int right, int top, int bottom);
static unsigned short read_u16(FILE *fp);
static unsigned int read_u32(FILE *fp);

#endif