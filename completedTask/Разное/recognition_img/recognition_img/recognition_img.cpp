// recognition_img.cpp : Defines the entry point for the console application.
//
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <array>
#include "stdafx.h"

// ������� ����, ������ ���� ����� �������
int digital_0[8][10] = { {0,0,0,1,1,1,1,0,0,0}, {0,0,1,1,1,1,1,1,0,0}, {0,1,1,0,0,0,0,1,1,0}, {1,1,0,0,0,0,0,0,1,1}, {1,1,0,0,0,0,0,0,1,1}, {0,1,1,0,0,0,0,1,1,0},{0,0,1,1,1,1,1,1,0,0}, {0,0,0,1,1,1,1,0,0,0}};
int digital_1[6][10] = { {0,0,1,0,0,0,0,0,0,1}, {0,1,1,0,0,0,0,0,0,1}, {1,1,1,1,1,1,1,1,1,1}, {1,1,1,1,1,1,1,1,1,1}, {0,0,0,0,0,0,0,0,0,1}, {0,0,0,0,0,0,0,0,0,1}};
int digital_2[8][10] = { {0,0,1,0,0,0,0,0,0,1}, {0,1,1,0,0,0,0,0,1,1}, {1,1,0,0,0,0,0,1,1,1}, {1,0,0,0,0,0,1,1,0,1}, {1,0,0,0,0,1,1,0,0,1}, {1,1,0,0,1,1,0,0,0,1}, {0,1,1,1,1,0,0,0,0,1}, {0,0,1,1,0,0,0,0,0,1} };
int digital_3[8][10] = { {0,1,0,0,0,0,0,0,1,0}, {1,1,0,0,0,0,0,0,1,1}, {1,0,0,0,0,0,0,0,0,1}, {1,0,0,0,1,0,0,0,0,1}, {1,0,0,0,1,0,0,0,0,1}, {1,1,0,1,1,1,0,0,1,1}, {0,1,1,1,0,1,1,1,1,0}, {0,0,1,0,0,0,1,1,0,0} };
int digital_4[8][10] = { {0,0,0,0,0,1,1,0,0,0}, {0,0,0,0,1,1,1,0,0,0}, {0,0,0,1,1,0,1,0,0,0}, {0,0,1,1,0,0,1,0,0,0}, {0,1,1,0,0,0,1,0,0,0}, {1,1,1,1,1,1,1,1,1,1}, {1,1,1,1,1,1,1,1,1,1}, {0,0,0,0,0,0,1,0,0,0} };
int digital_5[8][10] = { {1,1,1,1,1,0,0,1,0,0}, {1,1,1,1,1,0,0,1,1,0}, {1,0,0,0,1,0,0,0,1,1}, {1,0,0,1,0,0,0,0,0,1}, {1,0,0,1,0,0,0,0,0,1}, {1,0,0,1,1,0,0,0,1,1}, {1,0,0,0,1,1,1,1,1,0}, {0,0,0,0,0,1,1,1,0,0} };
int digital_6[8][10] = { {0,0,1,1,1,1,1,1,0,0}, {0,1,1,1,1,1,1,1,1,0}, {1,1,0,0,0,1,0,0,1,1}, {1,0,0,0,1,0,0,0,0,1}, {1,0,0,0,1,0,0,0,0,1}, {1,1,0,0,1,1,0,0,1,1}, {0,1,1,0,0,1,1,1,1,0}, {0,0,0,0,0,0,1,1,0,0}};
int digital_7[8][10] = { {1,0,0,0,0,0,0,0,1,1}, {1,0,0,0,0,0,0,1,1,1}, {1,0,0,0,0,0,1,1,0,0}, {1,0,0,0,0,1,1,0,0,0}, {1,0,0,0,1,1,0,0,0,0}, {1,0,0,1,1,0,0,0,0,0}, {1,1,1,1,0,0,0,0,0,0}, {1,1,1,0,0,0,0,0,0,0}};
int digital_8[8][10] = { {0,0,1,0,0,0,1,1,0,0}, {0,1,1,1,0,1,1,1,1,0}, {1,1,0,1,1,1,0,0,1,1}, {1,0,0,0,1,0,0,0,0,1}, {1,0,0,0,1,0,0,0,0,1}, {1,1,0,1,1,1,0,0,1,1}, {0,1,1,1,0,1,1,1,1,0}, {0,0,1,0,0,0,1,1,0,0}};
int digital_9[8][10] = { {0,0,1,1,0,0,0,0,0,0}, {0,1,1,1,1,0,0,1,1,0}, {1,1,0,0,1,1,0,0,1,1}, {1,0,0,0,0,1,0,0,0,1}, {1,0,0,0,0,1,0,0,0,1}, {1,1,0,0,1,0,0,0,1,1}, {0,1,1,1,1,1,1,1,1,0}, {0,0,1,1,1,1,1,1,0,0}};

int main() {
	char str[128];
	gets(str);
    FILE * pFile = fopen(str, "rb");
 
    // ��������� ��������� �����
    BITMAPFILEHEADER header;
    header.bfType      = read_u16(pFile);
    header.bfSize      = read_u32(pFile);
    header.bfReserved1 = read_u16(pFile);
    header.bfReserved2 = read_u16(pFile);
    header.bfOffBits   = read_u32(pFile);
 
    // ��������� ��������� �����������
    BITMAPINFOHEADER bmiHeader;
    bmiHeader.biSize          = read_u32(pFile);
    bmiHeader.biWidth         = read_u32(pFile);
    bmiHeader.biHeight        = read_u32(pFile);
    bmiHeader.biPlanes        = read_u16(pFile);
    bmiHeader.biBitCount      = read_u16(pFile);
    bmiHeader.biCompression   = read_u32(pFile);
    bmiHeader.biSizeImage     = read_u32(pFile);
    bmiHeader.biXPelsPerMeter = read_u32(pFile);
    bmiHeader.biYPelsPerMeter = read_u32(pFile);
    bmiHeader.biClrUsed       = read_u32(pFile);
    bmiHeader.biClrImportant  = read_u32(pFile);
 
 
	// �������� ������
	RGBQUAD **rgb = new RGBQUAD* [bmiHeader.biWidth];
    for (int i = 0; i < bmiHeader.biWidth; i++)
		rgb[i] = new RGBQUAD [bmiHeader.biHeight];

	// ��������� ���������� � �����������
	for (int i = bmiHeader.biHeight - 1; i >= 0; i--) {
		for (int j = 0; j < bmiHeader.biWidth; j++) {
            rgb[j][i].rgbBlue = getc(pFile);
            rgb[j][i].rgbGreen = getc(pFile);
            rgb[j][i].rgbRed = getc(pFile);
        }
		// ���������� ��������� ���� � ������
		for(int k = 0; k < bmiHeader.biWidth % 4;k++) 
			getc(pFile);
        
    }

	// �������� ����������� �����
	// ��������� ��������� ����������
	int Left = 0;
	int Top = 0;
	int Right = bmiHeader.biWidth - 1;
	int Bottom = bmiHeader.biHeight - 1;

	// � ����� ������� ��� �����
	while(true) {
		// ������� ����� ����������
		Left = GetLeft(rgb, Left, Right, Top, Bottom);
		// ������� ������ ����������
		Right = GetRight(rgb, Left, Right, Top, Bottom);

		// ���� ��������� ����� �����������
		if(Right == bmiHeader.biWidth) { // ������� ������ �� �����
			break;
		}
		// ������� ���������� �����
		Top = GetTop(rgb, Left, Right, Top, Bottom);
		// ������� ���������� ����
		Bottom = GetBottom(rgb, Left, Right, Top, Bottom);

		// ��������, �� ���������� �������� �����
		if(IsDigit(digital_0, rgb, Left, Right, Top, Bottom)) { printf("0 "); }
		else if(IsDigit(digital_1, rgb, Left, Right, Top, Bottom)) { printf("1 "); }
		else if(IsDigit(digital_2, rgb, Left, Right, Top, Bottom)) { printf("2 "); }
		else if(IsDigit(digital_3, rgb, Left, Right, Top, Bottom)) { printf("3 "); }
		else if(IsDigit(digital_4, rgb, Left, Right, Top, Bottom)) { printf("4 "); }
		else if(IsDigit(digital_5, rgb, Left, Right, Top, Bottom)) { printf("5 "); }
		else if(IsDigit(digital_6, rgb, Left, Right, Top, Bottom)) { printf("6 "); }
		else if(IsDigit(digital_7, rgb, Left, Right, Top, Bottom)) { printf("7 "); }
		else if(IsDigit(digital_8, rgb, Left, Right, Top, Bottom)) { printf("8 "); }
		else if(IsDigit(digital_9, rgb, Left, Right, Top, Bottom)) { printf("9 "); }

		// ��������������� ����������, ��� �� �� ��������� ������������
		Left = Right + 1;
		Right = bmiHeader.biWidth - 1;
		Top = 0;
		Bottom = bmiHeader.biHeight - 1;
	}

	// ����������� ����
    fclose(pFile);

	// ������������� ������ ��������� ��� ��������� ������������ ������:
	for (int i = 0; i < bmiHeader.biHeight; i++)
        delete [] rgb[i];
	gets(str);
    return 0;
}

// ������� �������� �� ����������� �����
static bool IsDigit(int digital[][10], RGBQUAD **rgb, int left, int right, int top, int bottom) {
	bool isOne = true;
	for(int x = left; x <=right;x++) {
		for(int y = top; y <= bottom; y++) {
			if(digital[x - left][y - top] != GetPixel(rgb, x, y)) { 
				return false;
			}
		}
	}
}

// ������� �������� �������, ���� �� ������ �� 1, ����� 0
static unsigned int GetPixel(RGBQUAD **rgb, int x, int y) {
	if (rgb[x][y].rgbBlue == 0 && rgb[x][y].rgbGreen == 0 && rgb[x][y].rgbRed == 0) return 1; 
	else return 0;

}

// ���������� ��������� �����
static unsigned int GetTop(RGBQUAD **rgb, int left, int right, int top, int bottom) {
	for (int y = top; y <= bottom; y++) {
		for (int x = left; x <= right; x++) {
			if (rgb[x][y].rgbBlue == 0 && rgb[x][y].rgbGreen == 0 && rgb[x][y].rgbRed == 0) {
				return y;
			}
		 }
	}
}
// ���������� ��������� ����
static unsigned int GetBottom(RGBQUAD **rgb, int left, int right, int top, int bottom) {
	for (int y = top; y <= bottom; y++) {
		int count = 0;
		for (int x = left; x <= right; x++) {
			if (rgb[x][y].rgbBlue == 0 && rgb[x][y].rgbGreen == 0 && rgb[x][y].rgbRed == 0) count++;
		 }

		if(count == 0) {
			return y - 1;
		}
	}
}
// ���������� ��������� ������ ����
static unsigned int GetLeft(RGBQUAD **rgb, int left, int right, int top, int bottom) {
	for (int x = left; x <= right; x++) {
		for (int y = top; y <= bottom; y++) {
			if (rgb[x][y].rgbBlue == 0 && rgb[x][y].rgbGreen == 0 && rgb[x][y].rgbRed == 0) {
				return x;
			}
		 }
	}
}
// ���������� ��������� ������� ����
static unsigned int GetRight(RGBQUAD **rgb, int left, int right, int top, int bottom) {
	for (int x = left; x <= right; x++) {
		int count = 0;
		for (int  y = top; y <= bottom; y++) {
			if (rgb[x][y].rgbBlue == 0 && rgb[x][y].rgbGreen == 0 && rgb[x][y].rgbRed == 0) count++;
		}

		if (count == 0) {
			return x - 1;
		}
	}
}

// ������ 2 ��������� �����
static unsigned short read_u16(FILE *fp) {
    unsigned char b0, b1;
 
    b0 = getc(fp);
    b1 = getc(fp);
 
    return ((b1 << 8) | b0);
}
// ������ 4 ��������� �����
static unsigned int read_u32(FILE *fp) {
    unsigned char b0, b1, b2, b3;
 
    b0 = getc(fp);
    b1 = getc(fp);
    b2 = getc(fp);
    b3 = getc(fp);
 
    return ((((((b3 << 8) | b2) << 8) | b1) << 8) | b0);
}

