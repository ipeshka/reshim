// Employee.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
using namespace std;

class Employee // ������� �����
{
public:
	string name; // ���
	int id; // ��
public:
	double total_salary; // ��������� ������
public:
	Employee(const string & n = "unknown", int i = 0);
	virtual void calc_salary() = 0;
	void show();
	friend bool operator<(const Employee & r1, const Employee & r2);
	virtual ~Employee() {}
};

// �������� ������� �������� �� ������������� ������
class Employee_monthly : public Employee
{
private:
	double m_salary;
public:
	Employee_monthly(const string & n = "Unknown", int i = 0, double m_sal = 0.0);
	virtual void calc_salary();

};

// �������� ������� �������� �� ������� ������
class Employee_hourly : public Employee
{
private:
	double h_salary;
public:
	Employee_hourly(const string & n = "Unknown", int i = 0, double h_sal = 0.0);
	virtual void calc_salary();
};

Employee::Employee(const string & n, int i) : name(n), id(i){}

Employee_monthly::Employee_monthly(const string & n, int i, double m_sal) : Employee(n,i)
{
	m_salary = m_sal;
}

Employee_hourly::Employee_hourly(const string & n, int i, double h_sal) : Employee(n,i)
{
	h_salary = h_sal;
}


void Employee_monthly::calc_salary()
{
	total_salary = m_salary;
}

void Employee_hourly::calc_salary()
{
	total_salary = 20.8 * 8 * h_salary;
}

void Employee::show()
{
	cout << "Name:\t" << name << endl;
	cout << "ID:\t" << id << endl;
	cout << "Salary:\t" << total_salary << endl << endl;
}

bool comp(const Employee *r1, const Employee *r2)
{
	if (r1->total_salary > r2->total_salary)
		return true;
	else if (r1->total_salary == r2->total_salary && r1->name < r2->name)
		return true;
	else
		return false;
}

list<Employee*> employees;
std::list<Employee*>::iterator it;

int main()
{
	char buff[50]; // ����� �������������� �������� ������������ �� ����� ������
	// ��������� ����
	fstream fin("database.txt"); // ������ ������ ������ ofstream ��� ������ � ��������� ��� � ������ database.txt

	// ��������� ���� ���������
	while (!fin.eof())
	{
		string name;
		int id;
		int salary;
		char type;

		fin>>name;
		fin>>id;
		fin>>salary;
		fin>>type;

		if(type == 'N') {
			employees.push_back(new Employee_monthly(name, id, salary));
		}else {
			employees.push_back(new Employee_hourly(name, id, salary));
		}
	}
	fin.close(); // ��������� ����
    
	// ���� �������
	char answer = 'Y';
	while(answer!='N') {
		string name;
		int id;
		int salary;
		char type;

		cout<<"Input name:"<<endl;
		cin>>name;

		cout<<"Input id:"<<endl;
		cin>>id;

		cout<<"Input salary:"<<endl;
		cin>>salary;

		cout<<"Input type employee (N - monthly, H - hourly):"<<endl;
		cin>>type;

		if(type == 'N') {
			employees.push_back(new Employee_monthly(name, id, salary));
		}else {
			employees.push_back(new Employee_hourly(name, id, salary));
		}

		cout<<"Next (Y/N)?"<<endl;
		cin>>answer;
	}

	// ������� � ���� ��������
	for (it = employees.begin(); it != employees.end(); ++it)
		(*it)->calc_salary();

	// ������� ������
	std::cout << "mylist:";
	for (it = employees.begin(); it != employees.end(); ++it)
		(*it)->show();

	// ���������
	employees.sort(comp);
	// ������� ���������������
	std::cout << "\n mylist (sort):";
	for (it = employees.begin(); it != employees.end(); ++it)
		(*it)->show();

	ofstream fout("database.txt"); // ������ ������ ������ ofstream ��� ������ � ��������� ��� � ������ database.txt

	// ���������
	for (it = employees.begin(); it != employees.end(); ++it) {
		fout<<(*it)->name<<" ";
		fout<<(*it)->id<<" ";
		fout<<(*it)->total_salary<<" ";
		if(typeid((*it)) == typeid(Employee_monthly))
			fout<<"M"<<"\n";
		else
			fout<<"N"<<"\n";
	}
	fout.close();
    system("PAUSE");
	
    return 0;   
}