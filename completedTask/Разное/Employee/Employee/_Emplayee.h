#include "stdafx.h"
#include <string>

using namespace std;
// ������� �����
class Employee
{
private:
	string name;
	int id;
protected:
	double total_salary;
public:
	Employee(const string & n = "unknown", int i = 0);
	virtual void calc_salary() = 0;
	void show();
	friend bool operator<(const Employee & r1, const Employee & r2);
	virtual ~Employee() {}
};

// �������� �� �����
class Employee_monthly : public Employee
{
private:
	double m_salary;
public:
	Employee_monthly(const string & n = "unknown", int i = 0, double m_sal = 0.0);
	virtual void calc_salary();

};

// ��������� ��������
class Employee_hourly : public Employee
{
private:
	double h_salary;
public:
	Employee_hourly(const string & n = "unknown", int i = 0, double h_sal = 0.0);
	virtual void calc_salary();
};
