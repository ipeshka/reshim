﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace Lab7
{
    class Lab7
    {
        public static void Main()
        {
            FileStream fin;
            string s;
            try
            {
                fin = new FileStream("D:\\test.txt", FileMode.Open);
            }
            catch (FileNotFoundException exc)
            {
                Console.WriteLine(exc.Message +
                "Не удается открыть файл.");
                return;
            }
            StreamReader fstrIn = new StreamReader(fin);

            var reg = new Regex(@"([1-9]{1}\d){1}");
            while ((s = fstrIn.ReadLine()) != null)
            {
                if (!reg.IsMatch(s))
                    Console.WriteLine(s);
            }

            fstrIn.Close();
            Console.ReadLine();
        }
    }
}