// file_printf.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL,"rus");

	string filePath;
	string text;
	char beginChar;
	char replaceChar;
	char answerExit = 'N';
	char buff; // ����� �������������� �������� ������������ �� ����� ������
	// ��������� ����

	while(answerExit != 'Y') {
		cout<<"Input path file:"<<endl;
		cin>>filePath;

		cout<<"Input find char:"<<endl;
		cin>>beginChar;

		cout<<"Input replace char:"<<endl;
		cin>>replaceChar;

		// ��������� ����
		fstream fout(filePath, ios::in); 
		if(fout.fail()) {
			cout<<"������ �������� ����� "<<filePath<<endl;
		}

		do {
			buff = fout.get();
			text.push_back(buff);
		} while(!fout.eof());
		fout.close();// ��������� ����


		if(!text.find(replaceChar)) {
			cout<<"������ � ����� �� ������!"<<endl;
		}else {
			std::replace (text.begin(), text.end(), beginChar, replaceChar);
		}

		// ��������� ���� ��� ������
		ofstream  fin(filePath, fstream::in | fstream::out | fstream::trunc);
		fin<<text<<endl; 
		fin.close();
		

		cout<<"Exit? (N/Y)"<<endl;
		cin>>answerExit;
	}
	return 0;
}

