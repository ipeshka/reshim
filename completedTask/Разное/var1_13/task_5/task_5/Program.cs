﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_5
{
    class Program
    {
        static void Main(string[] args)
        {
            double f = 0;
            Console.WriteLine("Введите a:");
            double a = Double.Parse(Console.ReadLine());

            if (a >= 1)
            { // если правая ветвь графика
                f = a - 1;
            }
            else if (a <= -1)
            {
                f = a + 1;
            }
            else
            {
                f = Math.Sqrt(1 - Math.Pow(a, 2));
            }


            Console.WriteLine("f - {0}", f);
            Console.ReadLine();
        }
    }
}
