﻿using System;

namespace task_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // задаём переменные
            double x = 17.421;
            double y = 10365;
            double z = 82800;

            // записываем функцию
            double f = (Math.Pow(y + Math.Pow(x - 1, 1 / 3), 1 / 4))
                / (Math.Abs(x - y) * (Math.Pow(Math.Sin(z), 2) + Math.Tan(z)))
                * (1 + Math.Pow(z, 2) / (3 - 0.2 * Math.Pow(z, 2)));
            // вывод результата
            Console.WriteLine(f);
            // ждём ввода от пользователя, что бы консоль не пряталась сразу
            Console.ReadLine();
        }
    }
}
