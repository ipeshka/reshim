﻿using System;

namespace task_4
{
    class Program
    {
        // f(x) = pow(x,2)
        static void Main(string[] args) {
            Console.WriteLine("Введите x:");
            double x = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите b:");
            double b = Double.Parse(Console.ReadLine());

            double s;
            if (x*b > 1 && x*b < 11) {
                Console.WriteLine("Ветвь 1.");
                s = 2 * Math.Exp(Math.Pow(x, 2));

            } else if (x*b > 11 && x*b < 55) {
                Console.WriteLine("Ветвь 2.");
                s = Math.Sqrt(Math.Abs(Math.Pow(x, 2) + 3*b));
            } 
            else {
                Console.WriteLine("Ветвь 3.");
                s = 3 * b* Math.Pow(x, 4);
            }

            Console.WriteLine("s - {0}", s);
            Console.ReadLine();
        }
    }
}
