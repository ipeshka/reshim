﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_6
{
    class Program
    {
        static void Main(string[] args)
        {
            double h = 0.1;
            double s, y;
            int n = 1;
            for (double x = 0; x <= 1; x += h)
            {
                y = Math.Log10(1/(2 + 2*x + Math.Pow(x, 2)));
                s = Math.Pow(-1, n) * Math.Pow(1 + x, 2 * n) / n;
                Console.WriteLine("x - {0}, y - {1}, s - {2}", x, y, s);
                n++;
            }
            Console.ReadLine();
        }
    }
}
