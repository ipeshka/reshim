﻿using System;

namespace task_3
{
    class Program
    {
        static void Main(string[] args) {
            Console.WriteLine("Введите радиус:");
            double radius = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите высоту:");
            double height = Double.Parse(Console.ReadLine());

            double v = Math.PI * Math.Pow(radius, 2) * height;
            Console.WriteLine("Объём цилиндра - {0}", v);
            Console.ReadLine();

        }
    }
}
