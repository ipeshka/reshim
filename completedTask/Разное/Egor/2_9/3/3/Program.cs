﻿using System;


namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            double h = 0.1;
            double s, y;
            int n = 1;
            for (double x = 0; x <= 1; x += h) {
                y = (x*x/4 + x/2 + 1)*Math.Pow(Math.E, x/2);
                s = Math.Pow(n, 2)/Fact(n)*Math.Pow(x/2, n);
                Console.WriteLine("x - {0}, y - {1}, s - {2}", x, y, s);
                n++;
            }
            Console.ReadLine();
        }

        static int Fact(int num)
        {
            return (num == 0) ? 1 : num * Fact(num - 1);
        }
    }
}
