﻿using System;


namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            double f = 0;
            Console.WriteLine("Введите x:");
            double x = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите y:");
            double y = Double.Parse(Console.ReadLine());

            if (x * x + y * y >= 1 && x * x + y * y <= 4)
            {
                Console.WriteLine("Принадлежит.");
            }
            else
            {
                Console.WriteLine("Не принадлежит.");
            }

            Console.ReadLine();
        }
    }
}
