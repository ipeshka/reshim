﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        // f(x) = pow(x,2)
        static void Main(string[] args)
        {
            Console.WriteLine("Введите x:");
            double x = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите p:");
            double p = Double.Parse(Console.ReadLine());

            double s = 0;
            if (x > p)
            {
                Console.WriteLine("Ветвь 1.");
                s = 2 * Math.Pow(Math.Pow(x, 2), 3);

            }
            else if (x > 3 && x < Math.Abs(p))
            {
                Console.WriteLine("Ветвь 2.");
                s = Math.Abs(Math.Pow(x, 2) - p);
            }
            else if (Math.Abs(x - Math.Abs(p)) < 0.001) // потому что сравниваем double
            {
                Console.WriteLine("Ветвь 3.");
                s = Math.Pow(Math.Pow(x, 2) - p, 2);
            }

            Console.WriteLine("s - {0}", s);
            Console.ReadLine();
        }
    }
}
