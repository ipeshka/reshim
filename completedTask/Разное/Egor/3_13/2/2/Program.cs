﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ', ':' });
            Console.WriteLine("Задание а)");
            CountWordWithEvenLenght(str); // задание а
            Console.WriteLine("Задание б)");
            SortAlphafit(str); // задание б
            Console.WriteLine("Задание в)");
            NewString(str);// задание в
            Console.ReadLine();
        }

        // задание а)
        private static void CountWordWithEvenLenght(string[] str)
        {
            List<string> buf = new List<string>();
            foreach (var s in str)
            {
                if (s.Length % 2 == 0) // если чётное
                    buf.Add(s);
            }
            Console.WriteLine("Количество слов: {0}", buf.Count);
            foreach (var s in buf)
            {
                Console.WriteLine(s);
            }
        }

        // задание б)
        private static void SortAlphafit(string[] str)
        {
            for (int j = 0; j < str.Length; j++)
            {
                for (int k = 0; k < str.Length; k++)
                {
                    if (str[j][0] < str[k][0])
                    {
                        string tmp = str[j];
                        str[j] = str[k];
                        str[k] = tmp;
                    }
                }
            }

            foreach (var s in str)
            {
                Console.WriteLine(s);
            }
        }

        // задание а)
        private static void NewString(string[] str)
        {
            string buf = "";
            foreach (var s in str) {
                string ss = "";
                foreach (var ch in s) {
                    ss += ch.ToString() + " ";
                }
                ss += "   ";
                buf+=ss;
            }
            Console.WriteLine(buf);
        }
    }
}
