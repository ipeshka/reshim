﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку двоичных цифр");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] {' '});
            foreach (var s in str) {
                Console.WriteLine(Convert.ToString(Convert.ToInt32(s, 2), 16));
            }

            Console.ReadLine();
        }
    }
}
