﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ', '-' });
            Console.WriteLine("Задание а)");
            ZadanieA(str); // задание а
            Console.WriteLine("Задание б)");
            ZadanieB(str); // задание б
            Console.WriteLine("Задание в)");
            ZadanieC(str);// задание в
            Console.ReadLine();
        }

        // задание а)
        private static void ZadanieA(string[] str)
        {
            char[] alpha = new[] { 'a', 'o', 'e', 'i', 'u', 'y' };
            List<string> buf = new List<string>();
            foreach (var s in str)
            {
                if (alpha.Contains(s[0])) // если чётное
                    buf.Add(s);
            }
            Console.WriteLine("Количество слов: {0}", buf.Count);
            foreach (var s in buf)
            {
                Console.WriteLine(s);
            }
        }

        // задание б)
        private static void ZadanieB(string[] str)
        {
            for (int j = 0; j < str.Length; j++)
            {
                for (int k = 0; k < str.Length; k++)
                {
                    if (str[j][0] > str[k][0])
                    {
                        string tmp = str[j];
                        str[j] = str[k];
                        str[k] = tmp;
                    }
                }
            }

            foreach (var s in str)
            {
                Console.WriteLine(s);
            }
        }

        // задание В)
        private static void ZadanieC(string[] str)
        {
            string buf = "";
            int i = 0;
            foreach (var s in str)
            {
                if (i++%2 == 0) // если чётная
                    buf += s + " ";
            }
            Console.WriteLine(buf);
        }
    }
}
