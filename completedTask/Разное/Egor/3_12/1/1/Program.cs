﻿using System;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку 16-ичных цифр");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ' });
            foreach (var s in str)
            {
                Console.WriteLine(Convert.ToString(Convert.ToInt32(s, 16), 2));
            }

            Console.ReadLine();
        }
    }
}
