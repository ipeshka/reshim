﻿using System;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите длинну ребра a:");
            double a = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите длинну ребра b:");
            double b = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите длинну ребра c:");
            double c = Double.Parse(Console.ReadLine());

            double s = 2 * (a * b + b * c + a * c);
            Console.WriteLine("Объём цилиндра - {0}", s);
            Console.ReadLine();
        }
    }
}
