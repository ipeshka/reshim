﻿using System;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            // задаём переменные
            double x = 14.26;
            double y = -1.22;
            double z = 0.035;

            // записываем функцию
            double t = (2 * Math.Cos(x - Math.PI/6) / 0.5 + Math.Pow(Math.Sin(y), 2))
                * (1 + Math.Pow(z, 2) / (3 - Math.Pow(z, 2)/5));
            // вывод результата
            Console.WriteLine(t);
            // ждём ввода от пользователя, что бы консоль не пряталась сразу
            Console.ReadLine();
        }
    }
}
