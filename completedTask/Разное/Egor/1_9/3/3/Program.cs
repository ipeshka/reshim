﻿using System;

namespace _3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите радиус:");
            double radius = Double.Parse(Console.ReadLine());

            Console.WriteLine("Введите высоту:");
            double height = Double.Parse(Console.ReadLine());

            double s = 2 * Math.PI * radius * (height + radius);
            Console.WriteLine("Плошадь цилиндра - {0}", s);
            Console.ReadLine();

        }
    }
}
