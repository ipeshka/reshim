﻿using System;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            // задаём переменные
            double x = 182.5;
            double y = 18.225;
            double z = -0.03298;

            // записываем функцию
            double f = Math.Abs(Math.Pow(x, x / y) - Math.Pow(y / x, 1 / 3))
                + (y - x) * ((Math.Cos(y) - z / (y - x)) / (1 + Math.Pow(y - x, 2)));
            // вывод результата
            Console.WriteLine(f);
            // ждём ввода от пользователя, что бы консоль не пряталась сразу
            Console.ReadLine();
        }
    }
}
