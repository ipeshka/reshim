﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ', ',' });
            Console.WriteLine("Задание а)");
            ZadanieA(str); // задание а
            Console.WriteLine("Задание б)");
            ZadanieB(str); // задание б
            Console.WriteLine("Задание в)");
            ZadanieC(str);// задание в
            Console.ReadLine();
        }

        // задание а)
        private static void ZadanieA(string[] str)
        {
            char[] alpha = new[] { 'a', 'o', 'e', 'i', 'u', 'y' };
            List<string> buf = new List<string>();
            foreach (var s in str)
            {
                if (alpha.Contains(s[s.Length - 1])) // если посленее равно букве из массива
                    buf.Add(s);
            }
            Console.WriteLine("Количество слов: {0}", buf.Count);
            foreach (var s in buf)
            {
                Console.WriteLine(s);
            }
        }

        // задание б)
        private static void ZadanieB(string[] str)
        {
            for (int j = 0; j < str.Length; j++)
            {
                for (int k = 0; k < str.Length; k++)
                {
                    if (str[j].Length < str[k].Length)
                    {
                        string tmp = str[j];
                        str[j] = str[k];
                        str[k] = tmp;
                    }
                }
            }

            foreach (var s in str)
            {
                Console.WriteLine(s);
            }
        }

        // задание c)
        private static void ZadanieC(string[] str)
        {
            string buf = "";
            foreach (var s in str) {
                if (!s.Contains('t')) // если не содержит букву то записываем в другую строку
                    buf += s + " ";
            }
            Console.WriteLine(buf);
        }
    }
}
