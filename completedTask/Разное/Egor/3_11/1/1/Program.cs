﻿using System;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку из 10 цифр");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ' });
            foreach (var s in str)
            {
                if (Int32.Parse(s) % 2 == 0)
                    Console.WriteLine(s);
            }

            Console.ReadLine();
        }
    }
}
