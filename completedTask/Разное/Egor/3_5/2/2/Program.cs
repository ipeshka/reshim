﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ', '-' });
            Console.WriteLine("Задание а)");
            ZadanieA(str); // задание а
            Console.WriteLine("Задание б)");
            ZadanieB(str); // задание б
            Console.WriteLine("Задание в)");
            ZadanieC(str);// задание в
            Console.ReadLine();
        }

        // задание а)
        private static void ZadanieA(string[] str)
        {
            List<string> buf = new List<string>();
            foreach (var s in str)
                if (s.Length % 2 == 0) // если чётное
                    buf.Add(s);

            Console.WriteLine("Количество слов: {0}", buf.Count);
            foreach (var s in buf)
                Console.WriteLine(s);

        }

        // задание б)
        private static void ZadanieB(string[] str) {
            string bufMin = new string('a', 256);
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i][str[i].Length - 1] == 'a' && bufMin.Length > str[i].Length) {
                    bufMin = str[i];
                }
            }
            Console.WriteLine(bufMin);
        }

        // задание с)
        private static void ZadanieC(string[] str)
        {
            string buf = "";
            int i = 0;
            foreach (var s in str)
                if (s[s.Length - 1] != 'a') // если заканчиваеться на а, то не добавляем
                    buf += s + " ";

            Console.WriteLine(buf);
        }
    }
}
