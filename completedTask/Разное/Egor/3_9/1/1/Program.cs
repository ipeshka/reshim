﻿using System;
using System.Text.RegularExpressions;

namespace _1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку");
            string line = Console.ReadLine();
            Match m = Regex.Match(line, "[0-9]+[.[0-9]+]?");
            double number = Convert.ToDouble(m.Value);
            Console.WriteLine(number);
            Console.ReadLine();
        }
    }
}
