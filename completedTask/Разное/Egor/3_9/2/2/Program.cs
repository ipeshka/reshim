﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите предложение");
            string line = Console.ReadLine();

            string[] str = line.Split(new[] { ' ', ':' });
            Console.WriteLine("Задание а)");
            ZadanieA(str, 7); // задание а
            Console.WriteLine("Задание б)");
            ZadanieB(str); // задание б
            Console.WriteLine("Задание в)");
            ZadanieC(str);// задание в
            Console.ReadLine();
        }

        // задание а)
        private static void ZadanieA(string[] str, int len)
        {
            List<string> buf = new List<string>();
            foreach (var s in str)
            {
                if (s.Length < len) // если чётное
                    buf.Add(s);
            }
            Console.WriteLine("Количество слов: {0}", buf.Count);
            foreach (var s in buf)
                Console.WriteLine(s);
        }

        private static string MinStr(string[] str)
        {
            string min = str[0];
            for (int k = 0; k < str.Length; k++)
            {
                if (min.Length > str[k].Length)
                {
                    min = str[k];
                }
            }

            return min;
        }

        // задание б)
        private static void ZadanieB(string[] str)
        {
            string min = MinStr(str);
            int minLenght = min.Length;
            int countMinLenght = 0;
            for (int j = 0; j < str.Length; j++)
            {
                if (str[j].Length == minLenght)
                {
                    countMinLenght++;
                }
            }

            Console.WriteLine("Процент:" + countMinLenght * 100 / str.Length);
        }

        // задание а)
        private static void ZadanieC(string[] str)
        {
            string min = MinStr(str);
            int minLenght = min.Length;
            string buf = "";
            foreach (var s in str)
            {
                if (s.Length != minLenght)
                    buf += s + " ";
            }
            Console.WriteLine(buf);
        }
    }
}
