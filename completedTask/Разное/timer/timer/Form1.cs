﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace timer
{
    public partial class Form1 : Form
    {
        private static int seconde = 0 ;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string[] line = System.IO.File.ReadAllLines("timer.txt");
            if (line.Count() < 3)
            {
                foreach (var str in line) {
                    System.IO.File.WriteAllText("timer.txt", "Измерение: " + str);
                }

                System.IO.File.WriteAllText("timer.txt", "Измерение: " + seconde.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            seconde++;
            label1.Text = seconde.ToString();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            timer1.Stop();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            seconde = 0;
            label1.Text = seconde.ToString();
        }
    }
}
