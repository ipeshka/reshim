// 10_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Polynom.h"


int _tmain(int argc, _TCHAR* argv[])
{
	int i;
	Polynom f,g,h;

	f.InputPolynom();
	g.InputPolynom();

	h = g + 4 * f;

	h.OutputPolynom();

	cin >> i;

	system("pause");

	return 0;
}

