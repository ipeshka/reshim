// 16_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <conio.h>
#include <string>
#include <iostream>
#include <cstring>
using namespace std;

const int N = 80;

void TrimRightC(char S[], char C)
{
	int j = strlen(S) - 1;
	if(S[j] == C)
		S[j] = '\0';
}

int main()
{
	char S[][N] = 
	{
		"peace",
		"bee",
		"bicycle",
		"beach",
		"baran"
	};

	for(int i=0; i<5; i++)
		cout << S[i] << "\n";

	cout << "\n\n";


	TrimRightC(S[0], 'e');
	TrimRightC(S[1], 'e');
	TrimRightC(S[2], 'e');
	TrimRightC(S[3], 'e');
	TrimRightC(S[4], 'e');

	for(int i=0; i<5; i++)
		cout << S[i] << "\n";

	_getch();	
	return 0;
}
