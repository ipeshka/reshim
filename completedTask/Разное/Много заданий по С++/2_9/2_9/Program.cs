﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_9 {
    class Program {
        static void Main (string[] args) {
            string filename;
            int N;
            Console.WriteLine("Введите имя файла: ");
            filename = Console.ReadLine();

            Console.WriteLine("Введите N (0 - 27): ");
            N = Convert.ToInt32(Console.ReadLine());
            string temp = "";
            using(StreamWriter outfile = new StreamWriter(filename)) {
                for(char i = 'a' ; i < 'a' + N ; i++) {
                    temp += i;
                    outfile.WriteLine(temp.ToString());
                }
            }
        }
    }
}
