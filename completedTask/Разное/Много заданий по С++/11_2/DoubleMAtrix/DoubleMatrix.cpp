#include "DoubleMatrix.h"

DoubleMatrix::DoubleMatrix(void)
{
}

DoubleMatrix::~DoubleMatrix(void)
{
	for(int i=0; i <_RowNum;i++)
	{
		delete [] _Array[i];
	}
}

DoubleMatrix::DoubleMatrix(int pRow,int pColumn)
{
	_Array = new float*[pColumn];

	for(int i=0; i <pRow;i++)
	{
		_Array[i] = new float[pRow];
	}

	for(int i=0;i<pRow ; i++)
	{
		for(int j=0; j<pColumn ; j++)
		{			
			_Array[i][j] = 0;		
		}	
	}

	_RowNum = pRow;
	_ColNum = pColumn;
}
//���������� �� �������
float DoubleMatrix::getValueById(int pRow,int pColumn)
{
	/*������ ����� ����� ���� ����������� ��� ���������� �������� �������*/
	/*�� ����� ��� �������������� �������������� ��������*/
	float lres = -1;
	if(pRow>=0 && pRow<=_RowNum && pColumn>=0 && pColumn<_ColNum)
	{
		lres = _Array[pRow][pColumn];
	}
	return lres;
}

// ��������� �������� �� ������� 
void  DoubleMatrix::setValueById(int pRow,int pColumn,float pValue)
{
	if(pColumn==1)
	{
		int a =10;
	}

	if(pRow>=0 && pRow<_RowNum && pColumn>=0 && pColumn<_ColNum)
	{
		_Array[pRow][pColumn] = pValue;
	}
}


void DoubleMatrix::Print()
{
	for( int i =0;i<_RowNum ; i++)
	{
		for( int j =0; j<_ColNum ; j++)
		{
			cout.width(5);
			cout<<_Array[i][j]<<" ";
		}
		cout<<"\n";
	}

}

//������������
bool  DoubleMatrix::isDiagonal(){

	bool lresult = false;

	bool stop = false ;
	if(_RowNum>0 &&_ColNum>0)
	{
		
		for(int i=0;i<_RowNum;i++)
		{
			if(stop)
			{
				lresult = false;
				break;
			}
			else
			{
				for(int j=0;j<_ColNum;j++)
				{
					if(i!=j)
					{
						if(_Array[i][j] !=0)
						{	
							stop= true;
							break;
						}
					}
				}
				lresult= true;
			}
		}

	}
	return lresult;
}
	
//������� 
//��������� �� ����� 
bool  DoubleMatrix::iszero()
{
	bool lresult = false;

	bool stop = false ;
	if(_RowNum>0 &&_ColNum>0)
	{
		
		for(int i=0;i<_RowNum;i++)
		{
			if(stop)
			{
				lresult = false;
				break;
			}
			else
			{
				for(int j=0;j<_ColNum;j++)
				{					
					if(_Array[i][j] !=0)
					{	
						stop= true;
						break;
					}
				}

				lresult= true;
			}
		}
	
	}
	return lresult;
}
//���������
bool  DoubleMatrix::isidentity()
{
	bool lresult = false;

	bool stop = false ;
	if(_RowNum>0 &&_ColNum>0)
	{
		
		for(int i=0;i<_RowNum;i++)
		{
			if(stop)
			{
				lresult = false;
				break;
			}
			else
			{
				for(int j=0;j<_ColNum;j++)
				{
					if(i!=j)
					{
						if(_Array[i][j] !=0)
						{	
							stop= true;
							break;
						}
					}
					else if(i==j)
					{
						if(_Array[i][j] !=1)
						{	
							stop= true;
							break;
						}
					}
				}
				lresult= true;
			}
		}

		
	}
	return lresult;
}
	
	//������������
bool  DoubleMatrix::issymmetric()
{
	bool lresult = false;

	bool stop = false ;
	if(_RowNum>0 &&_ColNum>0)
	{
		
		for(int i=0;i<_RowNum;i++)
		{
			if(stop)
			{
				lresult = false;
				break;
			}
			else
			{
				for(int j=0;j<_ColNum;j++)
				{
					if(i!=j)
					{
						if(_Array[i][j] != _Array[_ColNum -1 - i][_ColNum-1 - j])
						{	
							stop= true;
							break;
						}
					}
					
				}
				lresult= true;
			}
		}

		
	}
	return lresult;
}
	
//������� �����������
bool  DoubleMatrix::isUpperTriangular()
{
	bool lresult = false;

	int di = _RowNum/2;
	int dj = _ColNum/2;
	
	bool stop =false;


	if(_RowNum>0 &&_ColNum>0)
	{
		for(int i=0;i<_RowNum;i++)
		{
			if(stop )
			{
				lresult = false;
				break;			
			}
			else
			{			
				for(int j=0;j<_ColNum;j++)
				{
					// ���� ��������� 
					if(j>i)
					{
						if(_Array[i][j] != 0 )
						{	
							stop= true;
							break;
						}
					}
				}

				lresult = true;

			}
			
		}

	}
	return lresult;
	
}
	
	//������ �����������
bool  DoubleMatrix::isLowerTriangular()
{
	bool lresult = false;

	int di = _RowNum/2;
	int dj = _ColNum/2;
	
	bool stop =false;


	if(_RowNum>0 &&_ColNum>0)
	{
		for(int i=0;i<_RowNum;i++)
		{
			if(stop )
			{
				break;			
			}
			else
			{			
				for(int j=0;j<_ColNum;j++)
				{
					// ���� ��������� 
					if(j>i)
					{
						if(_Array[i][j] != 0 )
						{	
							stop= true;
							break;
						}
					}
				}

				lresult = true;
			}
			
		}

	}
	return lresult;
}

