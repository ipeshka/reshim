#pragma once
#include <iostream>
#include <math.h>


using namespace std;


#ifndef DOUBLEMATRIX_H
#define DOUBLEMATRIX_H

class DoubleMatrix
{

	//����� ����� 
	int _RowNum;

	//����� �������� 
	int _ColNum;

	//��������������� ������
	float** _Array;



public:
	DoubleMatrix(void);

	DoubleMatrix(int pRow,int pColumn);
	~DoubleMatrix(void);

	int get_RowNum(){return _RowNum;}
	int get_ColNum(){return _ColNum;}

	const int get_ColNum2(){return _ColNum;}

	 // ��������� �������� �� �������
	float getValueById(int pRow,int pColumn);
	void setValueById(int pRow,int pColumn,float pValue);

	//����������,

	bool IsSquare(){return _RowNum==_ColNum;}
		
    //������������
	bool isDiagonal();
	
	//�������
	bool iszero();
	
	//���������
	bool isidentity();
	
	//������������
	bool issymmetric();
	
	//������� �����������
	bool isUpperTriangular();
	
	//������ �����������
	bool isLowerTriangular();

	//��������� �� ���������
	//bool is();



	void Print();
};

#endif
