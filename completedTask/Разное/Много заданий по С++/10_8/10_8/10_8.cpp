// 10_8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;


double Factorial(int n) {
	return !n ? 1 : n * Factorial(n - 1);
}

int _tmain(int argc, _TCHAR* argv[])
{
	cout<<"Factorial: "<<Factorial(10)<<endl;
	cout<<"Factorial: "<<Factorial(11)<<endl;
	cout<<"Factorial: "<<Factorial(32)<<endl;
	cout<<"Factorial: "<<Factorial(22)<<endl;
	cout<<"Factorial: "<<Factorial(23)<<endl;

	system("pause");
	return 0;
}

