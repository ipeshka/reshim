// 6_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>

using namespace std;

class Vector
{
private:
	// ���������� �������
	double X;
	double Y;
	double Z;

public:
	// �����������
	Vector(double x, double y, double z)
	{
		X = x;
		Y = y;
		Z = z;
	}

	// ������ �������
	double GetLength() {
		return sqrt(X * X + Y * Y + Z * Z);
	}

	// ���������� ��������� +
	Vector operator +(const Vector &r) const {
		return Vector(X + r.X, Y + r.Y, Z + r.Z);
	}
	// ���������� ��������� -
	Vector operator -(const Vector &r) const {
		return Vector(X - X, Y - r.Y, Z - r.Z);
	}

	// ���������� ��������� *
	double operator *(const Vector &r) const {
		return (X * r.X + Y * r.Y + Z * r.Z);
	}

	// ���������� ��������
	static double Cos(Vector &l, Vector &r)  {
		return (l * r) / (l.GetLength() * r.GetLength());
	}

	// ����� ������
	void Show() {
		cout<<"X: "<<X<<"Y: "<<Y<<"Z: "<<Z<<endl;
	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	Vector v1(3, 2, -1);
	Vector v2(6, 1, -1);

	v1.Show();
	v2.Show();

	// ��������
	Vector v3 = v1 + v2;
	v3.Show();

	// ��������� ������������
	double v4 = v1 * v2;
	cout<<v4<<endl;

	system("pause");
	return 0;
}

