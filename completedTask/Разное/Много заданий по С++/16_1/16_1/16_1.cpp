// 16_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

enum State { errHeight, errWidth, errLenght, errWindow };
class Room {
private:
	double w;
	double h;
	double l;
	int window;

public:
	Room(double _w, double _h, double _l, int _window) {
		if(_w <= 0) { throw errWidth; }
		if(_h <= 0) { throw errHeight; }
		if(_l <= 0) { throw errLenght; }
		if(_window <= 0) { throw errWindow; }
		w = _w;
		h = _h;
		l = _l;
		window = _window;
	}

	double GetArea() { // �������
		return w * l;
	}
	
	double GetVolume() { // �����
		return w * h * l;
	}

	double GetWidth() {
		return w;
	}

	void SetWidth(double _w) {
		if(_w <= 0) { throw errWidth; }
		w = _w;
	}

	double GetHeight() {
		return h;
	}

	void SetHeight(double _h) {
		if(_h <= 0) { throw errHeight; }
		h = _h;
	}

	double GetLength() {
		return l;
	}

	void SetLength(double _l) {
		if(_l <= 0) { throw errLenght; }
		l = _l;
	}

	double GetWindow() {
		return w;
	}

	void SetWindow(double _w) {
		if(_w <= 0) { throw errWindow; }
		w = _w;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Room *room = new Room(10, 3, 30, 2);
	cout<<room->GetVolume();
	system("pause");
	return 0;
}

