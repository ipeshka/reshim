#include <iostream>

using namespace std;

#pragma once

#ifndef TRAIN_H
#define TRAIN_H

#define RowNum 256
#define _RouteNum 10



class Train
{

	char * _Destination;

	char * _RouteNumber;

	//��� ����������� 
	int _HH;

	//������ ����������� 
	int _MM;
public:
	Train(void);

	//������������ � ����������� 
	Train(char *pDistination, char * pRoute, int pHH, int pMM);

	~Train(void);

	/*��������� ������� ����������� */
	void set_HH(int pHour);	
	void set_MM(int pMinute);
	void set_RouteNumber(char * pNumber){_RouteNumber = pNumber;}

	int get_HH(){return _HH;}
	int get_MM(){return _MM;}

	char* get_Destination(){	return _Destination;}

	void PrintInfo();

	int  TimeToInt32();

	char * get_RouteNumber(){return _RouteNumber;}
	void Show_Time()
	{
		cout<<_HH<<" : "<<_MM;
	}

	bool operator==(Train &train);
};


#endif;