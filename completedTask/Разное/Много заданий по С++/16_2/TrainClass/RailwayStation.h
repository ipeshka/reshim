#pragma once
#include "Train.h"

#ifndef RAILWAYSTATION_H
#define RAILWAYSTATION_H

#define NumberTrain 10

class RailwayStation
{
	Train TrainArray[NumberTrain];

	int Number;
public:
	RailwayStation(void);
	~RailwayStation(void);

	void AddTrain(Train p1);

	//���� �� ������� 
	void GetInfoByIndex();

	//����� ���������� � �������, �������������� ����� ���������� � ���������� �������
	void GetInfoAfterTime(int pHH, int pMM);

	//����� ���������� � �������, �������������� � �������� ����� ����������
	void GetInfoByDestination(char *pDestination);

	//����� ���������� � ������ �� ������ � ������� �������;  
	void GetInfoByIndex(int pIndex);

	//���������� �� ������� �����������
	void SortByDerivedTime();

	void ShowArrivedByIndex(int pindex );

	void Print();
	

};

#endif;
