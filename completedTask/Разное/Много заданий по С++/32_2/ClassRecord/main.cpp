
#include <iostream>
using namespace std;

class Person{

public:
        Person(){}    
        ~Person(){}   
        
        
        string GetFamiliya(){ return persFamiliya;}
        string GetName(){ return persName;}
        string GetData(){ return persData;}
        string GetTelefon(){ return persTelefon;}

        
        void SetFamiliya(string fam){ persFamiliya=fam;}
        void SetName(string name){ persName=name;}
        void SetData(string data){ persData=data;}
        void SetTelefon(string tel){ persTelefon=tel;}

        
        void Print();
        
private:
        string persFamiliya;
        string persName;
        string persData;
        string persTelefon;
        
};


void Person::Print(){
        cout<<"Familiya: "<<persFamiliya.c_str()<<endl
                <<"Im'a: "<<persName.c_str()<<endl
                <<"Data rojdeniya: "<<persData.c_str()<<endl
                <<"Telefon: "<<persTelefon.c_str()<<endl;
}

#include <string>
#include <vector>




void PrintMenu();        
void PrintAll();         
void SortPerson();       
void FindPerson();       
void AddPerson();        
void DeletePerson();     
int GetNumber(int imin, int imax);

vector <Person> pers;

int main(){
        bool exit=false;
        while(true){
                PrintMenu();
                switch(GetNumber(1,6)){
                case 1: PrintAll(); break;
                case 2: SortPerson(); break;
                case 3: FindPerson(); break;
                case 4: AddPerson(); break;
                case 5: DeletePerson(); break;
                case 6: exit=true; break;
                }
                if(exit)
                        break;
        }
        return 0;
}


void PrintMenu(){
        cout<<endl
                <<"--------------------  men'u  --------------------"<<endl
                <<"1 - Pokazat' vse zapisi"<<endl
                <<"2 - Sortirovat' zapisi"<<endl
                <<"3 - Nayti zapis'"<<endl
                <<"4 - Dobavit' zapis'"<<endl
                <<"5 - Udalit' zapis'"<<endl
                <<"6 - Vyhod"<<endl
                <<"-------------------------------------------------" << endl;
}


void PrintAll(){
        cout<<"---------------  vse zapisi  --------------------\n";
        for(int n=0; n<pers.size(); n++){
                cout<<"\n- Zapis' N "<<n+1<<" -\n";
                pers[n].Print();
        }
        cout<<"-------------------------------------------------\n";
}


int GetNumber(int imin, int imax){
        int number=imin-1;
        while(true){
                cin >> number;
                if((number>=imin) && (number <=imax) && (cin.peek()=='\n'))
                        break;
                else{
                        cout << "Povtorite vvod (ojidaets'a chislo ot " << imin << " do " << imax << "):" << endl;
                        cin.clear();
                        while(cin.get()!='\n'){}
                }
        }
        return number;
}


int GetNumber(int imin, int imax);
void SortPerson() {
        cout<<"---------------  sortirovka  --------------------\n";
        cout<<"Sortirovka po pol'u\n";
        cout<<" 1 - Familiya\n"
            <<" 2 - Im'a\n"
            <<" 3 - Telefon\n"
            <<" 4 - Exit\n";
            int vybor=GetNumber(1,4);
        
                switch(vybor){
                case 1:{
                        int n, i, k, dl;
                        for(n=0; n<=pers.size()-1; n++) {
                                dl=pers[n].GetFamiliya().size();
                                for (i=n+1; i<pers.size(); i++)
                                        for (k=dl; k>=0; k--)
                                                if (pers[i].GetFamiliya()[k]<pers[n].GetFamiliya()[k]) {
                                                        Person buf=pers[n];
                                                        pers[n]=pers[i];
                                                        pers[i]=buf;
                                                }                       
                        cout<<"\n- Zapis' N "<<n+1<<" -\n";
                        pers[n].Print();
                        }
                           }
                        break;          
                case 2:{
                        int n, i, k, dl;
                        for(n=0; n<=pers.size()-1; n++) {
                                dl=pers[n].GetName().size();
                                for (i=n+1; i<pers.size(); i++)
                                        for (k=dl; k>=0; k--)
                                                if (pers[i].GetName()[k]<pers[n].GetName()[k]) {
                                                        Person buf=pers[n];
                                                        pers[n]=pers[i];
                                                        pers[i]=buf;
                                                }
                        cout<<"\n- Zapis' N "<<n+1<<" -\n";
                        pers[n].Print();
                        }
                           }
                        break;
        
                case 3:{
                        int n, i, k, dl;
                        for(n=0; n<=pers.size()-1; n++) {
                                dl=pers[n].GetTelefon().size();
                                for (i=n+1; i<pers.size(); i++)
                                        for (k=dl; k>=0; k--)
                                                if (pers[i].GetTelefon()[k]>pers[n].GetTelefon()[k]) {
                                                        Person buf=pers[n];
                                                        pers[n]=pers[i];
                                                        pers[i]=buf;
                                                }
                        cout<<"\n- Zapis' N "<<n+1<<" -\n";
                        pers[n].Print();
                        }
                           }
                        break;
                case 4:{
                        break;
                           }
                }
        cout<<"-------------------  gotovo  --------------------\n";
}


void FindPerson(){
        cout<<"---------------  poisk zapisi  ------------------\n";
        cout<<"Vyberite kriteriy poiska\n";
        cout<<" 1 - Familiya\n"
            <<" 2 - Im'a\n"
            <<" 3 - Data rojdeniya\n"
            <<" 4 - Telefon\n"
            <<" 5 - Exit\n";
        int vybor=GetNumber(1,5);
                switch(vybor){
                case 1:{
                        bool find=false;
                        string sbuf;
                        cout<<"Vvedite familiyu: ";
                        cin>>sbuf;
                        int n=0;
                        while(n<pers.size()){
                                if(sbuf==pers[n].GetFamiliya()) {
                                        pers[n].Print();
                                        find=true;
                                        break;}
                                else
                                        n++;
                        }
                        if (find==false) cout<<"Net takoy zapisi\n";
                        cout<<"-----------------------------------\n";
                        break;
                           }
                case 2:{
                        bool find=false;
                        string sbuf;
                        cout<<"Vvedite im'a: ";
                        cin>>sbuf;
                        int n=0;
                        while(n<pers.size()){
                                if(sbuf==pers[n].GetName()) {
                                        pers[n].Print();
                                        find=true;
                                        break;}
                                else
                                        n++;
                        }
                        if (find==false) cout<<"Net takoy zapisi\n";
                        cout<<"-----------------------------------\n";
                        break;
                           }
                
                case 3:{
                        bool find=false;
                        string sbuf;
                        cout<<"Vvedite datu rojdeniya: ";
                        cin>>sbuf;
                        int n=0;
                        while(n<pers.size()){
                                if(sbuf==pers[n].GetData()) {
                                        pers[n].Print();
                                        find=true;
                                        break;}
                                else
                                        n++;
                        }
                        if (find==false) cout<<"Net takoy zapisi\n";
                        cout<<"-----------------------------------\n";
                        break;
                                }
                case 4:{
                        bool find=false;
                        string sbuf;
                        cout<<"Vvedite telefon: ";
                        cin>>sbuf;
                        int n=0;
                        while(n<pers.size()){
                                if(sbuf==pers[n].GetTelefon()) {
                                        pers[n].Print();
                                        find=true;
                                        break;}
                                else
                                        n++;
                        }
                        if (find==false) cout<<"Net takoy zapisi";
                        cout<<"-----------------------------------\n";
                        break;
                                }
                case 6:{
                        break;
                           }
                }
        cout<<"-------------------  gotovo  --------------------\n";

}


void AddPerson(){
        string sbuf;
        Person bufPers;
        cout<<endl
                <<"----------------  novaya zapis' ----------------\n"
                <<"Familiya: ";
        cin>>sbuf;
        bufPers.SetFamiliya(sbuf);

        cout<<endl
                <<"Im'a: ";
        cin>>sbuf;
        bufPers.SetName(sbuf);

        cout<<endl
                <<"Data rojdeniya: ";
        cin>>sbuf;
        bufPers.SetData(sbuf);

        cout<<endl
                <<"Telefon:";
        cin>>sbuf;
        bufPers.SetTelefon(sbuf);

        pers.push_back(bufPers);
        cout<<"-------------------  gotovo  --------------------\n";
}


void DeletePerson(){
        string sbuf;
        cout<<"----------------  udalenie zapisi ---------------\n";
        cout<<"Vvedite familiyu: ";
        cin>>sbuf;

        int n=0;
        bool find=false;
        while(n<pers.size()){
                if(sbuf==pers[n].GetFamiliya()){
                        pers[n].Print();
                        find=true;
                        pers.erase(pers.begin()+n);
                        cout<<endl;
                }
                else            
                        n++;
        }
        if (find==false) cout<<"Net takoy zapisi\n";    
        cout<<"-------------------  gotovo  --------------------\n";
}

