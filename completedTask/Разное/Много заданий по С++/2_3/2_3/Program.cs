﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_3 {
    class Program {
        static void Main () {
            const double e = 0.001, dx = 0.1, a = 0, b = 1;
            const string funcname = "e^-x";
            int iters;
            Console.WriteLine("Рассчет функции: {0}\nС точностью {1} и шагом {2}\nВ диапазоне [{3},{4}]\n\n", funcname, e, dx, a, b);
            Console.WriteLine("Приближенное значение\t\tТочное значение\t\t\tСлагаемых");
            for(double x = a ; x < b ; x += dx)
                Console.WriteLine("{0,-25}\t{1,-25}\t{2}", F(x, e, out iters), Math.Pow(Math.E, -x), iters);
            Console.WriteLine("\n\nПрограмма завершена. Для выхода нажмите любую клавишу");
            Console.ReadKey();
        }

        // функция вычисления e^-x
        private static double F (double x, double eps, out int i) {
            double result = 1, lastresult = double.MinValue;
            // обнуляем
            i = 0;
            do {
                lastresult = result; // запоминаем предыдущее значение
                i++;
                result += Math.Pow(-1, i) * Math.Pow(x, i) / fact(i); // записываем форму ряда
            } while(Math.Abs(result - lastresult) >= eps); // пока не достигли точности

            return result;
        }

        // фунция возвращает факториал числа
        private static double fact (double i) {
            double f = 1;
            for(double j = 1 ; j <= i ; j++) f *= j;
            return f;
        }
    }
}
