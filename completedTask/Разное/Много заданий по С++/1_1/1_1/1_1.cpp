// 1_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "iostream"
using namespace std;

class Counter
{
private:
	int upLimit; // ������� ������� ��������� ��������
	int downLimit;    // ������ ������� ��������� ��������
	int counterValue; // ��������� �������� ��������

public:
	Counter() {
		counterValue = downLimit;
	}


	Counter(int _upLimit, int _downLimit, int _counterValue) {
		upLimit = _upLimit;
		downLimit = _downLimit;
		counterValue = _counterValue;
	}

	void SetDownLimit(int value) {
		downLimit = value;
	}

	int GetDownLimit() {
		return downLimit;
	}

	void SetUpLimit(int value) {
		upLimit = value;
	}

	int GetUpLimit() {
		return upLimit;
	}

	int CounterValue() {
		return counterValue;
	}


	// �����, ������� ��������� ����������� �������� �������� �� 1
	void Enlarge()
	{
		counterValue++;
		if (counterValue > upLimit)
			counterValue = downLimit;
	}

	// �����, ������� ��������� ��������� �������� �������� �� 1
	void Reduce()
	{
		counterValue--;
		if (counterValue < downLimit)
			counterValue = upLimit;
	}
};

// � ����� ������ ��������, �� ����� ����������� ���������!
// � �������� ������������ ���������� � �� � ��������� ����������!
int _tmain(int argc, _TCHAR* argv[])
{
	Counter *counter = new Counter(10, -10, 0);
	cout<<counter->CounterValue()<<endl;
	counter->Enlarge();
	counter->Enlarge();
	counter->Enlarge();
	cout<<counter->CounterValue()<<endl;
	system("pause");
	return 0;
}

