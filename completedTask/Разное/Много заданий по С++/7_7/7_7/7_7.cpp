// 6_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;


struct TDate {
	int Day;
	int Month;
	int Year;
	TDate(int day, int month, int year){
		Day = day;
		Month = month;
		Year = year;
	}
}typedef Date;

// ���������� �� ���?
bool LeapYear(Date date) {
	if (date.Year % 4 == 0 && date.Year % 100 != 0 && date.Year % 400 == 0) return true;
	else return false;
}
// ���������� ���-�� ���� ��� ������
int DaysInMonth(Date date) {
     if (date.Month == 1 || date.Month == 3 || date.Month == 5 || date.Month == 7 || date.Month == 8 || date.Month == 10 || date.Month == 12) return 31;
     else if (date.Month == 4 || date.Month == 6 || date.Month == 9 || date.Month == 11) return 30;
     else if (date.Month == 2) {
          if(LeapYear(date)) return 29;
          else return 28;
	 }	
}

// ������ �� ����
int CheckDate(Date date)
{
     if (date.Day > DaysInMonth(date) || date.Day < 0) return 2;
     if (date.Month > 12 || date.Month < 0) return 1;
	 return 0;
}

void Show(Date date) {
	cout<<"Day: "<<date.Day<<endl;
	cout<<"Month: "<<date.Month<<endl;
	cout<<"Year: "<<date.Year<<endl;
}

int _tmain(int argc, _TCHAR* argv[])
{
	Date d1(1, 12, 1990);
	Date d2(15, 1, 2010);
	Date d3(3, 3, 1998);
	Date d4(1, 6, 2013);
	Date d5(6, 1, 1990);

	cout<<"1 - "<<CheckDate(d1)<<endl;
	cout<<"2 - "<<CheckDate(d2)<<endl;
	cout<<"3 - "<<CheckDate(d3)<<endl;
	cout<<"4 - "<<CheckDate(d4)<<endl;
	cout<<"5 - "<<CheckDate(d5)<<endl;

	system("pause");
	return 0;
}

