// 32_5.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <math.h>
#include <iostream>
using namespace std;

double PowerN(int x, int n) {
	if(n < 0) { return 1/PowerN(x, -n); }
	if (n == 0) { return 1; }
	if (n == 2) { 
		return pow((double)x, 2); 
	} 
	else if (n % 2 == 0) {
		return pow((double)PowerN(x, n/2), 2);
	}
	if(n == 1) { return x; }
	else if(n % 2 != 0) {
		return x * PowerN(x, n-1);
	}
}


int _tmain(int argc, _TCHAR* argv[])
{
	cout<<PowerN(3, -2)<<endl;
	cout<<PowerN(3, 1)<<endl;
	cout<<PowerN(3, 2)<<endl;
	cout<<PowerN(3, 3)<<endl;
	cout<<PowerN(3, 4)<<endl;
	system("pause");
	return 0;
}

