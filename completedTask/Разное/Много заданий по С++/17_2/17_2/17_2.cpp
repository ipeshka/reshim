// 17_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <cmath>

using namespace std;

class NonlinearEquation {
private:
	double a;

	// ������� �������
	double f(double x) {
		return  a * x - cos(x);
	}
public:
	// �����������
	NonlinearEquation(double _a) {
		a = _a;
	}

	// ������� ���������
	// a, b - �������, e - ��������
	double Resolve(double a, double b, double e) {
		double x(0);
		do
		{
			x = (a + b) / 2;
			if (f(a) * f(x) < 0) {
				b = x;
			}
			else if (f(x) * f(b) < 0) {
				a = x;
			}
			else if ((f(x) * f(b) == 0) && (f(x) * f(a) == 0)) {
				return x;
			}
			else {
				cout << "� ��������� ��������� ������ ���, ��� ������ ������." << endl;
				break;
			}
		}
		while (fabs(a - b) > e); // ���� �� �������� ��������
		return x;
	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	// ������������
	setlocale( LC_ALL,"Russian" );
	long double a(0), b(0), e(0);
	cout << "����� ����������� �������:"<<endl;
	cout << "a = "; cin >> a;
	cout << "b = "; cin >> b;
	cout << "��������: "; cin >> e;

	NonlinearEquation Equation(4);
	cout<<Equation.Resolve(a, b, e)<<endl;
	system("payse");
	return 0;
}

