// 16_7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <iostream>
int C[30][30];
int Combin2(int n,int k) {
    if (C[n][k])
        return C[n][k];
    else
        return n==k || !k ? 1 : Combin2(n-1,k) + Combin2(n-1,k-1);
}

int main() {
    int n,k;
    scanf("%d%d",&n,&k);
    printf("%d\n",Combin2(n,k));
	system("pause");
    return 0;
}