
#include <iostream>

using namespace std;

#ifndef DIMMENSHIONALARRAY_H
#define  DIMMENSHIONALARRAY_H 

class DimmenshionalArray
{

	///������ ������� (�������)
	int _Size;
	///��������������� ������
	int* _Array;

public:

	DimmenshionalArray(){}

	//�����������
	~DimmenshionalArray(){}

	DimmenshionalArray(int pSize)
	{
		_Size = pSize;
		_Array = new int [pSize];

		for(int i=0; i<_Size;i++)
		{
			*(_Array+i)=0;
		}
	}

	///��������� ��������� �� ������� 
	int get_Array(int pindex)
	{
		int lresult = 0;
		// ���� ����� �������� ��������
		if(pindex > 0 && pindex < _Size )
		{
			lresult = _Array[pindex];
		}
		
		return lresult;
	}

	//��������� ����������� �������
	const int get_Size(){return _Size;}

	// ��������� �������� �� �������
	const int getValueByIndex(int pIndex);

	//�������� �������� 
	DimmenshionalArray operator+( DimmenshionalArray &pValue);
	//��������� �� ������ 
	void  multiplicationToScalar(int pValue);
	
	//������� �� ������ 
	void  divideToScalar(int pValue);

	
	/// ��������� �������� �� ������� �������� 
	/// ����� � ������� , �������� 
    void SetValueByIndex(int index, int Value);

    /// ����� ����������� �� �����
    void Print();

};


#endif