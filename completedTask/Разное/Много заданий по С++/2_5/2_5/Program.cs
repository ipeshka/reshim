﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_5 {
    class Program {
        // задание № 5. Одномерные массивы
        static void Main (string[] args) {
            // объявляем переменные
            int N = 10; // размернось массива
            int[] mass = new int[N]; // Массив типа int
            Console.WriteLine("Запонлните массив");
            for(int i = 0 ; i < N ; i++) {
                Console.Write(string.Format("Введите номер массива {0}: ", i + 1));
                mass[i] = Int32.Parse(Console.ReadLine());
            }
            PrintMass(mass);
            Console.Write(string.Format("\nСумма положительных элементов {0}\n", SummaPositive(mass)));
            Console.Write(string.Format("\nПроизведение между 2-му минимальный и максимальными элементами{0}\n", MulMaxMinElement(mass)));
            // сортируем
            Console.WriteLine("\nПреобразованный массив\n");
            Array.Sort(mass);
            mass = mass.Reverse().ToArray();
            PrintMass(mass);
            Console.ReadKey();
        }

        static int SummaPositive (int[] mass) {
            int sum = 0;
            for(int i = 0 ; i < mass.Length ; i++) {
                if(mass[i] > 0)
                    sum += mass[i];
            }
            return sum;
        }

        static void PrintMass (int[] mass) {
            for(int i = 0 ; i < mass.Length ; i++) {
                Console.Write(mass[i] + " ");
            }
        }

        // функция нахождения произведения между первым и последним положительным элементом
        static int MulMaxMinElement (int[] mass) {
            int mul = 1;
            int indexMin = IndexMinPos(mass); // индекс первого положительного элемента
            int indexMax = IndexMaxPos(mass); // индекс первого положительного элемента
            // цикл суммы от первого до последнего положительного элемента
            if(indexMin < indexMax) {
                for(int i = indexMin ; i <= indexMax ; i++) {
                    mul *= mass[i];
                }
            } else {
                for(int i = indexMin ; i <= indexMax ; i++) {
                    mul *= mass[i];
                }
            }

            return mul;
        }

        // функция возвращает индекс минимального по модулю
        static int IndexMinPos (int[] mass) {
            int minIndex = 0;
            int minElement = Math.Abs(mass[0]);
            for(int i = 0 ; i < mass.Length ; i++) {
                if(Math.Abs(mass[i]) < minElement) {
                    minIndex = i;
                    minElement = mass[i];
                }
            }

            return minIndex;
        }

        // функция возвращает индекс максимального по модулю
        static int IndexMaxPos (int[] mass) {
            int maxIndex = 0;
            int maxElement = Math.Abs(mass[0]);
            for(int i = 0 ; i < mass.Length ; i++) {
                if(Math.Abs(mass[i]) > maxElement) {
                    maxIndex = i;
                    maxElement = mass[i];
                }
            }

            return maxIndex;
        }
    }
}
