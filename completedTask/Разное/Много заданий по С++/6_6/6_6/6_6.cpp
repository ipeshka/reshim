// 6_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <algorithm>
#include <string>

using namespace std;

string DecToHex(int n)
{
	string res = "";
	while (n >= 16)
	{
		int temp = n % 16;
		n = n / 16;
		switch (temp)
		{
		case 10: res = 'A'+res;
			break;
		case 11: res = 'B'+res;
			break;
		case 12: res = 'C'+res;
			break;
		case 13: res = 'D'+res;
			break;
		case 14: res = 'E'+res;
			break;
		case 15: res = 'F'+res;
			break;
		default: res = to_string(temp)+res;
		}
	}
	switch (n)
	{
	case 10: res = 'A'+res;
		break;
	case 11: res = 'B'+res;
		break;
	case 12: res = 'C'+res;
		break;
	case 13: res = 'D'+res;
		break;
	case 14: res = 'E'+res;
		break;
	case 15: res = 'F'+res;
		break;
	default: res = to_string(n)+res;
	}
	return res;
}

int main() {
	const int countValue = 5;
	int val;

	for(int i = 1; i <= countValue; ++i) {
		cout << "[" << i << "-" << countValue << "]: ";
		cin >> val;
		cout << val << "(dec) -> " << DecToHex(val) << "(hex)\n" << std::endl;
	}

	system("pause");
	return 0;
}