#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
using namespace std;

const int N = 256;

int _tmain(int argc, _TCHAR* argv[])
{
	char buff[N];
	setlocale(LC_ALL, "rus"); // ���������� ����������� ���������
	char text[N*100]; // ��� ���������� ������
	text[0] = '\0';


	ifstream fin("D:/input.txt"); // ������� ���� ��� ������
	if (!fin.is_open()) { // ���� ���� �� ������
		cout << "���� �� ����� ���� ������!\n"; // �������� �� ����
		return - 1;
	}

	while(fin) {
		fin.get(buff, N); // ������� ������ �� �����
		strcat(text, buff); // �������� ������ 
	}
	fin.close(); // ��������� ����

	
	string s1 = text;
	string s2;
	// ������� ������ �������
	unique_copy(s1.begin(), s1.end(), back_inserter(s2),
                     [](char c1, char c2){ return c1 == ' ' && c2 == ' '; });

	// ������ ����� ����
	ofstream fout("D:/input.txt", ios_base::trunc); // ��������� ���� ��� �������� ����������� ����� ���� �� ���������
	fout<<s2;
	fout.close();

	
	system("pause");
	return 0;
}

