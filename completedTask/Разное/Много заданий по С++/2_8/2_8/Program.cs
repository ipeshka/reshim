﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_8 {

    public class StringArray {
        private string[] values;
        private int highIndex;
        public StringArray (int highIndex) {
            if(highIndex < 0)
                throw new ArgumentException();
            values = new string[highIndex];
            this.highIndex = highIndex;
        }

        public string this[int i] {
            get {
                if(highIndex < i)
                    throw new ArgumentException();

                return values[i];
            }
            set {
                if(highIndex < i)
                    throw new ArgumentException();

                values[i] = value;
            }
        }

        // поэлементное сцепление двух массивов
        public static StringArray operator + (StringArray a, StringArray b) {
            StringArray arrStr;
            if(a.values.Length > b.values.Length) {
                arrStr = new StringArray(a.values.Length);
                for(int i = 0 ; i < b.values.Length ; i++) {
                    arrStr[i] = a[i] + b[i];
                }
            } else {
                arrStr = new StringArray(b.values.Length);
                for(int i = 0 ; i < a.values.Length ; i++) {
                    arrStr[i] = a[i] + b[i];
                }
            }
            return arrStr;
        }

        public void Show () {
            foreach(var str in values) {
                Console.WriteLine(str);
            }
        }

        public void Show (int index) {
            if(highIndex < index)
                throw new ArgumentException();

            Console.WriteLine(values[index]);
        }
    }

    class Program {
        static void Main (string[] args) {

            StringArray strArr1 = new StringArray(6);
            strArr1[1] = "asdasdas";
            strArr1[2] = "sdfwe";
            strArr1[3] = "dff";
            strArr1[4] = "werewr";
            strArr1.Show();

            StringArray strArr2 = new StringArray(7);
            strArr2[0] = "345t";
            strArr2[1] = "345";
            strArr2[2] = "5676";
            strArr2[3] = "8978";
            strArr2.Show();

            StringArray strArr3 = strArr1 + strArr2;
            strArr3.Show();
            Console.ReadLine();
        }
    }
}
