#include "stdafx.h"
#include <stdint.h>
#include <iostream>
using namespace std;

typedef struct DATE {
	uint8_t date;
	uint8_t month;
	uint8_t year;
} t_date;

bool LeapYear(DATE date) {
	if(date.year % 4 == 0 && date.year % 100 != 0 && date.year % 400 == 0) return true;
	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{
	DATE date;
	date.date = 1;
	date.month = 3;
	date.year = 2007;

	cout<<((LeapYear(date)==1)?"true":"false")<<endl;

	date.year = 1890;
	cout<<((LeapYear(date)==1)?"true":"false")<<endl;

	date.year = 1980;
	cout<<((LeapYear(date)==1)?"true":"false")<<endl;

	date.year = 2000;
	cout<<((LeapYear(date)==1)?"true":"false")<<endl;

	date.year = 1996;
	cout<<((LeapYear(date)==1)?"true":"false")<<endl;

	system("pause");
	return 0;
}

