// 20_8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;

double Fact2Recur(int n) {
	if(n==1) { return 1; }
	else if(n==2) { return 2; }
	else { return n*Fact2Recur(n-2); }
}

void main () {
	int n;
	cout << " Enter number " << " " ;
	cin >> n;
	cout << "Fact2(" << n << ")=" << Fact2Recur(n) << endl;
	system("pause");
}
