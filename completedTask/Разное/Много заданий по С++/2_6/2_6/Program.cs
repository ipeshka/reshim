﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_6 {
    class Program {
        const int N = 5; // размернось массива
        const int M = 6; // размернось массива

        static void Main (string[] args) {
            // объявляем переменные
            int[,] mass = new int[N, M]; // Массив типа int
            Console.WriteLine("Запонлните массив");
            for(int i = 0 ; i < N ; i++) {
                for(int j = 0 ; j < M ; j++) {
                    Console.Write(string.Format("Введите номер массива {0}{1}: ", i + 1, j + 1));
                    mass[i, j] = Int32.Parse(Console.ReadLine());
                }
            }
            PrintMass(mass);

            int countColumn = M;
            // определяем колво столбцов не содержащих ни одного нулевого элемента

            for(int j = 0 ; j < M ; j++) {
                for(int i = 0 ; i < N ; i++) {
                    if(mass[i, j] == 0) {
                        countColumn--;
                        break;
                    }
                }
            }
            Console.WriteLine("Количество столбцов не имеющий нулей: {0}", countColumn);

            for(int y = 0 ; y < N; y++) {
                for(int i = 0 ; i < N - 1; i++) {
                    if(Summa(mass, i) > Summa(mass, i + 1)) {
                        for(int j = 0 ; j < M; j++) {
                            int tmp = mass[i, j];
                            mass[i, j] = mass[i + 1, j];
                            mass[i + 1, j] = tmp;
                        }
                    }
                }
            }

            PrintMass(mass);


            Console.ReadKey();
        }

        // вывод массива
        static int Summa (int[,] mass, int i) {
            int summa = 0;
            for(int j = 0 ; j < mass.GetLength(1) ; j++) {
                if(mass[i, j] > 0 && mass[i, j] % 2 == 0)
                    summa += mass[i, j];
            }

            return summa;
        }

        // вывод массива
        static void PrintMass (int[,] mass) {
            for(int i = 0 ; i < mass.GetLength(0) ; i++) {
                for(int j = 0 ; j < mass.GetLength(1) ; j++) {
                    Console.Write(string.Format("{0} ", mass[i, j]));
                }
                Console.WriteLine("");
            }
        }
    }
}
