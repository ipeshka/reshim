#include "Sklad.h"

Sklad::Sklad(void)
{
	CurrNum = 0;
}

Sklad::~Sklad(void)
{
}


void Sklad::AddToShop(Product &pval)
{
	if(CurrNum >= ProdNum)
	{
		CurrNum--;
	}
	productItem[CurrNum] = pval;

	CurrNum++;
}

void Sklad::Print()
{
	for(int i=0;i<CurrNum;i++)
	{
		cout<<"~*~*~*~*~*~*~*~*~*~*~*~*~*~*~*~"<<endl;
		ShowByIndex(i);					
	}
}

void Sklad::ShowByIndex(int index)
{
	if(index <CurrNum)
	{
		productItem[index].Print();
	}
}

void Sklad::ShowInfoByName(char *pName)
{
	//���� � ������� ���� �������� 
		if(CurrNum>0)
		{
			for(int i=0;i<CurrNum;i++)
			{
				int len = strcmp(productItem[i].get_Name(), pName);
				//���� ������ � ������ ����������� ����� 
				if(len == 0  )
				{
					ShowByIndex(i);
				}				
			}
		}
}


// ���������� �� ����� ������ 
void Sklad::SortByShopName()
{
	Product temp;                              
  
    for (int i=1;  i<CurrNum  ;  i++)
    {            
		for (int j=0;  j<CurrNum-i;  j++)
		{ 
			int len =  strcmp( productItem[j].get_Shop(),productItem[j+1].get_Shop());

			if(len > 0)
			{
				 temp = productItem[j];
				productItem[j] = productItem[j+1];
				productItem[j+1] = temp;
			}
		}
    }
}

void Sklad::SortByProdName()
{
	Product temp;                              
  
    for (int i=1;  i<CurrNum  ;  i++)
    {            
		for (int j=0;  j<CurrNum-i;  j++)
		{ 
			int len =  strcmp( productItem[j].get_Name(),productItem[j+1].get_Name());

			if(len > 0)
			{
				 temp = productItem[j];
				productItem[j] = productItem[j+1];
				productItem[j+1] = temp;
			}
		}
    }
}

void Sklad::SortByCost()
{
	Product temp;                              
  
    for (int i=1;  i<CurrNum  ;  i++)
    {            
		for (int j=0;  j<CurrNum-i;  j++)
		{ 
			
			if(productItem[j].get_Cost() > productItem[j+1].get_Cost())
			{
				 temp = productItem[j];
				productItem[j] = productItem[j+1];
				productItem[j+1] = temp;
			}
		}
    }

}