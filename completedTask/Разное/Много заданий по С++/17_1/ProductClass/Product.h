#pragma once

#include <iostream>

using namespace std;


#ifndef PRODUCT_H
#define PRODUCT_H
class Product
{

	//�������� �������� 
	char * _Name ;

	//�������� ������ 
	char * _Shop ;

	//���������
	double Cost;
public:
	Product(void);

	Product(char * pName ,char *pShop,double pQty);

	~Product(void);

	char * get_Name(){return _Name;}
	void set_Name(char * pName){_Name = pName;}

	char * get_Shop(){return _Shop;}
	void set_Shop(char * pval){_Shop = pval;}

	double get_Cost(){return Cost;}
	void set_Cost(double pval){Cost = pval;}

	void Print();

	double operator+(Product &pval);

	void AddToShop(Product &pval); 
};


#endif