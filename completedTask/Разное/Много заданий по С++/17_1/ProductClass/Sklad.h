#pragma once

#include "Product.h"

#define ProdNum 10

class Sklad
{

	Product productItem[ProdNum];

	int CurrNum;	
	

public:
	Sklad(void);
	~Sklad(void);

	void GetInfoByNum(int index);

	void ShowInfoByName(char *Name);

	void SortByShopName();

	void SortByProdName();

	void SortByCost();

	void Print();

	void ShowByIndex(int index);

	void AddToShop(Product &pval);

};
