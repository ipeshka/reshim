// 1_6.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
using namespace std;
// ���������� ���������� ���������� ������ ������ ��������
int count_same_chars(char const* str) {
	int count = 1;
	char ch = *str;

	while (*++str != '\0' && *str == ch) {
		++count;
	}

	return count;
}
// ������� ������� ������
void compress_str(char* str) {
	int count;
	char* p;

	while (*str != '\0') {
		count = count_same_chars(str);

		// ���� ����� �������� ������ 4
		if (count >= 4) {
			p = str;
			*++p = '{';
			++p;
			p += sprintf(p, "%u", (unsigned int)count);
			*p = '}';
			strcpy(++p, str + count);
			str = p;
		} else {
			str += count;
		}
	}
}


int main() {
	char str1[] = "bbbccccce";
	char str2[] = "bbbbcce";
	char str3[] = "bbbccceee";
	char str4[] = "aaaaaaaabbbbcccc";
	char str5[] = "aabbbcccc";

	compress_str(str1);
	compress_str(str2);
	compress_str(str3);
	compress_str(str4);
	compress_str(str5);

	cout<<str1<<endl;
	cout<<str2<<endl;
	cout<<str3<<endl;
	cout<<str4<<endl;
	cout<<str5<<endl;

	system("pause");

	return 0;
}
