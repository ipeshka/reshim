// 32_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
using namespace std;
class Processor
{
private:
	string _brand;
	int _casheSize;
	int _clockFrequency;
	double _cost;

public:
	Processor() {
		_brand = "None";
		_casheSize = 1;
		_clockFrequency = 1;
		_cost = 1;
	}

	Processor(Processor &p) {
		_brand = p._brand;
		_casheSize = p._casheSize;
		_clockFrequency = p._clockFrequency;
		_cost = p._cost;
	}

	Processor(string brand, int clockFrequency, int casheSize, double cost)
	{
		if(casheSize == 0) throw exception("����� ���� �� ����� ��������� ����");
		if(clockFrequency == 0) throw exception("������� �� ����� ��������� ����");
		_brand = brand;
		_casheSize = clockFrequency;
		_clockFrequency = casheSize;
		_cost = cost;
	}

	string GetBrand()
	{
		return _brand;
	}

	int GetCasheSize()
	{
		return _casheSize;
	}

	int GetClockFrequency()
	{
		return _clockFrequency;
	}

	int GetCost()
	{
		return _cost;
	}

};

class Motherboard
{
private: int ram;
		 Processor processor;
public:
	Motherboard(Processor _processor, int _ram)
	{
		processor = _processor;
		ram = _ram;
	}

	int GeRam()
	{
		return ram; 
	}

	void Show() {
		cout<<"Ram: "<<ram<<endl;
		cout<<"Processor brand: "<<processor.GetBrand()<<endl;
		cout<<"Processor casheSize: "<<processor.GetCasheSize()<<endl;
		cout<<"Processor clockFrequency: "<<processor.GetClockFrequency()<<endl;
		cout<<"Processor cost: "<<processor.GetCost()<<endl;

	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	Processor p("Intel", 1000, 16, 124);
	Motherboard m(p, 1024);
	m.Show();
	system("pause");
	return 0;
}

