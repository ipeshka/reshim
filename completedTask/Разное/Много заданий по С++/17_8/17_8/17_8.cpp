
#include "stdafx.h"
#include <iostream>
using namespace std;

long int Combin1(int n, int k) {
	int comb = 0;
	if (k==0 || k==n) { return 1; }
	else { comb = Combin1(n-1, k) + Combin1(n-1, k-1); }
	return comb;
}

int main () {
	cout << "Combin1 =" << Combin1(10, 7) << endl;
	cout << "Combin1 =" << Combin1(3, 2) << endl;
	cout << "Combin1 =" << Combin1(10, 5) << endl;
	cout << "Combin1 =" << Combin1(11, 8) << endl;
	cout << "Combin1 =" << Combin1(15, 7) << endl;
	system("pause");
	return 0;
}
