// 20_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

class ColorPoint {
private:
	unsigned int R;
	unsigned int G;
	unsigned int B;
public:
	ColorPoint() {
		R = 0;
		G = 0;
		B = 0;
	}

	ColorPoint(unsigned int r, unsigned int g, unsigned int b) {
		if(R > 255 || G > 255 || B > 255 || R < 0 || G < 0 || B < 0) {
			throw new std::string("Ne vernie znaceniaj!");
		}


		R = r;
		G = g;
		B = b;
	}

	void SetR(unsigned int r) {
		if(r > 255 ||  r < 0 ) {
			throw new std::string("Ne vernie znaceniaj!");
		}
		R = r;
	}

	unsigned char GetR() {
		return R;
	}

	void SetG(unsigned int g) {
		if(g > 255 ||  g < 0 ) {
			throw new std::string("Ne vernie znaceniaj!");
		}
		G = g;
	}

	unsigned char GetG() {
		return G;
	}

	void SetB(unsigned int b) {
		if(b > 255 ||  b < 0 ) {
			throw new std::string("Ne vernie znaceniaj!");
		}
		B = b;
	}

	unsigned char GetB() {
		return B;
	}

	void Display() {
		cout<<"R:"<<R<<", G:"<<G<<", B:"<<B<<endl;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	ColorPoint *point = new ColorPoint();
	point->Display();

	point->SetB(100);
	point->Display();

	system("pause");
	return 0;
}

