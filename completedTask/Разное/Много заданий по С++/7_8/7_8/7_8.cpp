// 7_8.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using namespace std;
const int N = 8;

int Max(int* a, int n,int m)
{
	if(a[n] >= m) 
		m=a[n];

	if(n < 1) 
		return m;
	return Max(a, n-1, m);
}

int main()
{
	int a[N];
	for(int i=0; i<N; i++){
		cout<<"a["<<i<<"]=";
		cin>>a[i];
	}
	int max=a[0];
	cout << Max(a, N-1, max) << endl;

	system("pause");
	return 0;
}

