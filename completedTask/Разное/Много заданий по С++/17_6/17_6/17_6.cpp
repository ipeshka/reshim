#include "stdafx.h"
#include <iostream>
#include <string>
using namespace std;

void TrimLeftC(std::string& s, char ch)
{
    size_t pos;
    if (s.size() && (pos = s.find_first_not_of(ch)) != std::string::npos)
        s = s.substr(pos);
}

int _tmain(int argc, _TCHAR* argv[])
{
	string str1 = "asdasdasd";
	string str2 = "efsdefef";
	string str3 = "yuihjkhjk";
	string str4 = "wertwer";
	string str5 = "sdfsdfsdf";

	cout<<"input:"<<endl;
	cout<<str1<<endl;
	cout<<str2<<endl;
	cout<<str3<<endl;
	cout<<str4<<endl;
	cout<<str5<<endl;

	TrimLeftC(str1, 'a');
	TrimLeftC(str2, 'e');
	TrimLeftC(str3, 'a');
	TrimLeftC(str4, 'w');
	TrimLeftC(str5, 'd');


	cout<<"Output:"<<endl;
	cout<<str1<<endl;
	cout<<str2<<endl;
	cout<<str3<<endl;
	cout<<str4<<endl;
	cout<<str5<<endl;

	system("pause");
	return 0;
}

