#pragma once
#include <iostream>
#include <math.h>


using namespace std;


#ifndef FLOATMATRIX_H
#define FLOATMATRIX_H

class FloatMatrix
{

	//����� ����� 
	int _RowNum;

	//����� �������� 
	int _ColNum;

	//��������������� ������
	float** _Array;



public:
	FloatMatrix(void);
/*
	//����������� ����������� 
	FloatMatrix( FloatMatrix &val)
	{
		_Array = new float*[val.get_ColNum() ];
		try
		{
			for(int i=0; i <val.get_RowNum();i++)
			{
			   _Array[i] = new float[val.get_ColNum()];
			}	
		
			for(int i=0; i <val.get_ColNum();i++)
			{
				for(int j=0; j <val.get_RowNum();j++)
				{
				
					//setValueById(i,j,val.getValueById(i,j));
					_Array[i][j] = val.getValueById(i,j);
				}
			}

		}
		catch(bad_alloc xa )
		{
			cout<<"Exeoption"<<endl;
			exit(EXIT_FAILURE);
		}

	}
*/
	FloatMatrix(int pRow,int pColumn);
	~FloatMatrix(void);

	int get_RowNum(){return _RowNum;}
	int get_ColNum(){return _ColNum;}

	const int get_ColNum2(){return _ColNum;}

	 // ��������� �������� �� �������
	float getValueById(int pRow,int pColumn);
	void setValueById(int pRow,int pColumn,float pValue);

	//��������� ������
	FloatMatrix * MultileArray( FloatMatrix &pValue);
	FloatMatrix * MultileScalar( float pValue);

	//������� 
	FloatMatrix * DivideArray( FloatMatrix &pValue);
	FloatMatrix * DivideScalar( float pValue);

	//���������� � ������� 
	FloatMatrix * Pow( float pValue);


	//���������, ������� (��� �� ������ �������, ��� � �� �����); 
	FloatMatrix operator*(const FloatMatrix &pValue);
	FloatMatrix operator/(const FloatMatrix &pValue);

	void Print();
};

#endif
