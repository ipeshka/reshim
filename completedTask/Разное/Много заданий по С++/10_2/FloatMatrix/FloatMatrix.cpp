#include "FloatMatrix.h"

FloatMatrix::FloatMatrix(void)
{
}

FloatMatrix::~FloatMatrix(void)
{
	for(int i=0; i <_RowNum;i++)
	{
		delete [] _Array[i];
	}
}

FloatMatrix::FloatMatrix(int pRow,int pColumn)
{
	_Array = new float*[pColumn];

	for(int i=0; i <pRow;i++)
	{
		_Array[i] = new float[pRow];
	}

	for(int i=0;i<pRow ; i++)
	{
		for(int j=0; j<pColumn ; j++)
		{			
			_Array[i][j] = 0;		
		}	
	}

	_RowNum = pRow;
	_ColNum = pColumn;
}
//���������� �� �������
float FloatMatrix::getValueById(int pRow,int pColumn)
{
	/*������ ����� ����� ���� ����������� ��� ���������� �������� �������*/
	/*�� ����� ��� �������������� �������������� ��������*/
	float lres = -1;
	if(pRow>=0 && pRow<=_RowNum && pColumn>=0 && pColumn<_ColNum)
	{
		lres = _Array[pRow][pColumn];
	}
	return lres;
}

// ��������� �������� �� ������� 
void  FloatMatrix::setValueById(int pRow,int pColumn,float pValue)
{
	if(pRow>=0 && pRow<=_RowNum && pColumn>=0 && pColumn<_ColNum)
	{
		_Array[pRow][pColumn] = pValue;
	}
}

// ��������� ������
FloatMatrix *   FloatMatrix::MultileArray(FloatMatrix &pValue)
{	
	int lrow = pValue.get_RowNum();
	int lcol = pValue.get_ColNum();

	FloatMatrix* lresult = new  FloatMatrix(lrow,lcol);

	float lvalue = 0;
	for(int i=0;i<lrow; i++)
	{
		for(int j=0; j<lcol; j++)
		{
			for(int m=0; m<lcol; m++)
			{
				//lresult.setValueById(i,j)+=
				lvalue+=getValueById(i,m) * pValue.getValueById(m,j);
				lresult->setValueById(i,j,lvalue);				
			}			
		}
	 
	}
		
	return lresult;
}

void FloatMatrix::Print()
{
	for( int i =0;i<_RowNum ; i++)
	{
		for( int j =0; j<_ColNum ; j++)
		{
			cout.width(5);
			cout<<_Array[i][j]<<" ";
		}
		cout<<"\n";
	}

}

FloatMatrix * FloatMatrix::Pow( float pValue)
{
	int lrow = _RowNum;
	int lcol = _ColNum;

	FloatMatrix* lresult = new  FloatMatrix(lrow,lcol);
	
	for( int i =0;i<lrow ; i++)
	{
		for( int j =0; j<lcol ; j++)
		{
			lresult->setValueById(i,j,pow( this->getValueById(i,j) , pValue));			
		}
		
	}

	return lresult;
}


FloatMatrix * FloatMatrix::MultileScalar( float pValue)
{
	int lrow = _RowNum;
	int lcol = _ColNum;

	FloatMatrix* lresult = new  FloatMatrix(lrow,lcol);
	
	for( int i =0;i<lrow ; i++)
	{
		for( int j =0; j<lcol ; j++)
		{
			lresult->setValueById(i,j, this->getValueById(i,j) * pValue);			
		}
		
	}

	return lresult;
}

	
FloatMatrix * FloatMatrix::DivideScalar( float pValue)
{
	int lrow = _RowNum;
	int lcol = _ColNum;

	if(pValue==0)
	{
		pValue =1;
	}
	pValue = 1/pValue;

	FloatMatrix* lresult = new  FloatMatrix(lrow,lcol);
	
	for( int i =0;i<lrow ; i++)
	{
		for( int j =0; j<lcol ; j++)
		{
			lresult->setValueById(i,j, this->getValueById(i,j) * pValue);			
		}
		
	}

	return lresult;
}

FloatMatrix *   FloatMatrix:: DivideArray(FloatMatrix &pValue)
{	
	int lrow = pValue.get_RowNum();
	int lcol = pValue.get_ColNum();

	FloatMatrix* lresult = new  FloatMatrix(lrow,lcol);

	float lvalue = 0;
	for(int i=0;i<lrow; i++)
	{
		for(int j=0; j<lcol; j++)
		{
			for(int m=0; m<lcol; m++)
			{
				lvalue+=getValueById(i,m) * (1 / pValue.getValueById(m,j));
				lresult->setValueById(i,j,lvalue);				
			}			
		}
	 
	}
		
	return lresult;
}