#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <Windows.h>

using namespace std;
char buf[256];


class Slovar {
private:
	map<string,string> slovar;
public:
	void SetSlovar(string s1, string s2){
		slovar[s1]=s2;
		slovar[s2]=s1;
	}

	string GetSlova(string s1) {
		if(slovar.find(s1)!=slovar.end())
			return slovar[s1];
		else return "-/-/-";
	}
};



char* CTOA(char* text)
{
	CharToOemA(text, buf);
	return buf;
}
int main()
{
	Slovar sl;

	ifstream in("slovar.txt");
	string s1,s2;
	map<string,string> slovar;
	while(in>>s1>>s2)
	{
		transform(s1.begin(),s1.end(),s1.begin(),tolower); //ïåðåâîä â íèæíèé ðåãèñòð
		transform(s2.begin(),s2.end(),s2.begin(),tolower);
		sl.SetSlovar(s2, s1);
	}
	in.close();


	while(1)
	{
		cout<<CTOA("Vvedite slovo na angliyskom ili na russkom kotoroye khotite perevesti, ili exit chtoby vyyti: ");
		cin>>s1;
		transform(s1.begin(),s1.end(),s1.begin(),tolower);
		if(s1=="exit") break;

		cout<<CTOA("Перевод слова: ")<<sl.GetSlova(s1)<<endl;
	}
}
