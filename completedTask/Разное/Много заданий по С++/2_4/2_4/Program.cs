﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_4 {
    class Program {
        class CounterHex {
            public const int Max = int.MaxValue;
            public const int Min = int.MinValue;
            public int CurrentValue { get; private set; }

            public CounterHex () : this(0) { }

            public CounterHex (int start) {
                CurrentValue = start;
            }

            public void Increase () {
                if(CurrentValue == Max)
                    throw new Exception("Max value reached.");
                CurrentValue++;
            }

            public void Decrease () {
                if(CurrentValue == Min)
                    throw new Exception("Min value reached.");
                CurrentValue--;
            }

            public override string ToString () {
                return string.Format("{0:X}", CurrentValue);
            }
        }


        static void Main (string[] args) {
            CounterHex counter = new CounterHex(456);
            Console.WriteLine(counter.ToString());
            counter.Decrease();
            Console.WriteLine(counter.ToString());
            Console.ReadLine();
        }
    }
}
