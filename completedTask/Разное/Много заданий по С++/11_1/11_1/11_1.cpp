// 11_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cmath>
#include <iostream>

using namespace std;
struct Point {
public:
	double x;
	double y;
	Point() {
		x = 0;
		y = 0;
	}

	Point(double _x, double _y) {
		x = _x;
		y = _y;
	}
};

class Triangle
{
private: 
	Point _points[3];

public:
	Triangle(Point a, Point b, Point c) { // �����������
	
		_points[0] = a;
		_points[1] = b;
		_points[2] = c;


		if (GetAB() >= GetBC() + GetAC() || GetAC() >= GetBC() + GetAB() || GetBC() >= GetAB() + GetAC())
		{
			throw exception("������������ ������ ������������. ����� ����� �� ����� ������ ��� ����� ����� ����� ����");
		}

	}


	void SetPoitA(Point p) {
		_points[0] = p;
	}

	Point GetPoitA() {
		return _points[0];
	}

	void SetPoitB(Point p) {
		_points[1] = p;
	}

	Point GetPoitB() {
		return _points[1];
	}

	void SetPoitC(Point p) {
		_points[2] = p;
	}

	Point GetPoitC() {
		return _points[2];
	}



	double GetAB() {
		return sqrt(pow(_points[0].x - _points[1].x, 2) + pow(_points[0].y - _points[1].y, 2));
	}

	double GetBC() {
		return sqrt(pow(_points[1].x - _points[2].x, 2) + pow(_points[1].y - _points[2].y, 2));
	}

	double GetAC() {
		return sqrt(pow(_points[0].x - _points[2].x, 2) + pow(_points[0].y - _points[2].y, 2));
	}

	Point GetCenter() {
		Point p((GetPoitA().x + GetPoitB().x + GetPoitC().x) / 3, (GetPoitA().y + GetPoitB().y + GetPoitC().y) / 3);
		return p;
	}

	double GetPerimeter() {
		return GetAB() + GetBC() + GetAC();
	}

	double GetSquare() {
		double p = GetPerimeter() / 2;
		double temp = p * (p - GetAB()) * (p - GetBC()) * (p - GetAC());
		return sqrt(temp);
	}
};


int _tmain(int argc, _TCHAR* argv[])
{
	Triangle *triangle = new Triangle(Point(1, 2), Point(10, -2), Point(3, 6));
	cout<<triangle->GetCenter().x;
	system("pause");
	return 0;
}

