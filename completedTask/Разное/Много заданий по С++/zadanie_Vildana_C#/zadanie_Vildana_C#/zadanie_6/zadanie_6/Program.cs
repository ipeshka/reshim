﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_6 {
    class Program {
        const int N = 5; // размернось массива
        const int M = 6; // размернось массива

        static void Main (string[] args) {
            // объявляем переменные
            int[,] mass = new int[N, M]; // Массив типа int
            Console.WriteLine("Запонлните массив");
            for(int i = 0 ; i < N ; i++) {
                for(int j = 0 ; j < M ; j++) {
                    Console.Write(string.Format("Введите номер массива {0}{1}: ", i + 1, j + 1));
                    mass[i, j] = Int32.Parse(Console.ReadLine());
                }
            }
            PrintMass(mass);


            // находим сумму элементах в строках которых есть один отрицательный элемент
            for(int i = 0 ; i < N; i++) {
                for(int j = 0 ; j < M ; j++)
                    if(mass[i, j] < 0) /*ищу элементы массива которые меньше 0*/ {
                        int sum = 0;
                        for(j = 0 ; j < M ; j++) /*проход по строке с отрицательным элементом, для суммирования*/
                            sum += mass[i, j]; /*нахожу сумму элементов заданной строки*/

                        Console.Write("Сумма в строке {0} равна {1}", i, sum);
                    }
            }

            // находим седловые точки
            for(int i = 0 ; i < mass.GetLength(0) ; i++) {
                for(int j = 0 ; j < mass.GetLength(1) ; j++) {

                    // если являеться седловой, выводим на экран
                    if(IsMinInRow(mass, i, mass[i, j]) && IsMaxInColumn(mass, j, mass[i, j]))
                        Console.WriteLine("Седловая точка: a[{0}, {1}] = {2}", i, j, mass[i, j]);
                }
            }


            Console.ReadKey();
        }

        // вывод массива
        static void PrintMass (int[,] mass) {
            for(int i = 0 ; i < mass.GetLength(0) ; i++) {
                for(int j = 0 ; j < mass.GetLength(1) ; j++) {
                    Console.Write(string.Format("{0} ", mass[i, j]));
                }
                Console.WriteLine("");
            }
        }

        // если являеться минималынм в строке
        static bool IsMinInRow (int[,] matrix, int i, int value) {
            for(int j = 0 ; j < matrix.GetLength(1) ; j++)
                if(matrix[i, j] < value)
                    return false;
            return true;
        }
        // если являеться максимальным в колонке
        static bool IsMaxInColumn (int[,] matrix, int j, int value) {
            for(int i = 0 ; i < matrix.GetLength(0) ; i++)
                if(matrix[i, j] > value)
                    return false;
            return true;
        }
    }
}
