﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_7 {
    class Program {
        static void Main (string[] args) {
            // переменная для хранения текста из файла
            string text = "";

            Console.WriteLine("Введите имя файла:");
            string fileName = Console.ReadLine();

            // читаем до конца
            try {
                FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read);
                StreamReader sw = new StreamReader(fs);
                text = sw.ReadToEnd();

                // освобождаем ресурсы
                sw.Close();
                fs.Dispose();
            } catch(Exception ex) {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
                return;
            }

            // предложение - это кусок текста который заканчивается на . или ! или ? 
            string[] sentences = text.Split(new char[] { '.', '!', '?' });

            //Переберем все не пустые предложения и смотрим есть ли там запятая, если нет то выводим
            foreach(string tmp in sentences) {
                //проверяем что строка не пустая и нет запятой 
                if(tmp.Length > 0 && tmp.IndexOf(',') == -1) {
                    //не пустая и запятой нет, выводим 
                    Console.WriteLine(tmp);
                }
            }

            Console.ReadLine();

        }
    }
}
