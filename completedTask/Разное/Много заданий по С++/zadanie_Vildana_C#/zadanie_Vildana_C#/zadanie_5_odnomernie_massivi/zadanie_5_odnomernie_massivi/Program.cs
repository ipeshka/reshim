﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_5_odnomernie_massivi {
    class Program {
        // задание № 5. Одномерные массивы
        static void Main (string[] args) {
            // объявляем переменные
            int N = 10; // размернось массива
            int[] mass = new int[N]; // Массив типа int
            Console.WriteLine("Запонлните массив");
            for(int i = 0 ; i < N ; i++) {
                Console.Write(string.Format("Введите номер массива {0}: ", i + 1));
                mass[i] = Int32.Parse(Console.ReadLine());
            }
            PrintMass(mass);
            Console.Write(string.Format("Минимальный элемент {0}", MinElement(mass)));
            Console.Write(string.Format("Сумма между 2-му положительными элемент {0}", SumElement(mass)));
            // ставим на первое место нулевые элементы
            Console.WriteLine("Преобразованный массив");
            FirstNullMassiv(ref mass);
            PrintMass(mass);
            Console.ReadKey();
        }

        static void PrintMass (int[] mass) {
            for(int i = 0 ; i < mass.Length ; i++) {
                Console.Write(mass[i] + " ");
            }
        }

        // функция ставит на первое место нулевые элементы
        static void FirstNullMassiv (ref int[] mass) {
            int indexNull = 0;
            for(int j = 0 ; j < mass.Length ; j++) {
                if(mass[j] == 0) { // переставляем элменты местами, нулевой и не нулевой
                    // нулевой в начало
                    mass[j] = mass[indexNull];
                    mass[indexNull] = 0;
                    indexNull++;
                }
            }
        }

        // функция нахождения минимального элемента
        static int MinElement (int[] mass) {
            int minElement = mass[0]; // примем что минимальный элемент, это первый элемент
            for(int i = 1 ; i < mass.Length ; i++) {
                if(mass[i] < minElement) {
                    minElement = mass[i];
                }
            }

            return minElement;
        }

        // функция нахождения суммы между первым и последним положительным элементом
        static int SumElement (int[] mass) {
            int sum = 0;
            int indexFirstPos = IndexFirstPos(mass); // индекс первого положительного элемента
            int indexLastPos = IndexLastPos(mass); // индекс первого положительного элемента
            // цикл суммы от первого до последнего положительного элемента
            for(int i = indexFirstPos ; i <= indexLastPos ; i++) {
                sum += mass[i];
            }

            return sum;
        }

        // функция возвращает первый положительный элемент
        static int IndexFirstPos (int[] mass) {
            for(int i = 0; i < mass.Length; i++) {
                if(mass[i] > 0) {
                    return i;
                }
            }

            return -1;
        }

        // функция возвращает последний положительный элемент
        static int IndexLastPos (int[] mass) {
            // смотрим пока не найдем положительный элемент
            for(int i = mass.Length - 1 ; i >= 0 ; i--) {
                if(mass[i] > 0) {
                    return i;
                }
            }
            // если не нашли возвращаем -1
            return -1;
        }
    }
}
