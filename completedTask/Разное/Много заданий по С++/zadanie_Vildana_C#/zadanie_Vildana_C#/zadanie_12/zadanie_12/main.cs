﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadanie_12 {
    public partial class main : Form {
        Input formInput = new Input();
        public main () {
            InitializeComponent();
        }

        // собыите при нажатие на кнопку выхода
        private void quitToolStripMenuItem_Click (object sender, EventArgs e) {
            // закрываем форму
            this.Close();
        }

        // открытие диалогового окна для ввода данных
        private void inputToolStripMenuItem_Click (object sender, EventArgs e) {

            if(formInput.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                // делаем доступным пункт calc
                calcToolStripMenuItem.Enabled = true;
            }
        }

        private void calcToolStripMenuItem_Click (object sender, EventArgs e) {
            // передаём данные в форму
            Calc form = new Calc(formInput.Number1, formInput.Number2, formInput.IsSumma, formInput.IsMaxDivisor, formInput.IsMyltiply);
            form.ShowDialog();
        }
    }
}
