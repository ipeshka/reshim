﻿namespace zadanie_12 {
    partial class Input {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose (bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent () {
            this.tbNumber1 = new System.Windows.Forms.TextBox();
            this.tbNumber2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cBSumma = new System.Windows.Forms.CheckBox();
            this.cBMaxDivisor = new System.Windows.Forms.CheckBox();
            this.cBMultiply = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbNumber1
            // 
            this.tbNumber1.Location = new System.Drawing.Point(63, 12);
            this.tbNumber1.Name = "tbNumber1";
            this.tbNumber1.Size = new System.Drawing.Size(100, 20);
            this.tbNumber1.TabIndex = 0;
            // 
            // tbNumber2
            // 
            this.tbNumber2.Location = new System.Drawing.Point(63, 38);
            this.tbNumber2.Name = "tbNumber2";
            this.tbNumber2.Size = new System.Drawing.Size(100, 20);
            this.tbNumber2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Number1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Number2";
            // 
            // cBSumma
            // 
            this.cBSumma.AutoSize = true;
            this.cBSumma.Location = new System.Drawing.Point(63, 64);
            this.cBSumma.Name = "cBSumma";
            this.cBSumma.Size = new System.Drawing.Size(61, 17);
            this.cBSumma.TabIndex = 4;
            this.cBSumma.Text = "Summa";
            this.cBSumma.UseVisualStyleBackColor = true;
            // 
            // cBMaxDivisor
            // 
            this.cBMaxDivisor.AutoSize = true;
            this.cBMaxDivisor.Location = new System.Drawing.Point(63, 87);
            this.cBMaxDivisor.Name = "cBMaxDivisor";
            this.cBMaxDivisor.Size = new System.Drawing.Size(81, 17);
            this.cBMaxDivisor.TabIndex = 5;
            this.cBMaxDivisor.Text = "Max Divisor";
            this.cBMaxDivisor.UseVisualStyleBackColor = true;
            // 
            // cBMultiply
            // 
            this.cBMultiply.AutoSize = true;
            this.cBMultiply.Location = new System.Drawing.Point(63, 110);
            this.cBMultiply.Name = "cBMultiply";
            this.cBMultiply.Size = new System.Drawing.Size(61, 17);
            this.cBMultiply.TabIndex = 6;
            this.cBMultiply.Text = "Multiply";
            this.cBMultiply.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 139);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "Ок";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Input
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 173);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cBMultiply);
            this.Controls.Add(this.cBMaxDivisor);
            this.Controls.Add(this.cBSumma);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbNumber2);
            this.Controls.Add(this.tbNumber1);
            this.Name = "Input";
            this.Text = "Input";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNumber1;
        private System.Windows.Forms.TextBox tbNumber2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.CheckBox cBSumma;
        private System.Windows.Forms.CheckBox cBMaxDivisor;
        private System.Windows.Forms.CheckBox cBMultiply;
        private System.Windows.Forms.Button button1;
    }
}