﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadanie_12 {
    public partial class Calc : Form {
        private int Number1;
        private int Number2;
        private bool IsSumma;
        private bool IsMaxDivisor;
        private bool IsMyltiply;

        public Calc (int number1, int number2, bool isSumma, bool isMaxDivisor, bool isMyltiply) {
            InitializeComponent();
            Number1 = number1;
            Number2 = number2;
            IsSumma = isSumma;
            IsMaxDivisor = isMaxDivisor;
            IsMyltiply = isMyltiply;
        }

        private static int NOD(int a, int b) {
            while(b != 0) {
                int temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

        private void Calc_Load (object sender, EventArgs e) {
            richTextBox1.AppendText("Рассчётные данные:\n");
            if(IsSumma) {
                richTextBox1.AppendText(string.Format("Сумма: {0}\n", Number1 + Number2));
            }

            if(IsMaxDivisor) {
                richTextBox1.AppendText(string.Format("НОД: {0}\n", NOD(Number1, Number2)));
            }

            if(IsMyltiply) {
                richTextBox1.AppendText(string.Format("Произведение: {0}\n", Number1 * Number2));
            }
        }
    }
}
