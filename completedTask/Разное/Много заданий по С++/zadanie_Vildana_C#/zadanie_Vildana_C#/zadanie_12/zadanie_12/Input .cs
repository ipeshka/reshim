﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zadanie_12 {
    public partial class Input : Form {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
        public bool IsSumma { get; set; }
        public bool IsMaxDivisor { get; set; }
        public bool IsMyltiply { get; set; }

        public Input () {
            InitializeComponent();
        }

        private void button1_Click (object sender, EventArgs e) {
            // сохраняем значения
            Number1 = Convert.ToInt32(tbNumber1.Text);
            Number2 = Convert.ToInt32(tbNumber2.Text);

            IsSumma = cBSumma.Checked;
            IsMaxDivisor = cBMaxDivisor.Checked;
            IsMyltiply = cBMultiply.Checked;

            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}
