﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_9_10 {
    class Program {
        static void Main (string[] args) {
        }
    }

    public class Element { //базовый класс 
        private string name_element; //имя элемента
        private int input_element; //количество входов
        private int output_element; //количество выходов
        public Element () { //конструктор без параметров
            name_element = "NoName";
            input_element = 1;
            output_element = 1;
        }
        public Element (string _name_element) { //конструктор, задающий имя и устанавливающий равным 1 количество входов и выходов
            name_element = _name_element;
            input_element = 1;
            output_element = 1;
        }

        public Element (string _name_element, int _input_element, int _exit_element) { //конструктор, задающий значения всех полей элемента
            name_element = _name_element;
            input_element = _input_element;
            output_element = _exit_element;
        }
        public string NameElemen //имя элемента (только чтение)
        {
            get {
                return name_element;
            }
        }
        public int CountInput //количество входов элемента
        {
            get {
                return input_element;
            }
            set {
                input_element = value;
            }
        }
        public int CountOutput //количество выходов элемента
        {
            get {
                return output_element;
            }
            set {
                output_element = value;
            }
        }
    }

    public class Combin : Element { //произвольный клас на основе класса Элемент
        int[] input;
        public Combin (string name_element, int input_element, int output_element)
            : base(name_element, input_element, output_element) //метод, задающий значение на выходах экземпляра класса
        {
            input = new int[input_element];
        }
        public void SetInput (int index, int value) //метод, позволяющий опрашивать состояние отдельного входа экземпляра класса
        {
            if(index >= CountInput)
                return;
            input[index] = value;
        }
        public int ValueInput (int index) {
            if(index >= CountInput)
                return -1;
            return input[index];
        }

        public void CalcOutput () //метод, вычисляющий значение выхода
        {
            double h1 = 0, h0 = 0;
            for(int i = 0 ; i < input.Length ; i++) {
                if(input[i] == 1) {
                    h1++;
                } else {
                    h0++;
                }
            }

            if(h1 == input.Length) { // если все входы 1, то инвертируем
                Console.WriteLine("Выход = 0");
            } else if(h0 == input.Length) { // если все входы 0, то инвертируем
                Console.WriteLine("Выход = 1");
            } else { // иначе 0
                Console.WriteLine("Выход = 0");
            }
        }
    }
}
