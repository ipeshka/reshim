﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_10 {
    class Program {
        static void Main (string[] args) {
            int max;

            Console.WriteLine("Введите число работников:");
            max = Convert.ToInt32(Console.ReadLine());

            Worker[] rab = new Worker[max];

            for(int i = 0 ; i < max ; ++i) {
                string name, lastname, dolzhnost;
                int year;
                Console.WriteLine("Добавление работника...");
                Console.WriteLine("Введите имя:");
                name = Console.ReadLine();
                Console.WriteLine("Введите фамилию:");
                lastname = Console.ReadLine();
                Console.WriteLine("Введите год поступления на работу:");
                year = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введите название занимаемой должности:");
                dolzhnost = Console.ReadLine();
                rab[i] = new Worker(name, lastname, dolzhnost, year);
            }
            Console.WriteLine("Идёт сортировка...");
            Array.Sort(rab);
            for(int i = 0 ; i < rab.Length ; ++i)
                Console.WriteLine(rab[i]);

            while(true) {
                Console.WriteLine("Введите стаж работы:");
                try {
                    int year = Convert.ToInt32(Console.ReadLine());
                    bool was = false;
                    for(int i = 0 ; i < rab.Length ; ++i)
                        if(rab[i].Year > year) {
                            Console.WriteLine(rab[i]);
                            was = true;
                            break;
                        }
                    if(!was)
                        Console.WriteLine("Клиента с таким номером нет в базе.");
                } catch(Exception e) {
                    Console.WriteLine(e.Message);
                    break;
                }
            }
        }

        public struct Worker : IComparable {
            private string _Name;
            private string _LastName;
            private string _Dolzhnost;
            private int _Year;

            public int Year {
                get { return _Year; }
            }
            public Worker (string name, string lastname, string dolzhnost, int year) {
                _Name = name;
                _LastName = lastname;
                _Dolzhnost = dolzhnost;
                _Year = year;
            }

            public override string ToString () {
                return _Name + ' ' + _LastName + "; " + _Dolzhnost + " ; " + _Year;
            }

            // метод сравнения, сравнивает по первой букве
            public int CompareTo (object obj) {
                Worker worker = (Worker)obj;
                if(this._Name[0] == worker._Name[0]) {
                    return 0;
                } else if(this._Name[0] > worker._Name[0]) {
                    return 1;
                } else {
                    return -1;
                }
            }

        }
    }
}
