﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zadanie_8 {
    class Program {
        static void Main (string[] args) {
            Home_Library homeLibrary = new Home_Library();
            homeLibrary.AddBook(2001, "Бедный папа, богатый папа", "Генри", "История");
            homeLibrary.AddBook(2002, "Бедный папа", "Генри_2", "Фнтастика");
            homeLibrary.AddBook(2003, "Богатый папа", "Генри_3", "История");
            homeLibrary.AddBook(2004, "Неуловимый", "Алексей Будний", "Триллер");


            Console.WriteLine("Выводим элементы");
            for(int i = 0 ; i < homeLibrary.CountBook ; i++) {
                Console.WriteLine(homeLibrary[i].ToString());
            }

            // ищем элемент с 2003 годом
            System.Collections.Generic.List<Book> bookYear = homeLibrary.SearchByYear(2001);

            Console.WriteLine("Выводим найденные элементы");
            for(int i = 0 ; i < bookYear.Count ; i++) {
                Console.WriteLine(bookYear[i].ToString());
            }

            // удаялем первый элемент
            bool isDelete = homeLibrary.DeleteBook(1);
            if(isDelete) {
                Console.WriteLine("Элемент удалён");
            }

            Console.WriteLine("Выводим элементы после удаления");
            for(int i = 0 ; i < homeLibrary.CountBook ; i++) {
                Console.WriteLine(homeLibrary[i].ToString());
            }
            Console.ReadLine();
        }


        class Book {
            // закрытые поля класса
            private int _Number;
            private int _PublicationYear;
            private string _Name;
            private string _Author;
            private string _Cathegory;

            // свойства для доступа, только просмотр изменение нельзя
            public int PublicationYear {
                get { return _PublicationYear; }
            }

            public int Number {
                get { return _Number; }
                set { _Number = value; }
            }

            public string Name {
                get { return _Name; }
            }

            public string Author {
                get { return _Author; }
            }

            public string Cathegory {
                get { return _Cathegory; }
            }


            public Book (int year, string name, string author, string cathegory, int number) {
                _PublicationYear = year;
                _Name = name;
                _Author = author;
                _Number = number;
                _Cathegory = cathegory;
            }

            public override string ToString () {
                return "Name:" + _Name + ", Author:" + _Author + ", Cathegory:" + _Cathegory + ", PublicationYear:" + _PublicationYear;
            }

        }
        // класс библиотеки
        class Library {
            private List<Book> _Library;

            // индексатор
            public Book this[int i] {
                get {
                    return _Library[i];
                }
                set {
                    _Library[i] = value;
                }
            }

            public int CountBook {
                get { return _Library.Count; }
            }

            public Library () {
                _Library = new List<Book>();
            }

            // добавление новой книги
            public void AddBook (Book book) {
                book.Number = _Library.Count + 1;
                _Library.Add(book);
            }

            public void AddBook (int year, string name, string author, string cathegory) {
                int number = _Library.Count + 1;
                _Library.Add(new Book(year, name, author, cathegory, number));
            }

            // удаление книги по номеру, поскольку номер это как идинтификатор, 
            // если будем удалять по другому, то может удалить несколько

            public bool DeleteBook (int number) {
                // находим книгу
                Book book = _Library.SingleOrDefault(x => x.Number == number);
                if(book != null) {
                    return _Library.Remove(book);
                }

                return false;
            }

            // поиск по параметрам, производиться с помощью LINQ
            public List<Book> SearchByName (string name) {
                return _Library.Where(x => x.Name == name).ToList();
            }
            public List<Book> SearchByAuthor (string author) {
                return _Library.Where(x => x.Author == author).ToList();
            }
            public List<Book> SearchByCathegory (string cathegory) {
                return _Library.Where(x => x.Cathegory == cathegory).ToList();
            }
            public List<Book> SearchByYear (int year) {
                return _Library.Where(x => x.PublicationYear == year).ToList();
            }

        }
        class Home_Library : Library {
            public Home_Library () : base () { 
            
            }
        }
    }
}
