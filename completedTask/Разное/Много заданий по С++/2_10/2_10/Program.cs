﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_10 {
    class Program {
        static void Main (string[] args) {
            string[] digital1 = File.ReadAllLines(@"D:\input1.txt");
            string[] digital2 = File.ReadAllLines(@"D:\input2.txt");
            string[] digital3 = File.ReadAllLines(@"D:\input3.txt");

            using(StreamWriter sw = new StreamWriter(@"D:\output.txt")) {
                for(int i = 0 ; i < digital1.Length ; i++) {
                    sw.WriteLine("|" + AddSpace(digital1[i]) + AddSpace(digital2[i]) + AddSpace(digital3[i]) + "|");
                }

            }

        }

        // функция добавляет доп пробелы что бы было 20
        static string AddSpace (string str) {
            int countChar = str.Length;
            for(int i = 0 ; i < 20 - countChar ; i++)
                str += ' ';
            return str;
        }
    }
}
