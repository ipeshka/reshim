#include <stdlib.h>
#include <iostream>
#include <cstring>

using namespace std ;

#pragma once

#ifndef PLANE_H
#define PLANE_H

class Plane
{
	//����� ���������� 
	char * _Destination ;

	//����� ����� 
	int _RouteNumber;

	//��� ����������� 
	int _HH;

	//������ ����������� 
	int _MM;

public:
	Plane(void);
	~Plane(void);

	//������������ � ����������� 
	Plane(char *pDistination, int pRoute, int pHH, int pMM);

	void set_RouteNumber(int pNumber);

	char * get_Destination()
	{
		return _Destination;
	}

	/*��������� ������� ����������� */
	void set_HH(int pHour);	
	void set_MM(int pMinute);

	int get_HH(){return _HH;}
	int get_MM(){return _MM;}


	int get_RouteNumber(){return _RouteNumber;}
	void Show_Time()
	{
		cout<<_HH<<" : "<<_MM;
	}

	//�������������� ������� � �������� �������� 
	int TimeToInt32();

	bool operator==(Plane &pvalue);
	
	void Print();
	
};

#endif
