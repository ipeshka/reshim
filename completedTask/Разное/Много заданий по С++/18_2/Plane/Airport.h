#pragma once

#include "Plane.h"

#ifndef AIRPORT_H
#define AIRPORT_H

#define MaxElement 10

class Airport
{
	//�������� ������ ��������� 
	Plane _PlaneList[MaxElement]; 

	//���������� ���������
	int _PlaneNumber;


public:
	Airport(void);
	~Airport(void);

	Airport(Plane pPlane);

	void AddPlane(Plane p1);
	
	bool operator==(Plane &pvalue);

	//���� � �������� �� ������� 
	void ShowArrivedByIndex(int index);

	//���� � �������� � ������������ ���� ��� 
	void InfoNextArrived(int phh, int pmm);

	void ShowPlaneByArrived(char * pDestination);

	void SortArrivedTime();

	void Print();

	//���������� 
	void  Sort();
};

#endif
