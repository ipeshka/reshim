#include "Plane.h"

Plane::Plane(void)
{
	_Destination = new char[256];
}

Plane::~Plane(void)
{
		
}

void Plane::set_RouteNumber(int pNumber)
{
	if(pNumber>999999 || pNumber<111111)
	{
		cout<<"Invalid route number\n";
		exit(1);
	}

	else
	{
		_RouteNumber = pNumber;
	}
}

void Plane::set_HH(int pHour)
{
	if(pHour<0 || pHour>24)
	{
		cout<<"Invalid time value \n";
		//��������� ������� �� ���������
		cout<<"Assigned to the default time"<<endl;
		_HH = 0;
	}
	else 
	{
		_HH = pHour;
	}
}
void Plane::set_MM(int pMinute)
{
	if(pMinute < 0 || pMinute > 60)
	{
		cout<<"Invalid time value \n";
		//��������� ������� �� ���������
		cout<<"Assigned to the default time"<<endl;
		_MM=0;
	}
	else 
	{
		_MM = pMinute;
	}
}

// ��������� ��������� ��������� - ������� ����� ������ � �� ����� 
// ���������� ��� ���������� 
int  Plane::TimeToInt32()
{
	int lresult = 0;
    if(_HH < 23)
	{
		lresult = _HH * 60 +  _MM;
	}


	return lresult;
}
Plane::Plane( char *pDistination, int pRouteNumber, int pHH, int pMM)
{
	_Destination  = pDistination;

	//� ������� ���� ����������� 
	set_RouteNumber(pRouteNumber);
	_HH = pHH;
	_MM = pMM;
}


bool Plane::operator==(Plane &pvalue)
	{
		bool lresult = false ;	

		int len = strcmp(_Destination,pvalue._Destination);
	
		if(_RouteNumber == pvalue.get_RouteNumber() && _HH == pvalue._HH
			&& _MM == pvalue._MM && len==0 )
		{
			lresult = true;
		}

		return lresult;
	}


void Plane::Print()
{
	cout<<"����� ���������� :	";
	cout<<_Destination<<endl;
	
	cout<<"����� ����������� :	";
	Show_Time();
	cout<<endl;

	cout<<"����� ����� :	";
	cout<<_RouteNumber<<endl;
}
