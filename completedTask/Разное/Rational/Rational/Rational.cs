﻿using System;


namespace Rational
{
    public class Rational
    {
        // числитель
        private int a;
        // знаменатель
        private int b;

        // конструктор
        public Rational(int a, int b)
        {
            if (b == 0) { this.a = 0; this.b = 1; }
            else
            {
                //приведение знака
                if (b < 0) { b = -b; a = -a; }

                // сокращении дроби
                int nod = Nod(a, b);
                this.a = a / nod;
                this.b = b / nod;
            }


        }


        // перегрузка операции сложения
        public static Rational operator +(Rational r1, Rational r2)
        {
            int u = r1.a * r1.b + r2.b * r1.a;
            int v = r1.b * r2.b;
            return (new Rational(u, v));
        }
        // перегрузка операции вычитания
        public static Rational operator -(Rational r1, Rational r2)
        {
            int u = r1.a * r2.b - r1.b * r2.a;
            int v = r1.b * r2.b;
            return (new Rational(u, v));
        }
        // перегрузка операции умножения
        public static Rational operator *(Rational r1, Rational r2)
        {
            int u = r1.a * r2.a;
            int v = r1.b * r2.b;
            return (new Rational(u, v));
        }
        // перегрузка операции деления
        public static Rational operator /(Rational r1, Rational r2)
        {
            int u = r1.a * r2.b;
            int v = r1.b * r2.a;
            return (new Rational(u, v));
        }
        // нахождения НОД
        private static int Nod(int m, int n)
        {
            int p = 0;
            m = Math.Abs(m); n = Math.Abs(n);
            if (n > m){
                p = m; 
                m = n; 
                n = p;
            }
            do
            {
                p = m % n; 
                m = n; 
                n = p;
            } while (n != 0);
            return m;
        }

        public override string ToString()
        {
            return string.Format("{0}/{1}", this.a, this.b);
        }
    }


}
