﻿using System;

namespace Rational
{
    class Program
    {
        static void Main(string[] args)
        {
            Rational r1 = new Rational(2, 3);
            Rational r2 = new Rational(1, 3);

            Rational r3 = r1 + r2;
            Rational r4 = r1 - r2;
            Rational r5 = r1 * r2;
            Rational r6 = r1 / r2;

            Console.WriteLine("add" + r3.ToString());
            Console.WriteLine("sub" + r4.ToString());
            Console.WriteLine("mul" + r5.ToString());
            Console.WriteLine("div" + r6.ToString());

            Console.ReadLine();
        }
    }
}
