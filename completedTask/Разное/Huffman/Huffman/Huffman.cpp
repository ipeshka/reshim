#include "stdafx.h"
#include <iostream>
#include <vector>
#include <map>
#include <list>
#include <fstream>
#include <string>
using namespace std;

class Node
{
public:
	int a;
	char c;
	Node *left, *right;

	Node() { 
		left=right=NULL;
	}

	Node(Node *L, Node *R)  {
		left =  L;
		right = R;
		a = L->a + R->a;  
	}
};

struct MyCompare {
	bool operator()(const Node* l, const Node* r) const { 
		return l->a < r->a; 
	}
};

vector<bool> code;                
map<char,vector<bool> > table;    

void BuildTable(Node *root) {
	if (root->left!=NULL) 
	{ code.push_back(0);
	BuildTable(root->left);}

	if (root->right!=NULL)
	{ code.push_back(1);
	BuildTable(root->right);}

	if (root->c) table[root->c]=code;     

	code.pop_back();
}

int main (int argc, char *argv[]) {
	setlocale(LC_ALL,"Russian"); // ���� ������� ������� ������������ � ��������� ������
	string filePath;
	char answerExit = 'N';

	while(answerExit != 'Y') {
		cout<<"Input path file:"<<endl;
		cin>>filePath;


		fstream f(filePath, ios::in);
		if(!f.is_open()) cout<<"������"<<endl;
		map<char,int> m;
		while (!f.eof()) { 
			char c = f.get(); 
			m[c]++;
		}


		// ���������� ��������� ���� � ������ list
		list<Node*> t;
		for( map<char,int>::iterator itr=m.begin(); itr!=m.end(); ++itr) {
			Node *p = new Node;
			p->c = itr->first;
			p->a = itr->second;  
			t.push_back(p);      
		}	


		// ������� ������		

		while (t.size()!=1) {
			t.sort(MyCompare());

			Node *SonL = t.front();
			t.pop_front();
			Node *SonR = t.front(); 
			t.pop_front();

			Node *parent = new Node(SonL,SonR); 
			t.push_back(parent);
		}

		Node *root = t.front(); // root - ��������� �� ������� ������

		// ������� ���� '������-���':
		BuildTable(root);   

		// ������� ���� � ���� output.txt
		f.clear(); f.seekg(0); // ���������� ��������� ����� � ������ �����

		string tmp;
		int count=0; char buf=0;
		while (!f.eof()) {
			char c = f.get();
			vector<bool> x = table[c];
			for(int n=0; n<x.size(); n++)
			{buf = buf | x[n]<<(7-count);
			count++;   
			if (count==8) {
				count=0;
				tmp.push_back(buf);
				buf=0; 
			} 
			}
		}
		f.close();
		cout<<"�������������: "<<(double)m.size()/tmp.length()<<endl;

		cout<<"Exit? (N/Y)"<<endl;
		cin>>answerExit;
	}



	system("pause");
	return 0;
}