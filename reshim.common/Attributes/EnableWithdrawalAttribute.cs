﻿namespace reshim.common.Attributes
{
    public class EnableWithdrawalAttribute : BaseAttribute
    {
        public EnableWithdrawalAttribute(bool value)
            : base(value)
        {
        }
    }
}
