﻿namespace reshim.common.Attributes
{
    public class NameAttribute : BaseAttribute
    {
        public NameAttribute(string value)
            : base(value)
        {
        }
    }
}
