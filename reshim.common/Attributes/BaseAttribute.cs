﻿using System;
namespace reshim.common.Attributes
{
    public abstract class BaseAttribute : Attribute
    {
        private readonly object _value;

        protected BaseAttribute(object value)
        {
            this._value = value;
        }

        public object GetValue()
        {
            return this._value;
        }
    }
}
