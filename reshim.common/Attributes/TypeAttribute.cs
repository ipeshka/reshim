﻿namespace reshim.common.Attributes
{
    public class TypeAttribute : BaseAttribute
    {
        public TypeAttribute(string value)
            : base(value)
        {
        }
    }
}
