﻿namespace reshim.common.Attributes
{
    public class IsDisplayAttribute : BaseAttribute
    {
        public IsDisplayAttribute(bool value)
            : base(value)
        {
        }
    }
}
