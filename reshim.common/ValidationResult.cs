﻿namespace reshim.common {
    public class ValidationResult {
        public ValidationResult() {
        }

        public ValidationResult(string memeberName, string message) {
            MemberName = memeberName;
            Message = message;
        }

        public ValidationResult(string message) {
            Message = message;
        }

        public string MemberName { get; set; }
        public string Message { get; set; }
    }
}
