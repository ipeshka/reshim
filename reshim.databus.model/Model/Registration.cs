﻿using reshim.databus.model.contracts.contracts;

namespace reshim.databus.model.Model
{
    public class Registration : IRegistration
    {
        public string Email { get; set; }
        public string Login { get; set; }
    }
}