﻿using reshim.databus.model.contracts.Contracts;

namespace reshim.databus.model.Model
{
    public class RestoryPassword : IRestoryPassword
    {
        public string Email { get; set; }
        public string Login { get; set; }
        public string Site { get; set; }
    }

    public class RestoryPasswordSuccess : IRestoryPasswordSuccess
    {
        public string Email { get; set; }
        public string Login { get; set; }
        public string Site { get; set; }
    }
}
