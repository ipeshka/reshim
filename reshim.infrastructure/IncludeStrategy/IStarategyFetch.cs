﻿namespace reshim.data.infrastructure.IncludeStrategy
{
    public interface IStarategyFetch<TEntity> where TEntity : class 
    {
        IncludeStrategy<TEntity> IncludeProperties { get; set; }
    }
}
