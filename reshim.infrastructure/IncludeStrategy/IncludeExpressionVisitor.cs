﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace reshim.data.infrastructure.IncludeStrategy
{
    public class IncludeExpressionVisitor : ExpressionVisitor
    {
        private string _navigationProperty = null;

        public IncludeExpressionVisitor(Expression expr)
        {
            base.Visit(expr);
        }
        public string NavigationProperty
        {
            get { return _navigationProperty; }
        }

        protected Expression VisitMemberAccess(MemberExpression m)
        {
            var pInfo = m.Member as PropertyInfo;

            if (pInfo == null)
                throw new Exception("You can only include Properties");

            if (m.Expression.NodeType != ExpressionType.Parameter)
                throw new Exception("You can only include Properties of the Expression Parameter");

            _navigationProperty = pInfo.Name;

            return m;
        }


        public override Expression Visit(Expression exp)
        {
            if (exp == null)
                return null;
            switch (exp.NodeType)
            {
                case ExpressionType.MemberAccess:
                    return VisitMemberAccess((MemberExpression)exp);
                case ExpressionType.Parameter:
                    return VisitParameter((ParameterExpression)exp);
                case ExpressionType.Call:
                    return VisitMethodCall((MethodCallExpression)exp);
                default:
                    throw new InvalidOperationException("Unsupported Expression");
            }
        } 


    }
}
