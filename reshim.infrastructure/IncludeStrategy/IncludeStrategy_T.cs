﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace reshim.data.infrastructure.IncludeStrategy
{
    public class IncludeStrategy<TEntity> where TEntity : class
    {
        private readonly List<string> _includes = new List<string>();

        public SubInclude<TNavProp> Include<TNavProp>(Expression<Func<TEntity, TNavProp>> expr) where TNavProp : class
        {
            return new SubInclude<TNavProp>(_includes.Add, new IncludeExpressionVisitor(expr).NavigationProperty);
        }

        public SubInclude<TNavProp> Include<TNavProp>(Expression<Func<TEntity, ICollection<TNavProp>>> expr) where TNavProp : class
        {
            return new SubInclude<TNavProp>(_includes.Add, new IncludeExpressionVisitor(expr).NavigationProperty);
        }

        public IQueryable<TEntity> ApplyTo(IQueryable<TEntity> query)
        {
            foreach (string include in _includes)
            {
                query = query.Include(include);
            }

            return query;
        }
    }
}
