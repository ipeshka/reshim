﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace reshim.data.infrastructure.IncludeStrategy
{
    public class SubInclude<TNavProp> where TNavProp : class
    {
        private readonly Action<string> _callBack;
        private readonly List<string> _paths;

        internal SubInclude(Action<string> callBack, params string[] path)
        {
            _callBack = callBack;
            _paths = path.ToList();
            _callBack(string.Join(".", _paths));

        }

        public SubInclude<TNextNavProp> Include<TNextNavProp>(Expression<Func<TNavProp, TNextNavProp>> expr) where TNextNavProp : class
        {
            _paths.Add(new IncludeExpressionVisitor(expr).NavigationProperty);
            return new SubInclude<TNextNavProp>(_callBack, _paths.ToArray());
        }

        public SubInclude<TNextNavProp> Include<TNextNavProp>(Expression<Func<TNavProp, ICollection<TNextNavProp>>> expr) where TNextNavProp : class
        {
            _paths.Add(new IncludeExpressionVisitor(expr).NavigationProperty);
            return new SubInclude<TNextNavProp>(_callBack, _paths.ToArray());
        }
    }
}
