﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories {
    public class PaymentDataRepository : RepositoryBase<PaymentData>, IPaymentDataRepository
    {
        public PaymentDataRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }

    public interface IPaymentDataRepository : IRepository<PaymentData>
    {
    }
}
