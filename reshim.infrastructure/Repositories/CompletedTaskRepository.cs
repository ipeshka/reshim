﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories
{
    public class CompletedTaskRepository : RepositoryBase<CompletedTask>, IComplitedTaskRepository
    {
        public CompletedTaskRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }

    public interface IComplitedTaskRepository : IRepository<CompletedTask>
    {
    }

    public class HistoryComplitedTaskRepository : RepositoryBase<HistoryCompletedTask>, IHistoryComplitedTaskRepository
    {
        public HistoryComplitedTaskRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IHistoryComplitedTaskRepository : IRepository<HistoryCompletedTask>
    {
    }
}
