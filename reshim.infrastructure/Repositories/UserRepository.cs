﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories {
    public class UserRepository : RepositoryBase<User>, IUserRepository {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }
    public interface IUserRepository : IRepository<User> {
    }
}
