﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories {
    public class TaskRepository : RepositoryBase<Task>, ITaskRepository {
        public TaskRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }
    public interface ITaskRepository : IRepository<Task> {
    }
}
