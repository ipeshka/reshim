﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories
{
    public class TypeWorkRepository : RepositoryBase<TypeWork>, ITypeWorkRepository
    {
        public TypeWorkRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface ITypeWorkRepository : IRepository<TypeWork>
    {
    }
    public class SubjectRepository : RepositoryBase<Subject>, ISubjectRepository
    {
        public SubjectRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface ISubjectRepository : IRepository<Subject>
    {
    }
    public class NewsRepository : RepositoryBase<News>, INewsRepository
    {
        public NewsRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface INewsRepository : IRepository<News>
    {
    }
    public class SeoArticleRepository : RepositoryBase<SeoArticle>, ISeoArticleRepository
    {
        public SeoArticleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface ISeoArticleRepository : IRepository<SeoArticle>
    {
    }
    public class CategoryArticleRepository : RepositoryBase<CategoryArticle>, ICategoryArticleRepository
    {
        public CategoryArticleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface ICategoryArticleRepository : IRepository<CategoryArticle>
    {
    }
    public class ArticleRepository : RepositoryBase<Article>, IArticleRepository
    {
        public ArticleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IArticleRepository : IRepository<Article>
    {
    }
    public class CategoryLessonRepository : RepositoryBase<CategoryLesson>, ICategoryLessonRepository
    {
        public CategoryLessonRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface ICategoryLessonRepository : IRepository<CategoryLesson>
    {
    }
    public class LessonRepository : RepositoryBase<Lesson>, ILessonRepository
    {
        public LessonRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface ILessonRepository : IRepository<Lesson>
    {
    }
    public class NameLessonRepository : RepositoryBase<NameLesson>, INameLessonRepository
    {
        public NameLessonRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface INameLessonRepository : IRepository<NameLesson>
    {
    }
    public class ChapterRepository : RepositoryBase<Chapter>, IChapterRepository
    {
        public ChapterRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IChapterRepository : IRepository<Chapter>
    {
    }
    public class ContentRepository : RepositoryBase<Content>, IContentRepository
    {
        public ContentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
    public interface IContentRepository : IRepository<Content>
    {
    }

}
