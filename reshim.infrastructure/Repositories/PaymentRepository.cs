﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;
namespace reshim.data.infrastructure.Repositories
{

    public class PaymentRepository : RepositoryBase<Payment>, IPaymentRepository
    {
        public PaymentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IPaymentRepository : IRepository<Payment>
    {
    }
}
