﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories
{
    public class WithdrawalRepository : RepositoryBase<Withdrawal>, IWithdrawalRepository
    {
        public WithdrawalRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IWithdrawalRepository : IRepository<Withdrawal>
    {
    }
}
