﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;
namespace reshim.data.infrastructure.Repositories
{
    public class MessageRepository : RepositoryBase<Message>, IMessageRepository {
        public MessageRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }
    public interface IMessageRepository : IRepository<Message> {
    }


    public class DialogRepository : RepositoryBase<Dialog>, IDialogRepository
    {
        public DialogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }
    public interface IDialogRepository : IRepository<Dialog>
    {
    }
}
