﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories
{
    public class ConnectionRepository : RepositoryBase<Connection>, IConnectionRepository
    {
        public ConnectionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IConnectionRepository : IRepository<Connection>
    {
    }
}
