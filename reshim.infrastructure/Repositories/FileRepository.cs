﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories {
    public class FileRepository : RepositoryBase<File>, IFileRepository {
        public FileRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory) {
        }
    }

    public interface IFileRepository : IRepository<File> {
    }
}
