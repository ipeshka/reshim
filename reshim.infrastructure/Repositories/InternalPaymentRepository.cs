﻿using reshim.data.infrastructure.Database;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Repositories
{
    public class InternalPaymentRepository : RepositoryBase<InternalPayment>, IInternalPaymentRepository
    {
        public InternalPaymentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }
    }

    public interface IInternalPaymentRepository : IRepository<InternalPayment>
    {
    }
}
