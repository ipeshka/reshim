﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace reshim.data.infrastructure.Database {
    public abstract class RepositoryBase<T> where T : class {
        private ReshimDataContex _dataContext;
        private readonly IDbSet<T> _dbset;
        protected RepositoryBase(IDatabaseFactory databaseFactory) {
            DatabaseFactory = databaseFactory;
            _dbset = DataContext.Set<T>();
        }

        protected IDatabaseFactory DatabaseFactory {
            get;
            private set;
        }

        protected ReshimDataContex DataContext {
            get { return _dataContext ?? (_dataContext = DatabaseFactory.Get()); }
        }
        public virtual void Add(T entity) {
            _dbset.Add(entity);
        }
        public virtual void Update(T entity) {
            _dbset.Attach(entity);
            _dataContext.Entry(entity).State = EntityState.Modified;
        }
        public virtual void Delete(T entity) {
            _dbset.Remove(entity);
        }
        public virtual void Delete(Expression<Func<T, bool>> where) {
            IEnumerable<T> objects = _dbset.Where(where).AsEnumerable();
            foreach (T obj in objects)
                _dbset.Remove(obj);
        }
        public virtual T GetById(long id) {
            return _dbset.Find(id);
        }
        public virtual T GetById(string id) {
            return _dbset.Find(id);
        }
        public virtual IEnumerable<T> GetAll()
        {
            return _dbset.ToList();
        }
        public virtual IQueryable<T> GetDataContext()
        {
            return _dbset;
        }
        public virtual IQueryable<T> GetMany(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where);
        }
        public T Get(Expression<Func<T, bool>> where) {
            return _dbset.Where(where).FirstOrDefault();
        }

        public IQueryable<T> AllIncluding(params Expression<Func<T, object>>[] includeProperties) {
            IQueryable<T> query = _dbset;
            foreach (var includeProperty in includeProperties) {
                query = query.Include(includeProperty);
            }
            return query;
        }

        public int Count(Expression<Func<T, bool>> where)
        {
            return _dbset.Where(where).Count();
        }

        public int Count()
        {
            return _dbset.Count();
        }
    }
}
