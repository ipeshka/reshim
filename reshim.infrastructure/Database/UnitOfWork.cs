﻿using System.Collections.Generic;

namespace reshim.data.infrastructure.Database {
    public class UnitOfWork : IUnitOfWork, ISql {
        private readonly IDatabaseFactory _databaseFactory;
        private ReshimDataContex _dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory) {
            _databaseFactory = databaseFactory;
        }

        protected ReshimDataContex DataContext {
            get { return _dataContext ?? (_dataContext = _databaseFactory.Get()); }
        }

        public void Commit() {
            DataContext.Commit();
        }

        public IEnumerable<T> ExecuteQuery<T>(string sqlQuery, params object[] parameters)
        {
            return DataContext.Database.SqlQuery<T>(sqlQuery, parameters);
        }

        public int ExecuteCommand(string sqlCommand, params object[] parameters)
        {
            return DataContext.Database.ExecuteSqlCommand(sqlCommand, parameters);
        }
    }
}
