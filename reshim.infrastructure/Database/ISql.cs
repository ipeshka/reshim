﻿using System.Collections.Generic;

namespace reshim.data.infrastructure.Database
{
    public interface ISql
    {
        IEnumerable<T> ExecuteQuery<T>(string sqlQuery, params object[] parameters);
        int ExecuteCommand(string sqlCommand, params object[] parameters);
    }
}
