﻿using System;

namespace reshim.data.infrastructure.Database {
    public interface IDatabaseFactory : IDisposable {
        ReshimDataContex Get();
    }
}
