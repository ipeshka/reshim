﻿namespace reshim.data.infrastructure.Database {
    public interface IUnitOfWork {
        void Commit();
    }
}
