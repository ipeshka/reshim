﻿namespace reshim.data.infrastructure.Database {
    public class DatabaseFactory : Disposable, IDatabaseFactory {
        private ReshimDataContex _dataContext;
        public ReshimDataContex Get() {
            return _dataContext ?? (_dataContext = new ReshimDataContex());
        }
        protected override void DisposeCore() {
            if (_dataContext != null)
                _dataContext.Dispose();
        }
    }
}
