﻿using System.Data.Entity;
using reshim.data.infrastructure.Configurations;
using reshim.model.Entities;
using System;

namespace reshim.data.infrastructure
{
    public class ReshimDataContex : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Message> Messages { get; set; }
        public DbSet<Connection> Connections { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<PaymentData> PaymentsData { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<File> Files { get; set; }
        public DbSet<CompletedTask> CompletedTasks { get; set; }
        public DbSet<HistoryCompletedTask> HistoriesCompletedTasks { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public virtual void Commit()
        {
            try
            {
                base.SaveChanges();
            }
            catch (Exception eve)
            {
                System.IO.File.AppendAllText(@"d:\errors.txt", eve.HelpLink + "-" + eve.InnerException + "-" + eve.Message);
                throw;
            }
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new WithdrawalConfigurations());
            modelBuilder.Configurations.Add(new PaymentConfigurations());
            modelBuilder.Configurations.Add(new InternalPaymentConfigurations());
            modelBuilder.Configurations.Add(new PaymentDataConfigurations());
            modelBuilder.Configurations.Add(new MessageConfigurations());
            modelBuilder.Configurations.Add(new DialogConfigurations());
            modelBuilder.Configurations.Add(new ContentConfigurations());
            modelBuilder.Configurations.Add(new CategoryArticleConfigurations());
            modelBuilder.Configurations.Add(new ArticleConfigurations());
            modelBuilder.Configurations.Add(new NewsConfigurations());
            modelBuilder.Configurations.Add(new CategoryLessonConfigurations());
            modelBuilder.Configurations.Add(new NameLessonConfigurations());
            modelBuilder.Configurations.Add(new ChapterConfigurations());
            modelBuilder.Configurations.Add(new LessonConfigurations());
            modelBuilder.Configurations.Add(new ReviewConfigurations());
            modelBuilder.Configurations.Add(new SubjectConfigurations());
            modelBuilder.Configurations.Add(new TypeWorkConfigurations());
            modelBuilder.Configurations.Add(new SeoArticleConfigurations());
            modelBuilder.Configurations.Add(new TaskConfigurations());
            modelBuilder.Configurations.Add(new UserConfigurations());
            modelBuilder.Configurations.Add(new ConnectionConfigurations());
            modelBuilder.Configurations.Add(new FileConfigurations());
            modelBuilder.Configurations.Add(new ProfileConfigurations());
            modelBuilder.Configurations.Add(new SubscriberConfigurations());
            modelBuilder.Configurations.Add(new CompletedTaskConfiguration());
            modelBuilder.Configurations.Add(new HistoryCompletedTaskConfiguration());
            modelBuilder.Configurations.Add(new TagConfiguration());
        }
    }
}
