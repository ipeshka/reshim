namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateTableHistoriesCompletedTasks : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HistoriesCompletedTasks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Create = c.DateTime(nullable: false),
                        State = c.Int(nullable: false),
                        Currency = c.Int(),
                        PaywayVia = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        CompletedTaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CompletedTasks", t => t.CompletedTaskId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.CompletedTaskId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HistoriesCompletedTasks", "UserId", "dbo.Users");
            DropForeignKey("dbo.HistoriesCompletedTasks", "CompletedTaskId", "dbo.CompletedTasks");
            DropIndex("dbo.HistoriesCompletedTasks", new[] { "CompletedTaskId" });
            DropIndex("dbo.HistoriesCompletedTasks", new[] { "UserId" });
            DropTable("dbo.HistoriesCompletedTasks");
        }
    }
}
