namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Field_Sender_Recipient_Table_Message : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Messages", "SenderId", c => c.Int(nullable: false));
            AddColumn("dbo.Messages", "RecipientId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "SenderId");
            CreateIndex("dbo.Messages", "RecipientId");
            AddForeignKey("dbo.Messages", "RecipientId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Messages", "SenderId", "dbo.Users", "UserId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "SenderId", "dbo.Users");
            DropForeignKey("dbo.Messages", "RecipientId", "dbo.Users");
            DropIndex("dbo.Messages", new[] { "RecipientId" });
            DropIndex("dbo.Messages", new[] { "SenderId" });
            DropColumn("dbo.Messages", "RecipientId");
            DropColumn("dbo.Messages", "SenderId");
        }
    }
}
