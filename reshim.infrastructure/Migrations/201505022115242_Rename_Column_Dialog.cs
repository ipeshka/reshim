namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rename_Column_Dialog : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Dialogues", name: "RecipientId", newName: "User1Id");
            RenameColumn(table: "dbo.Dialogues", name: "SenderId", newName: "User2Id");
            RenameIndex(table: "dbo.Dialogues", name: "IX_RecipientId", newName: "IX_User1Id");
            RenameIndex(table: "dbo.Dialogues", name: "IX_SenderId", newName: "IX_User2Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Dialogues", name: "IX_User2Id", newName: "IX_SenderId");
            RenameIndex(table: "dbo.Dialogues", name: "IX_User1Id", newName: "IX_RecipientId");
            RenameColumn(table: "dbo.Dialogues", name: "User2Id", newName: "SenderId");
            RenameColumn(table: "dbo.Dialogues", name: "User1Id", newName: "RecipientId");
        }
    }
}
