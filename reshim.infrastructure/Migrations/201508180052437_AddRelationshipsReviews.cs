namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipsReviews : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents");
            RenameColumn(table: "dbo.Reviews", name: "Article_Id", newName: "ArticleId");
            RenameColumn(table: "dbo.Reviews", name: "Lesson_Id", newName: "LessonId");
            RenameIndex(table: "dbo.Reviews", name: "IX_Article_Id", newName: "IX_ArticleId");
            RenameIndex(table: "dbo.Reviews", name: "IX_Lesson_Id", newName: "IX_LessonId");
            AddForeignKey("dbo.Reviews", "LessonId", "dbo.Contents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "LessonId", "dbo.Contents");
            RenameIndex(table: "dbo.Reviews", name: "IX_LessonId", newName: "IX_Lesson_Id");
            RenameIndex(table: "dbo.Reviews", name: "IX_ArticleId", newName: "IX_Article_Id");
            RenameColumn(table: "dbo.Reviews", name: "LessonId", newName: "Lesson_Id");
            RenameColumn(table: "dbo.Reviews", name: "ArticleId", newName: "Article_Id");
            AddForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents", "Id", cascadeDelete: true);
        }
    }
}
