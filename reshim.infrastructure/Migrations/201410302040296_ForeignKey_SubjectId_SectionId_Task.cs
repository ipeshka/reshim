namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_SubjectId_SectionId_Task : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Tasks", name: "Section_SectionId", newName: "SectionId");
            RenameColumn(table: "dbo.Tasks", name: "Subject_SubjectId", newName: "SubjectId");
            RenameIndex(table: "dbo.Tasks", name: "IX_Subject_SubjectId", newName: "IX_SubjectId");
            RenameIndex(table: "dbo.Tasks", name: "IX_Section_SectionId", newName: "IX_SectionId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.Tasks", name: "IX_SectionId", newName: "IX_Section_SectionId");
            RenameIndex(table: "dbo.Tasks", name: "IX_SubjectId", newName: "IX_Subject_SubjectId");
            RenameColumn(table: "dbo.Tasks", name: "SubjectId", newName: "Subject_SubjectId");
            RenameColumn(table: "dbo.Tasks", name: "SectionId", newName: "Section_SectionId");
        }
    }
}
