namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTablePaymentData : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PaymentDatas", newName: "PaymentsData");
            RenameColumn(table: "dbo.PaymentsData", name: "PaymentDataId", newName: "Id");
            RenameIndex(table: "dbo.PaymentsData", name: "IX_PaymentDataId", newName: "IX_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.PaymentsData", name: "IX_Id", newName: "IX_PaymentDataId");
            RenameColumn(table: "dbo.PaymentsData", name: "Id", newName: "PaymentDataId");
            RenameTable(name: "dbo.PaymentsData", newName: "PaymentDatas");
        }
    }
}
