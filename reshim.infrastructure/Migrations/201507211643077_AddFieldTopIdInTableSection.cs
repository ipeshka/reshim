namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldTopIdInTableSection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "SectionTopId", c => c.Int());
            CreateIndex("dbo.BaseArticles", "SectionTopId");
            AddForeignKey("dbo.BaseArticles", "SectionTopId", "dbo.BaseArticles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaseArticles", "SectionTopId", "dbo.BaseArticles");
            DropIndex("dbo.BaseArticles", new[] { "SectionTopId" });
            DropColumn("dbo.BaseArticles", "SectionTopId");
        }
    }
}
