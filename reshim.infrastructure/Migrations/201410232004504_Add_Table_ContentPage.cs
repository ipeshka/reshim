namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Add_Table_ContentPage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContentPages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(nullable: false),
                        Header = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        DateUpdate = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            AlterColumn("dbo.ContentBases", "Header", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ContentBases", "Header", c => c.String(nullable: false, maxLength: 128));
            DropTable("dbo.ContentPages");
        }
    }
}
