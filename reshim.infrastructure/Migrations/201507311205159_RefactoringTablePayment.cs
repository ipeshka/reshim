namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTablePayment : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Invoices", "PaymentDataId", "dbo.PaymentDatas");
            //DropIndex("dbo.Invoices", new[] { "PaymentDataId" });
            //CreateTable(
            //    "dbo.Payments",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Create = c.DateTime(nullable: false),
            //            Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            State = c.Int(nullable: false),
            //            Currency = c.Int(nullable: false),
            //            PaywayVia = c.Int(nullable: false),
            //            PaymentDataId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.PaymentDatas", t => t.PaymentDataId, cascadeDelete: true)
            //    .Index(t => t.PaymentDataId);
            
            //DropTable("dbo.Invoices");

            RenameTable(name: "dbo.Invoices", newName: "Payments");
            RenameColumn("Payments", "InvoiceId", "Id");

        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        Create = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        State = c.Int(nullable: false),
                        Currency = c.Int(nullable: false),
                        PaywayVia = c.Int(nullable: false),
                        PaymentDataId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InvoiceId);
            
            DropForeignKey("dbo.Payments", "PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.Payments", new[] { "PaymentDataId" });
            DropTable("dbo.Payments");
            CreateIndex("dbo.Invoices", "PaymentDataId");
            AddForeignKey("dbo.Invoices", "PaymentDataId", "dbo.PaymentDatas", "PaymentDataId", cascadeDelete: true);
        }
    }
}
