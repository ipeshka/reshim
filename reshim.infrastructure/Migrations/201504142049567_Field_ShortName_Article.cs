namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Field_ShortName_Article : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "ShortName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseArticles", "ShortName");
        }
    }
}
