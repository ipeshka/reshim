namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_ArticleCategoryId_Article : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Articles", "Category_Id", "dbo.ArticleCategories");
            DropIndex("dbo.Articles", new[] { "Category_Id" });
            RenameColumn(table: "dbo.Articles", name: "Category_Id", newName: "ArticleCategoryId");
            AlterColumn("dbo.Articles", "ArticleCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Articles", "ArticleCategoryId");
            AddForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories");
            DropIndex("dbo.Articles", new[] { "ArticleCategoryId" });
            AlterColumn("dbo.Articles", "ArticleCategoryId", c => c.Int());
            RenameColumn(table: "dbo.Articles", name: "ArticleCategoryId", newName: "Category_Id");
            CreateIndex("dbo.Articles", "Category_Id");
            AddForeignKey("dbo.Articles", "Category_Id", "dbo.ArticleCategories", "Id");
        }
    }
}
