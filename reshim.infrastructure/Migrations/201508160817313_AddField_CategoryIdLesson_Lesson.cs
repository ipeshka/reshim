namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddField_CategoryIdLesson_Lesson : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Contents", name: "NameLessonId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.Contents", name: "NameLessonId1", newName: "NameLessonId");
            RenameColumn(table: "dbo.Contents", name: "__mig_tmp__0", newName: "NameLessonId1");
            RenameIndex(table: "dbo.Contents", name: "IX_NameLessonId1", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.Contents", name: "IX_NameLessonId", newName: "IX_NameLessonId1");
            RenameIndex(table: "dbo.Contents", name: "__mig_tmp__0", newName: "IX_NameLessonId");
            AddColumn("dbo.Contents", "CategoryIdLesson", c => c.Int());
            CreateIndex("dbo.Contents", "CategoryIdLesson");
            AddForeignKey("dbo.Contents", "CategoryIdLesson", "dbo.Contents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "CategoryIdLesson", "dbo.Contents");
            DropIndex("dbo.Contents", new[] { "CategoryIdLesson" });
            DropColumn("dbo.Contents", "CategoryIdLesson");
            RenameIndex(table: "dbo.Contents", name: "IX_NameLessonId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.Contents", name: "IX_NameLessonId1", newName: "IX_NameLessonId");
            RenameIndex(table: "dbo.Contents", name: "__mig_tmp__0", newName: "IX_NameLessonId1");
            RenameColumn(table: "dbo.Contents", name: "NameLessonId1", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.Contents", name: "NameLessonId", newName: "NameLessonId1");
            RenameColumn(table: "dbo.Contents", name: "__mig_tmp__0", newName: "NameLessonId");
        }
    }
}
