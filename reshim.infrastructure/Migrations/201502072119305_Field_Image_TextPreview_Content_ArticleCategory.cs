namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Field_Image_TextPreview_Content_ArticleCategory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArticleCategories", "Image", c => c.String());
            AddColumn("dbo.ArticleCategories", "TextPreview", c => c.String());
            AddColumn("dbo.ArticleCategories", "Content", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ArticleCategories", "Content");
            DropColumn("dbo.ArticleCategories", "TextPreview");
            DropColumn("dbo.ArticleCategories", "Image");
        }
    }
}
