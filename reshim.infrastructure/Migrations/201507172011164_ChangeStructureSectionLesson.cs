namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureSectionLesson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "ParentId2", c => c.Int());
            CreateIndex("dbo.BaseArticles", "ParentId2");
            AddForeignKey("dbo.BaseArticles", "ParentId2", "dbo.BaseArticles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaseArticles", "ParentId2", "dbo.BaseArticles");
            DropIndex("dbo.BaseArticles", new[] { "ParentId2" });
            DropColumn("dbo.BaseArticles", "ParentId2");
        }
    }
}
