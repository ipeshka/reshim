namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReviewsToTableCompletedTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reviews", "CompletedTaskId", c => c.Int());
            CreateIndex("dbo.Reviews", "CompletedTaskId");
            AddForeignKey("dbo.Reviews", "CompletedTaskId", "dbo.CompletedTasks", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "CompletedTaskId", "dbo.CompletedTasks");
            DropIndex("dbo.Reviews", new[] { "CompletedTaskId" });
            DropColumn("dbo.Reviews", "CompletedTaskId");
        }
    }
}
