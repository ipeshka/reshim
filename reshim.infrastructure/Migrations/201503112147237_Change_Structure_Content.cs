namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Structure_Content : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaseArticles", "TitleSeo", c => c.String());
            AlterColumn("dbo.BaseArticles", "DescriptionSeo", c => c.String());
            AlterColumn("dbo.BaseArticles", "KeywordsSeo", c => c.String());
            AlterColumn("dbo.BaseArticles", "Content", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaseArticles", "Content", c => c.String(nullable: false));
            AlterColumn("dbo.BaseArticles", "KeywordsSeo", c => c.String(nullable: false));
            AlterColumn("dbo.BaseArticles", "DescriptionSeo", c => c.String(nullable: false));
            AlterColumn("dbo.BaseArticles", "TitleSeo", c => c.String(nullable: false));
        }
    }
}
