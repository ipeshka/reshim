namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTableInternalPayment : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.InsidePayments", "PaymentDataId", "dbo.PaymentDatas");
            //DropForeignKey("dbo.InsidePayments", "TaskId", "dbo.Tasks");
            //DropIndex("dbo.InsidePayments", new[] { "TaskId" });
            //DropIndex("dbo.InsidePayments", new[] { "PaymentDataId" });
            //CreateTable(
            //    "dbo.InternalPayments",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
            //            DatePayment = c.DateTime(nullable: false),
            //            TaskId = c.Int(nullable: false),
            //            PaymentDataId = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.PaymentDatas", t => t.PaymentDataId, cascadeDelete: true)
            //    .ForeignKey("dbo.Tasks", t => t.PaymentDataId, cascadeDelete: true)
            //    .Index(t => t.PaymentDataId);
            
            //DropTable("dbo.InsidePayments");

            RenameTable(name: "dbo.InsidePayments", newName: "InternalPayments");
            RenameColumn("InternalPayments", "InsidePaymentId", "Id");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.InsidePayments",
                c => new
                    {
                        InsidePaymentId = c.Int(nullable: false, identity: true),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DatePayment = c.DateTime(nullable: false),
                        TaskId = c.Int(nullable: false),
                        PaymentDataId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InsidePaymentId);
            
            DropForeignKey("dbo.InternalPayments", "PaymentDataId", "dbo.Tasks");
            DropForeignKey("dbo.InternalPayments", "PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.InternalPayments", new[] { "PaymentDataId" });
            DropTable("dbo.InternalPayments");
            CreateIndex("dbo.InsidePayments", "PaymentDataId");
            CreateIndex("dbo.InsidePayments", "TaskId");
            AddForeignKey("dbo.InsidePayments", "TaskId", "dbo.Tasks", "TaskId", cascadeDelete: true);
            AddForeignKey("dbo.InsidePayments", "PaymentDataId", "dbo.PaymentDatas", "PaymentDataId", cascadeDelete: true);
        }
    }
}
