namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_TaskId_PaymentDataId_InsidePayment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InsidePayments", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropForeignKey("dbo.InsidePayments", "Task_TaskId", "dbo.Tasks");
            DropIndex("dbo.InsidePayments", new[] { "PaymentData_PaymentDataId" });
            DropIndex("dbo.InsidePayments", new[] { "Task_TaskId" });
            RenameColumn(table: "dbo.InsidePayments", name: "PaymentData_PaymentDataId", newName: "PaymentDataId");
            RenameColumn(table: "dbo.InsidePayments", name: "Task_TaskId", newName: "TaskId");
            AlterColumn("dbo.InsidePayments", "PaymentDataId", c => c.Int(nullable: false));
            AlterColumn("dbo.InsidePayments", "TaskId", c => c.Int(nullable: false));
            CreateIndex("dbo.InsidePayments", "TaskId");
            CreateIndex("dbo.InsidePayments", "PaymentDataId");
            AddForeignKey("dbo.InsidePayments", "PaymentDataId", "dbo.PaymentDatas", "PaymentDataId", cascadeDelete: true);
            AddForeignKey("dbo.InsidePayments", "TaskId", "dbo.Tasks", "TaskId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InsidePayments", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.InsidePayments", "PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.InsidePayments", new[] { "PaymentDataId" });
            DropIndex("dbo.InsidePayments", new[] { "TaskId" });
            AlterColumn("dbo.InsidePayments", "TaskId", c => c.Int());
            AlterColumn("dbo.InsidePayments", "PaymentDataId", c => c.Int());
            RenameColumn(table: "dbo.InsidePayments", name: "TaskId", newName: "Task_TaskId");
            RenameColumn(table: "dbo.InsidePayments", name: "PaymentDataId", newName: "PaymentData_PaymentDataId");
            CreateIndex("dbo.InsidePayments", "Task_TaskId");
            CreateIndex("dbo.InsidePayments", "PaymentData_PaymentDataId");
            AddForeignKey("dbo.InsidePayments", "Task_TaskId", "dbo.Tasks", "TaskId");
            AddForeignKey("dbo.InsidePayments", "PaymentData_PaymentDataId", "dbo.PaymentDatas", "PaymentDataId");
        }
    }
}
