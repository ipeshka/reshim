namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureArticle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ArticleCategories", "TitleSeo", c => c.String(nullable: false));
            AddColumn("dbo.ArticleCategories", "DescriptionSeo", c => c.String(nullable: false));
            AddColumn("dbo.ArticleCategories", "KeywordsSeo", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "Name", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "TitleSeo", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "DescriptionSeo", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "KeywordsSeo", c => c.String(nullable: false));
            DropColumn("dbo.Articles", "Title");
            DropColumn("dbo.Articles", "Header");
            DropColumn("dbo.Articles", "Description");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Articles", "Description", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "Header", c => c.String(nullable: false));
            AddColumn("dbo.Articles", "Title", c => c.String(nullable: false));
            DropColumn("dbo.Articles", "KeywordsSeo");
            DropColumn("dbo.Articles", "DescriptionSeo");
            DropColumn("dbo.Articles", "TitleSeo");
            DropColumn("dbo.Articles", "Name");
            DropColumn("dbo.ArticleCategories", "KeywordsSeo");
            DropColumn("dbo.ArticleCategories", "DescriptionSeo");
            DropColumn("dbo.ArticleCategories", "TitleSeo");
        }
    }
}
