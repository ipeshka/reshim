namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Field_Preview_Content : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "Preview", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseArticles", "Preview");
        }
    }
}
