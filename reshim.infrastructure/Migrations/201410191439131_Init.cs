namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        Email = c.String(),
                        Login = c.String(),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        PasswordHash = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        LastLoginTime = c.DateTime(),
                        Activated = c.Boolean(nullable: false),
                        IsService = c.Int(nullable: false),
                        IdService = c.String(),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "dbo.Tasks",
                c => new
                    {
                        TaskId = c.Int(nullable: false, identity: true),
                        DateCreated = c.DateTime(nullable: false),
                        DateOfExecution = c.DateTime(nullable: false),
                        DateComplited = c.DateTime(nullable: false),
                        Cost = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Comment = c.String(),
                        Status = c.Int(nullable: false),
                        Condition_FileId = c.Int(),
                        Solution_FileId = c.Int(),
                        Decided_UserId = c.Int(),
                        Subject_SubjectId = c.Int(),
                        Section_SectionId = c.Int(),
                        Customer_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.TaskId)
                .ForeignKey("dbo.Files", t => t.Condition_FileId)
                .ForeignKey("dbo.Files", t => t.Solution_FileId)
                .ForeignKey("dbo.Users", t => t.Decided_UserId)
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectId)
                .ForeignKey("dbo.Sections", t => t.Section_SectionId)
                .ForeignKey("dbo.Users", t => t.Customer_UserId)
                .Index(t => t.Condition_FileId)
                .Index(t => t.Solution_FileId)
                .Index(t => t.Decided_UserId)
                .Index(t => t.Subject_SubjectId)
                .Index(t => t.Section_SectionId)
                .Index(t => t.Customer_UserId);
            
            CreateTable(
                "dbo.Files",
                c => new
                    {
                        FileId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Path = c.String(),
                        TypeFile = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.FileId);
            
            CreateTable(
                "dbo.Subscribers",
                c => new
                    {
                        SubscriberId = c.Int(nullable: false, identity: true),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        User_UserId = c.Int(),
                        Task_TaskId = c.Int(),
                    })
                .PrimaryKey(t => t.SubscriberId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .ForeignKey("dbo.Tasks", t => t.Task_TaskId)
                .Index(t => t.User_UserId)
                .Index(t => t.Task_TaskId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectId);
            
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        SectionId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Subject_SubjectId = c.Int(),
                    })
                .PrimaryKey(t => t.SectionId)
                .ForeignKey("dbo.Subjects", t => t.Subject_SubjectId)
                .Index(t => t.Subject_SubjectId);
            
            CreateTable(
                "dbo.InsidePayments",
                c => new
                    {
                        InsidePaymentId = c.Int(nullable: false, identity: true),
                        DatePayment = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Task_TaskId = c.Int(),
                        PaymentData_PaymentDataId = c.Int(),
                    })
                .PrimaryKey(t => t.InsidePaymentId)
                .ForeignKey("dbo.Tasks", t => t.Task_TaskId)
                .ForeignKey("dbo.PaymentDatas", t => t.PaymentData_PaymentDataId)
                .Index(t => t.Task_TaskId)
                .Index(t => t.PaymentData_PaymentDataId);
            
            CreateTable(
                "dbo.PaymentDatas",
                c => new
                    {
                        PaymentDataId = c.Int(nullable: false),
                        Ballance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PaymentDataId)
                .ForeignKey("dbo.Users", t => t.PaymentDataId, cascadeDelete: true)
                .Index(t => t.PaymentDataId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        Create = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        State = c.Int(nullable: false),
                        Currency = c.Int(nullable: false),
                        PaywayVia = c.Int(nullable: false),
                        PaymentData_PaymentDataId = c.Int(),
                    })
                .PrimaryKey(t => t.InvoiceId)
                .ForeignKey("dbo.PaymentDatas", t => t.PaymentData_PaymentDataId)
                .Index(t => t.PaymentData_PaymentDataId);
            
            CreateTable(
                "dbo.Withdrawals",
                c => new
                    {
                        WithdrawalId = c.Int(nullable: false, identity: true),
                        Create = c.DateTime(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Purse = c.String(),
                        Comment = c.String(),
                        State = c.Int(nullable: false),
                        PaywayVia = c.Int(nullable: false),
                        PaymentData_PaymentDataId = c.Int(),
                    })
                .PrimaryKey(t => t.WithdrawalId)
                .ForeignKey("dbo.PaymentDatas", t => t.PaymentData_PaymentDataId)
                .Index(t => t.PaymentData_PaymentDataId);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        DateCreate = c.DateTime(nullable: false),
                        IsNew = c.Boolean(nullable: false),
                        From_UserId = c.Int(),
                        To_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.MessageId)
                .ForeignKey("dbo.Users", t => t.From_UserId)
                .ForeignKey("dbo.Users", t => t.To_UserId)
                .Index(t => t.From_UserId)
                .Index(t => t.To_UserId);
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionId = c.String(nullable: false, maxLength: 128),
                        UserAgent = c.String(),
                        Connected = c.Boolean(nullable: false),
                        User_UserId = c.Int(),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.Users", t => t.User_UserId)
                .Index(t => t.User_UserId);
            
            CreateTable(
                "dbo.Profiles",
                c => new
                    {
                        ProfileId = c.Int(nullable: false),
                        Icq = c.String(),
                        Phone = c.String(),
                        Skype = c.String(),
                        UrlVk = c.String(),
                        Country = c.String(),
                        City = c.String(),
                        PlaceOfStudy = c.String(),
                        DateOfBirth = c.DateTime(),
                        Comments = c.String(),
                        Photo_FileId = c.Int(),
                    })
                .PrimaryKey(t => t.ProfileId)
                .ForeignKey("dbo.Files", t => t.Photo_FileId)
                .ForeignKey("dbo.Users", t => t.ProfileId, cascadeDelete: true)
                .Index(t => t.Photo_FileId)
                .Index(t => t.ProfileId);
            
            CreateTable(
                "dbo.ContentBases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(nullable: false, maxLength: 128),
                        Content = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PaymentDatas", "PaymentDataId", "dbo.Users");
            DropForeignKey("dbo.Profiles", "ProfileId", "dbo.Users");
            DropForeignKey("dbo.Profiles", "Photo_FileId", "dbo.Files");
            DropForeignKey("dbo.Connections", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Messages", "To_UserId", "dbo.Users");
            DropForeignKey("dbo.Messages", "From_UserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "Customer_UserId", "dbo.Users");
            DropForeignKey("dbo.InsidePayments", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropForeignKey("dbo.Withdrawals", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropForeignKey("dbo.Invoices", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropForeignKey("dbo.InsidePayments", "Task_TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Tasks", "Section_SectionId", "dbo.Sections");
            DropForeignKey("dbo.Tasks", "Subject_SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Sections", "Subject_SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Subscribers", "Task_TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Subscribers", "User_UserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "Decided_UserId", "dbo.Users");
            DropForeignKey("dbo.Tasks", "Solution_FileId", "dbo.Files");
            DropForeignKey("dbo.Tasks", "Condition_FileId", "dbo.Files");
            DropIndex("dbo.PaymentDatas", new[] { "PaymentDataId" });
            DropIndex("dbo.Profiles", new[] { "ProfileId" });
            DropIndex("dbo.Profiles", new[] { "Photo_FileId" });
            DropIndex("dbo.Connections", new[] { "User_UserId" });
            DropIndex("dbo.Messages", new[] { "To_UserId" });
            DropIndex("dbo.Messages", new[] { "From_UserId" });
            DropIndex("dbo.Tasks", new[] { "Customer_UserId" });
            DropIndex("dbo.InsidePayments", new[] { "PaymentData_PaymentDataId" });
            DropIndex("dbo.Withdrawals", new[] { "PaymentData_PaymentDataId" });
            DropIndex("dbo.Invoices", new[] { "PaymentData_PaymentDataId" });
            DropIndex("dbo.InsidePayments", new[] { "Task_TaskId" });
            DropIndex("dbo.Tasks", new[] { "Section_SectionId" });
            DropIndex("dbo.Tasks", new[] { "Subject_SubjectId" });
            DropIndex("dbo.Sections", new[] { "Subject_SubjectId" });
            DropIndex("dbo.Subscribers", new[] { "Task_TaskId" });
            DropIndex("dbo.Subscribers", new[] { "User_UserId" });
            DropIndex("dbo.Tasks", new[] { "Decided_UserId" });
            DropIndex("dbo.Tasks", new[] { "Solution_FileId" });
            DropIndex("dbo.Tasks", new[] { "Condition_FileId" });
            DropTable("dbo.ContentBases");
            DropTable("dbo.Profiles");
            DropTable("dbo.Connections");
            DropTable("dbo.Messages");
            DropTable("dbo.Withdrawals");
            DropTable("dbo.Invoices");
            DropTable("dbo.PaymentDatas");
            DropTable("dbo.InsidePayments");
            DropTable("dbo.Sections");
            DropTable("dbo.Subjects");
            DropTable("dbo.Subscribers");
            DropTable("dbo.Files");
            DropTable("dbo.Tasks");
            DropTable("dbo.Users");
        }
    }
}
