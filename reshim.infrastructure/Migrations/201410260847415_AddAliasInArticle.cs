namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class AddAliasInArticle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Articles", "Alias", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Articles", "Alias");
        }
    }
}
