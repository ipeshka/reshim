namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRelationshipTableLesson_NameLesson : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "NameLessonId1", c => c.Int());
            CreateIndex("dbo.Contents", "NameLessonId1");
            AddForeignKey("dbo.Contents", "NameLessonId1", "dbo.Contents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "NameLessonId1", "dbo.Contents");
            DropIndex("dbo.Contents", new[] { "NameLessonId1" });
            DropColumn("dbo.Contents", "NameLessonId1");
        }
    }
}
