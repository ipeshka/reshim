namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete_Table_Messages : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Messages", "ToId", "dbo.Users");
            DropForeignKey("dbo.Messages", "FromId", "dbo.Users");
            DropIndex("dbo.Messages", new[] { "FromId" });
            DropIndex("dbo.Messages", new[] { "ToId" });
            DropTable("dbo.Messages");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        MessageId = c.Int(nullable: false, identity: true),
                        Content = c.String(),
                        IsNew = c.Boolean(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        FromId = c.Int(nullable: false),
                        ToId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MessageId);
            
            CreateIndex("dbo.Messages", "ToId");
            CreateIndex("dbo.Messages", "FromId");
            AddForeignKey("dbo.Messages", "FromId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Messages", "ToId", "dbo.Users", "UserId");
        }
    }
}
