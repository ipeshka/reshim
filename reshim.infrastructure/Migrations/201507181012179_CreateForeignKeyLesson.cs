namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class CreateForeignKeyLesson : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.BaseArticles", name: "Section_Id", newName: "SectionId");
            RenameIndex(table: "dbo.BaseArticles", name: "IX_Section_Id", newName: "IX_SectionId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.BaseArticles", name: "IX_SectionId", newName: "IX_Section_Id");
            RenameColumn(table: "dbo.BaseArticles", name: "SectionId", newName: "Section_Id");
        }
    }
}
