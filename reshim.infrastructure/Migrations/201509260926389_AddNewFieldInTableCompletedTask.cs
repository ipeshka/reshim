namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewFieldInTableCompletedTask : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CompletedTasks", "Name", c => c.String());
            AddColumn("dbo.CompletedTasks", "TitleSeo", c => c.String());
            AddColumn("dbo.CompletedTasks", "DescriptionSeo", c => c.String());
            AddColumn("dbo.CompletedTasks", "KeywordsSeo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CompletedTasks", "KeywordsSeo");
            DropColumn("dbo.CompletedTasks", "DescriptionSeo");
            DropColumn("dbo.CompletedTasks", "TitleSeo");
            DropColumn("dbo.CompletedTasks", "Name");
        }
    }
}
