using System.Linq;

namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<ReshimDataContex>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "reshim.data.infrastructure.ReshimDataContex";
        }

        protected override void Seed(ReshimDataContex context)
        {
            if (context.Users.Any()) return;
            context.SaveChanges();

        }
    }
}
