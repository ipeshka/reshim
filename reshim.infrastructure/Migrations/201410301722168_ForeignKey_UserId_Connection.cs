namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_UserId_Connection : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Connections", "User_UserId", "dbo.Users");
            DropIndex("dbo.Connections", new[] { "User_UserId" });
            RenameColumn(table: "dbo.Connections", name: "User_UserId", newName: "UserId");
            AlterColumn("dbo.Connections", "UserId", c => c.Int(nullable: false));
            CreateIndex("dbo.Connections", "UserId");
            AddForeignKey("dbo.Connections", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Connections", "UserId", "dbo.Users");
            DropIndex("dbo.Connections", new[] { "UserId" });
            AlterColumn("dbo.Connections", "UserId", c => c.Int());
            RenameColumn(table: "dbo.Connections", name: "UserId", newName: "User_UserId");
            CreateIndex("dbo.Connections", "User_UserId");
            AddForeignKey("dbo.Connections", "User_UserId", "dbo.Users", "UserId");
        }
    }
}
