namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete_Field_SubjectId_SeoArticle : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.BaseArticles", "SubjectId", "dbo.BaseArticles");
            DropIndex("dbo.BaseArticles", new[] { "SubjectId" });
            DropColumn("dbo.BaseArticles", "SubjectId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BaseArticles", "SubjectId", c => c.Int());
            CreateIndex("dbo.BaseArticles", "SubjectId");
            AddForeignKey("dbo.BaseArticles", "SubjectId", "dbo.BaseArticles", "Id");
        }
    }
}
