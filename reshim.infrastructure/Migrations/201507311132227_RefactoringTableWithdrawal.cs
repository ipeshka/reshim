namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTableWithdrawal : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Withdrawals", newName: "Withdrawels");
            //DropPrimaryKey("dbo.Withdrawels");
            //AddColumn("dbo.Withdrawels", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Withdrawels", "Purse", c => c.String(nullable: false));
            AlterColumn("dbo.Withdrawels", "Comment", c => c.String(nullable: true));
            RenameColumn("Withdrawels", "WithdrawalId", "Id");
            //AddPrimaryKey("dbo.Withdrawels", "Id");
            //DropColumn("dbo.Withdrawels", "WithdrawalId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Withdrawels", "WithdrawalId", c => c.Int(nullable: false, identity: true));
            DropPrimaryKey("dbo.Withdrawels");
            AlterColumn("dbo.Withdrawels", "Comment", c => c.String());
            AlterColumn("dbo.Withdrawels", "Purse", c => c.String());
            DropColumn("dbo.Withdrawels", "Id");
            AddPrimaryKey("dbo.Withdrawels", "WithdrawalId");
            RenameTable(name: "dbo.Withdrawels", newName: "Withdrawals");
        }
    }
}
