namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChnageFieldSolutionCompletedTask : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CompletedTasks", "ConditionId", "dbo.Files");
            DropIndex("dbo.CompletedTasks", new[] { "ConditionId" });
            Sql("ALTER TABLE dbo.CompletedTasks DROP CONSTRAINT [FK_dbo.CompletedTasks_dbo.Files_Condition_FileId]");
            AddColumn("dbo.CompletedTasks", "Condition", c => c.String());
            DropColumn("dbo.CompletedTasks", "ConditionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.CompletedTasks", "ConditionId", c => c.Int(nullable: false));
            DropColumn("dbo.CompletedTasks", "Condition");
            CreateIndex("dbo.CompletedTasks", "ConditionId");
            AddForeignKey("dbo.CompletedTasks", "ConditionId", "dbo.Files", "FileId");
        }
    }
}
