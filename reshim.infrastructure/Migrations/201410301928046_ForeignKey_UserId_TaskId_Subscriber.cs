namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_UserId_TaskId_Subscriber : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Subscribers", "Task_TaskId", "dbo.Tasks");
            DropForeignKey("dbo.Subscribers", "User_UserId", "dbo.Users");
            DropIndex("dbo.Subscribers", new[] { "User_UserId" });
            DropIndex("dbo.Subscribers", new[] { "Task_TaskId" });
            RenameColumn(table: "dbo.Subscribers", name: "Task_TaskId", newName: "TaskId");
            RenameColumn(table: "dbo.Subscribers", name: "User_UserId", newName: "UserId");
            AlterColumn("dbo.Subscribers", "UserId", c => c.Int(nullable: false));
            AlterColumn("dbo.Subscribers", "TaskId", c => c.Int(nullable: false));
            CreateIndex("dbo.Subscribers", "UserId");
            CreateIndex("dbo.Subscribers", "TaskId");
            AddForeignKey("dbo.Subscribers", "TaskId", "dbo.Tasks", "TaskId", cascadeDelete: true);
            AddForeignKey("dbo.Subscribers", "UserId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Subscribers", "UserId", "dbo.Users");
            DropForeignKey("dbo.Subscribers", "TaskId", "dbo.Tasks");
            DropIndex("dbo.Subscribers", new[] { "TaskId" });
            DropIndex("dbo.Subscribers", new[] { "UserId" });
            AlterColumn("dbo.Subscribers", "TaskId", c => c.Int());
            AlterColumn("dbo.Subscribers", "UserId", c => c.Int());
            RenameColumn(table: "dbo.Subscribers", name: "UserId", newName: "User_UserId");
            RenameColumn(table: "dbo.Subscribers", name: "TaskId", newName: "Task_TaskId");
            CreateIndex("dbo.Subscribers", "Task_TaskId");
            CreateIndex("dbo.Subscribers", "User_UserId");
            AddForeignKey("dbo.Subscribers", "User_UserId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Subscribers", "Task_TaskId", "dbo.Tasks", "TaskId");
        }
    }
}
