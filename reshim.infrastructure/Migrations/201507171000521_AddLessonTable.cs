namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLessonTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "ParentId1", c => c.Int());
            AddColumn("dbo.BaseArticles", "CategoryId1", c => c.Int());
            AddColumn("dbo.BaseArticles", "Section_Id", c => c.Int());
            AddColumn("dbo.Reviews", "Lesson_Id", c => c.Int());
            CreateIndex("dbo.BaseArticles", "ParentId1");
            CreateIndex("dbo.BaseArticles", "CategoryId1");
            CreateIndex("dbo.BaseArticles", "Section_Id");
            CreateIndex("dbo.Reviews", "Lesson_Id");
            AddForeignKey("dbo.BaseArticles", "ParentId1", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.Reviews", "Lesson_Id", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.BaseArticles", "CategoryId1", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.BaseArticles", "Section_Id", "dbo.BaseArticles", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BaseArticles", "Section_Id", "dbo.BaseArticles");
            DropForeignKey("dbo.BaseArticles", "CategoryId1", "dbo.BaseArticles");
            DropForeignKey("dbo.Reviews", "Lesson_Id", "dbo.BaseArticles");
            DropForeignKey("dbo.BaseArticles", "ParentId1", "dbo.BaseArticles");
            DropIndex("dbo.Reviews", new[] { "Lesson_Id" });
            DropIndex("dbo.BaseArticles", new[] { "Section_Id" });
            DropIndex("dbo.BaseArticles", new[] { "CategoryId1" });
            DropIndex("dbo.BaseArticles", new[] { "ParentId1" });
            DropColumn("dbo.Reviews", "Lesson_Id");
            DropColumn("dbo.BaseArticles", "Section_Id");
            DropColumn("dbo.BaseArticles", "CategoryId1");
            DropColumn("dbo.BaseArticles", "ParentId1");
        }
    }
}
