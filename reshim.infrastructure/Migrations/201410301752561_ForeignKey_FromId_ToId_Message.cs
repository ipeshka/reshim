namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_FromId_ToId_Message : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.Messages", new[] { "To_UserId" });
            DropIndex("dbo.Messages", new[] { "From_UserId" });
            RenameColumn(table: "dbo.Messages", name: "To_UserId", newName: "ToId");
            RenameColumn(table: "dbo.Messages", name: "From_UserId", newName: "FromId");
            AlterColumn("dbo.Messages", "ToId", c => c.Int(nullable: false));
            AlterColumn("dbo.Messages", "FromId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "FromId");
            CreateIndex("dbo.Messages", "ToId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.Messages", new[] { "ToId" });
            DropIndex("dbo.Messages", new[] { "FromId" });
            AlterColumn("dbo.Messages", "FromId", c => c.Int());
            AlterColumn("dbo.Messages", "ToId", c => c.Int());
            RenameColumn(table: "dbo.Messages", name: "FromId", newName: "From_UserId");
            RenameColumn(table: "dbo.Messages", name: "ToId", newName: "To_UserId");
            CreateIndex("dbo.Messages", "From_UserId");
            CreateIndex("dbo.Messages", "To_UserId");
        }
    }
}
