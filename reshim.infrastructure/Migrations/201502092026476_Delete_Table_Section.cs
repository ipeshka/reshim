namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Delete_Table_Section : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sections", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Tasks", "SectionId", "dbo.Sections");
            DropIndex("dbo.Tasks", new[] { "SectionId" });
            DropIndex("dbo.Sections", new[] { "SubjectId" });
            Sql("ALTER TABLE dbo.Tasks DROP CONSTRAINT [FK_dbo.Tasks_dbo.Sections_Section_SectionId]");
            DropColumn("dbo.Tasks", "SectionId");
            DropTable("dbo.Sections");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Sections",
                c => new
                    {
                        SectionId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SectionId);
            
            AddColumn("dbo.Tasks", "SectionId", c => c.Int());
            CreateIndex("dbo.Sections", "SubjectId");
            CreateIndex("dbo.Tasks", "SectionId");
            AddForeignKey("dbo.Tasks", "SectionId", "dbo.Sections", "SectionId");
            AddForeignKey("dbo.Sections", "SubjectId", "dbo.Subjects", "SubjectId", cascadeDelete: true);
        }
    }
}
