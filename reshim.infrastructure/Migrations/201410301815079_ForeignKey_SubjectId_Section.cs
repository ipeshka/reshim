namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_SubjectId_Section : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Sections", "Subject_SubjectId", "dbo.Subjects");
            DropIndex("dbo.Sections", new[] { "Subject_SubjectId" });
            RenameColumn(table: "dbo.Sections", name: "Subject_SubjectId", newName: "SubjectId");
            AlterColumn("dbo.Sections", "SubjectId", c => c.Int(nullable: false));
            CreateIndex("dbo.Sections", "SubjectId");
            AddForeignKey("dbo.Sections", "SubjectId", "dbo.Subjects", "SubjectId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Sections", "SubjectId", "dbo.Subjects");
            DropIndex("dbo.Sections", new[] { "SubjectId" });
            AlterColumn("dbo.Sections", "SubjectId", c => c.Int());
            RenameColumn(table: "dbo.Sections", name: "SubjectId", newName: "Subject_SubjectId");
            CreateIndex("dbo.Sections", "Subject_SubjectId");
            AddForeignKey("dbo.Sections", "Subject_SubjectId", "dbo.Subjects", "SubjectId");
        }
    }
}
