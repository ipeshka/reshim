﻿namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Structure_Articles : DbMigration
    {
        public override void Up()
        {
            Sql("DELETE FROM dbo.ArticleCategories");
            Sql("DELETE FROM dbo.Articles");

            DropForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories");
            DropForeignKey("dbo.ArticleCategories", "ParentId", "dbo.ArticleCategories");
            RenameTable(name: "dbo.Articles", newName: "BaseArticles");
            
            
            DropIndex("dbo.ArticleCategories", new[] { "ParentId" });
            DropIndex("dbo.BaseArticles", new[] { "ArticleCategoryId" });
            RenameColumn(table: "dbo.BaseArticles", name: "ArticleCategoryId", newName: "CategoryId");
            AddColumn("dbo.BaseArticles", "ParentId", c => c.Int());
            AddColumn("dbo.BaseArticles", "TypeWorkId", c => c.Int());
            AddColumn("dbo.BaseArticles", "SubjectId", c => c.Int());
            AddColumn("dbo.BaseArticles", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddColumn("dbo.Tasks", "TypeWorkId", c => c.Int());
            AlterColumn("dbo.BaseArticles", "CategoryId", c => c.Int());
            CreateIndex("dbo.BaseArticles", "CategoryId");
            CreateIndex("dbo.BaseArticles", "ParentId");
            CreateIndex("dbo.BaseArticles", "TypeWorkId");
            CreateIndex("dbo.BaseArticles", "SubjectId");
            CreateIndex("dbo.Tasks", "TypeWorkId");
            AddForeignKey("dbo.BaseArticles", "ParentId", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.BaseArticles", "SubjectId", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.BaseArticles", "TypeWorkId", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.Tasks", "TypeWorkId", "dbo.BaseArticles", "Id");
            AddForeignKey("dbo.BaseArticles", "CategoryId", "dbo.BaseArticles", "Id");

            Sql("ALTER TABLE dbo.Tasks DROP CONSTRAINT [FK_dbo.Tasks_dbo.Subjects_Subject_SubjectId]");
            DropTable("dbo.ArticleCategories");
            DropTable("dbo.Subjects");
            DropTable("dbo.ContentBases");

            Sql("SET IDENTITY_INSERT [dbo].[BaseArticles] ON");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (7, 'programirovanie', N'Программирование', N'Программирование', N'Программирование', N'Программирование', N'Программирование', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (10, 'fizika', N'Физика', N'Физика', N'Физика', N'Физика', N'Физика', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (11, 'himiaj', N'Химия', N'Химия', N'Химия', N'Химия', N'Химия', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (9, 'electrotechnika', N'ТОЭ (Элетротехника)', N'ТОЭ (Элетротехника)', N'ТОЭ (Элетротехника)', N'ТОЭ (Элетротехника)', N'ТОЭ (Элетротехника)', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (1, 'angliskij_jazik', N'Английский язык', N'Английский язык', N'Английский язык', N'Английский язык', N'Английский язык', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (4, 'informatika', N'Информатика', N'Информатика', N'Информатика', N'Информатика', N'Информатика', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (5, 'matematika', N'Математика', N'Математика', N'Математика', N'Математика', N'Математика', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (38, 'raznoe', N'Разное', N'Разное', N'Разное', N'Разное', N'Разное', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (43, 'ekonamika', N'Экономика', N'Экономика', N'Экономика', N'Экономика', N'Экономика', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (16, 'byh_ychet', N'Бухгалтерский учет', N'Бухгалтерский учет', N'Бухгалтерский учет', N'Бухгалтерский учет', N'Бухгалтерский учет', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (2, 'biologiaj', N'Биология', N'Биология', N'Биология', N'Биология', N'Биология', '', 'Subject', '2014-11-08 13:08:18.503')");

            Sql("INSERT [dbo].[BaseArticles] ([Id], [Alias], [Name], [TitleSeo], [DescriptionSeo], [KeywordsSeo], [Content], [Image], [Discriminator], [DateCreate])" +
            " VALUES (100, 'zadacha', N'Задача', N'Задача', N'Задача', N'Задача', N'Задача', '', 'TypeWork', '2014-11-08 13:08:18.503')");

            Sql("UPDATE [dbo].[Tasks] SET TypeWorkId=100");

            AddForeignKey("dbo.Tasks", "SubjectId", "dbo.BaseArticles", "Id");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ContentBases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(nullable: false),
                        Content = c.String(nullable: false),
                        DateCreate = c.DateTime(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectId);
            
            CreateTable(
                "dbo.ArticleCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        TitleSeo = c.String(nullable: false),
                        DescriptionSeo = c.String(nullable: false),
                        KeywordsSeo = c.String(nullable: false),
                        Image = c.String(),
                        TextPreview = c.String(),
                        Content = c.String(),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            DropForeignKey("dbo.BaseArticles", "CategoryId", "dbo.BaseArticles");
            DropForeignKey("dbo.Tasks", "TypeWorkId", "dbo.BaseArticles");
            DropForeignKey("dbo.BaseArticles", "TypeWorkId", "dbo.BaseArticles");
            DropForeignKey("dbo.BaseArticles", "SubjectId", "dbo.BaseArticles");
            DropForeignKey("dbo.BaseArticles", "ParentId", "dbo.BaseArticles");
            DropIndex("dbo.Tasks", new[] { "TypeWorkId" });
            DropIndex("dbo.BaseArticles", new[] { "SubjectId" });
            DropIndex("dbo.BaseArticles", new[] { "TypeWorkId" });
            DropIndex("dbo.BaseArticles", new[] { "ParentId" });
            DropIndex("dbo.BaseArticles", new[] { "CategoryId" });
            AlterColumn("dbo.BaseArticles", "CategoryId", c => c.Int(nullable: false));
            DropColumn("dbo.Tasks", "TypeWorkId");
            DropColumn("dbo.BaseArticles", "Discriminator");
            DropColumn("dbo.BaseArticles", "SubjectId");
            DropColumn("dbo.BaseArticles", "TypeWorkId");
            DropColumn("dbo.BaseArticles", "ParentId");
            RenameColumn(table: "dbo.BaseArticles", name: "CategoryId", newName: "ArticleCategoryId");
            CreateIndex("dbo.BaseArticles", "ArticleCategoryId");
            CreateIndex("dbo.ArticleCategories", "ParentId");
            AddForeignKey("dbo.Articles", "ArticleCategoryId", "dbo.ArticleCategories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ArticleCategories", "ParentId", "dbo.ArticleCategories", "Id");
            RenameTable(name: "dbo.BaseArticles", newName: "Articles");
        }
    }
}
