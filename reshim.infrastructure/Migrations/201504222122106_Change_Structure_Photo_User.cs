namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Structure_Photo_User : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Profiles", "Photo_FileId", "dbo.Files");
            DropIndex("dbo.Profiles", new[] { "Photo_FileId" });
            AddColumn("dbo.Users", "Photo", c => c.String());
            DropColumn("dbo.Profiles", "Photo_FileId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Profiles", "Photo_FileId", c => c.Int());
            DropColumn("dbo.Users", "Photo");
            CreateIndex("dbo.Profiles", "Photo_FileId");
            AddForeignKey("dbo.Profiles", "Photo_FileId", "dbo.Files", "FileId");
        }
    }
}
