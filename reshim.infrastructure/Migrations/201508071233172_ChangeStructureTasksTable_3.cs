namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureTasksTable_3 : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.Tasks", "TaskId", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tasks", "TaskId", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.Subscribers", "TaskId", "dbo.Tasks");
            DropForeignKey("dbo.InternalPayments", "TaskId", "dbo.Tasks");
            DropPrimaryKey("dbo.Tasks");
            DropColumn("dbo.Tasks", "Id");
            AddPrimaryKey("dbo.Tasks", "TaskId");
            AddForeignKey("dbo.Subscribers", "TaskId", "dbo.Tasks", "Id", cascadeDelete: true);
            AddForeignKey("dbo.InternalPayments", "TaskId", "dbo.Tasks", "Id", cascadeDelete: true);
        }
    }
}
