namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTableLesson : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents");
            RenameColumn(table: "dbo.Contents", name: "SectionId", newName: "ChapterId");
            RenameIndex(table: "dbo.Contents", name: "IX_SectionId", newName: "IX_ChapterId");
            AddForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents");
            RenameIndex(table: "dbo.Contents", name: "IX_ChapterId", newName: "IX_SectionId");
            RenameColumn(table: "dbo.Contents", name: "ChapterId", newName: "SectionId");
            AddForeignKey("dbo.Reviews", "Lesson_Id", "dbo.Contents", "Id");
        }
    }
}
