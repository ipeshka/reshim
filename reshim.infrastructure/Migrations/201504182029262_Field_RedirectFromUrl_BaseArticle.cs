namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Field_RedirectFromUrl_BaseArticle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "RedirectFromUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseArticles", "RedirectFromUrl");
        }
    }
}
