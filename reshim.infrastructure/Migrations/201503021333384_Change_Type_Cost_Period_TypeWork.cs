namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Type_Cost_Period_TypeWork : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BaseArticles", "Cost", c => c.String());
            AlterColumn("dbo.BaseArticles", "Period", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BaseArticles", "Period", c => c.Int());
            AlterColumn("dbo.BaseArticles", "Cost", c => c.Decimal(precision: 18, scale: 2));
        }
    }
}
