namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Change_Structure_Chat : DbMigration
    {
        public override void Up()
        {
            Sql("TRUNCATE TABLE dbo.Messages");
            DropForeignKey("dbo.Messages", "FromId", "dbo.Users");
            DropForeignKey("dbo.Messages", "ToId", "dbo.Users");
            DropIndex("dbo.Messages", new[] { "FromId" });
            DropIndex("dbo.Messages", new[] { "ToId" });
            CreateTable(
                "dbo.Dialogues",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LastUpdate = c.DateTime(nullable: false),
                        SenderId = c.Int(nullable: false),
                        RecipientId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.RecipientId)
                .ForeignKey("dbo.Users", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.RecipientId);
            
            AddColumn("dbo.Messages", "DialogId", c => c.Int(nullable: false));
            CreateIndex("dbo.Messages", "DialogId");
            AddForeignKey("dbo.Messages", "DialogId", "dbo.Dialogues", "Id", cascadeDelete: true);
            DropColumn("dbo.Messages", "FromId");
            DropColumn("dbo.Messages", "ToId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Messages", "ToId", c => c.Int(nullable: false));
            AddColumn("dbo.Messages", "FromId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Dialogues", "SenderId", "dbo.Users");
            DropForeignKey("dbo.Dialogues", "RecipientId", "dbo.Users");
            DropForeignKey("dbo.Messages", "DialogId", "dbo.Dialogues");
            DropIndex("dbo.Messages", new[] { "DialogId" });
            DropIndex("dbo.Dialogues", new[] { "RecipientId" });
            DropIndex("dbo.Dialogues", new[] { "SenderId" });
            DropColumn("dbo.Messages", "DialogId");
            DropTable("dbo.Dialogues");
            CreateIndex("dbo.Messages", "ToId");
            CreateIndex("dbo.Messages", "FromId");
            AddForeignKey("dbo.Messages", "ToId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Messages", "FromId", "dbo.Users", "UserId");
        }
    }
}
