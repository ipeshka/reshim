namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_ConditionId_SolutionId_CustomerId_DecidedId_Task : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tasks", "Condition_FileId", "dbo.Files");
            DropIndex("dbo.Tasks", new[] { "Condition_FileId" });
            DropIndex("dbo.Tasks", new[] { "Customer_UserId" });
            RenameColumn(table: "dbo.Tasks", name: "Customer_UserId", newName: "CustomerId");
            RenameColumn(table: "dbo.Tasks", name: "Condition_FileId", newName: "ConditionId");
            RenameColumn(table: "dbo.Tasks", name: "Decided_UserId", newName: "DecidedId");
            RenameColumn(table: "dbo.Tasks", name: "Solution_FileId", newName: "SolutionId");
            RenameIndex(table: "dbo.Tasks", name: "IX_Solution_FileId", newName: "IX_SolutionId");
            RenameIndex(table: "dbo.Tasks", name: "IX_Decided_UserId", newName: "IX_DecidedId");
            AlterColumn("dbo.Tasks", "ConditionId", c => c.Int(nullable: false));
            AlterColumn("dbo.Tasks", "CustomerId", c => c.Int(nullable: false));
            CreateIndex("dbo.Tasks", "ConditionId");
            CreateIndex("dbo.Tasks", "CustomerId");
            AddForeignKey("dbo.Tasks", "ConditionId", "dbo.Files", "FileId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "ConditionId", "dbo.Files");
            DropIndex("dbo.Tasks", new[] { "CustomerId" });
            DropIndex("dbo.Tasks", new[] { "ConditionId" });
            AlterColumn("dbo.Tasks", "CustomerId", c => c.Int());
            AlterColumn("dbo.Tasks", "ConditionId", c => c.Int());
            RenameIndex(table: "dbo.Tasks", name: "IX_DecidedId", newName: "IX_Decided_UserId");
            RenameIndex(table: "dbo.Tasks", name: "IX_SolutionId", newName: "IX_Solution_FileId");
            RenameColumn(table: "dbo.Tasks", name: "SolutionId", newName: "Solution_FileId");
            RenameColumn(table: "dbo.Tasks", name: "DecidedId", newName: "Decided_UserId");
            RenameColumn(table: "dbo.Tasks", name: "ConditionId", newName: "Condition_FileId");
            RenameColumn(table: "dbo.Tasks", name: "CustomerId", newName: "Customer_UserId");
            CreateIndex("dbo.Tasks", "Customer_UserId");
            CreateIndex("dbo.Tasks", "Condition_FileId");
            AddForeignKey("dbo.Tasks", "Condition_FileId", "dbo.Files", "FileId");
        }
    }
}
