namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Field_Date_Connection : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Connections", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Connections", "Date");
        }
    }
}
