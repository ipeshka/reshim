namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_PaymentDataId_Invoice : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Invoices", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.Invoices", new[] { "PaymentData_PaymentDataId" });
            RenameColumn(table: "dbo.Invoices", name: "PaymentData_PaymentDataId", newName: "PaymentDataId");
            AlterColumn("dbo.Invoices", "PaymentDataId", c => c.Int(nullable: false));
            CreateIndex("dbo.Invoices", "PaymentDataId");
            AddForeignKey("dbo.Invoices", "PaymentDataId", "dbo.PaymentDatas", "PaymentDataId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Invoices", "PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.Invoices", new[] { "PaymentDataId" });
            AlterColumn("dbo.Invoices", "PaymentDataId", c => c.Int());
            RenameColumn(table: "dbo.Invoices", name: "PaymentDataId", newName: "PaymentData_PaymentDataId");
            CreateIndex("dbo.Invoices", "PaymentData_PaymentDataId");
            AddForeignKey("dbo.Invoices", "PaymentData_PaymentDataId", "dbo.PaymentDatas", "PaymentDataId");
        }
    }
}
