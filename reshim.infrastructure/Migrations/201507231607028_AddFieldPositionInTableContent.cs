namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldPositionInTableContent : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "Position", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseArticles", "Position");
        }
    }
}
