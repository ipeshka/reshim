namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTableChapter : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contents", "NameLessonId", c => c.Int());
            CreateIndex("dbo.Contents", "NameLessonId");
            AddForeignKey("dbo.Contents", "NameLessonId", "dbo.Contents", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Contents", "NameLessonId", "dbo.Contents");
            DropIndex("dbo.Contents", new[] { "NameLessonId" });
            DropColumn("dbo.Contents", "NameLessonId");
        }
    }
}
