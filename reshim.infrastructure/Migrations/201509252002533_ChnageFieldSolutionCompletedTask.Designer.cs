// <auto-generated />
namespace reshim.data.infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class ChnageFieldSolutionCompletedTask : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChnageFieldSolutionCompletedTask));
        
        string IMigrationMetadata.Id
        {
            get { return "201509252002533_ChnageFieldSolutionCompletedTask"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
