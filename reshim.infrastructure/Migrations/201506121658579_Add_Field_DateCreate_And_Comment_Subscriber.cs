namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Field_DateCreate_And_Comment_Subscriber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Subscribers", "DateCreate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Subscribers", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Subscribers", "Comment");
            DropColumn("dbo.Subscribers", "DateCreate");
        }
    }
}
