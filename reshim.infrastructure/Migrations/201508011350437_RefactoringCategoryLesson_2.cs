namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringCategoryLesson_2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Contents", "ParentId2", "dbo.Contents");
            DropForeignKey("dbo.Contents", "SectionTopId", "dbo.Contents");
            DropIndex("dbo.Contents", new[] { "ParentId2" });
            DropIndex("dbo.Contents", new[] { "SectionTopId" });
            DropColumn("dbo.Contents", "ParentId2");
            DropColumn("dbo.Contents", "SectionTopId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Contents", name: "CategoryId1", newName: "SectionTopId");
            RenameColumn(table: "dbo.Contents", name: "CategoryId1", newName: "ParentId2");
            AddColumn("dbo.Contents", "CategoryId1", c => c.Int());
            AddColumn("dbo.Contents", "CategoryId1", c => c.Int());
            CreateIndex("dbo.Contents", "SectionTopId");
            CreateIndex("dbo.Contents", "ParentId2");
        }
    }
}
