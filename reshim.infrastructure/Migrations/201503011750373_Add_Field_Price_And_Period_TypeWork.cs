namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_Field_Price_And_Period_TypeWork : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BaseArticles", "Cost", c => c.Decimal(precision: 18, scale: 2));
            AddColumn("dbo.BaseArticles", "Period", c => c.Int());
            Sql("UPDATE [dbo].[BaseArticles] SET Cost = 0");
            Sql("UPDATE [dbo].[BaseArticles] SET Period = 0");
        }
        
        public override void Down()
        {
            DropColumn("dbo.BaseArticles", "Period");
            DropColumn("dbo.BaseArticles", "Cost");
        }
    }
}
