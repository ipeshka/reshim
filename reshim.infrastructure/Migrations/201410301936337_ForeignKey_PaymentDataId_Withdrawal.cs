namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_PaymentDataId_Withdrawal : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Withdrawals", "PaymentData_PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.Withdrawals", new[] { "PaymentData_PaymentDataId" });
            RenameColumn(table: "dbo.Withdrawals", name: "PaymentData_PaymentDataId", newName: "PaymentDataId");
            AlterColumn("dbo.Withdrawals", "PaymentDataId", c => c.Int(nullable: false));
            CreateIndex("dbo.Withdrawals", "PaymentDataId");
            AddForeignKey("dbo.Withdrawals", "PaymentDataId", "dbo.PaymentDatas", "PaymentDataId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Withdrawals", "PaymentDataId", "dbo.PaymentDatas");
            DropIndex("dbo.Withdrawals", new[] { "PaymentDataId" });
            AlterColumn("dbo.Withdrawals", "PaymentDataId", c => c.Int());
            RenameColumn(table: "dbo.Withdrawals", name: "PaymentDataId", newName: "PaymentData_PaymentDataId");
            CreateIndex("dbo.Withdrawals", "PaymentData_PaymentDataId");
            AddForeignKey("dbo.Withdrawals", "PaymentData_PaymentDataId", "dbo.PaymentDatas", "PaymentDataId");
        }
    }
}
