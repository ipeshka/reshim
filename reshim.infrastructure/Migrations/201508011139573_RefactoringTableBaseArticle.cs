namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringTableBaseArticle : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.BaseArticles", newName: "Contents");
            RenameColumn("dbo.Contents", "Content", "Text");
            //AddColumn("dbo.Contents", "Text", c => c.String());
            AlterColumn("dbo.Contents", "Position", c => c.Int());
            //DropColumn("dbo.Contents", "Content");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contents", "Content", c => c.String());
            AlterColumn("dbo.Contents", "Position", c => c.Int(nullable: false));
            DropColumn("dbo.Contents", "Text");
            RenameTable(name: "dbo.Contents", newName: "BaseArticles");
        }
    }
}
