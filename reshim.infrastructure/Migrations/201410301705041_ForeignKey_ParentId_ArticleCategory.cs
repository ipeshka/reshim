namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ForeignKey_ParentId_ArticleCategory : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ArticleCategories", name: "Parent_Id", newName: "ParentId");
            RenameIndex(table: "dbo.ArticleCategories", name: "IX_Parent_Id", newName: "IX_ParentId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ArticleCategories", name: "IX_ParentId", newName: "IX_Parent_Id");
            RenameColumn(table: "dbo.ArticleCategories", name: "ParentId", newName: "Parent_Id");
        }
    }
}
