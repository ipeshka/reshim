// <auto-generated />
namespace reshim.data.infrastructure.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class Field_Date_Connection : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Field_Date_Connection));
        
        string IMigrationMetadata.Id
        {
            get { return "201411021820122_Field_Date_Connection"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
