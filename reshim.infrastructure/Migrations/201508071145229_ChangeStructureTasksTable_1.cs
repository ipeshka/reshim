namespace reshim.data.infrastructure.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureTasksTable_1 : DbMigration
    {
        public override void Up()
        {
            AddForeignKey("dbo.InternalPayments", "TaskId", "dbo.Tasks", "TaskId");
        }
        
        public override void Down()
        {
            DropIndex("dbo.InternalPayments", new[] { "PaymentDataId" });
            RenameIndex(table: "dbo.InternalPayments", name: "IX_TaskId", newName: "IX_PaymentDataId");
            RenameColumn(table: "dbo.InternalPayments", name: "TaskId", newName: "PaymentDataId");
            AddColumn("dbo.InternalPayments", "TaskId", c => c.Int(nullable: false));
        }
    }
}
