namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeStructureTasksTable_2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Tasks", "ConditionId", "dbo.Files");
            AlterColumn("dbo.Tasks", "Cost", c => c.Decimal(precision: 18, scale: 2));
            AddForeignKey("dbo.Tasks", "ConditionId", "dbo.Files", "FileId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tasks", "ConditionId", "dbo.Files");
            AlterColumn("dbo.Tasks", "Cost", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddForeignKey("dbo.Tasks", "ConditionId", "dbo.Files", "FileId", cascadeDelete: true);
        }
    }
}
