namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CompliteTasksAndTags : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.CompletedTasks", new[] { "Condition_FileId" });
            DropIndex("dbo.CompletedTasks", new[] { "Solution_FileId" });
            DropColumn("dbo.CompletedTasks", "DecidedId");
            DropColumn("dbo.CompletedTasks", "ConditionId");
            DropColumn("dbo.CompletedTasks", "SolutionId");
            RenameColumn(table: "dbo.CompletedTasks", name: "Decided_UserId", newName: "DecidedId");
            RenameColumn(table: "dbo.CompletedTasks", name: "Condition_FileId", newName: "ConditionId");
            RenameColumn(table: "dbo.CompletedTasks", name: "Solution_FileId", newName: "SolutionId");
            RenameIndex(table: "dbo.CompletedTasks", name: "IX_Decided_UserId", newName: "IX_DecidedId");
            CreateTable(
                "dbo.Tags",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TagCompletedTaskRelationships",
                c => new
                    {
                        TagId = c.Int(nullable: false),
                        ComplitedTaskId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TagId, t.ComplitedTaskId })
                .ForeignKey("dbo.CompletedTasks", t => t.TagId, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.ComplitedTaskId, cascadeDelete: true)
                .Index(t => t.TagId)
                .Index(t => t.ComplitedTaskId);
            
            AlterColumn("dbo.CompletedTasks", "ConditionId", c => c.Int(nullable: false));
            AlterColumn("dbo.CompletedTasks", "SolutionId", c => c.Int(nullable: false));
            CreateIndex("dbo.CompletedTasks", "ConditionId");
            CreateIndex("dbo.CompletedTasks", "SolutionId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TagCompletedTaskRelationships", "ComplitedTaskId", "dbo.Tags");
            DropForeignKey("dbo.TagCompletedTaskRelationships", "TagId", "dbo.CompletedTasks");
            DropIndex("dbo.TagCompletedTaskRelationships", new[] { "ComplitedTaskId" });
            DropIndex("dbo.TagCompletedTaskRelationships", new[] { "TagId" });
            DropIndex("dbo.CompletedTasks", new[] { "SolutionId" });
            DropIndex("dbo.CompletedTasks", new[] { "ConditionId" });
            AlterColumn("dbo.CompletedTasks", "SolutionId", c => c.Int());
            AlterColumn("dbo.CompletedTasks", "ConditionId", c => c.Int());
            DropTable("dbo.TagCompletedTaskRelationships");
            DropTable("dbo.Tags");
            RenameIndex(table: "dbo.CompletedTasks", name: "IX_DecidedId", newName: "IX_Decided_UserId");
            RenameColumn(table: "dbo.CompletedTasks", name: "SolutionId", newName: "Solution_FileId");
            RenameColumn(table: "dbo.CompletedTasks", name: "ConditionId", newName: "Condition_FileId");
            RenameColumn(table: "dbo.CompletedTasks", name: "DecidedId", newName: "Decided_UserId");
            AddColumn("dbo.CompletedTasks", "SolutionId", c => c.Int(nullable: false));
            AddColumn("dbo.CompletedTasks", "ConditionId", c => c.Int(nullable: false));
            AddColumn("dbo.CompletedTasks", "DecidedId", c => c.Int());
            CreateIndex("dbo.CompletedTasks", "Solution_FileId");
            CreateIndex("dbo.CompletedTasks", "Condition_FileId");
        }
    }
}
