namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RefactoringCategoryLesson : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "Ip", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "Ip", c => c.String());
        }
    }
}
