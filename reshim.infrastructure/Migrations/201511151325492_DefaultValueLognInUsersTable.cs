namespace reshim.data.infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DefaultValueLognInUsersTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Users", "Email", c => c.String(maxLength: 128));
            AlterColumn("dbo.Users", "Login", c => c.String(nullable: false, maxLength: 128));
            Sql("ALTER TABLE dbo.Users ADD CONSTRAINT [unique_user_login] UNIQUE(Login)");
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Login", c => c.String());
            AlterColumn("dbo.Users", "Email", c => c.String());
        }
    }
}
