﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class InternalPaymentConfigurations : EntityTypeConfiguration<InternalPayment>
    {
        public InternalPaymentConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.DatePayment).IsRequired();
            Property(t => t.Amount).IsRequired();
            Property(t => t.PaymentDataId).IsRequired();
            Property(t => t.TaskId).IsRequired();

            // table
            ToTable("InternalPayments");

            // relationship
            HasRequired(x => x.PaymentData).WithMany(x => x.InternalPayments).HasForeignKey(x => x.PaymentDataId).WillCascadeOnDelete(true);
            HasRequired(x => x.Task).WithMany(x => x.InternalsPayments).HasForeignKey(x => x.TaskId).WillCascadeOnDelete(true);
        }
    }
}
