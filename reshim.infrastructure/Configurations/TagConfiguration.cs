﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class TagConfiguration : EntityTypeConfiguration<Tag>
    {
        public TagConfiguration()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Name).IsRequired();

            // table
            ToTable("Tags");
        }
    }
}
