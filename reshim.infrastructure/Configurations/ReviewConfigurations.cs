﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class ReviewConfigurations : EntityTypeConfiguration<Review>
    {
        public ReviewConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Ip).IsRequired();

            // table
            ToTable("Reviews");

            // relationship  
            HasOptional(x => x.Article).WithMany(x => x.Reviews).HasForeignKey(x => x.ArticleId).WillCascadeOnDelete(false);
            HasOptional(x => x.Lesson).WithMany(x => x.Reviews).HasForeignKey(x => x.LessonId).WillCascadeOnDelete(false);
            HasOptional(x => x.CompletedTask).WithMany(x => x.Reviews).HasForeignKey(x => x.CompletedTaskId).WillCascadeOnDelete(false);
        }
    }
}
