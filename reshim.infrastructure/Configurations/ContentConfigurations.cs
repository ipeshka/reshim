﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class ContentConfigurations : EntityTypeConfiguration<Content>
    {
        public ContentConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Alias).IsRequired();
            Property(t => t.Name).IsRequired();
            Property(t => t.ShortName).IsOptional();
            Property(t => t.TitleSeo).IsOptional();
            Property(t => t.DescriptionSeo).IsOptional();
            Property(t => t.KeywordsSeo).IsOptional();
            Property(t => t.Preview).IsOptional();
            Property(t => t.Text).IsOptional();
            Property(t => t.Image).IsOptional();
            Property(t => t.DateCreate).IsRequired();
            Property(t => t.DateUpdate).IsOptional();
            Property(t => t.RedirectFromUrl).IsOptional();
            Property(t => t.Position).IsOptional();
            
            // table
            ToTable("Contents");
        }
    }

    public class CategoryArticleConfigurations : EntityTypeConfiguration<CategoryArticle>
    {
        public CategoryArticleConfigurations()
        {
            // relationship  
            HasMany(x => x.Children).WithOptional(x => x.Parent).HasForeignKey(x => x.ParentId).WillCascadeOnDelete(false);
        }
    }

    public class ArticleConfigurations : EntityTypeConfiguration<Article>
    {
        public ArticleConfigurations()
        {
            // relationship  
            HasRequired(x => x.Category).WithMany(x => x.Articles).HasForeignKey(x => x.CategoryId).WillCascadeOnDelete(false);
        }
    }

    public class NewsConfigurations : EntityTypeConfiguration<News>
    {
        public NewsConfigurations()
        {
        }
    }

    public class CategoryLessonConfigurations : EntityTypeConfiguration<CategoryLesson>
    {
        public CategoryLessonConfigurations()
        {
            // relationship  
            HasMany(x => x.Children).WithOptional(x => x.Parent).HasForeignKey(x => x.ParentId).WillCascadeOnDelete(false);
            HasMany(x => x.NameLessons).WithRequired(x => x.Category).HasForeignKey(x => x.CategoryId).WillCascadeOnDelete(false);
            HasMany(x => x.Lessons).WithRequired(x => x.CategoryLesson).HasForeignKey(x => x.CategoryIdLesson).WillCascadeOnDelete(false);
        }
    }

    public class NameLessonConfigurations : EntityTypeConfiguration<NameLesson>
    {
        public NameLessonConfigurations()
        {
            // relationship  
            HasMany(x => x.Chapters).WithRequired(x => x.NameLesson).HasForeignKey(x => x.NameLessonId).WillCascadeOnDelete(false);
            HasMany(x => x.Lessons).WithRequired(x => x.NameLesson).HasForeignKey(x => x.NameLessonId).WillCascadeOnDelete(false);
        }
    }

    public class ChapterConfigurations : EntityTypeConfiguration<Chapter>
    {
        public ChapterConfigurations()
        {
            // relationship  
            HasMany(x => x.Lessons).WithRequired(x => x.Chapter).HasForeignKey(x => x.ChapterId).WillCascadeOnDelete(false);
        }
    }

    public class LessonConfigurations : EntityTypeConfiguration<Lesson>
    {
        public LessonConfigurations()
        {
            // fields
            Property(t => t.NameLessonId).IsRequired();

            // relationship  
            HasMany(x => x.Reviews).WithOptional().WillCascadeOnDelete(true);
            HasMany(x => x.Reviews).WithOptional().WillCascadeOnDelete(true);
        }
    }

    public class SubjectConfigurations : EntityTypeConfiguration<Subject>
    {
        public SubjectConfigurations()
        {
        }
    }

    public class TypeWorkConfigurations : EntityTypeConfiguration<TypeWork>
    {
        public TypeWorkConfigurations()
        {
            // fields
            Property(t => t.Cost).IsRequired();
            Property(t => t.Period).IsRequired();
        }
    }

    public class SeoArticleConfigurations : EntityTypeConfiguration<SeoArticle>
    {
        public SeoArticleConfigurations()
        {
            // relationship  
            HasRequired(x => x.TypeWork).WithMany(x => x.SeoArticles).HasForeignKey(x => x.TypeWorkId).WillCascadeOnDelete(false);
        }
    }
}
