﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class WithdrawalConfigurations : EntityTypeConfiguration<Withdrawal>
    {
        public WithdrawalConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Create).IsRequired();
            Property(t => t.Amount).IsRequired();
            Property(t => t.Purse).IsRequired();
            Property(t => t.Comment).IsOptional();
            Property(t => t.State).IsRequired();
            Property(t => t.PaywayVia).IsRequired();
            Property(t => t.PaymentDataId).IsRequired();

            // table
            ToTable("Withdrawels");

            // relationship  
            HasRequired(x => x.PaymentData).WithMany(x => x.Withdrawals).HasForeignKey(x => x.PaymentDataId).WillCascadeOnDelete(true);
        }
    }
}
