﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class PaymentDataConfigurations : EntityTypeConfiguration<PaymentData>
    {
        public PaymentDataConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Ballance).IsRequired();

            // table
            ToTable("PaymentsData");

            // relationship  
        }
    }
}
