﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class PaymentConfigurations : EntityTypeConfiguration<Payment>
    {
        public PaymentConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Create).IsRequired();
            Property(t => t.Amount).IsRequired();
            Property(t => t.State).IsRequired();
            Property(t => t.PaywayVia).IsRequired();
            Property(t => t.Currency).IsRequired();
            Property(t => t.PaymentDataId).IsRequired();

            // table
            ToTable("Payments");

            // relationship  
            HasRequired(x => x.PaymentData).WithMany(x => x.Payments).HasForeignKey(x => x.PaymentDataId).WillCascadeOnDelete(true);
        }
    }
}
