﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class UserConfigurations : EntityTypeConfiguration<User>
    {
        public UserConfigurations()
        {
            //key 
            HasKey(x => x.UserId);

            // fields
            Property(t => t.Login).HasMaxLength(128).IsRequired().HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(t => t.Email).HasMaxLength(128).IsOptional();

            // table
            ToTable("Users");

            // relationship  
            HasRequired(x => x.Profile).WithRequiredPrincipal(x => x.User).WillCascadeOnDelete(true);
            HasRequired(x => x.PaymentData).WithRequiredPrincipal(x => x.User).WillCascadeOnDelete(true);

            HasMany(x => x.Dialogs1).WithRequired(x => x.User1).HasForeignKey(x => x.User1Id).WillCascadeOnDelete(false);
            HasMany(x => x.Dialogs2).WithRequired(x => x.User2).HasForeignKey(x => x.User2Id).WillCascadeOnDelete(false);

            HasMany(x => x.RecipientMessages).WithRequired(x => x.Recipient).HasForeignKey(x => x.RecipientId).WillCascadeOnDelete(false);
            HasMany(x => x.SenderMessages).WithRequired(x => x.Sender).HasForeignKey(x => x.SenderId).WillCascadeOnDelete(false);
        }
    }
}
