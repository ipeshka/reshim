﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class DialogConfigurations : EntityTypeConfiguration<Dialog>
    {
        public DialogConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.LastUpdate).IsRequired();

            // table
            ToTable("Dialogues");

            // relationship  
            HasMany(x => x.Messages).WithRequired(x => x.Dialog).HasForeignKey(x => x.DialogId).WillCascadeOnDelete(true);
        }
    }
}
