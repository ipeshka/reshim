﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class HistoryCompletedTaskConfiguration : EntityTypeConfiguration<HistoryCompletedTask>
    {
        public HistoryCompletedTaskConfiguration()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Create).IsRequired();
            Property(t => t.State).IsRequired();
            Property(t => t.Currency).IsOptional();
            Property(t => t.PaywayVia).IsRequired();

            // table
            ToTable("HistoriesCompletedTasks");

            // relationship  
            HasRequired(x => x.User).WithMany(x => x.HistoriesCompletedTasks).HasForeignKey(x => x.UserId).WillCascadeOnDelete(false);
            HasRequired(x => x.CompletedTask).WithMany(x => x.HistoriesCompletedTasks).HasForeignKey(x => x.CompletedTaskId).WillCascadeOnDelete(false);
        }
    }
}
