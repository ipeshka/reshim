﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class ProfileConfigurations : EntityTypeConfiguration<Profile>
    {
        public ProfileConfigurations()
        {
            HasKey(x => x.ProfileId);

            // table
            ToTable("Profiles");
        }
    }
}
