﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class SubscriberConfigurations : EntityTypeConfiguration<Subscriber>
    {
        public SubscriberConfigurations()
        {
            // key
            HasKey(x => x.SubscriberId);

            // fields
            Property(t => t.Value).IsRequired();
            Property(t => t.DateCreate).IsRequired();
            Property(t => t.Comment).IsOptional();

            // table
            ToTable("Subscribers");

            // relationship  
            HasRequired(x => x.User).WithMany(x => x.Subscribers).HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
            HasRequired(x => x.Task).WithMany(x => x.Subscribers).HasForeignKey(x => x.TaskId).WillCascadeOnDelete(true);
        }
    }
}
