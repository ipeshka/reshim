﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class CompletedTaskConfiguration : EntityTypeConfiguration<CompletedTask>
    {
        public CompletedTaskConfiguration()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.Name).IsRequired();
            Property(t => t.TitleSeo).IsRequired();
            Property(t => t.DescriptionSeo).IsRequired();
            Property(t => t.KeywordsSeo).IsRequired();
            Property(t => t.Cost).IsRequired();
            Property(t => t.Comment).IsOptional();
            Property(t => t.Condition).IsOptional();

            // table
            ToTable("CompletedTasks");

            // relationship  
            HasRequired(x => x.Solution).WithMany().HasForeignKey(x => x.SolutionId).WillCascadeOnDelete(false);
            HasOptional(x => x.Subject).WithMany().HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
            HasOptional(x => x.Decided).WithMany(x => x.ComplitedUserTasks).HasForeignKey(x => x.DecidedId).WillCascadeOnDelete(false);
            HasMany(x => x.Tags).WithMany(x => x.CompletedTasks)
                                   .Map(tc =>
                                   {
                                       tc.MapLeftKey("TagId");
                                       tc.MapRightKey("ComplitedTaskId");
                                       tc.ToTable("TagCompletedTaskRelationships");
                                   });
        }
    }
}
