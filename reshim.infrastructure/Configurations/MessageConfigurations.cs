﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class MessageConfigurations : EntityTypeConfiguration<Message>
    {

        public MessageConfigurations()
        {
            HasKey(x => x.Id);

            // fields
            Property(t => t.DateCreate).IsRequired();
            Property(t => t.Content).IsRequired();
            Property(t => t.IsNew).IsRequired();

            // table
            ToTable("Messages");
        }
    }
}
