﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class ConnectionConfigurations : EntityTypeConfiguration<Connection>
    {
        public ConnectionConfigurations()
        {
            // fields
            Property(t => t.Date).IsRequired();

            // table
            ToTable("Connections");

            // relationship  
            HasRequired(x => x.User).WithMany(x => x.Connections).HasForeignKey(x => x.UserId).WillCascadeOnDelete(true);
        }
    }
}
