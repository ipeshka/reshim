﻿using System.Data.Entity.ModelConfiguration;
using Task = reshim.model.Entities.Task;

namespace reshim.data.infrastructure.Configurations
{
    public class TaskConfigurations : EntityTypeConfiguration<Task>
    {
        public TaskConfigurations()
        {
            // key
            HasKey(x => x.Id);

            // fields
            Property(t => t.DateCreated).IsRequired();
            Property(t => t.DateOfExecution).IsRequired();
            Property(t => t.DateComplited).IsRequired();
            Property(t => t.Cost).IsOptional();
            Property(t => t.Comment).IsOptional();

            // table
            ToTable("Tasks");

            // relationships
            HasRequired(x => x.Condition).WithMany().HasForeignKey(x => x.ConditionId).WillCascadeOnDelete(false);
            HasOptional(x => x.Solution).WithMany().HasForeignKey(x => x.SolutionId).WillCascadeOnDelete(false);

            HasRequired(x => x.Customer).WithMany(x => x.OrderedTasks).HasForeignKey(x => x.CustomerId).WillCascadeOnDelete(false);
            HasOptional(x => x.Decided).WithMany(x => x.ComplitedTasks).HasForeignKey(x => x.DecidedId).WillCascadeOnDelete(false);

            HasOptional(x => x.Subject).WithMany().HasForeignKey(x => x.SubjectId).WillCascadeOnDelete(false);
            HasOptional(x => x.TypeWork).WithMany().HasForeignKey(x => x.TypeWorkId).WillCascadeOnDelete(false);
        }
    }
}
