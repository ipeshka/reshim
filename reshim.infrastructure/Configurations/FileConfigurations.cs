﻿using System.Data.Entity.ModelConfiguration;
using reshim.model.Entities;

namespace reshim.data.infrastructure.Configurations
{
    public class FileConfigurations : EntityTypeConfiguration<File>
    {
        public FileConfigurations()
        {
            // key
            HasKey(x => x.FileId);

            // fields
            Property(t => t.TypeFile).IsRequired();

            // table
            ToTable("Files");
        }
    }
}
