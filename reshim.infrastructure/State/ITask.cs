﻿using System;
using System.Collections.Generic;
using reshim.common;

namespace reshim.data.infrastructure.State
{
    public interface IEmployeeTaskContext
    {
        IEnumerable<ValidationResult> Estimate(int taskId, int userId, decimal value, string comment);
        IEnumerable<ValidationResult> Unsubscribe(int taskId, int userId);
        IEnumerable<ValidationResult> UploadSolution(int taskId, int fileId, int userId);
    }

    public abstract class EmployeeTaskState
    {
        public virtual IEnumerable<ValidationResult> Estimate(int taskId, int userId, decimal value, string comment)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете оценить заказ.") };
        }

        public virtual IEnumerable<ValidationResult> Unsubscribe(int taskId, int userId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете отписаться от заказа.") };
        }

        public virtual IEnumerable<ValidationResult> UploadSolution(int taskId, int fileId, int userId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете загрузить заказ.") };
        }
    }



    public interface IUserTaskContext
    {

        IEnumerable<ValidationResult> Edit(int taskId,
            int fileId,
            DateTime dateOfExecution,
            decimal? cost,
            int subjectId,
            int typeWorkId,
            string comment);

        IEnumerable<ValidationResult> Delete(int taskId, int userId);
        IEnumerable<ValidationResult> SelectEmployee(int taskId, int subscriberId);
        IEnumerable<ValidationResult> Pay(int taskId, int userId);


        IEnumerable<ValidationResult> Download(int taskId);
    }

    public abstract class UserTaskState
    {

        public virtual IEnumerable<ValidationResult> Edit(int taskId,
            int fileId,
            DateTime dateOfExecution,
            decimal? cost,
            int subjectId,
            int sectionId,
            string comment)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете редактировать заказ.") };
        }

        public virtual IEnumerable<ValidationResult> Delete(int taskId, int userId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете удалить заказ.") };
        }

        public virtual IEnumerable<ValidationResult> SelectEmployee(int taskId, int subscriberId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете выбрать исполнителя.") };
        }


        public virtual IEnumerable<ValidationResult> Pay(int taskId, int userId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете оплатить заказ.") };
        }

        public virtual IEnumerable<ValidationResult> Download(int taskId)
        {
            return new List<ValidationResult>() { new ValidationResult("Вы не можете скачать заказ.") };
        }
    }
}
