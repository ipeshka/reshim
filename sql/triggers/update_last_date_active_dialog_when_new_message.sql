USE [reshim]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (
    SELECT *
    FROM [sys].[objects]
    WHERE [type] = 'TR' AND [name] = 'update_last_date_active_dialog_when_new_message'
    )
    DROP TRIGGER update_last_date_active_dialog_when_new_message;
GO


CREATE TRIGGER [dbo].[update_last_date_active_dialog_when_new_message] ON [reshim].[dbo].[Messages] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @dialog_id as int
	SELECT @dialog_id = DialogId FROM INSERTED
    UPDATE Dialogues
    SET LastUpdate = GETDATE()
	WHERE Id = @dialog_id
END
