USE [reshim]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO

IF EXISTS (
    SELECT *
    FROM [sys].[objects]
    WHERE [type] = 'TR' AND [name] = 'update_last_date_login_in_users'
    )
    DROP TRIGGER update_last_date_login_in_users;
GO


CREATE TRIGGER [dbo].[update_last_date_login_in_users] ON [reshim].[dbo].[Connections] AFTER INSERT, UPDATE
AS
BEGIN
	DECLARE @user_id as int
	SELECT @user_id = UserId FROM INSERTED
    UPDATE Users
    SET LastLoginTime = GETDATE()
	WHERE UserId = @user_id
END
