﻿using System;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace reshim.common.core.Pagination
{
    public class Pager : IHtmlString
    {
        private readonly HtmlHelper _htmlHelper;
        private readonly int _pageSize;
        private readonly int _currentPage;
        private readonly int _totalItemCount;
        protected readonly PagerOptions _pagerOptions;

        public Pager(HtmlHelper htmlHelper, int pageSize, int currentPage, int totalItemCount)
        {
            this._htmlHelper = htmlHelper;
            this._pageSize = pageSize;
            this._currentPage = currentPage;
            this._totalItemCount = totalItemCount;
            this._pagerOptions = new PagerOptions();
        }

        public Pager Options(Action<PagerOptionsBuilder> buildOptions)
        {
            buildOptions(new PagerOptionsBuilder(this._pagerOptions));
            return this;
        }

        public virtual PaginationModel BuildPaginationModel(Func<int, string> generateUrl)
        {
            var pageCount = (int)Math.Ceiling(_totalItemCount / (double)_pageSize);
            var model = new PaginationModel { PageSize = this._pageSize, CurrentPage = this._currentPage, TotalItemCount = this._totalItemCount, PageCount = pageCount };

            // First page
            if (this._pagerOptions.DisplayFirstAndLastPage)
            {
                model.PaginationLinks.Add(new PaginationLink { Active = (_currentPage > 1 ? true : false), DisplayText = this._pagerOptions.FirstPageText, DisplayTitle = this._pagerOptions.FirstPageTitle, PageIndex = 1, Url = generateUrl(1) });
            }

            // Previous page
            var previousPageText = this._pagerOptions.PreviousPageText;
            model.PaginationLinks.Add(_currentPage > 1 ? new PaginationLink { Active = true, DisplayText = previousPageText, DisplayTitle = this._pagerOptions.PreviousPageTitle, PageIndex = _currentPage - 1, Url = generateUrl(_currentPage - 1) } : new PaginationLink { Active = false, DisplayText = previousPageText });

            var start = 1;
            var end = pageCount;
            var nrOfPagesToDisplay = this._pagerOptions.MaxNrOfPages;

            if (pageCount > nrOfPagesToDisplay)
            {
                var middle = (int)Math.Ceiling(nrOfPagesToDisplay / 2d) - 1;
                var below = (_currentPage - middle);
                var above = (_currentPage + middle);

                if (below < 2)
                {
                    above = nrOfPagesToDisplay;
                    below = 1;
                }
                else if (above > (pageCount - 2))
                {
                    above = pageCount;
                    below = (pageCount - nrOfPagesToDisplay + 1);
                }

                start = below;
                end = above;
            }

            if (start > 1)
            {
                model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = 1, DisplayText = "1", Url = generateUrl(1) });
                if (start > 3)
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = 2, DisplayText = "2", Url = generateUrl(2) });
                }
                if (start > 2)
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = false, DisplayText = "...", IsSpacer = true });
                }
            }

            for (var i = start; i <= end; i++)
            {
                if (i == _currentPage || (_currentPage <= 0 && i == 1))
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = i, IsCurrent = true, DisplayText = i.ToString() });
                }
                else
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = i, DisplayText = i.ToString(), Url = generateUrl(i) });
                }
            }

            if (end < pageCount)
            {
                if (end < pageCount - 1)
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = false, DisplayText = "...", IsSpacer = true });
                }
                if (pageCount - 2 > end)
                {
                    model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = pageCount - 1, DisplayText = (pageCount - 1).ToString(), Url = generateUrl(pageCount - 1) });
                }

                model.PaginationLinks.Add(new PaginationLink { Active = true, PageIndex = pageCount, DisplayText = pageCount.ToString(), Url = generateUrl(pageCount) });
            }

            // Next page
            var nextPageText = this._pagerOptions.NextPageText;
            model.PaginationLinks.Add(_currentPage < pageCount ? new PaginationLink { Active = true, PageIndex = _currentPage + 1, DisplayText = nextPageText, DisplayTitle = this._pagerOptions.NextPageTitle, Url = generateUrl(_currentPage + 1) } : new PaginationLink { Active = false, DisplayText = nextPageText });

            // Last page
            if (this._pagerOptions.DisplayFirstAndLastPage)
            {
                model.PaginationLinks.Add(new PaginationLink { Active = (_currentPage < pageCount ? true : false), DisplayText = this._pagerOptions.LastPageText, DisplayTitle = this._pagerOptions.LastPageTitle, PageIndex = pageCount, Url = generateUrl(pageCount) });
            }

            // AjaxOptions
            if (_pagerOptions.AjaxOptions != null)
            {
                model.AjaxOptions = _pagerOptions.AjaxOptions;
            }

            model.Options = _pagerOptions;
            return model;
        }

        public virtual string ToHtmlString()
        {
            var model = BuildPaginationModel(GeneratePageUrl);
            var sb = new StringBuilder();

            foreach (var paginationLink in model.PaginationLinks)
            {
                if (paginationLink.Active)
                {
                    if (paginationLink.IsCurrent)
                    {
                        sb.AppendFormat("<li class=\"active\"><a>{0}</span></li>", paginationLink.DisplayText);
                    }
                    else if (!paginationLink.PageIndex.HasValue)
                    {
                        sb.AppendFormat(paginationLink.DisplayText);
                    }
                    else
                    {
                        var linkBuilder = new StringBuilder("<li><a");

                        if (_pagerOptions.AjaxOptions != null)
                            foreach (var ajaxOption in _pagerOptions.AjaxOptions.ToUnobtrusiveHtmlAttributes())
                                linkBuilder.AppendFormat(" {0}=\"{1}\"", ajaxOption.Key, ajaxOption.Value);

                        linkBuilder.AppendFormat(" href=\"{0}\" title=\"{1}\">{2}</a></li>", paginationLink.Url, paginationLink.DisplayTitle, paginationLink.DisplayText);

                        sb.Append(linkBuilder.ToString());
                    }
                }
                else
                {
                    if (!paginationLink.IsSpacer)
                    {
                        sb.AppendFormat("<li class=\"disabled\"><a>{0}</a></li>", paginationLink.DisplayText);
                    }
                    else
                    {
                        sb.AppendFormat("<li class=\"spacer\"><a>{0}</span></li>", paginationLink.DisplayText);
                    }
                }
            }

            return "<ul class=\"pagination\">" + sb.ToString() + "</ul>";
        }

        protected virtual string GeneratePageUrl(int pageNumber)
        {
            var viewContext = this._htmlHelper.ViewContext;
            var routeDataValues = viewContext.RequestContext.RouteData.Values;
            RouteValueDictionary pageLinkValueDictionary;

            // Avoid canonical errors when pageNumber is equal to 1.
            if (pageNumber == 1 && !this._pagerOptions.AlwaysAddFirstPageNumber)
            {
                pageLinkValueDictionary = new RouteValueDictionary(this._pagerOptions.RouteValues);

                if (routeDataValues.ContainsKey(this._pagerOptions.PageRouteValueKey))
                {
                    routeDataValues.Remove(this._pagerOptions.PageRouteValueKey);
                }
            }
            else
            {
                pageLinkValueDictionary = new RouteValueDictionary(this._pagerOptions.RouteValues) { { this._pagerOptions.PageRouteValueKey, pageNumber } };
            }

            // To be sure we get the right route, ensure the controller and action are specified.
            if (!pageLinkValueDictionary.ContainsKey("controller") && routeDataValues.ContainsKey("controller"))
            {
                pageLinkValueDictionary.Add("controller", routeDataValues["controller"]);
            }

            if (!pageLinkValueDictionary.ContainsKey("action") && routeDataValues.ContainsKey("action"))
            {
                pageLinkValueDictionary.Add("action", routeDataValues["action"]);
            }

            // Fix the dictionary if there are arrays in it.
            pageLinkValueDictionary = pageLinkValueDictionary.FixListRouteDataValues();

            // 'Render' virtual path.
            var virtualPathForArea = RouteTable.Routes.GetVirtualPathForArea(viewContext.RequestContext, pageLinkValueDictionary);

            return virtualPathForArea == null ? null : virtualPathForArea.VirtualPath;
        }
    }

    public class Pager<TModel> : Pager
    {
        private HtmlHelper<TModel> htmlHelper;

        public Pager(HtmlHelper<TModel> htmlHelper, int pageSize, int currentPage, int totalItemCount)
            : base(htmlHelper, pageSize, currentPage, totalItemCount)
        {
            this.htmlHelper = htmlHelper;
        }

        public Pager<TModel> Options(Action<PagerOptionsBuilder<TModel>> buildOptions)
        {
            buildOptions(new PagerOptionsBuilder<TModel>(this._pagerOptions, htmlHelper));
            return this;
        }
    }
}
