﻿using System;
using System.Collections.Generic;
using System.Linq;
using reshim.common.Attributes;

namespace reshim.common.core.Extensions
{
    public static class EnumEx
    {
        public static T GetEnumFromTypeAttribute<T>(string valueType)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();

            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field, typeof(TypeAttribute)) as TypeAttribute;

                if (attribute != null)
                {
                    if ((string)attribute.GetValue() == valueType)
                        return (T)field.GetValue(null);
                }

                else
                {
                    if (field.Name == valueType)
                        return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException("Not found.", "valueType");
        }

        public static TValue GetAttributeValue<TEnum, TValue>(this TEnum enumItem, Type attributeType, TValue defaultValue)
        {
            var attribute = enumItem.GetType().GetField(enumItem.ToString()).GetCustomAttributes(attributeType, true).OfType<BaseAttribute>().FirstOrDefault();
            return attribute == null ? defaultValue : (TValue)attribute.GetValue();
        }

        public static Dictionary<string, string> GetKeyValue<TEnum, TType, TValue1, TValue2>(Type ignoreType = null)
        {
            var allType = typeof(TEnum).GetFields()
                .Where(f => ignoreType == null || f.CustomAttributes.FirstOrDefault(attr => attr.AttributeType == ignoreType) == null)
                .Select(x => x)
                .Where(x => Attribute.GetCustomAttribute(x, typeof(TType)) != null)
                .ToDictionary(
                    x =>
                    {
                        var attribute = x.GetCustomAttributes(typeof(TValue1), true).OfType<BaseAttribute>().FirstOrDefault();
                        return attribute != null ? attribute.GetValue() as string : null;
                    },
                    x =>
                    {
                        var attribute = x.GetCustomAttributes(typeof(TValue2), true).OfType<BaseAttribute>().FirstOrDefault();
                        return attribute != null ? attribute.GetValue() as string : null;
                    });

            return allType;
        }

    }
}