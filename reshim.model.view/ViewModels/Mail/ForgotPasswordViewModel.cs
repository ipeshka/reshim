﻿namespace reshim.model.view.ViewModels.Mail
{
    public class ForgotPasswordViewModel
    {
        public string Login { get; set; }
        public string Link { get; set; }
    }
}