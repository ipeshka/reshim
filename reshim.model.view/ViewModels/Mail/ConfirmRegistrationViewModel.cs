﻿namespace reshim.model.view.ViewModels.Mail
{
    public class ConfirmRegistrationViewModel
    {
        public string Login { get; set; }
        public string Link { get; set; }
    }
}