﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Auth
{
    public class UpdatePasswordFormModel
    {
        [Required]
        public int UserId { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = @"{0} должен содержать минимум {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = @"Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = @"Повтор пароля")]
        [Compare("Password", ErrorMessage = @"Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }
    }
}