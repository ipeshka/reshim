﻿using System.Collections.Generic;
using reshim.common;

namespace reshim.model.view.ViewModels.Auth
{
    public class ConfirmForgotPasswordViewModel
    {
        public List<ValidationResult> Errors { get; set; }
        public string EmailOrLogin { get; set; }
        public int UserId { get; set; }
    }
}
