﻿namespace reshim.model.view.ViewModels.Auth
{
    public class ForgotPasswordFormModel
    {
        public string EmailOrLogin { get; set; }
    }
}