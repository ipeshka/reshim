﻿using System.ComponentModel.DataAnnotations;
using reshim.model.view.ViewModels.Task;

namespace reshim.model.view.ViewModels.Auth
{
    public class SimplyRegisterFormModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        public int CompletedTaskId { get; set; }

        public string PaymentNumber { get; set; }
        public string CheckoutId { get; set; }
        public string Action { get; set; }
        public string Amount { get; set; }

    }

    public class RegisterFormModel
    {
        [Required]
        [Display(Name = "Логин")]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "{0} должен содержать минимум {2} символов.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Повтор пароля")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        public string ConfirmPassword { get; set; }
    }

    public class QuickRegisterFormModel
    {
        public RegisterFormModel User { get; set; }

        public CreateTaskFormModel Task { get; set; }
    }
}