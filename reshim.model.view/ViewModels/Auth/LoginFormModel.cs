﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Auth
{
    public class LoginFormModel
    {
        [Required(ErrorMessage = @"Обязательное поле")]
        [Display(Name = @"Email или логин")]
        public string EmailOrLogin { get; set; }

        [Required(ErrorMessage = @"Обязательное поле")]
        [DataType(DataType.Password)]
        [Display(Name = @"Пароль")]
        public string Password { get; set; }
    }
}