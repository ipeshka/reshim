﻿namespace reshim.model.view.ViewModels.Auth
{
    public class ConfirmRegisterFromServiceViewModel
    {
        public string RedirectUrl { get; set; }
    }
}