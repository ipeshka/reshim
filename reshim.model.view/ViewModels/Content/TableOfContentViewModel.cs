﻿using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.model.view.ViewModels.Content
{
    public class TableOfContentViewModel
    {
        public NameLessonsViewModel NameLesson { get; set; }
        public int CurrrentLessonId { get; set; }
    }
}
