﻿using System.Collections.Generic;
using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class LessonsViewModel
    {
        // sorting
        public int? NameLessonId { get; set; }
        public IEnumerable<NameLessonsViewModel> NamesLessons { get; set; }
        public int? ChapterId { get; set; }
        public IEnumerable<ChapterViewModel> Chapters { get; set; }
        // ~
        public string CategoryAlias { get; set; }

        public IPagedList<LessonViewModel> Lessons { get; set; }
    }
}
