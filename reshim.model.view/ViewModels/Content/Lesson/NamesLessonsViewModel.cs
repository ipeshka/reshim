﻿using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class NamesLessonsViewModel
    {
        public int? NameLessonId { get; set; }
        public IPagedList<NameLessonsViewModel> NamesLessons { get; set; }
    }
}
