﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class NameLessonsFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public IEnumerable<CategoryLessonsViewModel> Categories { get; set; }
    }
}
