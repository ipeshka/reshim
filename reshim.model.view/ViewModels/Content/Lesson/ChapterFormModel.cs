﻿namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class ChapterFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int NameLessonId { get; set; }
    }
}
