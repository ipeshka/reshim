﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class ChapterViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public int NameLessonId { get; set; }
        public int Position { get; set; }
        public NameLessonsViewModel NameLesson { get; set; }
        public IEnumerable<LessonViewModel> Lessons { get; set; }
    }
}
