﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class LessonFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }

        public int NameLessonId { get; set; }
        public IEnumerable<NameLessonsViewModel> NamesLessons { get; set; }
        public int? ChapterId { get; set; }
        public IEnumerable<ChapterViewModel> Chapters { get; set; }
    }
}
