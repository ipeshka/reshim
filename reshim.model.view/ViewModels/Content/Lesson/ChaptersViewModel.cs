﻿using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class ChaptersViewModel
    {
        public int? ChapterId { get; set; }
        public int NameLessonId { get; set; }
        public IPagedList<ChapterViewModel> Chapters { get; set; }
        public NameLessonsViewModel NameLesson { get; set; }
    }
}
