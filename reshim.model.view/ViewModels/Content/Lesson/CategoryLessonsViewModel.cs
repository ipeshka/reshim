﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class CategoryLessonsViewModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public IEnumerable<CategoryLessonsViewModel> Children { get; set; }
        public CategoryLessonsViewModel Parent { get; set; }
    }
}
