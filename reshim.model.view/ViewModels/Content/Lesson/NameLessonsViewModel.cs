﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class NameLessonsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public int CategoryId { get; set; }
        public CategoryLessonsViewModel Category { get; set; }
        public IEnumerable<ChapterViewModel> Chapters { get; set; }
        public IEnumerable<LessonViewModel> Lessons { get; set; }
    }
}
