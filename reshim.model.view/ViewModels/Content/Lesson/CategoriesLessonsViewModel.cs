﻿using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class CategoriesLessonsViewModel
    {
        public int? CategoryId { get; set; }
        public int? ParentId { get; set; }
        public IPagedList<CategoryLessonsViewModel> Categories { get; set; }
        public CategoryLessonsViewModel Parent { get; set; }
    }
}
