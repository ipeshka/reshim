﻿namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class CategoryLessonsFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public CategoryLessonsViewModel Parent { get; set; }
    }
}
