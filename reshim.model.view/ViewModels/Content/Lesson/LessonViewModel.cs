﻿namespace reshim.model.view.ViewModels.Content.Lesson
{
    public class LessonViewModel
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public int Position { get; set; }

        public int CategoryId { get; set; }
        public CategoryLessonsViewModel Category { get; set; }

        public int NameLessonId { get; set; }
        public NameLessonsViewModel NameLesson { get; set; }

        public int? ChapterId { get; set; }
        public ChapterViewModel Chapter { get; set; }

        public int CountReviews { get; set; }
    }
}
