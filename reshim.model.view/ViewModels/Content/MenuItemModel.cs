﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content
{
    public class MenuItemModel
    {
        public MenuItemModel()
        {
            SubMenu = new List<MenuItemModel>();
        }

        public string Text { get; set; }
        public string Alias { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public bool Selected { get; set; }
        public object RouteValues { get; set; }

        public List<MenuItemModel> SubMenu { get; set; }
    }
}
