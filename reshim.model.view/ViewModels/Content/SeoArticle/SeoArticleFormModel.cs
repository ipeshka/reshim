﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.model.view.ViewModels.Content.SeoArticle
{
    public class SeoArticleFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string RedirectFromUrl { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        [Required(ErrorMessage = @"Введите содержимое статьи")]
        public string Text { get; set; }

        [Required(ErrorMessage = @"Выберите предмет")]
        public int TypeWorkId { get; set; }
        public IEnumerable<TypeWorkViewModel> TypesWorks { get; set; }
    }
}
