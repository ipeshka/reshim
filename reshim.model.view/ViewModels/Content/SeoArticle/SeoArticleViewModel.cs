﻿using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.model.view.ViewModels.Content.SeoArticle
{
    public class SeoArticleViewModel
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string RedirectFromUrl { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Text { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public int TypeWorkId { get; set; }
        public TypeWorkViewModel TypeWork { get; set; }
    }
}
