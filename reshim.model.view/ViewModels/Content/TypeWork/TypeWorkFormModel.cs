﻿namespace reshim.model.view.ViewModels.Content.TypeWork
{
    public class TypeWorkFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string RedirectFromUrl { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Text { get; set; }
        public string Cost { get; set; }
        public string Period { get; set; }
    }
}
