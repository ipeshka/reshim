﻿namespace reshim.model.view.ViewModels.Content.Subject
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Preview { get; set; }
        public string Image { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
    }
}
