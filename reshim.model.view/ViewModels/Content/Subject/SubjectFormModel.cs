﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Content.Subject
{
    public class SubjectFormModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = @"Укажите название")]
        public string Name { get; set; }
        public string Preview { get; set; }
        public string Image { get; set; }
    }
}
