﻿
using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Article
{
    public class CategoriesArticleViewModel
    {
        public int? CategoryId { get; set; }
        public int? ParentId { get; set; }
        public CategoryArticleViewModel Parent { get; set; }
        public IPagedList<CategoryArticleViewModel> Categories { get; set; }
    }
}
