﻿using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Content.Article
{
    public class ArticlesViewModel
    {
        public IPagedList<ArticleViewModel> Articles { get; set; }
        public CategoryArticleViewModel Category { get; set; }
    }
}
