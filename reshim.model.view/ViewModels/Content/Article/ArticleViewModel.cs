﻿namespace reshim.model.view.ViewModels.Content.Article
{
    public class ArticleViewModel
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }

        public int CategoryId { get; set; }
        public CategoryArticleViewModel Category { get; set; }
        public int CountReviews { get; set; }
    }
}
