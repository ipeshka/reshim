﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Content.Article
{
    public class CategoryArticleViewModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string Alias { get; set; }
        public string DateCreate { get; set; }
        public string DateUpdate { get; set; }
        public IEnumerable<CategoryArticleViewModel> Children { get; set; }
        public CategoryArticleViewModel Parent { get; set; }
    }
}
