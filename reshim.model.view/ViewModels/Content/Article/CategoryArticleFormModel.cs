﻿namespace reshim.model.view.ViewModels.Content.Article
{
    public class CategoryArticleFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
        public CategoryArticleViewModel Parent { get; set; }
    }
}
