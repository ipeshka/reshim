﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Content.Article
{
    public class ArticleFormModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }


        [Required(ErrorMessage = @"Выберите категорию")]
        public int CategoryId { get; set; }
        public IEnumerable<CategoryArticleViewModel> Categories { get; set; }
    }
}
