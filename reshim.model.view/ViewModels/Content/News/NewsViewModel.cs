﻿namespace reshim.model.view.ViewModels.Content.News
{
    public class NewsViewModel
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string Text { get; set; }
        public string Preview { get; set; }
        public string DateCreate { get; set; }
        public string DayCreate { get; set; }
        public string MonthCreate { get; set; }
        public string DateUpdate { get; set; }
    }
}
