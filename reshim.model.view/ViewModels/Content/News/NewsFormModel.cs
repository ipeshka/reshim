﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Content.News
{
    public class NewsFormModel
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = @"Укажите название")]
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string DescriptionSeo { get; set; }
        [Required(ErrorMessage = @"Введите текст")]
        public string Text { get; set; }
        [Required(ErrorMessage = @"Введите прьвею")]
        public string Preview { get; set; }
    }
}
