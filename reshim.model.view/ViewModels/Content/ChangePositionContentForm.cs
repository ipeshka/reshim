﻿namespace reshim.model.view.ViewModels.Content
{
    public class ChangePositionContentForm
    {
        public int[] Data { get; set; }
        public int CurPage { get; set; }
    }
}
