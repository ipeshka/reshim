﻿namespace reshim.model.view.ViewModels.Billing
{
    public class InternalPaymentViewModel
    {
        public int Id { get; set; }
        public string Create { get; set; }
        public int TaskId { get; set; }
        public string Amount { get; set; }
    }
}