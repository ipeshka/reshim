﻿using reshim.model.Entities;

namespace reshim.model.view.ViewModels.Billing
{
    public class CreateRequestInterkassaFormModel
    {
        public string Action { get; set; }
        public string CheckoutId { get; set; }
        public string PaymentNumber { get; set; }
        public decimal Amount { get; set; }
        public PaywayVia PaywayVia { get; set; }

        public bool HasCompletedTask { get; set; }
        public string Email { get; set; }
        public int CompletedTaskId { get; set; }
    }
}
