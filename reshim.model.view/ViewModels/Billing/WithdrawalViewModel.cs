﻿using System;
using reshim.model.Entities;

namespace reshim.model.view.ViewModels.Billing
{
    public class WithdrawalViewModel
    {
        public int Id { get; set; }
        public string Create { get; set; }
        public string Amount { get; set; }
        public string Purse { get; set; }
        public string Comment { get; set; }
        public WithdrawalState State { get; set; }
        public PaywayVia PaywayVia { get; set; }
    }
}