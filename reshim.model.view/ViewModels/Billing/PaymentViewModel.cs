﻿using reshim.model.Entities;

namespace reshim.model.view.ViewModels.Billing
{
    public class PaymentViewModel
    {
        public int Id { get; set; }
        public string Create { get; set; }
        public string Amount { get; set; }
        public PaymentState State { get; set; }
        public Currency Currency { get; set; }
        public PaywayVia PaywayVia { get; set; }
    }
}