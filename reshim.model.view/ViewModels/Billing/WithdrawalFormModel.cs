﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Billing
{
    public class WithdrawalFormModel
    {
        public Dictionary<string, string> TypePayments { get; set; }
        public decimal Amount { get; set; }
        public string TypePayment { get; set; }
        public string Purse { get; set; }
        public string Comment { get; set; }
    }
}