﻿using reshim.model.Entities;

namespace reshim.model.view.ViewModels.Billing
{
    public class ConfirmRequestInterkassaFormModel
    {
        public string CheckoutId { get; set; }
        public string PaymentNumber { get; set; }
        public string Amount { get; set; }
        public PaywayVia PaywayVia { get; set; }
        public Currency Currency { get; set; }
        public PaymentState State { get; set; }
        public bool HasCompletedTask { get; set; }
    }
}