﻿namespace reshim.model.view.ViewModels.Crop
{
    public class HelperCropImageFormModel
    {
        public string ImagePath { get; set; }
        public int? CropPointX { get; set; }
        public int? CropPointY { get; set; }
        public int? ImageCropWidth { get; set; }
        public int? ImageCropHeight { get; set; }

    }
}
