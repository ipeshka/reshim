﻿using System;
using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Task
{
    public class SubscriberViewModel
    {
        public int SubscriberId { get; set; }
        public int UserId { get; set; }
        public UserViewModel User { get; set; }
        public int Value { get; set; }
        public string FormatValue { get; set; }
        public DateTime DateCreate { get; set; }
        public string FormatDateCreate { get; set; }
        public string Comment { get; set; }
        public bool IsCurrentUser { get; set; }
    }
}