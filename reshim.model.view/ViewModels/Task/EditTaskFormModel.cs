﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.model.view.ViewModels.Task
{
    public class EditTaskFormModel
    {
        [Required(ErrorMessage = @"Укажите id задания")]
        public int TaskId { get; set; }

        public int? FileId { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = @"Укажите срок решения")]
        public DateTime DateOfExecution { get; set; }

        [Range(30, 10000, ErrorMessage = @"Цена может лежать в диапазоне от 30 до 10000 рублей")]
        public decimal? Cost { get; set; }

        [Required(ErrorMessage = @"Укажите предмет")]
        public int SubjectId { get; set; }

        [Required(ErrorMessage = @"Укажите тип работы")]
        public int TypeWorkId { get; set; }

        public string Comment { get; set; }

        public IEnumerable<SubjectViewModel> Subjects { get; set; }
        public IEnumerable<TypeWorkViewModel> TypeWorks { get; set; }
    }
}