﻿namespace reshim.model.view.ViewModels.Task
{
    public class RenderPartialTabsDetailTaskViewModel
    {
        public TaskViewModel Task { get; set; }
        public string Layout { get; set; }
    }
}