﻿using System;
using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Task
{
    public class CountTaskViewModel
    {
        public List<Tuple<string, string, int>> StatusTask { get; set; }
    }
}
