﻿using System;
using System.Collections.Generic;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;
using reshim.model.view.ViewModels.File;
using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Task
{
    public class TaskViewModel {
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateOfExecution { get; set; }
        public DateTime DateComplited { get; set; }
        public string ShortFormatDateCreated { get; set; }
        public string FullFormatDateCreated { get; set; }
        public string ShortFormatDateOfExecution { get; set; }
        public string FullFormatDateOfExecution { get; set; }
        public string ShortFormatDateComplited { get; set; }
        public string FullFormatDateComplited { get; set; }
        public decimal? Cost { get; set; }
        public string FormatCost { get; set; }
        public string FormatHalfCost { get; set; }
        public FileViewModel Condition { get; set; } // условие
        public FileViewModel Solution { get; set; } // решение

        public UserViewModel Customer { get; set; } // заказчик
        public UserViewModel Decided { get; set; } // исполнитель
        public List<SubscriberViewModel> Subscribers { get; set; } // подписчики на задание и стоимость
        public StatusTask Status { get; set; } // статус
        public string Comment { get; set; }

        public SubjectViewModel Subject { get; set; }
        public TypeWorkViewModel TypeWork { get; set; }

        public Dictionary<string, string> Statuses { get; set; }
    }
}