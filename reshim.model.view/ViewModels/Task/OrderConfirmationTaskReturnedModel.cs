﻿namespace reshim.model.view.ViewModels.Task
{
    public class OrderConfirmationTaskReturnedModel
    {
        public decimal Ballance { get; set; }
        public decimal Amount { get; set; }
        public bool IsValid { get; set; }
    }
}