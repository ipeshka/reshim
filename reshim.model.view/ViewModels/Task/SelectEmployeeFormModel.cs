﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Task
{
    public class SelectEmployeeFormModel
    {
        [Required(ErrorMessage = @"Не выбрано задание")]
        public int TaskId { get; set; }
        [Required(ErrorMessage = @"Не выбран подписчик")]
        public int SubscriberId { get; set; }
    }
}