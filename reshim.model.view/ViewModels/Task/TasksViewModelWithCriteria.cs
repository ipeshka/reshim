﻿using System.Collections.Generic;
using reshim.data.infrastructure.Pagination;

namespace reshim.model.view.ViewModels.Task
{
    public class TasksViewModelWithCriteria
    {
        public IPagedList<TaskViewModel> Tasks { get; set; }
        public Dictionary<string, string> Status { get; set; }
        public string SelectedStatus { get; set; }
        public bool SelectedMyTask { get; set; }
    }
}