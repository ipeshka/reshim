﻿using System.ComponentModel.DataAnnotations;

namespace reshim.model.view.ViewModels.Task
{
    public class EstimateTaskFormModel
    {
        [Required(ErrorMessage = @"Укажите задание")]
        public int TaskId { get; set; }

        [Range(10, 10000, ErrorMessage = @"Цена может лежать в диапазоне от 10 до 10000 рублей")]
        public int Value { get; set; }

        public string Comment { get; set; }
    }
}