﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.model.view.ViewModels.Task
{
    public class CreateTaskFormModel
    {
        [Required(ErrorMessage = @"Укажите файл условия")]
        [JsonProperty("idfile")]
        public int FileId { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = @"Укажите срок решения")]
        [JsonProperty("datatimeorder")]
        public DateTime DateOfExecution { get; set; }

        [Range(30, 100000, ErrorMessage = @"Цена может лежать в диапазоне от 0 до 10000 рублей")]
        [JsonProperty("cost")]
        public decimal? Cost { get; set; }

        [Required(ErrorMessage = @"Укажите предмет")]
        [JsonProperty("subject")]
        public int SubjectId { get; set; }

        [Required(ErrorMessage = @"Укажите тип работы")]
        [JsonProperty("typework")]
        public int TypeWorkId { get; set; }

        [JsonProperty("comment")]
        public string Comment { get; set; }

        public IEnumerable<SubjectViewModel> Subjects { get; set; }
        public IEnumerable<TypeWorkViewModel> TypeWorks { get; set; }
    }
}