﻿namespace reshim.model.view.ViewModels.Task
{
    public class UploadSolutionFormModel
    {
        public int TaskId { get; set; }
        public int FileId { get; set; }
    }
}