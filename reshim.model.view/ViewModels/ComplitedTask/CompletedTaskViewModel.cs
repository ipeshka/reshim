﻿using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.File;
using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.ComplitedTask
{
    public class CompletedTaskViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public decimal Cost { get; set; }
        public string FormatCost { get; set; }
        public string Comment { get; set; }

        public string Condition { get; set; }
        public int SolutionId { get; set; }
        public FileViewModel Solution { get; set; }
        public int? SubjectId { get; set; }
        public SubjectViewModel Subject { get; set; }
        public int? DecidedId { get; set; }
        public UserViewModel Decided { get; set; }

        public int CountPurchases { get; set; }
        public int CountPurchasesSuccess { get; set; }
        public int CountReviews { get; set; }
    }
}
