﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.model.view.ViewModels.ComplitedTask
{
    public class CompletedTaskForm
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = @"Укажите название задачи")]
        public string Name { get; set; }
        [Required(ErrorMessage = @"Заголовок (СЕО)")]
        public string TitleSeo { get; set; }
        [Required(ErrorMessage = @"Описание (СЕО)")]
        public string DescriptionSeo { get; set; }
        [Required(ErrorMessage = @"Ключевые слова (СЕО)")]
        public string KeywordsSeo { get; set; }

        [Required(ErrorMessage = @"Укажите стоимость задачи")]
        public decimal Cost { get; set; }
        public string Comment { get; set; }
        [Required(ErrorMessage = @"Вы не ввели условие задачи")]
        public string Condition { get; set; }
        [Required(ErrorMessage = @"Загрузите решение задачи")]
        public int SolutionId { get; set; }
        public int? SubjectId { get; set; }
        public int? DecidedId { get; set; }

        public IEnumerable<SubjectViewModel> Subjects { get; set; }
    }
}
