﻿using System;
using reshim.model.Entities;
using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.ComplitedTask
{
    public class HistoryCompletedTaskViewModel
    {
        public int Id { get; set; }
        public DateTime Create { get; set; }
        public string FormatCreate { get; set; }

        public PaymentState State { get; set; }
        public Currency Currency { get; set; }
        public PaywayVia PaywayVia { get; set; }

        public UserViewModel User { get; set; }
        public CompletedTaskViewModel CompletedTask { get; set; }
    }
}
