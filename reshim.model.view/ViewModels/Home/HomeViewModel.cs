﻿using System.Collections.Generic;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.model.view.ViewModels.Home
{
    public class HomeViewModel
    {
        public IEnumerable<SubjectViewModel> Subjects { get; set; }

    }
}
