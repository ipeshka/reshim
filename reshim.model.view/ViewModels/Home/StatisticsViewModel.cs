﻿using System.Collections.Generic;
using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Home
{
    public class StatisticsSystemViewModel
    {
        public List<UserViewModel> OnlineUsers { get; set; }
        public int CountUsers { get; set; }
        public int CountTasks { get; set; }
        public int CountComplitedTasks { get; set; }
        public UserViewModel LastRegisterUser { get; set; }
    }
}