﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Home
{
    public class PricesViewModel
    {
        public IEnumerable<PriceTypeWorkViewModel> PricesDescription { get; set; }

        public class PriceTypeWorkViewModel
        {
            public string NameTypeWork { get; set; }
            public IEnumerable<PriceSubjectViewModel> Subjects { get; set; }
        }

        public class PriceSubjectViewModel
        {
            public int CountOrder { get; set; }
            public int MinPrice { get; set; }
            public int MiddlePrice { get; set; }
            public int MaxPrice { get; set; }
            public string NameSubject { get; set; }
            public string Url { get; set; }
        }
    }
}
