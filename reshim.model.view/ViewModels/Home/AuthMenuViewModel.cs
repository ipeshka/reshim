﻿using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Home
{
    public class AuthMenuViewModel
    {
        public int CountNewMessage { get; set; }
        public UserViewModel User { get; set; }
    }
}
