﻿using System;

namespace reshim.model.view.ViewModels.User
{
    public class ProfileViewModel
    {
        public int ProfileId { get; set; }
        public string Icq { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string UrlVk { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string PlaceOfStudy { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string FormatDateOfBirth { get; set; }
        public string Comments { get; set; }
    }
}