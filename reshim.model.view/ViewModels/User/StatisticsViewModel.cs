﻿namespace reshim.model.view.ViewModels.User
{
    public class StatisticsViewModel
    {
        public int CountOrderTask { get; set; }
        public int CountSolvedTask { get; set; }
    }
}