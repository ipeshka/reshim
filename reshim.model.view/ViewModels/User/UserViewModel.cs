﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.model.view.ViewModels.User {
    public class UserViewModel {
        [Display(Name = @"ID")]
        public int UserId { get; set; }
        [Display(Name = @"Логин")]
        public string Login { get; set; }
        [Display(Name = @"Email")]
        public string Email { get; set; }
        [Display(Name = @"Имя")]
        public string FirstName { get; set; }
        [Display(Name = @"Фамилия")]
        public string LastName { get; set; }
        [Display(Name = @"Дата регистрации")]
        public DateTime DateCreated { get; set; }
        public string FullFormatDateCreated { get; set; }
        [Display(Name = @"Последняя активность")]
        public string LastLoginTime { get; set; }
        [Display(Name = @"Активация")]
        public bool Activated { get; set; }
        [Display(Name = @"Роль")]
        public UserRoles Role { get; set; }
        public ProfileViewModel Profile { get; set; }
        public PaymentDataViewModel PaymentData { get; set; }
        public string Photo { get; set; }
        public bool IsPhoto { get { return !string.IsNullOrEmpty(Photo); } }

        public string GetPhoto { get { return !string.IsNullOrEmpty(Photo) ? Photo : ConfigurationManager.AppSettings["DefaultAvatar"]; } }
    }
}