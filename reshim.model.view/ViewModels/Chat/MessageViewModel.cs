﻿using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Chat
{
    public class MessageViewModel
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public string DateCreate { get; set; }
        public string TimeCreate { get; set; }
        public string Interval { get; set; }
        public bool IsNew { get; set; }
        public int DialogId { get; set; }
        public int SenderId { get; set; }
        public UserViewModel Sender { get; set; }
        public int RecipientId { get; set; }
        public UserViewModel Recipient { get; set; }
    }
}