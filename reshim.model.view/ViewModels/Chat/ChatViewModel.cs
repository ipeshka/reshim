﻿using System.Collections.Generic;

namespace reshim.model.view.ViewModels.Chat
{
    public class ChatViewModel
    {
        public IEnumerable<MessageViewModel> Messages { get; set; }
        public int User1Id { get; set; }
        public int User2Id { get; set; }
        public int DialogId { get; set; }
    }
}
