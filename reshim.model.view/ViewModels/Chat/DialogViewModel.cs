﻿using reshim.model.view.ViewModels.User;

namespace reshim.model.view.ViewModels.Chat
{
    public class DialogViewModel
    {
        public int Id { get; set; }
        public UserViewModel User1 { get; set; }
        public UserViewModel User2 { get; set; }
        public int CountNewMessage { get; set; }
        public int CountAllMessage { get; set; }
    }
}
