﻿namespace reshim.model.view.ViewModels.Chat
{
    public class GetMessagesFormModel
    {
        public int Page { get; set; }
        public int Count { get; set; }
        public int DialogId { get; set; }
    }
}
