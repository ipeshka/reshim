﻿namespace reshim.model.view.ViewModels.File
{
    public class FileViewModel
    {
        public int FileId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
    }
}