﻿namespace reshim.model.view.ViewModels.File
{
    public class CreateFileReturn
    {
        public int LastId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public bool IsUploaded { get; set; }
    }
}