﻿using System;
using System.Security.Principal;
using System.Web.Security;

namespace reshim.web.core.Model
{
    [Serializable]
    public class ReshimUser : IIdentity
    {
        public ReshimUser()
        {
        }

        public ReshimUser(string login, int userId)
        {
            this.Name = login;
            this.UserId = userId;
        }
        public ReshimUser(string login, int userId, string roleName)
        {
            this.Name = login;
            this.UserId = userId;
            this.RoleName = roleName;
        }

        public ReshimUser(string login, UserInfo userInfo)
            : this(userInfo.Login, userInfo.UserId, userInfo.RoleName)
        {
            if (userInfo == null) throw new ArgumentNullException("userInfo");
            this.UserId = userInfo.UserId;
            this.Name = login;
        }

        public ReshimUser(FormsAuthenticationTicket ticket)
            : this(ticket.Name, UserInfo.FromString(ticket.UserData))
        {
            if (ticket == null) throw new ArgumentNullException("ticket");
        }

        

        public string AuthenticationType
        {
            get { return "ReshimForms"; }
        }

        public string Name { get; private set; }
        public string RoleName { get; private set; }
        public int UserId { get; private set; }


        public bool IsAuthenticated { get; set; }
    
    }
}
