﻿using System.IO;
using System.Xml.Serialization;

namespace reshim.web.core.Model {
    public class UserInfo {
        public string Login { get; set; }
        public int UserId { get; set; }
        public string RoleName { get; set; }
        public string Img { get; set; }

        public override string ToString() {
            var serializer = new XmlSerializer(typeof(UserInfo));
            using (var stream = new StringWriter()) {
                serializer.Serialize(stream, this);
                return stream.ToString();
            }
        }

        public static UserInfo FromString(string userContextData) {
            var serializer = new XmlSerializer(typeof(UserInfo));
            using (var stream = new StringReader(userContextData)) {
                return serializer.Deserialize(stream) as UserInfo;
            }
        }
    }
}
