﻿using System.Web;
using System.Web.Security;

namespace reshim.web.core.Auth {
    public interface IFormsAuthentication {
        void Signout();
        void SetAuthCookie(HttpContextBase httpContext, FormsAuthenticationTicket authenticationTicket);
        void SetAuthCookie(HttpContext httpContext, FormsAuthenticationTicket authenticationTicket);
        void SetAuthCookie(string userName, bool persistent);
        FormsAuthenticationTicket Decrypt(string encryptedTicket);
    }
}
