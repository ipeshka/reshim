﻿using System;
using System.Web.Security;
using reshim.model.Entities;
using reshim.model.view.ViewModels.User;
using reshim.web.core.Model;

namespace reshim.web.core.Auth {
    public class UserAuthenticationTicketBuilder {
        public static FormsAuthenticationTicket CreateAuthenticationTicket(UserViewModel user) {
            UserInfo userInfo = CreateUserContextFromUser(user);

            var ticket = new FormsAuthenticationTicket(
                1,
                user.Login,
                DateTime.Now,
                DateTime.Now.Add(new TimeSpan(1, 0, 0, 0)),
                false,
                userInfo.ToString());

            return ticket;
        }

        private static UserInfo CreateUserContextFromUser(UserViewModel user)
        {
            var userContext = new UserInfo {
                UserId = user.UserId,
                Login = user.Login,
                RoleName = Enum.GetName(typeof(UserRoles), user.Role)
            };

            return userContext;
        }
    }
}
