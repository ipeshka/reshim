﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;

namespace reshim.web.core.Extensions
{
    public static class CommonExtensions
    {
        public static string ToFullPath(this string url)
        {
            return ConfigurationManager.AppSettings["AbsolutePath"].Replace("\\", "/").TrimEnd(new[] { '/' }) + url;
        }

        public static int? ToNullableInt32(this string s)
        {
            int i;
            if (Int32.TryParse(s, out i)) return i;
            return null;
        }

        public static string ToSeoUrl(this string url)
        {
            // make the url lowercase
            string encodedUrl = (url ?? "").ToLower();

            // replace & with and
            encodedUrl = Regex.Replace(encodedUrl, @"\&+", "and");

            // remove characters
            encodedUrl = encodedUrl.Replace("'", "");

            // remove invalid characters
            encodedUrl = Regex.Replace(encodedUrl, @"[^a-z0-9]", "-");

            // remove duplicates
            encodedUrl = Regex.Replace(encodedUrl, @"-+", "-");

            // trim leading & trailing characters
            encodedUrl = encodedUrl.Trim('-');

            return encodedUrl;
        }

        public static bool IsEmail(string emailaddress)
        {
            try
            {
                var m = new MailAddress(emailaddress);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static string Interval(this DateTime dateTime)
        {
            DateTime curDateTime = DateTime.Now;

            if ((curDateTime - dateTime).Days >= 1)
            {
                return string.Format("{0} дней назад", (curDateTime - dateTime).Days);
            }
            else if ((curDateTime - dateTime).Hours >= 1)
            {
                return string.Format("{0} часов назод", (curDateTime - dateTime).Hours);
            }
            else if ((curDateTime - dateTime).Minutes >= 1)
            {
                return string.Format("{0} минут назад", (curDateTime - dateTime).Minutes);
            }
            else
            {
                return string.Format("{0} секунд назад", (curDateTime - dateTime).Seconds);
            }
        }

        public static model.Entities.File RenameFile(this model.Entities.File file, string newFilename)
        {
            var directory = Path.GetDirectoryName(file.Path);
            if (directory != null)
            {
                var newpath = Path.Combine(directory, newFilename);

                var path = HttpContext.Current.Server.MapPath("~");
                string fromFile = path + file.Path;
                string toFile = path + newpath;
                File.Move(fromFile, toFile);

                file.Name = newFilename;
                file.Path = newpath;
            }

            return file;
        }
    }
}
