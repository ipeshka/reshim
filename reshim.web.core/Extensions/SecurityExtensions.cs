﻿using System.Linq;
using System.Security.Principal;
using reshim.web.core.Model;

namespace reshim.web.core.Extensions {
    public static class SecurityExtensions {
        public static string Name(this IPrincipal user) {
            return user.Identity.Name;
        }

        public static bool InAnyRole(this IPrincipal user, params string[] roles) {
            return roles.Any(user.IsInRole);
        }

        public static ReshimUser GetReshimUser(this IPrincipal principal) {
            if (principal.Identity is ReshimUser)
                return (ReshimUser)principal.Identity;
            return new ReshimUser(string.Empty, new UserInfo());
        }
    }
}
