﻿using System.Collections.Generic;
using System.Web.Http.ModelBinding;
using reshim.common;

namespace reshim.web.core.Extensions
{
    public static class ApiExtensions
    {
        public static IEnumerable<ValidationResult> GetError(this ModelStateDictionary modelState)
        {
            foreach (var key in modelState.Keys)
            {
                foreach (var error in modelState[key].Errors)
                {
                    yield return new ValidationResult(key, error.ErrorMessage);
                }
            }
        }
    }
}