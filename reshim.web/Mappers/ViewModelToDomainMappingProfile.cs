﻿using System;
using System.Globalization;
using AutoMapper;
using reshim.common.core.Extensions;
using reshim.domain.Commands.Billing;
using reshim.domain.Commands.CompletedTask;
using reshim.domain.Commands.Content;
using reshim.domain.Commands.Profile;
using reshim.domain.Commands.Security;
using reshim.domain.Commands.Task;
using reshim.domain.CommandsHandlers.CompletedTask;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Auth;
using reshim.model.view.ViewModels.Billing;
using reshim.model.view.ViewModels.ComplitedTask;
using reshim.model.view.ViewModels.Content.Article;
using reshim.model.view.ViewModels.Content.Lesson;
using reshim.model.view.ViewModels.Content.News;
using reshim.model.view.ViewModels.Content.SeoArticle;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;
using reshim.model.view.ViewModels.Task;
using reshim.model.view.ViewModels.User;
using reshim.web.core.Extensions;
using Profile = AutoMapper.Profile;

namespace reshim.web.Mappers
{
    class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMappings"; }
        }

        protected override void Configure()
        {
            // billing
            Mapper.CreateMap<CreateRequestInterkassaFormModel, CreateRequestInterkassaCommand>();
            Mapper.CreateMap<ConfirmRequestInterkassaFormModel, ConfirmRequestInterkassaCommand>()
                .ForMember(x => x.Amount, y => y.MapFrom(s => Decimal.Parse(s.Amount, new NumberFormatInfo() { NumberDecimalSeparator = "." })));
            Mapper.CreateMap<WithdrawalFormModel, AddWithdrawalCommand>()
                .ForMember(x => x.TypePayment, y => y.MapFrom(s => EnumEx.GetEnumFromTypeAttribute<PaywayVia>(s.TypePayment)));

            // content
            Mapper.CreateMap<TypeWorkFormModel, CreateOrEditTypeWorkCommand>();
            Mapper.CreateMap<SubjectFormModel, CreateOrEditSubjectCommand>();
            Mapper.CreateMap<NewsFormModel, CreateOrEditNewsCommand>();
            Mapper.CreateMap<SeoArticleFormModel, CreateOrEditSeoArticleCommand>();
            Mapper.CreateMap<CategoryArticleFormModel, CreateOrEditCategoryArticleCommand>();
            Mapper.CreateMap<ArticleFormModel, CreateOrEditArticleCommand>();
            Mapper.CreateMap<CategoryLessonsFormModel, CreateOrEditCategoryLessonCommand>();
            Mapper.CreateMap<NameLessonsFormModel, CreateOrEditNameLessonCommand>();
            Mapper.CreateMap<ChapterFormModel, CreateOrEditChapterCommand>();
            Mapper.CreateMap<LessonFormModel, CreateOrEditLessonCommand>();         
           

            // auth
            Mapper.CreateMap<RegisterFormModel, UserRegisterCommand>();
            Mapper.CreateMap<QuickRegisterFormModel, UserQuickRegisterCommand>();
            Mapper.CreateMap<LoginFormModel, UserLoginCommand>()
                .ForMember(x => x.HasEmail, y => y.MapFrom(s => CommonExtensions.IsEmail(s.EmailOrLogin)));
            Mapper.CreateMap<ForgotPasswordFormModel, UserForgotPasswordCommand>()
                .ForMember(x => x.HasEmail, y => y.MapFrom(s => CommonExtensions.IsEmail(s.EmailOrLogin)));
            Mapper.CreateMap<UpdatePasswordFormModel, UpdatePasswordCommand>();

            // task
            Mapper.CreateMap<CreateTaskFormModel, CreateTaskCommand>();
            Mapper.CreateMap<EstimateTaskFormModel, EstimateTaskCommand>();
            Mapper.CreateMap<SelectEmployeeFormModel, SelectEmployeeTaskCommand>();
            Mapper.CreateMap<UploadSolutionFormModel, UploadSolutionCommand>();

            // completed task
            Mapper.CreateMap<CompletedTaskForm, CreateCompletedTaskCommand>()
                .ForMember(x => x.FileId, y => y.MapFrom(s => s.SolutionId));
            Mapper.CreateMap<CompletedTaskForm, UpdateCompletedTaskCommand>()
                .ForMember(x => x.FileId, y => y.MapFrom(s => s.SolutionId));
            Mapper.CreateMap<CompletedTaskViewModel, CompletedTaskForm>();
            
            Mapper.CreateMap<CreateRequestInterkassaFormModel, SimplyUserRegisterCommand>();


            // content
            // user
            Mapper.CreateMap<ProfileViewModel, EditProfileCommand>();
        }
    }
}