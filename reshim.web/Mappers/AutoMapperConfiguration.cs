﻿using AutoMapper;

namespace reshim.web.Mappers {
    public class AutoMapperConfiguration {

        public static void Configure() {
            Mapper.Initialize(x =>
            {
                x.AddProfile<ViewModelToDomainMappingProfile>();
                x.AddProfile<DomainToViewModelMappingProfile>();
            });
        }
    }
}