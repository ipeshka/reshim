﻿using System.Globalization;
using System.Linq;
using System.Web;
using AutoMapper;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;
using reshim.model.view.ViewModels.Chat;
using reshim.model.view.ViewModels.ComplitedTask;
using reshim.model.view.ViewModels.Content.Article;
using reshim.model.view.ViewModels.Content.Lesson;
using reshim.model.view.ViewModels.Content.News;
using reshim.model.view.ViewModels.Content.SeoArticle;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;
using reshim.model.view.ViewModels.File;
using reshim.model.view.ViewModels.Task;
using reshim.model.view.ViewModels.User;
using reshim.web.core.Extensions;

namespace reshim.web.Mappers
{
    public class DomainToViewModelMappingProfile : AutoMapper.Profile
    {
        public override string ProfileName { get { return "DomainToViewModelMappings"; } }

        protected override void Configure()
        {
            // billings
            Mapper.CreateMap<PaymentData, PaymentDataViewModel>()
                .ForMember(x => x.Ballance, y => y.MapFrom(s => s.Ballance.ToString("C", new CultureInfo("ru-RU"))));
            Mapper.CreateMap<Payment, PaymentViewModel>()
                .ForMember(x => x.Amount, y => y.MapFrom(s => s.Amount.ToString("C", new CultureInfo("ru-RU"))))
                .ForMember(x => x.Create, y => y.MapFrom(s => s.Create.ToString("f", new CultureInfo("ru-RU"))));
            Mapper.CreateMap<Withdrawal, WithdrawalViewModel>()
                .ForMember(x => x.Amount, y => y.MapFrom(s => s.Amount.ToString("C", new CultureInfo("ru-RU"))))
                .ForMember(x => x.Create, y => y.MapFrom(s => s.Create.ToString("f", new CultureInfo("ru-RU"))));
            Mapper.CreateMap<InternalPayment, InternalPaymentViewModel>()
                .ForMember(x => x.Amount, y => y.MapFrom(s => s.Amount.ToString("C", new CultureInfo("ru-RU"))))
                .ForMember(x => x.Create, y => y.MapFrom(s => s.DatePayment.ToString("f", new CultureInfo("ru-RU"))));

            // чат
            Mapper.CreateMap<Message, MessageViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", new CultureInfo("ru-RU"))))
                .ForMember(x => x.Interval, y => y.MapFrom(s => s.DateCreate.Interval()))
                .ForMember(x => x.TimeCreate, y => y.MapFrom(s => s.DateCreate.ToString("H:mm:ss")));
            Mapper.CreateMap<Dialog, DialogViewModel>()
                .ForMember(x => x.CountAllMessage, y => y.MapFrom(s => s.Messages.Count))
                .ForMember(x => x.CountNewMessage, y => y.MapFrom(s => s.Messages != null ? s.Messages.Count(x => x.IsNew && x.RecipientId == HttpContext.Current.User.GetReshimUser().UserId && x.DialogId == s.Id) : 0));

            // content
            Mapper.CreateMap<TypeWork, TypeWorkViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<Subject, SubjectViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<News, NewsViewModel>()
                .ForMember(x => x.DayCreate, y => y.MapFrom(s => s.DateCreate.Day))
                .ForMember(x => x.MonthCreate, y => y.MapFrom(s => s.DateCreate.ToString("MMM", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<SeoArticle, SeoArticleViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<CategoryArticle, CategoryArticleViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<Article, ArticleViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"))
                .ForMember(x => x.CountReviews, y => y.MapFrom(s => s.Reviews != null ? s.Reviews.Count : 0));
            Mapper.CreateMap<CategoryLesson, CategoryLessonsViewModel>()
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<NameLesson, NameLessonsViewModel>()
                .ForMember(x => x.Lessons, y => y.MapFrom(s => s.Lessons != null ? s.Lessons.OrderBy(x => x.Position) : null))
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<Chapter, ChapterViewModel>()
                .ForMember(x => x.Lessons, y => y.MapFrom(s => s.Lessons != null ? s.Lessons.OrderBy(x => x.Position) : null))
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"));
            Mapper.CreateMap<Lesson, LessonViewModel>()
                .ForMember(x => x.CategoryId, y => y.MapFrom(s => s.CategoryIdLesson))
                .ForMember(x => x.Category, y => y.MapFrom(s => s.CategoryLesson))
                .ForMember(x => x.DateCreate, y => y.MapFrom(s => s.DateCreate.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU"))))
                .ForMember(x => x.DateUpdate, y => y.MapFrom(s => s.DateUpdate.HasValue ? s.DateUpdate.Value.ToString("D", CultureInfo.CreateSpecificCulture("ru-RU")) : "Не обновлялся"))
                .ForMember(x => x.CountReviews, y => y.MapFrom(s => s.Reviews != null ? s.Reviews.Count : 0));

            // user
            Mapper.CreateMap<User, UserViewModel>()
                .ForMember(x => x.FullFormatDateCreated, y => y.MapFrom(s => string.Format("{0} в {1}", s.DateCreated.ToString("D", new CultureInfo("ru-RU")), s.DateCreated.ToString("H:mm:ss"))))
                .ForMember(x => x.LastLoginTime, y => y.MapFrom(s => s.LastLoginTime.HasValue ? string.Format("{0} в {1}", s.LastLoginTime.Value.ToString("D", new CultureInfo("ru-RU")), s.LastLoginTime.Value.ToString("H:mm:ss")) : "Активность не наблюдалась"));
            Mapper.CreateMap<model.Entities.Profile, ProfileViewModel>()
                .ForMember(x => x.FormatDateOfBirth, y => y.MapFrom(s => s.DateOfBirth.HasValue ? s.DateOfBirth.Value.ToString("D", new CultureInfo("ru-RU")) : "Пользователь не указал"));

            // task
            Mapper.CreateMap<Task, TaskViewModel>().ForMember(x => x.Status, y => y.MapFrom(s => s.Status))
                .ForMember(x => x.FormatCost, y => y.MapFrom(s => s.Cost != 0 ? s.Cost.ToString("C", new CultureInfo("ru-RU")) : "Договорная цена"))
                .ForMember(x => x.FormatHalfCost, y => y.MapFrom(s => s.Cost != 0 ? (s.Cost / 2).ToString("C", new CultureInfo("ru-RU")) : "Договорная цена"))
                .ForMember(x => x.ShortFormatDateCreated, y => y.MapFrom(s => s.DateCreated.ToString("g", new CultureInfo("ru-RU"))))
                .ForMember(x => x.ShortFormatDateComplited, y => y.MapFrom(s => s.DateComplited.ToString("g", new CultureInfo("ru-RU"))))
                .ForMember(x => x.ShortFormatDateOfExecution, y => y.MapFrom(s => s.DateOfExecution.ToString("D", new CultureInfo("ru-RU"))))
                .ForMember(x => x.FullFormatDateCreated, y => y.MapFrom(s => s.DateCreated.ToString("f", new CultureInfo("ru-RU"))))
                .ForMember(x => x.FullFormatDateComplited, y => y.MapFrom(s => s.DateComplited.ToString("f", new CultureInfo("ru-RU"))))
                .ForMember(x => x.FullFormatDateOfExecution, y => y.MapFrom(s => s.DateOfExecution.ToString("f", new CultureInfo("ru-RU"))));
            Mapper.CreateMap<File, FileViewModel>();
            Mapper.CreateMap<Subscriber, SubscriberViewModel>()
                .ForMember(x => x.FormatValue, y => y.MapFrom(s => s.Value.ToString("C", new CultureInfo("ru-RU"))))
                .ForMember(x => x.FormatDateCreate, y => y.MapFrom(s => s.DateCreate.ToString("F", new CultureInfo("ru-RU"))));

            // completed task
            Mapper.CreateMap<CompletedTask, CompletedTaskViewModel>()
                .ForMember(x => x.CountReviews, y => y.MapFrom(c => c.Reviews != null ? c.Reviews.Count : 0))
                .ForMember(x => x.CountPurchases, y => y.MapFrom(c => c.HistoriesCompletedTasks != null ? c.HistoriesCompletedTasks.Count : 0))
                .ForMember(x => x.CountPurchasesSuccess, y => y.MapFrom(c => c.HistoriesCompletedTasks != null ? c.HistoriesCompletedTasks.Count(x => x.State == PaymentState.Success) : 0))
                .ForMember(x => x.FormatCost, y => y.MapFrom(c => c.Cost.ToString("C", new CultureInfo("ru-RU"))));
            Mapper.CreateMap<HistoryCompletedTask, HistoryCompletedTaskViewModel>()
                .ForMember(x => x.FormatCreate, y => y.MapFrom(c => c.Create.ToString("f", new CultureInfo("ru-RU"))));

        }
    }
}