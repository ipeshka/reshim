using System;
using System.Collections.Generic;
using Autofac;


namespace reshim.web.DI.Autofac
{
    public class AutofacDependencyInjectionContainer
        : IDependencyInjectionContainer
    {
        public AutofacDependencyInjectionContainer(IContainer container)
        {
            if (container == null)
                throw new ArgumentNullException("container");
            this._container = container;
        }
        private readonly IContainer _container;

        public object GetInstance(Type type)
        {
            if (type == null)
            {
                return null;
            }

            return this._container.Resolve(type);
        }

        public object TryGetInstance(Type type)
        {
            if (type == null)
            {
                return null;
            }

            return this._container.ResolveOptional(type);
        }

        public IEnumerable<object> GetAllInstances(Type type)
        {
            Type lookupType = typeof(IEnumerable<>).MakeGenericType(new Type[] { type });
            object obj = this._container.Resolve(lookupType);
            return (IEnumerable<object>)obj;
        }

        public void Release(object instance)
        {
// Do nothing
        }

    }
}
