﻿using System.Collections.Generic;
using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.domain.Queryies.Content.Subject;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.domain.Queryies.Task;
using reshim.domain.StrategiesFetch;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;
using reshim.web.core.Extensions;
using reshim.web.Helpers.Binders;

namespace reshim.web.Controllers
{
    [Authorize]
    public class TaskController : BaseController
    {
        private readonly IQueryService _queryService;

        public TaskController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        [AllowAnonymous]
        public ActionResult Order(int? typeWork)
        {
            var subject = _queryService.ExecuteQuery(new GetAllSubjects());
            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            var model = new CreateTaskFormModel() { Subjects = subject, TypeWorks = typeWorks };
            if (typeWork.HasValue) model.TypeWorkId = typeWork.Value;
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Index([ModelBinder(typeof(TaskWithCriteriaBinder))]GetTasksByCriteria model)
        {
            model.Count = CountRecord;
            model.IncludeProperties = new TaskFetchStrategyNumber3();
            var taskView = _queryService.ExecuteQuery(model);
            return View(taskView);
        }

        [AllowAnonymous]
        public ActionResult Detail(int id)
        {
            return View(id);
        }

        public ActionResult EditTask(int id)
        {
            var subject = _queryService.ExecuteQuery(new GetAllSubjects());
            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            var taskView = _queryService.ExecuteQuery(new GetTaskById() { Id = id });
            if (taskView.Customer.UserId != User.UserId)
            {
                this.AddModelErrors(new List<ValidationResult>() { new ValidationResult("Вам запрещено редактировать этот заказ!") });
                return View();
            }
            return View(new EditTaskFormModel()
            {
                TaskId = taskView.Id,
                DateOfExecution = taskView.DateOfExecution,
                Comment = taskView.Comment,
                Cost = taskView.Cost,
                FileId = taskView.Condition.FileId,
                SubjectId = taskView.Subject.Id,
                Subjects = subject,
                TypeWorkId = taskView.TypeWork.Id,
                TypeWorks = typeWorks
            });
        }

        #region partial

        private const string PathViews = "~/Views/Task/Partials/State/";

        private readonly Dictionary<StatusTask, string> _templatesIsCustomer = new Dictionary<StatusTask, string>
        {
            { StatusTask.New, PathViews +  "New/_Index.cshtml"},
            { StatusTask.Rating, PathViews +  "Rating/_Customer.cshtml"},
            { StatusTask.Agreement, PathViews +  "Agreement/_Customer.cshtml"},
            { StatusTask.CompletedNotPay, PathViews +  "CompletedNotPay/_Customer.cshtml"},
            { StatusTask.Completed, PathViews +  "Completed/_Customer.cshtml"}
        };

        private readonly Dictionary<StatusTask, string> _templates = new Dictionary<StatusTask, string>
        {
            { StatusTask.New, PathViews +  "New/_Index.cshtml"},
            { StatusTask.Rating, PathViews +  "Rating/_User.cshtml"},
            { StatusTask.Agreement, PathViews +  "Agreement/_User.cshtml"},
            { StatusTask.CompletedNotPay, PathViews +  "CompletedNotPay/_User.cshtml"},
            { StatusTask.Completed, PathViews +  "Completed/_User.cshtml"}
        };

        [AllowAnonymous]
        public ActionResult PartialDetailTask(int taskId)
        {
            var taskView = _queryService.ExecuteQuery(new GetTaskById()
            {
                Id = taskId,
                IncludeProperties = new TaskFetchStrategyNumber1()
            });
            taskView.Statuses = EnumEx.GetKeyValue<StatusTask, NameAttribute, NameAttribute, TypeAttribute>(typeof(IsDisplayAttribute));


            if (taskView.Customer.UserId == User.UserId)
            {
                return PartialView("~/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesIsCustomer[taskView.Status],
                        Task = taskView
                    });
            }


            return PartialView("~/Views/Task/Partials/_PartialDetailTask.cshtml",
                new RenderPartialTabsDetailTaskViewModel()
                {
                    Layout = _templates[taskView.Status],
                    Task = taskView
                });
        }

        #endregion


    }
}
