﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.Content.Article;
using reshim.domain.Queryies.Content.Lesson;
using reshim.domain.Queryies.Content.News;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.domain.StrategiesFetch;
using reshim.model.view.ViewModels.Content;
using reshim.model.view.ViewModels.Content.Article;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.web.Controllers
{
    public class ContentController : BaseController
    {
        private readonly IQueryService _queryService;

        public ContentController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        #region seoarticle
        public ActionResult TypeWork(string alias)
        {
            var typeWork = _queryService.ExecuteQuery(new GetTypeWorkByAlias() { Alias = alias });
            return View(typeWork);
        }

        public ActionResult ArticleSeo(string alias, string tw)
        {
            var articleSeo = _queryService.ExecuteQuery(new GetSeoArticleByAlias() { Alias = alias });
            return View(articleSeo);
        }
        #endregion

        #region news
        public ActionResult AllNews(int? page)
        {
            int p = page ?? 1;
            var newsView = _queryService.ExecuteQuery(new GetNews() { Count = CountRecordsNews, Page = p });
            return View(newsView);
        }

        public ActionResult News(string alias)
        {
            var news = _queryService.ExecuteQuery(new GetNewsByAlias() { Alias = alias });
            return View(news);
        }
        #endregion

        #region articles
        public ActionResult Articles(int? page, string category)
        {
            var model = new ArticlesViewModel();

            int p = page ?? 1;
            var articlesView = _queryService.ExecuteQuery(new GetArticles()
            {
                Count = CountRecordsNews,
                Page = p,
                CategoryAlias = category
            });

            model.Articles = articlesView;

            if(!string.IsNullOrEmpty(category))
                model.Category = _queryService.ExecuteQuery(new GetCategoryArticleByAlias() { Alias = category });

            return View(model);
        }

        public ActionResult Article(string alias)
        {
            var article = _queryService.ExecuteQuery(new GetArticleByAlias() { Alias = alias });
            return View(article);
        }
        #endregion

        #region lessons
        public ActionResult Lessons(int? page, string category)
        {
            var model = new LessonsViewModel();
            int p = page ?? 1;

            var lessons = _queryService.ExecuteQuery(new GetLessons()
            {
                Count = CountRecordsNews,
                Page = p,
                IncludeProperties = new LessonFetchStrategyNumber1(),
                CategoryAlias = category,
                IsDesc = true
            });

            model.Lessons = lessons;
            model.CategoryAlias = category;

            return View(model);
        }

        public ActionResult Lesson(string alias)
        {
            var lesson = _queryService.ExecuteQuery(new GetLessonByAlias() { Alias = alias });
            return View(lesson);
        }

        public ActionResult TableOfContentLesson(int nameLessonId, int lessonId, int? chapterId)
        {
            var modelView = new TableOfContentViewModel
            {
                CurrrentLessonId = lessonId,
                NameLesson = _queryService.ExecuteQuery(new GetNameLessonsById() { Id = nameLessonId , IncludeProperties = new NameLessonsFetchStrategyNumber1() })
            };
            return PartialView("Partial/_TableOfContentLesson", modelView);
        }
        #endregion
    }
}
