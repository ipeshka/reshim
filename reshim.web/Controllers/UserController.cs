﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.Profile;
using reshim.domain.Queryies.User;
using reshim.domain.StrategiesFetch;

namespace reshim.web.Controllers
{
    [Authorize]
    public class UserController : BaseController
    {
        private readonly IQueryService _queryService;

        public UserController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        [AllowAnonymous]
        public ActionResult Index(int? id)
        {
            var userId = id ?? User.UserId;
            return View(userId);
        }

        [AllowAnonymous]
        public PartialViewResult ProfileUser(int id)
        {
            var userView = _queryService.ExecuteQuery(new GetUserById()
            {
                Id = id,
                IncludeProperties = new UserFetchStrategy()
            });
            return PartialView("Partials/_ProfileUser", userView);
        }

        public ActionResult Profile()
        {
            return View();
        }

        public PartialViewResult EditProfile()
        {
            var viewProfile = _queryService.ExecuteQuery(new GetProfileByUserId() { Id = User.UserId });
            return PartialView("Partials/_EditProfile", viewProfile);
        }

        public PartialViewResult EditPhoto()
        {
            var viewProfile = _queryService.ExecuteQuery(new GetUserById() { Id = User.UserId });
            return PartialView("Partials/_EditPhoto", viewProfile);
        }

        public ActionResult PersonalCabinet()
        {
            var model = _queryService.ExecuteQuery(new GetStatisticsTaskUser() { UserId = User.UserId });
            return View(model);
        }

    }
}
