﻿using System;
using System.Web;
using System.Web.Mvc;
using reshim.web.core.Extensions;
using reshim.web.core.Model;

namespace reshim.web.Controllers
{
    public class BaseController : Controller
    {
        protected const int CountRecord = 10;
        protected const int CountRecordsCompletedTask = 9;
        protected const int CountRecordsNews = 5;

        public string SiteUrl
        {
            get
            {
                var request = HttpContext.Request;
                return string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);
            }
        }

        public void SetFlagCookie(string name)
        {
            var cookie = new HttpCookie(name)
            {
                Value = DateTime.Now.ToString("dd.MM.yyyy"),
                Expires = DateTime.Now.AddDays(365)
            };
            Response.SetCookie(cookie);
        }

        public void DeleteCookie(string name)
        {
            if (Request.Cookies[name] != null)
            {
                var c = new HttpCookie(name) { Expires = DateTime.Now.AddDays(-1) };
                Response.Cookies.Add(c);
            }
        }

        public new ReshimUser User
        {
            get { return base.User.GetReshimUser(); }
        }

        public bool IsValidCookies(HttpCookie authCookie)
        {
            return authCookie != null && !String.IsNullOrEmpty(authCookie.Value);
        }
    }
}