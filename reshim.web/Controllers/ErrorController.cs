namespace reshim.web.Controllers
{
    using System.Web.Mvc;
	using Moveax.Mvc.ErrorHandler;

    public class ErrorController : ErrorHandlerControllerBase
    {
        public override ActionResult Default(ErrorDescription errorDescription)
        {
            return View("Default");
        }

        public ActionResult Http404(ErrorDescription errorDescription)
        {
            return View();
        }

        public ActionResult Http401(ErrorDescription errorDescription)
        {
            return View();
        }

        public ActionResult Http403(ErrorDescription errorDescription)
        {
            return View();
        }

		/// <summary>Called before the action method is invoked. Use it for error logging etc</summary>
        /// <param name="errorDescription">The error description.</param>
        protected override void HandleError(ErrorDescription errorDescription)
        {
            // TODO: Add a logging code here (just a reminder).
        }
    }
}
