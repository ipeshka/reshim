﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.domain.Queryies.Billing;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;
using reshim.web.Helpers.Binders;

namespace reshim.web.Controllers
{
    [Authorize]
    public class BillingController : BaseController
    {
        private readonly IQueryService _queryService;
        
        public BillingController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PaymentForm(PaywayVia payway)
        {
            return View(payway);
        }

        public ActionResult WithdrawalForm()
        {
            var model = new WithdrawalFormModel
            {
                TypePayments = EnumEx.GetKeyValue<PaywayVia, EnableWithdrawalAttribute, NameAttribute, TypeAttribute>()
            };
            return View(model);
        }

        public ActionResult HistoryPayments(int? page)
        {
            int p = page ?? 1;
            var invocesView = _queryService.ExecuteQuery(new GetPaymentsByUserId() { Count = CountRecord, Page = p, UserId = User.UserId });
            return View(invocesView);
        }

        public ActionResult HistoryWithdrawels(int? page)
        {
            int p = page ?? 1;
            var withdrawalsView = _queryService.ExecuteQuery(new GetWithdrawelsByUserId() { Count = CountRecord, Page = p, UserId = User.UserId });
            return View(withdrawalsView);
        }

        public ActionResult HistoryInternalPayments(int? page)
        {
            int p = page ?? 1;
            var paymentsView = _queryService.ExecuteQuery(new GetInternalPaymentsByUserId() { Count = CountRecord, Page = p, UserId = User.UserId });
            return View(paymentsView);
        }

        [HttpPost]
        public ActionResult Success([ModelBinder(typeof(InterkassaSuccesOrFailBinder))]ConfirmRequestInterkassaFormModel model)
        {
            var cookieTask = Request.Cookies["ComplitedTask"];
            model.HasCompletedTask = IsValidCookies(cookieTask);
            if (model.HasCompletedTask) DeleteCookie("ComplitedTask");
            return View(model);
        }

        [HttpPost]
        public ActionResult Fail([ModelBinder(typeof(InterkassaSuccesOrFailBinder))]ConfirmRequestInterkassaFormModel model)
        {
            return View(model);
        }
    }
}
