﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.CompletedTask;
using reshim.domain.Queryies.Content.Article;
using reshim.domain.Queryies.Content.Lesson;
using reshim.domain.Queryies.Content.News;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.web.Helpers.SiteMap;
using reshim.domain.Queryies.Content.TypeWork;

namespace reshim.web.Controllers
{
    public class SeoController : BaseController
    {
        private readonly IQueryService _queryService;

        public SeoController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        public ActionResult Sitemap()
        {
            var builder = new SitemapBuilder();

            builder.AppendUrl(Url.Action("Index", "Home", null, Request.Url.Scheme), ChangefreqEnum.Hourly);

            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            foreach (var tw in typeWorks)
            {
                builder.AppendUrl(string.Format("{0}{1}", Url.Action("Index", "Home", null, Request.Url.Scheme), tw.Alias), ChangefreqEnum.Never, 1);
            }

            var articlesSeo = _queryService.ExecuteQuery(new GetAllSeoArticles());
            foreach (var article in articlesSeo)
            {
                builder.AppendUrl(string.Format("{0}{1}", Url.Action("Index", "Home", null, Request.Url.Scheme), article.Alias), ChangefreqEnum.Never, 1);
            }

            var news = _queryService.ExecuteQuery(new GetAllNews());
            foreach (var n in news)
            {
                builder.AppendUrl(Url.Action("News", "Content", new { alias = n.Alias }, Request.Url.Scheme), ChangefreqEnum.Never, 1);
            }

            var articles = _queryService.ExecuteQuery(new GetAllArticles());
            foreach (var n in articles)
            {
                builder.AppendUrl(Url.Action("Article", "Content", new { alias = n.Alias }, Request.Url.Scheme), ChangefreqEnum.Never, 1);
            }

            var lessons = _queryService.ExecuteQuery(new GetAllLessons());
            foreach (var n in lessons)
            {
                builder.AppendUrl(Url.Action("Lesson", "Content", new { alias = n.Alias }, Request.Url.Scheme), ChangefreqEnum.Never, 1);
            }

            var complitedTasks = _queryService.ExecuteQuery(new GetAllComplitedTasks());
            foreach (var n in complitedTasks)
            {
                builder.AppendUrl(Url.Action("Detail", "CompletedTask", new { id = n.Id }, Request.Url.Scheme), ChangefreqEnum.Never, 1);
            }

            return new XmlViewResult(builder.XmlDocument);
        }
    }
}