﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.CompletedTask;

namespace reshim.web.Controllers
{
    public class CompletedTaskController : BaseController
    {
        private readonly IQueryService _queryService;
        public CompletedTaskController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public ActionResult Index(int? page)
        {
            var taskView = _queryService.ExecuteQuery(new GetComplitedTasksByCriteria() { Count = CountRecordsCompletedTask, Page = page - 1 ?? 0 });
            return View(taskView);
        }

        public ActionResult Detail(int id)
        {
            var completedTask = _queryService.ExecuteQuery(new GetCompletedTaskById() { Id = id });
            return View(completedTask);
        }

        public ActionResult History(int? page)
        {
            var histories = _queryService.ExecuteQuery(new GetHistoryCompletedTaskByUserId() { Count = 10, Page = page ?? 0, UserId = User.UserId });
            return View(histories);
        }
    }
}