﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies;
using reshim.domain.Queryies.Content.Article;
using reshim.domain.Queryies.Content.Lesson;
using reshim.domain.Queryies.Content.News;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.domain.Queryies.Content.Subject;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.domain.Queryies.Task;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content;
using reshim.model.view.ViewModels.Home;
using reshim.model.view.ViewModels.Task;

namespace reshim.web.Controllers
{

    public class HomeController : BaseController
    {
        private readonly IQueryService _queryService;

        public HomeController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        public ActionResult Index()
        {
            var model = new HomeViewModel { Subjects = _queryService.ExecuteQuery(new GetAllSubjects()) };
            return View(model);
        }

        public ActionResult Support()
        {
            return View();
        }
        public ActionResult RulesOrder()
        {
            return View();
        }
        public ActionResult Price()
        {
            var prices = new PricesViewModel() {
                PricesDescription = new List<PricesViewModel.PriceTypeWorkViewModel>() {
                    new PricesViewModel.PriceTypeWorkViewModel() {
                        NameTypeWork ="Задача",
                        Subjects = new List<PricesViewModel.PriceSubjectViewModel>() {
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 59, MinPrice = 30, MiddlePrice = 45, MaxPrice = 100, NameSubject = "Математика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 71, MinPrice = 30, MiddlePrice = 50, MaxPrice = 90, NameSubject = "Физика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 110, MinPrice = 40, MiddlePrice = 100, MaxPrice = 300, NameSubject = "Программирование", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 11, MinPrice = 35, MiddlePrice = 70, MaxPrice = 110, NameSubject = "Химия", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 74, MinPrice = 30, MiddlePrice = 45, MaxPrice = 95, NameSubject = "Теория вероятности", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 95, MinPrice = 40, MiddlePrice = 90, MaxPrice = 200, NameSubject = "Электротехника", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 101, MinPrice = 40, MiddlePrice = 90, MaxPrice = 250, NameSubject = "Информатика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 3, MinPrice = 35, MiddlePrice = 50, MaxPrice = 200, NameSubject = "Иностранный язык", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 15, MinPrice = 35, MiddlePrice = 50, MaxPrice = 130, NameSubject = "Биология", Url = "/"}
                        }
                    },
                    new PricesViewModel.PriceTypeWorkViewModel() {
                        NameTypeWork ="Контрольная работа | Лабораторная работа | Типовые задачи",
                        Subjects = new List<PricesViewModel.PriceSubjectViewModel>() {
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 27, MinPrice = 250, MiddlePrice = 400, MaxPrice = 560, NameSubject = "Математика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 42, MinPrice = 260, MiddlePrice = 450, MaxPrice = 610, NameSubject = "Физика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 71, MinPrice = 400, MiddlePrice = 800, MaxPrice = 1560, NameSubject = "Программирование", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 1, MinPrice = 350, MiddlePrice = 350, MaxPrice = 350, NameSubject = "Химия", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 44, MinPrice = 260, MiddlePrice = 400, MaxPrice = 550, NameSubject = "Теория вероятности", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 65, MinPrice = 300, MiddlePrice = 600, MaxPrice = 1060, NameSubject = "Электротехника", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 70, MinPrice = 360, MiddlePrice = 550, MaxPrice = 1360, NameSubject = "Информатика", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 2, MinPrice = 200, MiddlePrice = 300, MaxPrice = 400, NameSubject = "Иностранный язык", Url = "/"},
                            new PricesViewModel.PriceSubjectViewModel() { CountOrder = 3, MinPrice = 210, MiddlePrice = 385, MaxPrice = 560, NameSubject = "Биология", Url = "/"}
                        }
                    }
                }
            };
            return View(prices);
        }
        public ActionResult Partnership()
        {
            return View();
        }
        public ActionResult Guaranty()
        {
            return View();
        }

        [Authorize]
        public PartialViewResult AuthMenu()
        {
            var autMenu = _queryService.ExecuteQuery(new GetAuthMenu() { UserId = User.UserId });
            return PartialView("Partials/AuthMenu", autMenu);
        }

        public PartialViewResult Statastics()
        {
            var modelView = _queryService.ExecuteQuery(new GetStatisticsSystem());
            return PartialView("Partials/_Statastics", modelView);
        }

        public PartialViewResult LastNews()
        {
            var newsView = _queryService.ExecuteQuery(new GetLastNews() { LastCount = 2 });
            return PartialView("Partials/_LastNews", newsView);
        }

        public PartialViewResult LastTask()
        {
            var newsView = _queryService.ExecuteQuery(new GetLastTask() { CountLast = 8, Status = StatusTask.Completed });
            return PartialView("Partials/_LastTask", newsView);
        }
        public PartialViewResult TypeWorks()
        {
            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            return PartialView("Partials/_TypeWorks", typeWorks);
        }

        public PartialViewResult OrderForm()
        {
            var subject = _queryService.ExecuteQuery(new GetAllSubjects());
            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            return PartialView("Partials/_OrderForm", new CreateTaskFormModel() { Subjects = subject, TypeWorks = typeWorks });
        }

        [ChildActionOnly]
        public ActionResult SidebarTypeWork(string aliasArticle, string aliasTypeWork)
        {

            var articlesSeo = _queryService.ExecuteQuery(new GetSeoArticlesByTypeWorkAlias() { AliasTypeWork = aliasTypeWork });
            var items = new List<MenuItemModel>();

            var action = ControllerContext.ParentActionViewContext.RouteData.Values["action"];

            foreach (var article in articlesSeo)
            {
                items.Add(new MenuItemModel()
                {
                    Text = article.ShortName,
                    Alias = article.Alias,
                    Selected = "articleseo" == action.ToString().ToLower() && article.Alias == aliasArticle
                });
            }


            return PartialView("~/Views/Shared/Partials/_SidebarTypeWork.cshtml", items);
        }

        [ChildActionOnly]
        public ActionResult MenuArticle()
        {
            var categories = _queryService.ExecuteQuery(new GetAllTopArticleCategories() { });
            var items = new List<MenuItemModel>();

            foreach (var cat in categories)
            {
                items.Add(new MenuItemModel()
                {
                    Text = cat.Name,
                    Alias = cat.Alias,
                    SubMenu = cat.Children.Select(x => new MenuItemModel() { Alias = x.Alias, Text = x.Name }).ToList()
                });
            }

            return PartialView("~/Views/Shared/Partials/_MenuArticle.cshtml", items);
        }

        [ChildActionOnly]
        public ActionResult MenuLessons()
        {
            var categories = _queryService.ExecuteQuery(new GetAllTopCategoriesLessons() { });
            var items = new List<MenuItemModel>();

            foreach (var cat in categories)
            {
                items.Add(new MenuItemModel()
                {
                    Text = cat.Name,
                    Alias = cat.Alias,
                    SubMenu = cat.Children.Select(x => new MenuItemModel() { Alias = x.Alias, Text = x.Name }).ToList()
                });
            }

            return PartialView("~/Views/Shared/Partials/_MenuLessons.cshtml", items);
        }
    }
}