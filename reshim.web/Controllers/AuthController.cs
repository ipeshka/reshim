﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MassTransit;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.databus.model.contracts.Contracts.Alerts;
using reshim.domain.Commands.Profile;
using reshim.domain.Commands.Security;
using reshim.domain.Commands.Task;
using reshim.domain.Queryies.User;
using reshim.model.view.ViewModels.Auth;
using reshim.model.view.ViewModels.Task;
using reshim.oauth2;
using reshim.oauth2.Client;
using reshim.web.core.Auth;
using reshim.web.core.Model;
using reshim.web.Helpers;

namespace reshim.web.Controllers
{
    public class AuthController : BaseController
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IFormsAuthentication _formAuthentication;
        private readonly IBus _bus;
        private readonly AuthorizationRoot _authorizationRoot = new AuthorizationRoot();

        public AuthController(ICommandBus commandBus, IQueryService queryService, IBus bus, IFormsAuthentication formAuthentication)
        {
            _commandBus = commandBus;
            _queryService = queryService;
            _bus = bus;
            _formAuthentication = formAuthentication;
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword(string token, string login)
        {
            var model = new ConfirmForgotPasswordViewModel()
            {
                Errors = new List<ValidationResult>()
            };

            var user = _queryService.ExecuteQuery(new GetUserByLogin() { Login = login });

            if (user == null)
            {
                model.Errors.Add(new ValidationResult("", "Данного пользователя не существует."));
                return View(model);
            }

            model.Errors.AddRange(_commandBus.Validate(new CanUpdatePasswordValidation()
            {
                Email = user.Email,
                Login = user.Login,
                Token = token
            }));

            model.EmailOrLogin = user.Email;
            model.UserId = user.UserId;
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            _commandBus.Submit(new UserLogoutCommand());
            return RedirectToAction("Index", "Home");
        }


        #region регистрация через социальные сети
        private const string ProviderNameKey = "RESHIM";

        private string ProviderName
        {
            get
            {
                var value = (string)Session[ProviderNameKey];
                if (string.IsNullOrEmpty(value))
                    return ProviderNameKey;
                return value;

            }
            set { Session[ProviderNameKey] = value; }
        }

        private IClient GetClient()
        {
            return _authorizationRoot.Clients.First(c => c.ProviderName.ToLower() == ProviderName.ToLower());
        }

        [HttpGet] // 1 step
        public ActionResult RegisterFromService(string providerName)
        {
            ProviderName = providerName;
            return new RedirectResult(GetClient().GetLoginLinkUri());
        }

        [HttpGet] // 2 step
        public ActionResult ConfirmRegisterFromService()
        {
            HttpCookie cookieTask = Request.Cookies["task"];

            var model = new ConfirmRegisterFromServiceViewModel();
            model.RedirectUrl = SiteUrl;

            var userInfo = GetClient().GetUserInfo(Request.QueryString);


            var isUser = _queryService.ExecuteQuery(new GetUserByIdService() { IdService = userInfo.Id });

            if (isUser != null) // auth
            {
                _formAuthentication.SetAuthCookie(HttpContext, UserAuthenticationTicketBuilder.CreateAuthenticationTicket(isUser));
                if (IsValidCookies(cookieTask))
                {
                    var obj = JsonConvert.DeserializeObject<CreateTaskFormModel>(cookieTask.Value, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" });
                    var command = Mapper.Map<CreateTaskFormModel, CreateTaskCommand>(obj);
                    command.CustomerId = isUser.UserId;

                    var errors = _commandBus.Validate(command).ToList();

                    if (!errors.Any())
                    {
                        var result = _commandBus.Submit(command);
                        if (result.Success)
                        {
                            if (IsValidCookies(cookieTask))
                            {
                                DeleteCookie("task");
                                SetFlagCookie("IsConfirmCreateTask");
                            }
                        }
                    }
                }
                return View(model);

            }
            else // register
            {
                var fullPath = userInfo.PhotoUri.SaveFile("Profile");
                var command = new UserRegisterFromServiceCommand()
                {
                    FirstName = userInfo.FirstName,
                    LastName = userInfo.LastName,
                    IdService = userInfo.Id,
                    Service = (int)Enum.Parse(typeof(Service), ProviderName),
                    UrlVk = userInfo.Id,
                    UpdatePhoto = new UpdatePhotoCommand()
                    {
                        Path = fullPath
                    },
                    CreateTask = IsValidCookies(cookieTask) ?
                    Mapper.Map<CreateTaskFormModel, CreateTaskCommand>(JsonConvert.DeserializeObject<CreateTaskFormModel>(cookieTask.Value, new IsoDateTimeConverter { DateTimeFormat = "dd/MM/yyyy" }))
                    :
                    null
                };

                var result = _commandBus.Submit(command);
                if (result.Success)
                {
                    if (IsValidCookies(cookieTask))
                    {
                        _bus.Publish(new AlertNewTask(result.LastId, SiteUrl));
                        DeleteCookie("task");
                        SetFlagCookie("IsConfirmCreateTask");
                    }

                    var newUser = _queryService.ExecuteQuery(new GetUserByIdService() { IdService = userInfo.Id });
                    _formAuthentication.SetAuthCookie(HttpContext, UserAuthenticationTicketBuilder.CreateAuthenticationTicket(newUser));
                    return View(model);
                }

                SetFlagCookie("IsPopunFailRegistration");
                return View(model);
            }
        }
        #endregion
    }
}
