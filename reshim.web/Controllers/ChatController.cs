﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Chat;
using reshim.domain.Queryies.Chat;
using reshim.model.view.ViewModels.Chat;

namespace reshim.web.Controllers
{
    [Authorize]
    public class ChatController : BaseController
    {
        private readonly IQueryService _queryService;
        private readonly ICommandBus _commandBus;

        public ChatController(IQueryService queryService, ICommandBus commandBus)
        {
            _queryService = queryService;
            _commandBus = commandBus;
        }

        public ActionResult Index()
        {
            var dialogues = _queryService.ExecuteQuery(new GetDialoguesByUserId() { UserId = User.UserId });
            return View(dialogues);
        }

        public ActionResult Dialog(int? dialogId, int? userId)
        {
            if (dialogId.HasValue)
            {
                _commandBus.Submit(new ReadAllMessagesCommand() { DialogId = dialogId.Value, UserId = User.UserId });
                var chatView = _queryService.ExecuteQuery(new GetMessagesByDialogId() { DialogId = dialogId.Value, Page = 1, Count = 10 });
                if (chatView.User1Id != User.UserId && chatView.User2Id != User.UserId) throw new HttpException(403, "Данный ресур не может быть просмотрен.");
                return View(chatView);
            }
            else if (userId.HasValue)
            {
                var isDialog = _queryService.ExecuteQuery(new IsDialogById() { UserId1 = userId.Value, UserId2 = User.UserId });

                if (!isDialog)
                {
                    var result = _commandBus.Submit(new CreateDialogCommand() { RecipientId = userId.Value, SenderId = User.UserId });
                    return RedirectToAction("Dialog", new { dialogId = result.LastId });
                }

                var dialog = _queryService.ExecuteQuery(new GetDialogByUserIds()
                {
                    UserId1 = userId.Value, 
                    UserId2 = User.UserId
                }); 
                return RedirectToAction("Dialog", new { dialogId = dialog.Id });
            }

            return View(new ChatViewModel() { Messages = new List<MessageViewModel>() });
        }


    }

}
