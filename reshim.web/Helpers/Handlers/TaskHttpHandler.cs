﻿using System;
using System.Net;
using System.Web;
using reshim.data.infrastructure;
using reshim.model.Entities;
using reshim.web.core.Extensions;

namespace reshim.web.Helpers.Handlers
{
    public class TaskHttpHandler : IHttpHandler
    {
        private readonly ReshimDataContex _context = new ReshimDataContex();

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            if (request.FilePath.ToLower().Contains("/solution/"))
            {
                var taskId = Int32.Parse(request.QueryString["id"]);
                var user = _context.Users.Find(context.User.GetReshimUser().UserId);
                var task = _context.Tasks.Find(taskId);

                if (user == null)
                {
                    throw new HttpException(401, "Данный ресурс может просмотривать только авторизированный пользователь.");
                }

                if (task.Status != StatusTask.Completed && user.Role == UserRoles.User)
                {
                    throw new HttpException(403, "Данный ресур не может быть просмотрен.");
                }
            }


            try
            {
                response.ContentType = request.ContentType;
                response.WriteFile(request.PhysicalPath);
            }
            catch
            {
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

        }

        public bool IsReusable
        {
            get { return false; }
        }

    }
}