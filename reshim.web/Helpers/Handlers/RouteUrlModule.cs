﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.Handlers
{
    // TODO: инициализировать только один раз
    // и делать перестройку роутов при добавлении
    public class RouteUrlModule : IHttpModule
    {
        private static Dictionary<RouteUrlModel, string> Urls = new Dictionary<RouteUrlModel, string>();

        static RouteUrlModule()
        {
        }

        public static void ReInit()
        {
            var context = new ReshimDataContex();
            Urls.Clear();
            var typeWorks = context.Contents.OfType<TypeWork>().ToList();
            foreach (var typeWork in typeWorks)
            {
                Urls.Add(new RouteUrlModel() { Alias = typeWork.Alias, RedirectFromUrl = typeWork.RedirectFromUrl }, string.Format("content/typework?alias={0}", typeWork.Alias));
            }

            var seoArticles = context.Contents.OfType<SeoArticle>().ToList();
            foreach (var art in seoArticles)
            {
                Urls.Add(new RouteUrlModel() { Alias = art.Alias, RedirectFromUrl = art.RedirectFromUrl }, string.Format("content/articleseo?alias={0}&tw={1}", art.Alias, art.TypeWork.Alias));
            }
        }

        public void Init(HttpApplication application)
        {
            var _context = new ReshimDataContex();
            Urls.Clear();
            var typeWorks = _context.Contents.OfType<TypeWork>().ToList();
            foreach (var typeWork in typeWorks)
            {
                Urls.Add(new RouteUrlModel() { Alias = typeWork.Alias, RedirectFromUrl = typeWork.RedirectFromUrl }, string.Format("content/typework?alias={0}", typeWork.Alias));
            }

            var seoArticles = _context.Contents.OfType<SeoArticle>().ToList();
            foreach (var art in seoArticles)
            {
                Urls.Add(new RouteUrlModel() { Alias = art.Alias, RedirectFromUrl = art.RedirectFromUrl }, string.Format("content/articleseo?alias={0}&tw={1}", art.Alias, art.TypeWork.Alias));
            }

            application.BeginRequest += application_BeginRequest;
        }

        private void application_BeginRequest(object source, EventArgs e)
        {
            var application = (HttpApplication)source;
            var context = application.Context;
            string query = context.Request.Url.PathAndQuery.Trim('/');

            var pairByKey = Urls.FirstOrDefault(x => x.Key.Alias == query);
            var pairByValue = Urls.FirstOrDefault(x => x.Value == query);

            // если зашли по строму адресу, дефолтная навигация asp mvc
            if (pairByKey.Equals(default(KeyValuePair<RouteUrlModel, string>)) && pairByValue.Value == query)
            {
                context.Response.Status = "301 Moved Permanently";
                context.Response.AddHeader("Location", "/" + pairByValue.Key.Alias);
                context.Response.End();
            }
            // если нашли значение для вывода содержимого, делаем ретрив
            else if (!pairByKey.Equals(default(KeyValuePair<RouteUrlModel, string>)))
            {
                var dic = Urls.First(x => x.Key.Alias == pairByKey.Key.Alias);
                context.RewritePath(dic.Value);
            }

            // ищем значения для редиректа 301
            var pairByKeyTwo = Urls.FirstOrDefault(x => !string.IsNullOrEmpty(x.Key.RedirectFromUrl) && x.Key.RedirectFromUrl == query);
            if (!pairByKeyTwo.Equals(default(KeyValuePair<RouteUrlModel, string>)))
            {
                context.Response.Status = "301 Moved Permanently";
                context.Response.AddHeader("Location", "/" + pairByKeyTwo.Key.Alias);
                context.Response.End();
            }
        }

        public void Dispose()
        {
        }


    }
}
