﻿using System.Net;
using System.Web;
using reshim.data.infrastructure;
using reshim.model.Entities;
using reshim.web.core.Extensions;
using System.Linq;

namespace reshim.web.Helpers.Handlers
{
    public class CompletedTaskHttpHandler : IHttpHandler
    {
        private readonly ReshimDataContex _context = new ReshimDataContex();

        public void ProcessRequest(HttpContext context)
        {
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            var userId = context.User.GetReshimUser().UserId;

            if (request.FilePath.ToLower().Contains("/completedtask/"))
            {
                int? taskId = request.QueryString["taskId"] != null ? int.Parse(request.QueryString["taskId"]) : (int?)null;
                int? historyTaskId = request.QueryString["historyTaskId"] != null ? int.Parse(request.QueryString["historyTaskId"]) : (int?)null;
                if (taskId.HasValue)
                {
                    var user = _context.Users.FirstOrDefault(x => x.UserId == userId);
                    if (user == null || user.Role != UserRoles.Admin)
                        throw new HttpException(403, "Данный ресур не может быть просмотрен.");
                }
                else if (historyTaskId.HasValue)
                {
                    var user = _context.Users.FirstOrDefault(x => x.UserId == userId);
                    var logHistoryTask = _context.HistoriesCompletedTasks.FirstOrDefault(x => x.Id == historyTaskId.Value);

                    if (user == null) throw new HttpException(401, "Данный ресурс может просмотривать только авторизированный пользователь.");
                    if (logHistoryTask == null) throw new HttpException(401, "Данный ресурс не существует.");
                    if (user.UserId != logHistoryTask.UserId) throw new HttpException(401, "Вы не можете просмотреть данный ресурс.");
                    if (logHistoryTask.State != PaymentState.Success) throw new HttpException(401, "Для просмотра нужно оплатить ресурс.");
                }
                else
                    throw new HttpException(403, "Данный ресур не может быть просмотрен.");




            }


            try
            {
                response.ContentType = request.ContentType;
                response.WriteFile(request.PhysicalPath);
            }
            catch
            {
                response.StatusCode = (int)HttpStatusCode.InternalServerError;
            }

        }

        public bool IsReusable
        {
            get { return false; }
        }

    }
}