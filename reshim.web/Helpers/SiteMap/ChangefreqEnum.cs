﻿
namespace reshim.web.Helpers.SiteMap
{
    public enum ChangefreqEnum
    {
        Always, Hourly, Daily, Weekly, Monthly, Yearly, Never
    }
}
