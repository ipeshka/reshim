﻿using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Task;

namespace reshim.web.Helpers.Jobs
{
    public class ChangeStatusTasksJob
    {
        private readonly ICommandBus _commandBus;

        public ChangeStatusTasksJob(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public void Start()
        {
            _commandBus.Submit(new ChangeStatusTaskCommand());
        }
    }
}