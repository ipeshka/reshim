﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using System.Data.Entity;

namespace reshim.web.Helpers.NodeProvider
{
    public class CompletedTaskDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var tasks = context.CompletedTasks.Include(x => x.Subject).ToList();
                foreach (var task in tasks)
                {
                    var n1 = new DynamicNode
                    {
                        Title = task.Subject.Name,
                        Key = "completedtask_" + task.Id,
                        Controller = "CompletedTask",
                        Action = "Detail",
                        Description = task.DescriptionSeo
                    };
                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", task.TitleSeo));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", task.KeywordsSeo));
                    n1.RouteValues.Add("id", task.Id);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}