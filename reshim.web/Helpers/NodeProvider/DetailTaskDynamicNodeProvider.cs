﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;

namespace reshim.web.Helpers.NodeProvider
{
    public class DetailTaskDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var tasks = context.Tasks.Include(x => x.Subject).Include(x => x.TypeWork).ToList();
                foreach (var task in tasks)
                {
                    var n1 = new DynamicNode
                    {
                        Title = string.Format("Задача №{0}, {1}", task.Id, task.Subject.Name),
                        Key = "task_" + task.Id,
                        Controller = "Task",
                        Action = "Detail",
                        Description = string.Format("Задача №{0}, {1}", task.Id, task.Subject.Name),
                    };
                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", string.Format("Задача №{0}, {1}", task.Id, task.Subject.Name)));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", string.Format("условие задачи №{0}, {1}, {2}", task.Id, task.Subject.Name, task.TypeWork.Name)));
                    n1.RouteValues.Add("id", task.Id);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}