﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.NodeProvider
{
    public class NewsDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var news = context.Contents.OfType<News>().ToList();
                foreach (var n in news)
                {
                    var n1 = new DynamicNode
                    {
                        Title = n.Name,
                        Key = "news_1_" + n.Id,
                        Controller = "Content",
                        Action = "News",
                        Description = n.DescriptionSeo,

                    };
                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", n.TitleSeo));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", n.KeywordsSeo));
                    n1.RouteValues.Add("alias", n.Alias);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}