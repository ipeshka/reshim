﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.NodeProvider
{
    public class TypeWorksDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var typeWorks = context.Contents.OfType<TypeWork>().ToList();
                foreach (var typework in typeWorks)
                {
                    var n1 = new DynamicNode
                    {
                        Title = typework.Name,
                        Key = "typework_1_" + typework.Id,
                        Controller = "Content",
                        Action = "TypeWork",
                        Description = typework.DescriptionSeo
                    };
                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", typework.TitleSeo));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", typework.KeywordsSeo));
                    n1.RouteValues.Add("alias", typework.Alias);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}