﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.NodeProvider
{
    public class ArticleDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var articles = context.Contents.OfType<Article>().Include(x => x.Category).ToList();
                foreach (var article in articles)
                {
                    var n1 = new DynamicNode
                    {
                        Title = article.Name,
                        Key = "article_1_" + article.Id,
                        Controller = "Content",
                        Action = "Article",
                        Description = article.DescriptionSeo,
                        ParentKey = "categoryArticle_" + article.CategoryId

                    };
                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", article.TitleSeo));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", article.KeywordsSeo));
                    n1.RouteValues.Add("alias", article.Alias);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}