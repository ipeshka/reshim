﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.NodeProvider
{
    public class LessonDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var lessons = context.Contents.OfType<Lesson>().Include(x => x.CategoryLesson).ToList();
                foreach (var lesson in lessons)
                {
                    var n1 = new DynamicNode
                    {
                        Title = lesson.Name,
                        Key = "lesson_1_" + lesson.Id,
                        Controller = "Content",
                        Action = "Lesson",
                        Description = lesson.DescriptionSeo,
                        ParentKey = "categoryLesson_" + lesson.CategoryIdLesson

                    };

                    n1.Attributes.Add(new KeyValuePair<string, object>("titleSeo", lesson.TitleSeo));
                    n1.Attributes.Add(new KeyValuePair<string, object>("keywordsSeo", lesson.KeywordsSeo));
                    n1.RouteValues.Add("alias", lesson.Alias);
                    returnValue.Add(n1);
                }
            }

            return returnValue;
        }
    }
}