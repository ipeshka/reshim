﻿using System.Collections.Generic;
using System.Linq;
using MvcSiteMapProvider;
using reshim.data.infrastructure;
using reshim.model.Entities;

namespace reshim.web.Helpers.NodeProvider
{
    public class ArticleCategoriesDynamicNodeProvider : DynamicNodeProviderBase
    {
        public override IEnumerable<DynamicNode> GetDynamicNodeCollection(ISiteMapNode node)
        {
            var returnValue = new List<DynamicNode>();
            using (var context = new ReshimDataContex())
            {
                var categories = context.Contents.OfType<CategoryArticle>().ToList();
                foreach (var n in categories)
                {
                    var n1 = new DynamicNode
                    {
                        Title = n.Name,
                        Key = "categoryArticle_" + n.Id,
                        Controller = "Content",
                        Action = "Articles"
                    };
                    n1.RouteValues.Add("category", n.Alias);
                    returnValue.Add(n1);
                }
            }

            var defaultValue = new DynamicNode
            {
                Title = "Статьи",
                Key = "categoryArticle",
                Controller = "Content",
                Action = "Articles"
            };
            defaultValue.RouteValues.Add("category", "");

            returnValue.Add(defaultValue);
            return returnValue;
        }
    }
}