﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using reshim.data.infrastructure;
using reshim.model.Entities;
using reshim.web.core.Extensions;

namespace reshim.web.Helpers
{
    public class Access : AuthorizeAttribute
    {
        private readonly UserRoles[] _role;


        public Access(UserRoles[] role)
        {
            _role = role;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            using (var context = new ReshimDataContex())
            {
                //Проверяем на авторизированность
                var reshimUser = httpContext.User.GetReshimUser();
                if (reshimUser.IsAuthenticated)
                {
                    //Получаем пользователя из БД
                    var user = context.Users.FirstOrDefault(x => x.UserId == reshimUser.UserId);
                    if (user != null && _role.Any(x => x == user.Role)) return true;
                }
            }
            return false;
        }
    }

    public class AccessApi : System.Web.Http.AuthorizeAttribute
    {
        private readonly UserRoles[] _role;


        public AccessApi(UserRoles[] role)
        {
            _role = role;
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            using (var context = new ReshimDataContex())
            {
                //Проверяем на авторизированность
                var reshimUser = actionContext.RequestContext.Principal.GetReshimUser();
                if (reshimUser.IsAuthenticated)
                {
                    //Получаем пользователя из БД
                    var user = context.Users.FirstOrDefault(x => x.UserId == reshimUser.UserId);
                    if (user != null && _role.Any(x => x == user.Role))
                    {
                        HttpContext.Current.Response.AddHeader("AuthenticationStatus", "Authorized");
                        return;
                    }
                }
            }

            HttpContext.Current.Response.AddHeader("AuthenticationStatus", "NotAuthorized");
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
        }
    }
}