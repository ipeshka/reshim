﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using reshim.common;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;
using reshim.web.core.Model;

namespace reshim.web.Helpers
{
    public static class CustomHtmlHelper
    {
        private static readonly string _storageRoot = ConfigurationManager.AppSettings["FileUploadPath"];

        public static MvcHtmlString ActionImage(this HtmlHelper html, string action, object routeValues, string imagePath, string alt, object htmlAttributes)
        {
            var url = new UrlHelper(html.ViewContext.RequestContext);

            var imgBuilder = new TagBuilder("img");
            imgBuilder.MergeAttribute("src", url.Content(imagePath));
            imgBuilder.MergeAttribute("alt", alt);
            string imgHtml = imgBuilder.ToString(TagRenderMode.SelfClosing);

            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(action, routeValues));
            anchorBuilder.InnerHtml = imgHtml;
            if (htmlAttributes != null)
            {
                var attributes = new RouteValueDictionary(htmlAttributes);
                anchorBuilder.MergeAttributes(attributes, false);
            }

            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            return MvcHtmlString.Create(anchorHtml);
        }

        public static MvcHtmlString ReshimValidationSummary(this HtmlHelper helper)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("info-place");
            div.AddCssClass("error");

            var imgErr = new TagBuilder("div");
            imgErr.AddCssClass("img");
            imgErr.AddCssClass("error");

            div.InnerHtml += imgErr.ToString();

            var content = new TagBuilder("div");
            content.AddCssClass("content");

            foreach (var key in helper.ViewData.ModelState.Keys)
            {
                foreach (var err in helper.ViewData.ModelState[key].Errors)
                    content.InnerHtml += helper.Encode(err.ErrorMessage) + "<br>";
            }

            div.InnerHtml += content.ToString();

            var clearfix = new TagBuilder("div");
            clearfix.AddCssClass("clearfix");

            div.InnerHtml += clearfix.ToString();

            if (helper.ViewData.ModelState.IsValid) return MvcHtmlString.Create("");
            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString AdminValidationSummary(this HtmlHelper helper)
        {
            var div = new TagBuilder("div");
            div.AddCssClass("info-place");
            div.AddCssClass("error");

            var imgErr = new TagBuilder("div");
            imgErr.AddCssClass("img");
            imgErr.AddCssClass("error");

            div.InnerHtml += imgErr.ToString();

            var content = new TagBuilder("div");
            content.AddCssClass("content");

            foreach (var key in helper.ViewData.ModelState.Keys)
            {
                foreach (var err in helper.ViewData.ModelState[key].Errors)
                    content.InnerHtml += helper.Encode(err.ErrorMessage) + "<br>";
            }

            div.InnerHtml += content.ToString();

            var clearfix = new TagBuilder("div");
            clearfix.AddCssClass("clearfix");

            div.InnerHtml += clearfix.ToString();

            if (helper.ViewData.ModelState.IsValid) return MvcHtmlString.Create("");
            return MvcHtmlString.Create(div.ToString());
        }

        public static MvcHtmlString Truncate(this HtmlHelper htmlHelper, string input, int requiredLength, string ending = "...")
        {
            if (input.Length < requiredLength) return new MvcHtmlString(input);

            var requiredtext = input.Substring(0, requiredLength - 1);
            var ouputtext = string.Concat(requiredtext.Substring(0, requiredtext.LastIndexOf(' ')), ending);
            return new MvcHtmlString(ouputtext);
        }


        public static string SaveFile(this HttpPostedFile file, string catalog)
        {
            var http = HttpContext.Current.Server.MapPath(_storageRoot);
            var fullName = Path.GetFileName(Path.GetRandomFileName().Replace(".", "") + "-" + file.FileName);

            string fullPath = _storageRoot + catalog + "/" + fullName;
            Directory.CreateDirectory(http + catalog);
            file.SaveAs(http + catalog + "/" + fullName);

            return fullPath;
        }

        public static string SaveFile(this string photoUri, string catalog)
        {
            // TODO: вставить проверку на url
            var http = HttpContext.Current.Server.MapPath(_storageRoot);
            var fullName = Path.GetRandomFileName().Replace(".", "") + ".jpg";

            string fullPath = _storageRoot + catalog + "/" + fullName;
            var avatar = new Bitmap(ImageProcessingUtility.ScaleImage(ImageProcessingUtility.GetImageFromUrl(photoUri), 300, 300));
            Directory.CreateDirectory(http + catalog);
            avatar.Save(http + catalog + "/" + fullName);
            return fullPath;
        }


        public static MvcHtmlString MetaTagsForSeo(this HtmlHelper helper)
        {
            if (MvcSiteMapProvider.SiteMaps.Current != null && MvcSiteMapProvider.SiteMaps.Current.CurrentNode != null)
            {
                var tmp = new StringBuilder();
                MvcSiteMapProvider.ISiteMapNode node = MvcSiteMapProvider.SiteMaps.Current.CurrentNode;

                if (node != null && node.Attributes.ContainsKey("titleSeo") && node.Attributes["titleSeo"] != null &&
                    !String.IsNullOrWhiteSpace(node.Attributes["titleSeo"].ToString()))
                    tmp.Append(String.Format("<title>{0}</title>",
                        node.Attributes["titleSeo"].ToString()));

                if (node != null && node.Attributes.ContainsKey("keywordsSeo") && node.Attributes["keywordsSeo"] != null &&
                    !String.IsNullOrWhiteSpace(node.Attributes["keywordsSeo"].ToString()))
                    tmp.Append(String.Format("<meta name=\"keywords\" content=\"{0}\" />",
                        node.Attributes["keywordsSeo"].ToString()));

                if (node != null && node.Description != null && !String.IsNullOrWhiteSpace(node.Description))
                    tmp.Append(String.Format("<meta name=\"description\" content=\"{0}\" />", node.Description));

                return new MvcHtmlString(tmp.ToString());
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString MetaTagsTitle(this HtmlHelper helper)
        {
            if (MvcSiteMapProvider.SiteMaps.Current != null && MvcSiteMapProvider.SiteMaps.Current.CurrentNode != null)
            {
                var tmp = new StringBuilder();
                MvcSiteMapProvider.ISiteMapNode node = MvcSiteMapProvider.SiteMaps.Current.CurrentNode;

                if (node != null && node.Attributes.ContainsKey("titleSeo") && node.Attributes["titleSeo"] != null &&
                    !String.IsNullOrWhiteSpace(node.Attributes["titleSeo"].ToString()))
                    tmp.Append(String.Format("{0}",
                        node.Attributes["titleSeo"]));

                return new MvcHtmlString(tmp.ToString());
            }

            return new MvcHtmlString(string.Empty);
        }

        public static MvcHtmlString ColorForTask(this HtmlHelper helper, StatusTask status)
        {
            if (status == StatusTask.New)
            {
                return new MvcHtmlString("new");
            }
            if (status == StatusTask.Rating)
            {
                return new MvcHtmlString("rating");
            }
            if (status == StatusTask.Agreement)
            {
                return new MvcHtmlString("agreement");
            }
            if (status == StatusTask.CompletedNotPay)
            {
                return new MvcHtmlString("completed-not-pay");
            }
            if (status == StatusTask.Completed)
            {
                return new MvcHtmlString("completed");
            }
            if (status == StatusTask.NotRelevant)
            {
                return new MvcHtmlString("not-relevant");
            }

            return new MvcHtmlString(string.Empty);
        }

        public static IHtmlString GroupDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, Dictionary<string, IEnumerable<SelectListItem>> selectList, object htmlAttributes)
        {
            /*
             * <select name="tmodel">
             *   <optgroup title="Items">
             *   <option value="item">Item</option>
             * </select>
             */

            var select = new TagBuilder("select");
            select.Attributes.Add("name", ExpressionHelper.GetExpressionText(expression));

            if (htmlAttributes != null)
            {
                var attributes = new RouteValueDictionary(htmlAttributes);
                select.MergeAttributes(attributes, false);
            }

            var optgroups = new StringBuilder();

            foreach (var kvp in selectList)
            {
                var optgroup = new TagBuilder("optgroup");
                optgroup.Attributes.Add("label", kvp.Key);

                var options = new StringBuilder();

                foreach (var item in kvp.Value)
                {
                    var option = new TagBuilder("option");

                    option.Attributes.Add("value", item.Value);
                    option.SetInnerText(item.Text);

                    if (item.Selected)
                    {
                        option.Attributes.Add("selected", "selected");
                    }

                    options.Append(option.ToString(TagRenderMode.Normal));
                }

                optgroup.InnerHtml = options.ToString();

                optgroups.Append(optgroup.ToString(TagRenderMode.Normal));
            }

            select.InnerHtml = optgroups.ToString();

            return MvcHtmlString.Create(select.ToString(TagRenderMode.Normal));
        }

        public static MvcHtmlString SolutionLink(this HtmlHelper helper, TaskViewModel task, ReshimUser curUser, IDictionary<string, object> htmlAttributes)
        {
            string solutionPath = string.Empty;
            if (task.Solution != null) solutionPath = string.Format("{0}?id={1}", task.Solution.Path, task.Id);

            if (!string.IsNullOrEmpty(solutionPath))
            {
                var link = new TagBuilder("a");
                link.MergeAttribute("href", solutionPath);
                link.MergeAttribute("data-is-access", task.Status == StatusTask.CompletedNotPay && curUser.RoleName == "User" ? "false" : "true");

                if (htmlAttributes != null)
                {
                    link.MergeAttributes(htmlAttributes, false);
                }

                link.InnerHtml = "Решение";
                return MvcHtmlString.Create(link.ToString());
            }
            else
            {
                return MvcHtmlString.Create("Не решено");
            }
        }

        public static MvcHtmlString TextWithIco(this HtmlHelper helper, MvcHtmlString ico, string text = null, bool isTable = false, string colorIco = "white", string colorText = "white", object htmlAttributes = null)
        {
            var div = new TagBuilder("div");

            if (htmlAttributes != null)
            {
                var attributes = new RouteValueDictionary(htmlAttributes);
                div.MergeAttributes(attributes, true);
            }

            div.AddCssClass("svg-icon");
            div.AddCssClass("wrapper");
            if (isTable) div.AddCssClass("table");

            var icon = new TagBuilder("span");
            if(isTable) icon.AddCssClass("table-cell");
            icon.AddCssClass("icon");
            icon.AddCssClass(colorIco);
            icon.InnerHtml += ico;
            div.InnerHtml += icon.ToString();

            if (!string.IsNullOrEmpty(text))
            {
                var title = new TagBuilder("span");
                if (isTable) title.AddCssClass("table-cell");
                title.AddCssClass("title");
                title.AddCssClass(colorText);
                title.InnerHtml = text;
                div.InnerHtml += title.ToString();
            }

            return MvcHtmlString.Create(div.ToString());
        }
    }
}