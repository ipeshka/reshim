﻿using System.Net;
using System.Web;
using System.Web.Mvc;

namespace reshim.web.Helpers
{
    public class SVGHelper
    {
        public static MvcHtmlString Include()
        {
            var filePath = HttpContext.Current.Server.MapPath(@"~/Content/SVG/source.html");
            return MvcHtmlString.Create(new WebClient().DownloadString(filePath));
        }

        public static MvcHtmlString Checkmark2ICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-checkmark2'><use xlink:href='#icon-checkmark2'></use></svg>");
        }

        public static MvcHtmlString UserCheckICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-user-check'><use xlink:href='#icon-user-check'></use></svg>");
        }

        public static MvcHtmlString UserICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-user'><use xlink:href='#icon-user'></use></svg>");
        }

        public static MvcHtmlString UserTieICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-user-tie'><use xlink:href='#icon-user-tie'></use></svg>");
        }

        public static MvcHtmlString CancelCirclICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-cancel-circle'><use xlink:href='#icon-cancel-circle'></use></svg>");
        }

        public static MvcHtmlString NotificationICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-notification'><use xlink:href='#icon-notification'></use></svg>");
        }

        public static MvcHtmlString SearchICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-search'><use xlink:href='#icon-search'></use></svg>");
        }

        public static MvcHtmlString FolderUploadICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-folder-upload'><use xlink:href='#icon-folder-upload'></use></svg>");
        }

        public static MvcHtmlString SadICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-sad'><use xlink:href='#icon-sad'></use></svg>");
        }

        public static MvcHtmlString HourGlassICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-hour-glass'><use xlink:href='#icon-hour-glass'></use></svg>");
        }

        public static MvcHtmlString WalletICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-wallet'><use xlink:href='#icon-wallet'></use></svg>");
        }

        public static MvcHtmlString VkICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-vk'><use xlink:href='#icon-vk'></use></svg>");
        }

        public static MvcHtmlString TwitterICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-twitter'><use xlink:href='#icon-twitter'></use></svg>");
        }

        public static MvcHtmlString EyeICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-eye'><use xlink:href='#icon-eye'></use></svg>");
        }

        public static MvcHtmlString CalendarICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-calendar'><use xlink:href='#icon-calendar'></use></svg>");
        }

        public static MvcHtmlString CartICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-cart'><use xlink:href='#icon-cart'></use></svg>");
        }

        public static MvcHtmlString ArrowRightICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-arrow-right'><use xlink:href='#icon-arrow-right'></use></svg>");
        }

        public static MvcHtmlString FileWordICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-file-word'><use xlink:href='#icon-file-word'></use></svg>");
        }

        public static MvcHtmlString SubjectICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-subject'><use xlink:href='#icon-subject'></use></svg>");
        }
        public static MvcHtmlString ListNumberedICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-list-numbered'><use xlink:href='#icon-list-numbered'></use></svg>");
        }

        public static MvcHtmlString NewspaperICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-newspaper'><use xlink:href='#icon-newspaper'></use></svg>");
        }

        public static MvcHtmlString CircleDownICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-circle-down'><use xlink:href='#icon-circle-down'></use></svg>");
        }

        public static MvcHtmlString СlockICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-clock'><use xlink:href='#icon-clock'></use></svg>");
        }

        public static MvcHtmlString UserPlusICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-user-plus'><use xlink:href='#icon-user-plus'></use></svg>");
        }

        public static MvcHtmlString Key2ICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-key2'><use xlink:href='#icon-key2'></use></svg>");
        }

        public static MvcHtmlString PriceTagICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-price-tag'><use xlink:href='#icon-price-tag'></use></svg>");
        }

        public static MvcHtmlString AttachmentICO()
        {
            return MvcHtmlString.Create("<svg class='icon icon-attachment'><use xlink:href='#icon-attachment'></use></svg>");
        }
    }
}