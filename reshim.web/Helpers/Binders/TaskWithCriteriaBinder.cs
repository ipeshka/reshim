﻿using System;
using System.Web;
using System.Web.Mvc;
using reshim.domain.Queryies.Task;
using reshim.web.core.Extensions;

namespace reshim.web.Helpers.Binders
{
    public class TaskWithCriteriaBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
                                ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            var obj = new GetTasksByCriteria
            {
                Page = Int32.Parse(request.Params["page"] ?? "1") - 1,
                Status = request.Params["status"],
                IsMyTask = request.Params["isMyTask"] == "on",
                UserId = controllerContext.HttpContext.User.GetReshimUser().UserId
            };

            return obj;
        }
    }
}