﻿using System;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using reshim.common.core.Extensions;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.web.Helpers.Binders
{
    public class ConfirmRequestInterkassaBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var httpContextWrapper = actionContext.Request.Properties["MS_HttpContext"] as HttpContextWrapper;

            if (httpContextWrapper != null)
            {
                var request = httpContextWrapper.Request;

                if (bindingContext.ModelType != typeof(ConfirmRequestInterkassaFormModel))
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName,
                        "InterkassaConfirmViewModel type expected, not type: " + bindingContext.ModelType.FullName);
                    return false;
                }

                var obj = new ConfirmRequestInterkassaFormModel();
                try
                {
                    obj.Amount = request.Form["ik_am"];
                    obj.CheckoutId = request.Form["ik_co_id"];
                    obj.PaymentNumber = request.Form["ik_pm_no"];
                    obj.Currency = EnumEx.GetEnumFromTypeAttribute<Currency>(request.Form["ik_cur"]);
                    obj.PaywayVia = EnumEx.GetEnumFromTypeAttribute<PaywayVia>(request.Form["ik_pw_via"]);
                    obj.State = EnumEx.GetEnumFromTypeAttribute<PaymentState>(request.Form["ik_inv_st"]);
                    obj.HasCompletedTask = request.Form["ik_x_hasCompletedTask"] != null && Boolean.Parse(request.Form["ik_x_hasCompletedTask"]);
                }
                catch (Exception ex)
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex.Message);
                    return false;
                }

                bindingContext.Model = obj;
                return true;
            }

            return false;

        }
    }
}