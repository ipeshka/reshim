﻿using System;
using System.Web;
using System.Web.Mvc;
using reshim.domain.Queryies.Task;

namespace reshim.web.Helpers.Binders
{
    public class TaskWithCriteriaAdminBinder : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext,
                                ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;

            int page = Int32.Parse(request.Params["page"] ?? "0");

            var obj = new GetTasksByCriteria
            {
                Page = page == 0 ? 0 : page - 1,
                Status = request.Params["status"]
            };

            return obj;
        }
    }
}