﻿using System;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.ModelBinding;
using reshim.common.core.Extensions;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.web.Helpers.Binders
{
    public class CreateRequestInterkassaBinder : IModelBinder
    {
        public bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var httpContextWrapper = actionContext.Request.Properties["MS_HttpContext"] as HttpContextWrapper;

            if (httpContextWrapper != null)
            {
                var request = httpContextWrapper.Request;

                if (bindingContext.ModelType != typeof(CreateRequestInterkassaFormModel))
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName,
                        "InterkassaConfirmViewModel type expected, not type: " + bindingContext.ModelType.FullName);
                    return false;
                }

                var obj = new CreateRequestInterkassaFormModel();
                try
                {
                    obj.PaywayVia = EnumEx.GetEnumFromTypeAttribute<PaywayVia>(request.Form["PaywayVia"]);
                    obj.Amount = request.Form["Amount"] != null ? Decimal.Parse(request.Form["Amount"]) : 0;
                    obj.HasCompletedTask = request.Form["HasCompletedTask"] != null && Boolean.Parse(request.Form["HasCompletedTask"]);
                    obj.CompletedTaskId = request.Form["CompletedTaskId"] != null ? Int32.Parse(request.Form["CompletedTaskId"]) : 0;
                    obj.Email = request.Form["Email"];
                }
                catch (Exception ex)
                {
                    bindingContext.ModelState.AddModelError(bindingContext.ModelName, ex.Message);
                    return false;
                }

                bindingContext.Model = obj;
                return true;
            }

            return false;

        }
    }
}