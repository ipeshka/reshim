﻿using System;
using System.Web;
using System.Web.Mvc;
using reshim.common.core.Extensions;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.web.Helpers.Binders
{
    public class InterkassaSuccesOrFailBinder : IModelBinder
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        public object BindModel(ControllerContext controllerContext,
                                ModelBindingContext bindingContext)
        {
            HttpRequestBase request = controllerContext.HttpContext.Request;
            var obj = new ConfirmRequestInterkassaFormModel
            {
                CheckoutId = request.Form["ik_co_id"],
                PaymentNumber = request.Form["ik_pm_no"],
                State = EnumEx.GetEnumFromTypeAttribute<PaymentState>(request.Form["ik_inv_st"])
            };

            return obj;
        }
    }
}