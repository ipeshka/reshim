﻿using System.Text;

namespace reshim.web.Helpers
{
    public static class Helper
    {
        public static string Level(int count)
        {
            var separator = new StringBuilder();
            for (int i = 0; i < count; i++) separator.Append("—");
            return separator.ToString();
        }
    }
}