﻿namespace reshim.web.Helpers
{
    public class RouteUrlModel
    {
        public string Alias { get; set; }
        public string RedirectFromUrl { get; set; }
    }
}