﻿// глобальные настройки
$(window).load(function () {
    var isPopunFailRegistration = $.cookie('IsPopunFailRegistration');
    var isConfirmCreateTask = $.cookie('IsConfirmCreateTask');
    var complitedTask = $.cookie('ComplitedTask');

    if (isPopunFailRegistration != undefined) {
        var infoPlace = $.InfoPlace({ text: "Произошла непредвиденная ошибка при регистрации! Обратитесь к адмигнистратору.", image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

        $.Dialog("fail-registration-from-service", {
            caption: {
                title: "Ошибка при регистрации",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml(),
            sysBtnCloseClick: function (event) {
                $.removeCookie("IsPopunFailRegistration", { path: "/" });
            }
        });
    }

    if (isConfirmCreateTask != undefined) { // успешный вход
        var infoPlace = $.InfoPlace({
            text: "Успешное добавление Вашего заказа, для оценки, в нашей системе. После закрытия окна, Вы сможете управлять заказом.",
            image: RESHIM.ICO.NotificationICO(),
            type: 'info'
        });

        $.Dialog("confirm-create-task-from-service", {
            caption: {
                title: "Успешная авторизация",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml(),
            sysBtnCloseClick: function (event) {
                $.removeCookie("IsConfirmCreateTask", { path: "/" });
                window.location = "/task?status=&isMyTask=on";
            }
        });
    }

    if (complitedTask != undefined) {
        $.removeCookie("ComplitedTask", { path: "/" });
        $.Loader({ overlay: true });
        var request = $.ajax({
            url: "/api/pay/statuscompletedtask/" + complitedTask,
            type: "POST"
        });

        request.done(function (data) {
            $.Loader.close();
            var infoPlace = $.InfoPlace({
                text: "Вы заказали задачу из раздела \"Готовые задачи\" №" + data.Id + "." +
                    "Статус заказа можно проследить на странице <a href='/completedtask/history'><b>История заказов</b></a>." +
                    "После поступления оплаты, Вы получите оповещение в личный кабинет и на Ваш email.",
                image: RESHIM.ICO.NotificationICO(),
                type: 'info'
            });

            $.Dialog("confirm-completed-task-done", {
                caption: {
                    title: "Информация о заказе готовой задачи №" + data.Id,
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

            $.Dialog("completed-task-error", {
                caption: {
                    title: "Ошибка обработка заказа",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

        });
    }

});

(function ($) {
    $.Reshim = function (params) {
        params = $.extend({}, params);
    };

})(jQuery);

$(function () {
    $(window).on('resize', function () {
        var dialog = $(".window");
        if (dialog) {
            var top = ($(window).height() - dialog.outerHeight()) / 2;
            var left = ($(window).width() - dialog.outerWidth()) / 2;
            dialog.css({
                top: top, left: left
            });
        }
    });
});