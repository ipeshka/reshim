// скрипт который должен выполняться на всех странциах
var TEMPLATES = {};
var NEW = 1, RATING = 2, AGREEMENT = 3, COMPLETEDNOTPAY = 4, COMPLETED = 5;
var EMAIL = 'email', VK = 'vk';
var WIDTH_POPUN = 400;

$(document).ready(function () {

    _.template.formatdate = function (date) {
        // http://stackoverflow.com/questions/2182246/javascript-dates-in-ie-nan-firefox-chrome-ok
        var d = RESHIM.COMMON.parseISO8601(date),
            fragments = [
                d.getDate(),
                d.getMonth() + 1,
                d.getFullYear()
            ];
        return fragments.join('/');
    };

    // инициализация шаблонов
    $.ajax({ type: "GET", url: "/templates/templates.xml", dataType: "text" })
	.done(
		function (templates) {
		    TEMPLATES = $(templates);
		}
	);

    // activation hover menu
    $(".block-info a").hover(
		function () {
		    $(this).css("background-color", "#066bc6");
		    $(this).css("color", "#FFF");
		    var overlay = $(this).prev().prev().find(".overlay");
		    overlay.css("background-color", "#066bc6");
		}, function () {
		    $(this).css("background-color", "#FFF");
		    $(this).css("color", "#066bc6");

		    var overlay = $(this).prev().prev().find(".overlay");
		    overlay.css("background-color", "#5a5a5a");
		}
	);

    // click menu user
    var authMenu = $(".auth-menu li");
    $(authMenu).on('click', function (e) {
        var link = $(this).data("link");
        window.location.href = link;
    });

    // popun login
    $(".login").click(
        function () {
            // если пробывали авторизироваться через быструю регистрацию
            localStorage.removeItem('task');
            $.Dialog("login", {
                caption: {
                    title: "Вход",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                width: WIDTH_POPUN,
                padding: 10,
                content: _.template(TEMPLATES.find("#login").html())
            });

        }
    );

    // popun reg
    $(".reg").click(
        function () {
            localStorage.removeItem('task');
            $.Dialog("registration", {
                caption: {
                    title: "Регистрация",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: _.template(TEMPLATES.find("#registration").wrapTemplate())
            });
        }
    );

    $(".custom-select2").select2({
        formatNoMatches: "Не найдено",
        language: "ru"
    });


    $('.slider-subjects').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000
    });
    
});