﻿(function ($) {
    $.widget("reshim.datepicker", {
        version: "1.0.0",

        options: {
            format: "dd/mm/yyyy",
            locale: $.Reshim.currentLocale,
            selected: function (d, d0) { },
            selectdate: undefined,
            _calendar: undefined
        },


        _create: function () {
            var that = this,
                element = this.element,
                input = element.children("input"),
                button = element.children("input.btn-date");

            if (element.data('format') != undefined) this.options.format = element.data('format');
            if (element.data('position') != undefined) this.options.position = element.data('position');
            if (element.data('selectdate') != undefined) this.options.selectdate = element.data('selectdate');
            if (element.data('locale') != undefined) this.options.locale = element.data('locale');

            this._createCalendar(element);

            input.attr('readonly', true);

            button.on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (that.options._calendar.css('display') == 'none') {
                    that._show();
                } else {
                    that._hide();
                }
            });

            element.on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();

                if (that.options._calendar.css('display') == 'none') {
                    that._show();
                } else {
                    that._hide();
                }
            });

            $('html').on('click', function(e) {
                $(".calendar-dropdown").hide();
            });
        },

        _createCalendar: function(to) {
            var _calendar, that = this;

            _calendar = $("<div/>").css({
                position: 'absolute',
                display: 'none',
                'max-width': 300,
                'z-index': 9999
            }).addClass('calendar calendar-dropdown').appendTo(to);

            if (that.options.date != undefined) {
                _calendar.data('date', that.options.date);
            }
            _calendar.calendar({
                format: that.options.format,
                locale: that.options.locale,
                selectdate: that.options.selectdate,
                selectDay: function (d, d0) {
                    to.children("input[type=text]").val(d);
                    that.options.selected(d, d0);
                    that._hide();
                }
            });

            if (that.options.selectdate != undefined) {
                to.children("input[type=text]").val(that.options.selectdate);
            }

            // Set position
            _calendar.css({ top: '100%', left: 0 });

            this.options._calendar = _calendar;
        },

        _show: function() {
            $(".calendar-dropdown").slideUp('fast');
            this.options._calendar.slideDown('fast');
        },

        _hide: function() {
            this.options._calendar.slideUp('fast');
        },

        _destroy: function() {
        },

        _setOption: function(key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);


$(function () {
    $('[data-role=datepicker]').datepicker();
});