﻿// Calendar

(function ($) {
    $.widget("reshim.calendar", {
        version: "1.0.0",

        options: {
            format: "dd/mm/yyyy",
            date: new Date(),
            buttons: true,
            locale: $.Reshim.currentLocale,
            getDates: function (d) { },
            selectDay: function (d, d0) { }, // click select day
            _storage: undefined, // save picked date,
            selectdate: undefined
        },

        _year: 0,
        _month: 0,
        _day: 0,
        _today: new Date(),

        _create: function () {
            var element = this.element;
            
            if (element.data('format') != undefined) this.options.format = element.data("format");
            if (element.data('date') != undefined) this.options.date = new Date(element.data("date"));
            if (element.data('locale') != undefined) this.options.locale = element.data("locale");

            this._year = this.options.date.getFullYear();
            this._month = this.options.date.getMonth();
            this._day = this.options.date.getDate();


            if (this.options.selectdate != undefined) {
                var pattern = /(\d{2})\/(\d{2})\/(\d{4})/;
                var dt = new Date(this.options.selectdate.replace(pattern, '$3-$2-$1'));
                element.data("_storage", dt);
            }

            this._renderCalendar();
        },

        _renderMonth: function () {
            var year = this._year,
                month = this._month,
                feb = 28;

            // высокосный год
            if (month == 1) {
                if ((year % 100 != 0) && (year % 4 == 0) || (year % 400 == 0)) {
                    feb = 29;
                }
            }
            
            var totalDays = ["31", "" + feb + "", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"];
            var daysInMonth = totalDays[month];
            var first_week_day = new Date(year, month, 1).getDay();

            var table, tr, td, i;

            this.element.html("");

            table = $("<table/>");

            // add calendar header
            tr = $("<tr/>");

            $("<td/>").html("<a class='btn-previous-year' href='#'><<</a>").appendTo(tr);
            $("<td/>").html("<a class='btn-previous-month' href='#'><</a>").appendTo(tr);

            $("<td/>").attr("colspan", 3).html("<a class='btn-select-month' href='#'>" + $.Reshim.Locale[this.options.locale].months[month] + ' ' + year + "</a>").appendTo(tr);

            $("<td/>").html("<a class='btn-next-month' href='#'>></a>").appendTo(tr);
            $("<td/>").html("<a class='btn-next-year' href='#'>>></a>").appendTo(tr);

            tr.addClass("calendar-header").appendTo(table);

            // add day names
            tr = $("<tr/>");
            for (i = 0; i < 7; i++) {
                td = $("<td/>").addClass("day-of-week").html($.Reshim.Locale[this.options.locale].days[i]).appendTo(tr);
            }
            tr.addClass("calendar-subheader").appendTo(table);

            
            // add empty days for previos month
            var prevMonth = this._month - 1;
            if (prevMonth < 0) prevMonth = 11;
            var daysInPrevMonth = totalDays[prevMonth];
            var _first_week_day = (first_week_day + 6) % 7;
            var htmlPrevDay = "";
            tr = $("<tr/>");
            for (i = 0; i < _first_week_day; i++) {
                htmlPrevDay = daysInPrevMonth - (_first_week_day - i - 1);
                td = $("<td/>").addClass("empty").html("<small class='other-day'>" + htmlPrevDay + "</small>").appendTo(tr);
            }

            // add days current month
            var week_day = (first_week_day + 6) % 7;

            for (i = 1; i <= daysInMonth; i++) {
                week_day %= 7;

                if (week_day == 0) {
                    tr.appendTo(table);
                    tr = $("<tr/>");
                }

                td = $("<td/>").addClass("day").html("<a href='#'>" + i + "</a>");
                if (year == this._today.getFullYear() && month == this._today.getMonth() && this._today.getDate() == i) {
                    td.addClass("today");
                }

                var d = (new Date(this._year, this._month, i));
                var selectDate = this.element.data('_storage');
                if (selectDate != undefined && selectDate.setHours(0, 0, 0, 0) == d.setHours(0, 0, 0, 0)) {
                    td.find("a").addClass("selected");
                }

                td.appendTo(tr);
                week_day++;
            }

            // next month other days
            var htmlOtherDays = "";
            for (i = week_day + 1; i <= 7; i++) {
                htmlOtherDays = i - week_day;
                td = $("<td/>").addClass("empty").html("<small class='other-day'>" + htmlOtherDays + "</small>").appendTo(tr);
            }
            tr.appendTo(table);

            table.appendTo(this.element);

            this.options.getDates(this.element.data('_storage'));
        },

        _renderCalendar: function () {
            this._renderMonth();
            this._initButtons();
        },

        _initButtons: function () {
            // add actions
            var that = this, table = this.element.find('table');

            table.find('.btn-previous-month').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                that._month -= 1;
                if (that._month < 0) {
                    that._year -= 1;
                    that._month = 11;
                }
                that._renderCalendar();
            });
            table.find('.btn-next-month').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                that._month += 1;
                if (that._month == 12) {
                    that._year += 1;
                    that._month = 0;
                }
                that._renderCalendar();
            });
            table.find('.btn-previous-year').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                that._year -= 1;
                that._renderCalendar();
            });
            table.find('.btn-next-year').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                that._year += 1;
                that._renderCalendar();
            });
            table.find('.day a').on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var d = (new Date(that._year, that._month, parseInt($(this).html()))).format(that.options.format);
                var d0 = (new Date(that._year, that._month, parseInt($(this).html())));

                table.find('.day a').removeClass('selected');
                $(this).addClass("selected");

                that._addDate(d0);

                // events
                that.options.getDates(that.element.data('_storage'));
                that.options.selectDay(d, d0);
            });
        },

        _addDate: function (d) {
            this.element.data('_storage', d);
        },

        getDate: function () {
            return this.element.data('_storage').format(this.options.format);
        },

        _destroy: function () { },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);


$(function () {
    $('[data-role=calendar]').calendar();
});
