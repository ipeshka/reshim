﻿(function ($) {
    var infoPlaceHtml = '';

    var InfoPlace = {
        
        options: {
            text: '',
            image: false,
            imageClose: false,
            type: 'info',
            container: false
        },

        init: function (options) {
            this.options = $.extend({}, this.options, options);
            infoPlaceHtml = this._build();
            return this;
        },

        _build: function () {
            var o = this.options;

            var wrapper = $("<div/>").addClass("info-place").addClass(o.type);
            var iconWrapper = $("<div/>").addClass("icon-wrapper");
            var contentWrapper = $("<div/>").addClass("content").html(o.text);
            var linkWrapper = $("<a/>").attr("href", "#");
            
            var closeWrapper = $("<div/>").addClass("close");

            // icon
            if (o.image != false) {
                $(o.image).appendTo(iconWrapper);
                iconWrapper.appendTo(wrapper);
            }

            // text
            contentWrapper.appendTo(wrapper);

            // close
            if (o.imageClose != false) {
                $(o.imageClose).appendTo(linkWrapper);
                linkWrapper.appendTo(closeWrapper);

                linkWrapper.click(function (e) {
                    e.preventDefault();
                    wrapper.slideUp(100, function () {
                        $(this).hide();
                    });
                });
                
                closeWrapper.appendTo(wrapper);
            }

            if (o.container != false) {
                wrapper.insertBefore(o.container);
            }

            return wrapper;
        },
        
        toHtml: function () {
            return infoPlaceHtml;
        }

    };

    $.InfoPlace = function(options) {
        return Object.create(InfoPlace).init(options);
    };


    $.widget("reshim.infoPlace", {
        options: {
        },

        _create: function () {
            this._showBar();
        },

        _showBar: function () {
            var element = this.element;

            var image = element.data('image') || false;
            var imgClose = element.data('close') || false;
            var type = element.data('type') || 'info';
            var content = element.html();

            var infoPlace = $.InfoPlace({ text: content, image: image, type: type, imageClose: imgClose });

            $(infoPlace.toHtml()).insertBefore(element);
            element.remove();

            return infoPlace.toHtml();
        },

        _destroy: function () {

        },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);

$(function () {
    $('[data-role=infoPlace]').infoPlace();
});
