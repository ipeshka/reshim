﻿(function ($) {
    $.widget("reshim.radio", {
        options: {
        },

        _create: function () {
            this._showBar();
        },

        _showBar: function () {
            var element = this.element;

            var wrapper = $("<div/>").addClass("input-control").addClass("radio");
            var label = $("<label/>");
            var button = $("<span/>").addClass("check");
            var clone = element.clone(true);
            var caption = $("<span/>").addClass("caption").html(element.data('caption') != undefined ? element.data('caption') : "");

            label.appendTo(wrapper);
            clone.appendTo(label);
            button.appendTo(label);
            caption.appendTo(label);

            wrapper.insertBefore(element);
            element.remove();

            return wrapper;
        },

        _destroy: function () {

        },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);

$(function () {
    $('[data-role=radio]').radio();
});
