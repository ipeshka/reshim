﻿(function ($) {
    $.widget("reshim.fileinput", {
        options: {
            nameInputText: "namefile",
            nameIdFile: "idfile",
            idButton: "SelectFile",
            idTypeFile: "FileUpload",
            nameInputFile: "file"
        },

        _create: function () {
            var element = this.element;
            debugger;
            if (element.data('name-id-file') != undefined) {
                this.options.nameIdFile = element.data('name-id-file');
            }

            if (element.data('nameInputText') != undefined) {
                this.options.nameInputText = element.data('nameInputText');
            }

            if (element.data('idButton') != undefined) {
                this.options.idButton = element.data('idButton');
            }

            if (element.data('urlfile') != undefined) {
                this.options.urlfile = element.data('urlfile');
            }

            this._showBar();
        },

        _showBar: function () {
            var element = this.element;
            var parent = element;

            element.html('');

            var wrapper = $("<div/>").addClass("input-control").addClass("file").addClass("text");
            
            var inptUrlFile = $("<input />");
            inptUrlFile.attr("type", "text");
            inptUrlFile.attr("readonly", "true");
            inptUrlFile.attr("name", this.options.nameInputText);

            var inptIdFile = $("<input />");
            inptIdFile.attr("type", "hidden");
            inptIdFile.attr("name", this.options.nameIdFile);

            var btn = $("<button />");
            btn.addClass("green");
            btn.attr("type", "button");
            btn.text("Выбрать");
            btn.attr("id", this.options.idButton);

            var inpfile = $("<input />");
            inpfile.attr("type", "file");
            inpfile.attr("multiple", "true");
            inpfile.attr("id", this.options.idTypeFile);
            inpfile.attr("name", this.options.nameInputFile);

            wrapper.insertAfter(parent);
            inptUrlFile.appendTo(wrapper);
            inptIdFile.appendTo(wrapper);
            btn.appendTo(wrapper);
            inpfile.appendTo(wrapper);

            element.remove();

            return wrapper;
        },

        _destroy: function () {

        },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);

$(function () {
    $('[data-role=file-input]').fileinput();
});
