﻿(function ($) {
    var _notify_container = false;
    var _notifies = [];

    var Notify = {
        _container: null,
        _notify: null,
        _timer: null,
        options: {
            content: '',
            width: 'auto',
            height: 'auto',
            position: 'right',
            btnClose: true,
            id: false,
            timeout: 300000,
            sysBtnCloseClick: function (event, data) { }
        },

        init: function (options) {
            this.options = $.extend({}, this.options, options);
            this._build();
            return this;
        },

        _build: function () {
            this._container = _notify_container || $("<div/>").addClass("notify-container").appendTo('body');
            _notify_container = this._container;
            var o = this.options;

            if (o.content == '' || o.content == undefined) return false;

            this._notify = $("<div/>").addClass("notify");

            if (o.content != '' && o.content != undefined) {
                $("<div/>").addClass("content").addClass("clearfix").html(o.content).appendTo(this._notify);
            }

            if (o.width != 'auto') this._notify.css('min-width', o.width);
            if (o.height != 'auto') this._notify.css('min-height', o.height);

            // формирование залоговка всплывашки
            var self = this;
            if (o.btnClose) {
                $("<button/>").addClass("btn-close").on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    self._hide();
                    o.sysBtnCloseClick(e, $(this).data("id"));
                }).appendTo(this._notify).attr("data-id", o.id);
            }


            this._notify.hide().appendTo(this._container).fadeIn('slow');
            _notifies.push(this._notify);

            this.close(o.timeout);
        },

        close: function (timeout) {

            this._clearTime();
            if (timeout == parseInt(timeout)) { // запуск таймера
                var self = this;
                this._timer = setTimeout(function () {
                    self._timer = null;
                    self._hide();
                }, timeout);
                return this;
            } else if (timeout == undefined) {
                return this._hide();
            }
            return this;
        },

        _clearTime: function () {

            if (this._timer != null) {
                clearTimeout(this._timer);
                this._timer = null;
                return this;
            } else {
                return false;
            }
        },

        _hide: function () {
            this._clearTime();

            if (this._notify != undefined) {
                this._notify.hide('fast', function () {
                    $(this).remove();
                    _notifies.splice(_notifies.indexOf(this._notify), 1);
                });
                return this;
            } else {
                return false;
            }
        },
    };

    $.Notify = function(options) {
        return Object.create(Notify).init(options);
    };
}
)(jQuery);