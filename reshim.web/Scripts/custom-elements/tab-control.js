﻿(function ($, document, window, location) {

    var settings = {},
        tabs = {},
        url = {},
        urlString,
        i,
        temp,

        $window = $(window);

    // Url
    var getUrl = function () {
        urlString = location.hash;

        if (urlString) {
            urlString = urlString.split("|");

            if (urlString.length > 1) {
                for (i = 1; i < urlString.length; i += 1) {
                    temp = urlString[i].split(":");
                    url[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
                }
            }

            urlString = "";
        }
    };

    var Tabs = function (container) {
        this.container = container;
    };

    Tabs.prototype = {
        init: function () {

            var $container = this.container,
                $tabs = $container.find(".tabs > li > a"),
                $items = $container.find(".frames div.frame"),
                $this,

                name = $container.data("name"),
                id;

            $tabs.each(function () {
                $this = $(this);
                id = "btn__" + name + "__" + $this.data("target");
                $this.prop("id", id);
            });

            $items.each(function () {
                $this = $(this);
                id = "tab__" + name + "__" + $this.data("name");
                $this.prop("id", id);
            });


            $tabs.on("click.tabs", function (e) {
                e.preventDefault();
                setTab($(this).data("target"));
            });


            var setTab = function (target) {
                target = decodeURIComponent(target);
                id = "#btn__" + name + "__" + target;
                $(id).parent().addClass("active").siblings().removeClass("active");

                id = "#tab__" + name + "__" + target;
                $(id).css("display", "block").siblings().css("display", "none");

                setUrl(target);


                // trigger event and execute callback
                $window.trigger("tabsChange", {
                    group: name,
                    tab: target,
                    tabId: id
                });

                if (typeof settings.onChange === "function") {
                    settings.onChange({
                        group: name,
                        tab: target,
                        tabId: id
                    });
                }
            };

            var setUrl = function (target) {
                var prop;
                url[name] = target;
                urlString = "tabs";

                for (prop in url) {
                    if (url.hasOwnProperty(prop)) {
                        urlString += "|" + encodeURIComponent(prop) + ":" + encodeURIComponent(url[prop]);
                    }
                }

                location.hash = urlString;
            };


            // Set tabs at start
            if (url[name]) {
                setTab(url[name]);
            } else {
                setTab($tabs.eq(0).data("target"));
            }

        }
    };

    $.widget("reshim.tabcontrol", {
        _create: function () {
            var $this = this.element;
            getUrl();

            var name = encodeURIComponent($this.data("name"));
            tabs[name] = new Tabs($this);
            tabs[name].init();
        },

        _destroy: function () {
        },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
}(jQuery, document, window, location));

$(function () {
    $('[data-role=tab-control]').tabcontrol();
});



