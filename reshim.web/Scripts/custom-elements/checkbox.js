﻿(function ($) {
    $.widget("reshim.checkbox", {
        options: {
        },

        _create: function () {
            this._showBar();
        },

        _showBar: function () {
            var element = this.element;
            var id = element.attr('id') == undefined ? "checkbox" : element.attr('id');

            var wrapper = $("<div/>").addClass("input-control").addClass("checkbox");
            var input = $("<input/>").attr("type", "checkbox")
                .attr("id", id).attr("name", element.data('name') == undefined ? "" : element.data('name'))
                .attr('checked', element.data('checked') == "True");

            var label = $("<label/>").addClass("check").attr("for", id);
            var parent = element;
            var caption = $("<span/>").addClass("caption").html(element.data('caption') != undefined ? element.data('caption') : "");

            wrapper.insertAfter(parent);
            input.appendTo(wrapper);
            label.appendTo(wrapper);
            caption.appendTo(wrapper);

            element.remove();

            return wrapper;
        },

        _destroy: function () {

        },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);

$(function () {
    $('[data-role=checkbox]').checkbox();
});
