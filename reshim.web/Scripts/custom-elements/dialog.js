(function ($) {
    var dialogs = {};

    $.Dialog = function(id, params) {
        // настройки
        params = $.extend({
            // заголовок
            caption: {
                icon: false,
                title: false,
                sysButtons: {
                    btnClose: false
                }
            },
            content: '',
            // затемнение
            overlay: false,
            overlayClickClose: false,
            width: 'auto',
            height: 'auto',
            // позиционирование
            position: {
                left: false,
                top: false
            },
            // отступы контекта
            padding: false,
            onShow: function(_dialog) {
            },
            sysBtnCloseClick: function(event) {
            }
        }, params);

        var _overlay, _window, _caption, _content;

        _window = $("<div/>").addClass("window")
            .css({ display: 'none' }); // скрываем попапчик
        _content = $("<div/>").addClass("content");

        _content.css({
            paddingTop: params.padding,
            paddingLeft: params.padding,
            paddingRight: params.padding,
            paddingBottom: params.padding
        });

        // формирование залоговка всплывашки
        if (params.caption.icon || params.caption.title || params.caption.sysButtons.btnClose) {
            _caption = $("<div/>").addClass("caption");

            if (params.caption.sysButtons) {
                if (params.caption.sysButtons.btnClose) {
                    $("<button/>").addClass("btn-close").on('click', function(e) {
                        e.preventDefault();
                        e.stopPropagation();
                        $.Dialog.close(id);
                        params.sysBtnCloseClick(e);
                    }).appendTo(_caption);
                }
            }

            if (params.caption.title) $("<div/>").addClass("title").html(params.caption.title).appendTo(_caption);
            if (params.caption.icon) $(params.caption.icon).addClass("icon").appendTo(_caption);

            _caption.appendTo(_window);
        }

        // формирование попапчика
        _content.html(params.content);
        _content.appendTo(_window);

        // затемнение
        if (params.overlay) {
            _overlay = $("<div/>").addClass("dialog-overlay");
            _overlay.css({
                position: "fixed",
                backgroundColor: 'rgba(0,0,0,.4)'
            }).css("z-index", 10000);
            _window.appendTo(_overlay);
            _overlay.hide().appendTo('body').fadeIn(200);
        }

        if (params.overlay && params.overlayClickClose) {
            _overlay.on('click', function(e) {
                e.preventDefault();
                $.Dialog.close(id);
            });
        }

        // формирование высоты и ширины
        if (params.width != 'auto') _window.css('width', params.width);
        if (params.height != 'auto') _window.css('max-height', params.height);

        DIALOG = _window;
        dialogs[id] = DIALOG;

        // позиционирование попапчика
        _window
            .css("position", "absolute")
            .css("z-index", 9999);

        if (!params.position.top && !params.position.left) {
            var xWindow = ($(window).width() - DIALOG.width()) / 2;
            var yWindow = ($(window).height() - DIALOG.height()) / 2;

            DIALOG.css('display', 'block');
            DIALOG.css('left', xWindow + 'px');
            DIALOG.css('top', yWindow + 'px');

            DIALOG.css('top', -DIALOG.height() + 'px');
            DIALOG.animate({ top: yWindow }, 100);

        } else {
            DIALOG
                .css("top", params.position.top)
                .css("left", params.position.left);
        }

        DIALOG.on('click', function(e) {
            e.stopPropagation();
        });

        params.onShow(DIALOG);

        return DIALOG;
    };

    $.Dialog.content = function(id, newContent) {
        DIALOG = dialogs[id];

        if (DIALOG == undefined) {
            return false;
        }
        if (newContent) {
            DIALOG.children(".content").html(newContent);
            $.Dialog.autoPositionWidth(id);
            return true;
        } else {
            return DIALOG.children(".content").html();
        }
    };

    $.Dialog.title = function(id, newTitle) {
        DIALOG = dialogs[id];

        if (DIALOG == undefined) {
            return false;
        }

        var _title = DIALOG.children('.caption').children('.title');

        if (newTitle) {
            _title.html(newTitle);
        } else {
            _title.html();
        }

        return true;
    };

    $.Dialog.autoPositionWidth = function(id) {
        DIALOG = dialogs[id];

        if (DIALOG == undefined) {
            return false;
        }

        var left = ($(window).width() - DIALOG.outerWidth()) / 2;

        DIALOG.css({
            left: left
        });

        return true;
    };

    $.Dialog.close = function(id) {
        DIALOG = dialogs[id];

        if (DIALOG == undefined) {
            return false;
        }
        $(document).off("click");
        if (DIALOG.parent(".dialog-overlay").length != 0) {
            var _overlay = DIALOG.parent(".dialog-overlay");

            DIALOG.animate({ top: -DIALOG.height() }, 100, function() {
                _overlay.remove();
            });

        } else {
            DIALOG.animate({ top: -DIALOG.height() }, 100, function() {
                $(this).remove();
            });
        }


        return false;
    };
})(jQuery);
