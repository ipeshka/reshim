﻿(function ($) {
    $.Loader = function(params) {
        if (!$.Loader.opened) {
            $.Loader.opened = true;
        } else {
            return LOADER;
        }

        $.Loader.settings = params;

        params = $.extend({
            overlay: false,
            lighting: false,
            load: 'loader-reshim',
            container: false,
            onShow: function(_loader) {
            }
        }, params);


        var _overlay, _loader;

        // затемнение фона
        _overlay = $("<div/>").addClass("loader-overlay");
        if (params.overlay) {
            _overlay.css({
                backgroundColor: 'rgba(0,0,0,.4)'
            });
        }

        if (params.lighting) {
            _overlay.css({
                backgroundColor: 'rgba(255,255,255,.6)'
            });
        }

        // формирование попапчика
        _loader = $("<div/>").addClass(params.load);
        _loader.appendTo(_overlay);

        // TODO: отрефакторить
        if (params.container == false) {
            _overlay.hide().appendTo('body').fadeIn(0);
            _loader
                .css("top", $(window).height() / 2 - parseInt(_loader.css("height")) / 2)
                .css("left", $(window).width() / 2 - parseInt(_loader.css("width")) / 2);
        } else {
            _overlay.css({
                position: 'absolute'
            });

            $(params.container).css({
                position: 'relative'
            });
            _overlay.hide().appendTo(params.container).fadeIn(0);

            _loader
                .css("top", $(params.container).height() / 2 - parseInt(_loader.css("height")) / 2)
                .css("left", $(params.container).width() / 2 - parseInt(_loader.css("width")) / 2);
        }

        // позиционирование
        _loader.css("z-index", parseInt(_overlay.css('z-index')) + 10);


        params.onShow(_loader);

        LOADER = _loader;

        return LOADER;
    };

    $.Loader.close = function() {
        if (!$.Loader.opened || LOADER == undefined) {
            return false;
        }

        $.Loader.opened = false;
        var _overlay = LOADER.parent(".loader-overlay");
        _overlay.fadeOut(0, function() {
            $(this).remove();
        });

        return false;
    };
})(jQuery);