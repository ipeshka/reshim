﻿(function ($) {
    $.Menu = function(params) {
        if (!$.Menu.opened) { // not opened
            $.Menu.opened = true;
        } else {
            $(document).off("click");
            $.Menu.close();
        }

        // настройки
        params = $.extend({
            width: 'auto',
            height: 'auto',
            // позиционирование
            position: {
                left: false,
                top: false
            },
            menu: [{ name: "Оценить задачу", id: "estimate-task", disable: false }],
            onShow: function(_menu) {
            },
            clickMenu: function(_id) {

            }
        }, params);


        var _container = $("<div/>").addClass("context-menu");
        var _menu = $("<ul/>");
        // формирование меню
        for (var i in params.menu) {


            var _list = $('<li class=' + (params.menu[i].disable == true ? 'disable' : '') + '></li>');
            var _link = $('<a href="#" id=' + params.menu[i].id + '> ' + params.menu[i].name + '</a>');

            if (params.menu[i].disable != true) {
                _link.click(function(e) {
                    e.preventDefault();
                    params.clickMenu(e.currentTarget.id);
                });
            } else {
                _link.click(function(e) {
                    e.preventDefault();
                });
            }

            _link.appendTo(_list);
            _list.appendTo(_menu);
        }

        _menu.appendTo(_container);

        _container.hide().appendTo('body').fadeIn('fast', function() {
            $(document).on("click", function() {
                $.Menu.close();
                $(document).off("click");
            });
        });

        // позиционирование меню
        _container
            .css("position", "absolute")
            .css("z-index", 9999);

        if (!params.position.top && !params.position.left) {
            _container
                .css("top", ($(window).height() - _container.outerHeight()) / 2)
                .css("left", ($(window).width() - _container.outerWidth()) / 2);
        } else {
            _container
                .css("top", params.position.top)
                .css("left", params.position.left);
        }

        _container.on('click', function(e) {
            e.stopPropagation();
        });

        params.onShow(_container);
        MENU = _container;
        return MENU;
    };

    $.Menu.close = function() {
        if (MENU == undefined) {
            return false;
        }

        var _container = MENU;
        _container.fadeOut(function() {
            $(this).remove();
        });
        return false;
    };

})(jQuery);
