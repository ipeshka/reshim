﻿(function ($) {
    $.widget("reshim.accessFileSolution", {
        _create: function () {
            var that = this.element;
            that.click(function (e) {
                $.Loader({ overlay: false });

                var addressValue = that.attr("href");
                var id = that.parent().parent().data('taskId') || that.data('taskId');
                var shouldSign = $.cookie('.ASPXAUTH');
                var isAccess = that.data('isAccess');
                e.preventDefault();

                if (shouldSign == undefined) { // авторизированный ли пользователь
                    $.Loader.close();
                    
                    var infoPlace = $.InfoPlace({
                        text: "Данный файл недоступен для неавторизированного пользователя. Для просмотра файла, пожалуйста, авторизируйтесь.",
                        image: RESHIM.ICO.NotificationICO(),
                        type: 'info'
                    });

                    $.Dialog("forbidden-access", {
                        caption: {
                            title: "Доступ к файлу запрещен",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml()
                    });
                    return;
                } else if (isAccess == false) {
                    var request = $.ajax({
                        url: "/api/task/detail/" + id,
                        type: "GET"
                    });

                    request.done(function (task) {
                        $.Loader.close();

                        var infoPlace = $.InfoPlace({
                            text: "Пользователь <b>" + task.Customer.Login + "</b> не оплатил решение. Для получения данного решения, пользователю нужно оплатить решение.",
                            image: RESHIM.ICO.NotificationICO(),
                            type: 'info'
                        });

                        $.Dialog("forbidden-access", {
                            caption: {
                                title: "Данное решение не оплачено",
                                sysButtons: {
                                    btnClose: true
                                }
                            },
                            overlay: true,
                            padding: 10,
                            width: WIDTH_POPUN,
                            content: infoPlace.toHtml()
                        });

                    });

                    request.error(function (jqXHR, textStatus, errorThrown) {
                        $.Loader.close();
                        
                        var errors = "";
                        $.each(jqXHR.responseJSON, function (index, value) {
                            errors += value.Message + "<br/>";
                        });
                        
                        var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });


                        $.Dialog("detail-order-error", {
                            caption: {
                                title: "Серверная ошибка",
                                sysButtons: {
                                    btnClose: true
                                }
                            },
                            overlay: true,
                            padding: 10,
                            width: WIDTH_POPUN,
                            content: infoPlace.toHtml()
                        });
                    });

                    return;
                }

                $.Loader.close();
                window.location = addressValue;
            });

        },

        _destroy: function () { },

        _setOption: function (key, value) {
            this._super('_setOption', key, value);
        }
    });
})(jQuery);

$(function () {
    $('[data-role=accessFileSolution]').accessFileSolution();
});