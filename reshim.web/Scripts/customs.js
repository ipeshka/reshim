﻿// самописные функции и обёртки
(function ($) {
    // оборочивание шаблонов
    jQuery.fn.wrapTemplate = function() {
        var template = this.html().toString().replace(new RegExp("&lt;", 'g'), "<");
        template = template.replace(new RegExp("&gt;", 'g'), ">");
        return template;
    };

    // дополнительные функции
    window.RESHIM = {};
    RESHIM.COMMON = {
        getUrlVar: function(key, obj) {
            var result = new RegExp(key + "=([^&]*)", "i").exec(obj);
            return result && unescape(result[1]) || "";
        },

        showAuthConsumerPopup: function(url) {
            var width, height, left, top;
            width = 700;
            height = 400;
            left = Math.floor((screen.width - width) / 2);
            top = Math.floor((screen.height - height) / 2);
            var popup = window.open(url, 'oauthPopup', 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top);
        },

        toFormatDateString: function(strDate, charSplit) {
            var dateExecuteSplit = strDate.split(charSplit);
            return dateExecuteSplit[1] + charSplit + dateExecuteSplit[2] + charSplit + dateExecuteSplit[0];
        },

        truncateString: function(str, length) {
            return str.length > length ? str.substring(0, length - 3) + '...' : str;
        },

        parseISO8601: function(dateStringInRange) {
            var isoExp = /^\s*(\d{4})-(\d\d)-(\d\d)\s*$/,
                date = new Date(NaN),
                month,
                parts = isoExp.exec(dateStringInRange);

            if (parts) {
                month = + parts[2];
                date.setFullYear(parts[1], month - 1, parts[3]);
                if (month != date.getMonth() + 1) {
                    date.setTime(NaN);
                }
            }
            return date;
        },
        
        errorDialog: function(errorsJson, imageCls) {
            $.Loader.close();


            var errors = "";
            $.each(errorsJson, function (index, value) {
                errors += value.Message + "<br/>";
            });

            $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });
        }
    };

    RESHIM.ICO = {
        Checkmark2ICO: function () {
            return "<svg class='icon icon-checkmark2'><use xlink:href='#icon-checkmark2'></use></svg>";
        },
        CancelCircleICO: function () {
            return "<svg class='icon icon-cancel-circle'><use xlink:href='#icon-cancel-circle'></use></svg>";
        },
        NotificationICO: function () {
            return "<svg class='icon icon-notification'><use xlink:href='#icon-notification'></use></svg>";
        },
        VKICO: function () {
            return "<svg class='icon icon-vk'><use xlink:href='#icon-vk'></use></svg>";
        }
        
    };

})(jQuery);