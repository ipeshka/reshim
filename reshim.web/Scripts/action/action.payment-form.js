﻿$(document).ready(function () {

    $("form[class='pay']").validate({
        rules: {
            amount: {
                required: true,
                number: true,
                min: 1
            }
        },

        messages: {
            amount: {
                required: "Введите сумму",
                number: "Только число",
                min: "Минимальная сумма пополнения 1 рублей"
            }
        },

        invalidHandler: function (event, validator) {
            $.each(validator.errorList, function (index) {
                var wrap = $(this.element).parent(".input-control").addClass("error");
                wrap.attr("original-title", this.message);
            });
        },

        unhighlight: function (element, errorClass, validClass) {
            var el = $(element).parent(".input-control");
            el.removeClass(errorClass);
            el.removeAttr("original-title");
            el.children("label.error").remove();
        },

        submitHandler: function (form) {
            var amount = $("input[name='amount']").val();
            var paywayVia = $("input[name='ik_pw_via']").val();
            $.Loader({ overlay: true });
            var request = $.ajax({
                url: "/api/pay/input",
                type: "POST",
                data: { Amount: amount, PaywayVia: paywayVia }
            });

            request.done(function (data) {
                $.Loader.close();
                $("input[name='ik_co_id']").val(data.CheckoutId);
                $("input[name='ik_pm_no']").val(data.PaymentNumber);
                $("input[name='ik_am']").val(data.Amount);
                $(form).attr("action", data.Action);
                form.submit();
            });

            request.error(function (jqXHR, textStatus, errorThrown) {
                $.Loader.close();

                $(".info-place.error").remove();
                var errors = "";
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

                $(infoPlace.toHtml()).insertBefore($("form[class='pay']"));
            });
        }
    });

    $("form[class='pay']").find(".input-control").tipsy({ gravity: 's' });

});