﻿$(document).ready(function () {
    var self = $(this);
    var entityId = self.find(".entity-reviews").data("id");
    var type = self.find(".entity-reviews").data("type");

    $.ajax({
        url: "/api/helper/addcountpreview/" + entityId + "/" + type,
        type: "POST"
    });
});