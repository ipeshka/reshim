﻿// форма запроса сброса пароля
$("#FormForgotpassword").validate({
    rules: {
        forgot_emailOrLogin: {
            required: true
        },

    },

    messages: {
        forgot_emailOrLogin: {
            required: "Введите логин или пароль"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parent(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parent(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },

    submitHandler: function (form) {
        var emailOrLogin = $(form).find("input[name='forgot_emailOrLogin']").val();

        $.Loader({ overlay: true });

        var request = $.ajax({
            url: "/api/auth/forgotpassword",
            type: "POST",
            data: { EmailOrLogin: emailOrLogin }
        });

        request.done(function () {
            $.Loader.close();
            $.Dialog.close("forgot-password");

            var infoPlace = $.InfoPlace({
                text: "На Вашу почту отправлена инструкция по востановлению пароля. Проверьте почту.",
                image: RESHIM.ICO.NotificationICO(),
                type: 'info'
            });

            $.Dialog("forgotpassword-done", {
                caption: {
                    title: "Отправка пароля на почту",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml(),
                sysBtnCloseClick: function (event) {
                    window.location = "/";
                }
            });

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            $(".info-place.error").remove();
            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({
                text: errors,
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error',
                imageClose: RESHIM.ICO.CancelCircleICO()
            });

            $(infoPlace.toHtml()).insertBefore($("#FormForgotpassword"));
        });
    }
});

$("#FormForgotpassword").find(".input-control").tipsy({ gravity: 's' });

// форма запроса сброса пароля
$("form.update-password").validate({
    rules: {
        password: {
            required: true,
            minlength: 6
        },

        confirmpassword: {
            equalTo: "#password"
        }

    },

    messages: {
        password: {
            required: "Введите пароль",
            minlength: "Минимальная длинна пароля 6 символов"
        },

        confirmpassword: {
            equalTo: "Пароли не совпадают"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parent(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parent(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },

    submitHandler: function (form) {
        var password = $("input[name='password']").val();
        var confirmpassword = $("input[name='confirmpassword']").val();
        var userId = $("input[name='userId']").val();

        $.Loader({ overlay: true });

        var request = $.ajax({
            url: "/api/auth/updatepassword",
            type: "POST",
            data: { UserId: userId, Password: password, ConfirmPassword: confirmpassword }
        });

        request.done(function () {
            $.Loader.close();

            var infoPlace = $.InfoPlace({ text: "Ваш пароль успешно изменён. Войдите в систему под новым паролем.", image: RESHIM.ICO.Checkmark2ICO(), type: 'success' });

            $("#_page_2").prepend();

            $.Dialog("update-password-done", {
                caption: {
                    title: "Успешная смена пароля",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml(),
                sysBtnCloseClick: function(event) {
                    window.location = "/";
                }
            });

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            $(".info-place.error").remove();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });
            
            $(infoPlace.toHtml()).insertBefore($("form.update-password"));
        });
    }
});

$("form.update-password").find(".input-control").tipsy({ gravity: 's' });