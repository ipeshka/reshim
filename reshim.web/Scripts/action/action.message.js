﻿$(document).ready(function () {
    var chat = $.connection.chat;

    $.connection.hub.start({ transport: ['longPolling'] }).done(function () {
        chat.server.getNewMessages().done(function (messages) {
            $(".CountAllNewMessage").html("(" + messages.length + ")");

            _.each(messages, function (msg) {
                var content = _.template(TEMPLATES.find("#message-notify").wrapTemplate(), { message: msg });
                $.Notify({
                    id: msg.Id,
                    content: content,
                    sysBtnCloseClick: function (e, id) {
                        chat.server.readMessage(id).done(function (result) {
                            if (result.Success) {
                                $(".CountAllNewMessage").html("(" + result.CountMessages + ")");
                            } else {
                                $.Notify({ content: "Произошла внутренняя ошибка. Перезагрузите страницу." });
                            }
                        });
                    }
                });
            });
        });
    });

    chat.client.messageIn = function (msg) {
        $.playSound('/Content/sounds-in-message');
        var content = _.template(TEMPLATES.find("#message-notify").wrapTemplate(), { message: msg });
        $.Notify({
            id: msg.Id,
            content: content,
            sysBtnCloseClick: function (e, id) {
                chat.server.readMessage(id).done(function (result) {
                    if (result.Success) {
                        $(".CountAllNewMessage").html("(" + result.CountMessages + ")");
                    } else {
                        $.Notify({ content: "Произошла внутренняя ошибка. Перезагрузите страницу." });
                    }
                });
            }
        });
    };


});

