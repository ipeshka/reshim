﻿$(document).ready(function () {
    var MAX_SIZE_FILE = 2000000; // 15 Mb
    var defaultAvatar = "/Content/img/ico/user64x64.png";

    var file = $('#FileUpload');
    var selectFile = $("#SelectFile");

    var pb = $('#ProgressUploadTask').progressbar();

    file.attr("data-url", "/api/user/updatephoto");

    selectFile.click(function (e) {
        e.preventDefault();
        file.focus().click();
    });

    // file uploader
    file.fileupload({
        dataType: 'json',
        formData: { type: 'Profile' },
        add: function (e, data) {
            debugger;
            if (data.files[0].size > MAX_SIZE_FILE) {
                alert("Размер файла больше 2 мегабайт");
                return;
            } else if (!(new RegExp(/(\.|\/)(gif|jpe?g|png)$/i).test(data.files[0].name.toLowerCase()))) {
                alert("Можно загружать только gif, jpg, jpeg, png");
                return;
            } else {
                data.submit();
            }
        },

        done: function (e, data) {
            var btnDelete = $(".delete:disabled");
            btnDelete.removeAttr('disabled');

            $(".photo-profile").each(function () {
                $(this).attr("src", data.result.Path);
            });
        },

        error: function (e, data) {
            var errors = "";
            $.each(e.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

            $.Dialog("uploadfile-error", {
                caption: {
                    title: "Ошибка файла",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        },

        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            pb.progressbar('value', progress);
        }
    });

    $(".delete").click(function (e) {
        e.preventDefault();
        $.Loader({ overlay: true });
        var self = $(this);

        var request = $.ajax({
            url: "/api/user/deletephoto",
            type: "POST"
        });


        request.done(function (data) {
            self.attr('disabled', 'disabled');
            $(".photo-profile").each(function () {
                $(this).attr("src", defaultAvatar);
            });

            $.Loader.close();
            
            var infoPlace = $.InfoPlace({
                text: "Фото успешно удалено.",
                image: RESHIM.ICO.Checkmark2ICO(),
                type: 'success'
            });
            
            $.Dialog("delete-photo-done", {
                caption: {
                    title: "Фото удалено",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

            $.Dialog("delete-photo-error", {
                caption: {
                    title: "Ошибка удаление фото",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

        });
    });

});