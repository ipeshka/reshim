﻿$(document).ready(function () {
    $.connection.hub.logging = true;
    var chat = $.connection.chat;

    $.Loader({ load: "three-quarters", container: ".chat", lighting: true });
    var message = $("textarea[name='message']");
    var sbmt = $("#SendMessage");
    var showMore = $("#ShowMore");
    var page = 2;
    var dialogId = $("input[name='dialogId']").val();
    var currentUserId = $("input[name='userId']").val();
    var containerMessages = $('.messages');

    $.connection.hub.start({ transport: ['longPolling'] }).done(function () {
        $.Loader.close();
    });
    

    if (sbmt.length) {
        sbmt.click(function () {
            var isEmptyDialog = $(this).data('empty');
            if (isEmptyDialog == 'True') {
                $(".mCSB_container").empty();
                $(this).data('empty', false);
            }

            $.Loader({ load: "three-quarters", container: ".chat", lighting: true });
            if (message.val() == '') {
                $.Loader.close();
                return false;
            }

            chat.server.send(dialogId, message.val()).done(function (result) {
                $.Loader.close();
                if (!result.Success) {
                    $.Notify({ content: "Произошла ошибка сервера. Перезагрузите страницу." });
                } else {
                    var messageContainer = $(".messages");
                    var content = _.template(TEMPLATES.find("#message-chat").wrapTemplate(), { message: result.Message, currentUserId: currentUserId });
                    messageContainer.trigger("append", [content]);
                    message.val('');
                }
            }).fail(function () {
                $.Notify({ content: "Произошла ошибка сервера. Перезагрузите страницу." });
            });
            
            return false;
        });

        message.keydown(function (e) {
            if (e.ctrlKey && e.keyCode == 13) {
                sbmt.trigger("click");
            }
        });
    }


    chat.client.messageIn = function (msg) {
        $.playSound('~/Content/sounds-in-message');

        // if open current dialog
        if (dialogId != msg.DialogId) {
           var content = _.template(TEMPLATES.find("#message-notify").wrapTemplate(), { message: msg });
            $.Notify({
                id: msg.Id,
                content: content,
                sysBtnCloseClick: function (e, id) {
                    chat.server.readMessage(id).done(function (result) {
                        if (result.Success) {
                            $(".CountNewMessage").html("(" + result.CountMessages + ")");
                        } else {
                            $.Notify({ content: "Произошла внутренняя ошибка. Перезагрузите страницу." });
                        }
                    });
                }
            });
        } else {
            var messageContainer = $(".messages");
            var content = _.template(TEMPLATES.find("#message-chat").wrapTemplate(), { message: msg, currentUserId: currentUserId });
            messageContainer.trigger("append", [content]);
            
            chat.server.readMessage(msg.Id).done(function (result) {
                if (result.Success) {
                    $(".CountNewMessage").html("(" + result.CountMessages + ")");
                } else {
                    $.Notify({ content: "Произошла внутренняя ошибка. Перезагрузите страницу." });
                }
            });
        }
    };


    // container scrollbar
    if (containerMessages.length) {
        containerMessages.mCustomScrollbar({
            theme: "reshim",
            axis: "y",
            scrollInertia: 333,
            mouseWheel: {
                enable: true,
                preventDefault: true
            },
            callbacks: {
                onInit:function(){
                    containerMessages.mCustomScrollbar("scrollTo", "bottom");
                }
            }
        });

        if (showMore.length) {
            showMore.click(function (e) {
                e.preventDefault();
                
                $.Loader({ load: "three-quarters", container: ".chat", lighting: true });
                var request = $.ajax({
                    url: "/api/helper/getmessages",
                    type: "POST",
                    data: { Page: page++, Count: 10, DialogId: dialogId }
                });

                request.done(function (data) {
                    $.Loader.close();
                    var messages = data.Messages;
                    messages.reverse();
                    if (messages.length > 0) {
                        $.each(messages, function (i, msg) {
                            var content = _.template(TEMPLATES.find("#message-chat").wrapTemplate(), { message: msg, currentUserId: currentUserId });
                            containerMessages.trigger("prepend", [content]);
                        });
                        containerMessages.mCustomScrollbar("scrollTo", "0");
                    } else {
                        showMore.attr("disabled", "disabled");
                    }
                });

                request.error(function (jqXHR, textStatus, errorThrown) {
                    $.Loader.close();
                    $.Notify({ content: "Произошла ошибка сервера. Перезагрузите страницу." });
                });

            });
        }

        $(".chat").on("append", "div.messages", function (e, text) {
            $(".mCSB_container").append(text);
            containerMessages.mCustomScrollbar("update");
            containerMessages.mCustomScrollbar("scrollTo", "bottom");
        });

        $(".chat").on("prepend", "div.messages", function (e, text) {
            $(".mCSB_container").prepend(text);
            containerMessages.mCustomScrollbar("update");
            containerMessages.mCustomScrollbar("scrollTo", "bottom");
        });
    }
});