﻿$(document).ready(function () {
    var timeHideShow = 100;
    var error = $(".info-place.error");

    error.hide();

    // select offer
    $(".offer:not(.select-offer)").click(function () {
        $(".offer").removeClass("highlight");
        $(".wrapper:last-child .icon").hide();
        
        var self = $(this);
        self.toggleClass("highlight");
        self.find(".wrapper:last-child .icon").toggle();

        if (self.hasClass("highlight")) {
            error.slideUp(timeHideShow);
        }
    });

    // submit
    $("a#SelectEmployee").click(function (e) {
        e.preventDefault();
        var self = $(this);
        var subscriberId = self.parent().parent().find(".highlight").attr("id");
        
        // error
        if (subscriberId == null) {
            error.slideDown(timeHideShow, function () {
                var $this = $(this);
                $this.find(".content").text("Выберите исполнителя");
            });
        } else {
            error.slideUp(timeHideShow);

            var taskId = $("input[name='taskId']").val();
            $.Loader({ overlay: true });

            var request = $.ajax({
                url: "/api/task/user/selectemployee/",
                type: "POST",
                data: { TaskId: taskId, SubscriberId: subscriberId }
            });

            request.done(function (data) {
                // change view                 var requestChangeView = $.ajax({
                    url: "/task/partialdetailtask",
                    type: "GET",
                    data: { taskId: taskId }
                });

                requestChangeView.done(function (view) {
                    $("#WrapperDetailTask").html(view);
                });


                var infoPlaceSuccess = $.InfoPlace({
                    text: "Исполнитель приступил к выполнению заказа",
                    image: RESHIM.ICO.Checkmark2ICO(),
                    type: 'success'
                });

                $.Dialog("select-employee-complite", {
                    caption: {
                        title: "Исполнитель успешно выбран",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlaceSuccess.toHtml()

                });
                
                $.Loader.close();
            });

            request.error(function (jqXHR, textStatus, errorThrown) {
                $.Loader.close();
                var errors = "";
                
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

                $.Dialog("select-employee-error", {
                    caption: {
                        title: "Ошибка выбора исполнителя",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

            });
        }
    });
});
