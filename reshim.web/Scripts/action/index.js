﻿$(document).ready(function () {
    $("a.all-typeWorks").click(function (e) {
        e.preventDefault();
        $.Loader({ overlay: false });

        var request = $.ajax({
            url: "/api/helper/gettypeworks/",
            type: "POST"
        });

        request.done(function (typeWorks) {
            $.Loader.close();
            $.Dialog("all-typeWorks", {
                caption: {
                    title: "Все выполняемые заказы на сайте reshim.net",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: _.template(TEMPLATES.find("#all-typeWorks").wrapTemplate(), { typeWorks: typeWorks, message: "Список выполняемых работ на нашем сайте:" })
            });
        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            var infoPlace = $.InfoPlace({
                text: "На строне сервера произошла ошибка, обратитесь к администратору!",
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error'
            });

            $.Dialog("typeWorks-error", {
                caption: {
                    title: "Серверная ошибка",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        });

    });

});