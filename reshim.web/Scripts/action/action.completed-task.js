﻿$("#OrderCompletedTask").click(function (e) {
	e.preventDefault();

	$.Loader({ overlay: true });

	var taskId = $(this).data("id");

	var request = $.ajax({
		url: "/api/completedtask/get/" + taskId,
		type: "POST"
	});

	request.done(function (data) {
		$.Loader.close();
	    var message = "Введите email для заказа работы (на данный email будет отправлено решение).";
		$.Dialog("completed-task-order", {
			caption: {
				title: "Покупка задачи",
				sysButtons: {
					btnClose: true
				}
			},
			overlay: true,
			width: WIDTH_POPUN,
			padding: 10,
			content: _.template(TEMPLATES.find("#completed-task-order").html(), { message: message, completedTaskId: data.Id, price: data.Cost })
		});

	});

	request.error(function (jqXHR, textStatus, errorThrown) {
		var errors = "";
		$.each(jqXHR.responseJSON, function (index, value) {
			errors += value.Message + "<br/>";
		});

		var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

		$.Dialog("completed-task-order-error", {
			caption: {
				title: "Ошибка заказа задачи",
				sysButtons: {
					btnClose: true
				}
			},
			overlay: true,
			padding: 10,
			width: WIDTH_POPUN,
			content: infoPlace.toHtml()
		});


		$.Loader.close();
	});
});