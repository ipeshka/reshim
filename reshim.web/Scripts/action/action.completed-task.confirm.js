﻿$("#SelectSystemsPayments").validate({
    rules: {
        email: {
            required: true,
            email: true
        }
    },

    messages: {
        email: {
            required: "Введите email",
            email: "Введите корректный email"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parents(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parents(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },


    submitHandler: function (form) {
        $.Loader({ overlay: true });
        var paywayVia = $("a.active").data("value");
        var email = $("input[name='email']").val();
        var completedTaskId = $("input[name='completedTaskId']").val();

        var request = $.ajax({
            url: "/api/pay/input",
            type: "POST",
            data: { Email: email, CompletedtaskId: completedTaskId, PaywayVia: paywayVia, HasCompletedTask: true }
        });

        request.done(function (data) {
            $.Loader.close();
            $("input[name='ik_co_id']").val(data.CheckoutId);
            $("input[name='ik_pm_no']").val(data.PaymentNumber);
            $("input[name='ik_am']").val(data.Amount);
            $(form).attr("action", data.Action);
            form.submit();
            $.cookie("ComplitedTask", data.PaymentNumber, { expires: 1, path: '/' });
        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            $(".info-place.error").remove();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

            $.Dialog("error-confirm-completed-task", {
                caption: {
                    title: "Ошибка заказа задачи",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        });

    }
});

$("#SelectSystemsPayments").find(".input-control").tipsy({ gravity: 's' });
$(".payments .payment a").click(function (e) {
    e.preventDefault();
    var self = $(this);
    $(".payments .payment a").removeClass("active");
    self.toggleClass("active");
    $("input[name='ik_pw_via']").val(self.data('value'));
});