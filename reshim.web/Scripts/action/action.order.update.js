﻿$(document).ready(function () {
    var MAX_SIZE_FILE = 15000000; // 15 Mb

    var file = $('#FileUpload');
    var selectFile = $("#SelectFile");
    var taskId = $("form[class='order']").data("id");

    var pb = $('#ProgressUploadTask').progressbar();

    file.attr("data-url", "/api/file/");

    selectFile.click(function (e) {
        e.preventDefault();
        file.focus().click();
    });


    // file uploader
    file.fileupload({
        dataType: 'json',
        formData: { type: 'Condition' },
        add: function (e, data) {
            if (data.files[0].size > MAX_SIZE_FILE) {
                alert("Размер файла больше 15 метров");
                return;
            } else {
                data.submit();
            }
        },

        done: function (e, data) {
            $("input[name='namefile']").val(data.result.Name);
            $("input[name='namefile']").blur();
            $("input[name='idfile']").val(data.result.LastId);
        },

        error: function (e, data) {
            var errors = "";
            $.each(e.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({
                text: errors,
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error'
            });

            $.Dialog("uploadfile-error", {
                caption: {
                    title: "Ошибка файла",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        },

        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            pb.progressbar('value', progress);
        }
    });


    // validation and handler
    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '');
    }, "Выберите значение из списка");


    $("form[class='order']").validate({
        ignore: "",
        rules: {
            subject: {
                selectcheck: true
            },

            typeWork: {
                selectcheck: true
            },

            datatimeorder: {
                required: true
            },

            cost: {
                digits: true
            }
        },

        messages: {
            subject: {
                selectcheck: "Выберите предмет"
            },

            typeWork: {
                selectcheck: "Выберите тип работы"
            },

            datatimeorder: {
                required: "Укажите срок решения"
            },

            cost: {
                digits: "Введите число"
            }
        },

        invalidHandler: function (event, validator) {
            $.each(validator.errorList, function (index) {
                var wrap = $(this.element).parent(".input-control").addClass("error");
                wrap.attr("original-title", this.message);
            });
        },

        unhighlight: function (element, errorClass, validClass) {
            var el = $(element).parent(".input-control");
            el.removeClass(errorClass);
            el.removeAttr("original-title");
            el.children("label.error").remove();
        },


        submitHandler: function (form) {
            var idFile = $("input[name='idfile']").val();
            var subject = $("select[name='subject']").val();
            var typeWok = $("select[name='typeWork']").val();
            var dateOfExecution = $("input[name='datatimeorder']").val();
            var cost = $("input[name='cost']").val();
            var comment = $("textarea[name='comment']").val();

            $.Loader({ overlay: true });

            var request = $.ajax({
                url: "/api/task/user/editorder",
                type: "POST",
                data: {
                    TaskId: taskId,
                    FileId: idFile,
                    DateOfExecution: RESHIM.COMMON.toFormatDateString(dateOfExecution, "/"),
                    Cost: cost,
                    SubjectId: subject,
                    TypeWorkId: typeWok,
                    Comment: comment
                }
            });


            request.done(function (data) {
                $.Loader.close();
                
                var infoPlace = $.InfoPlace({
                    text: "Ваш заказ обновлен.",
                    image: RESHIM.ICO.Checkmark2ICO(),
                    type: 'success'
                });

                $.Dialog("editorder-done", {
                    caption: {
                        title: "Ваш заказ обновлен",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

            });

            request.error(function (jqXHR, textStatus, errorThrown) {
                $.Loader.close();

                var errors = "";
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

                $.Dialog("editorder-error", {
                    caption: {
                        title: "Ошибка обновления заказа",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

            });


        }
    });

    $("form[class='order']").find(".input-control").tipsy({ gravity: 's' });

});