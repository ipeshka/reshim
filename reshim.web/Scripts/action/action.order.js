﻿$(document).ready(function () {
    var MAX_SIZE_FILE = 15000000; // 15 Mb

    var file = $('#FileUpload');
    var selectFile = $("#SelectFile");

    var pb = $('#ProgressUploadTask').progressbar();

    var yes = $("#Yes");
    var btnSend = $("#SendOrder");

    var notifies = $("input[name='notify']");

    file.attr("data-url", "/api/file/");

    selectFile.click(function (e) {
        e.preventDefault();
        file.focus().click();
    });

    // file uploader
    file.fileupload({
        dataType: 'json',
        formData: { type: 'Condition' },
        add: function (e, data) {
            if (data.files[0].size > MAX_SIZE_FILE) {
                alert("Размер файла больше 15 метров");
                return;
            } else if (!(new RegExp(/(\.|\/)(gif|jpe?g|png|docx?|zip|rar)$/i).test(data.files[0].name.toLowerCase()))) {
                alert("Можно загружать только gif, jpg, png, doc, docx, rar, zip");
                return;
            } else {
                data.submit();
            }
        },

        done: function (e, data) {
            $("input[name='namefile']").val(data.result.Name);
            $("input[name='namefile']").blur();
            $("input[name='idfile']").val(data.result.LastId);
        },

        error: function (e, data) {
            var errors = "";
            $.each(e.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({
                text: errors,
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error'
            });


            $.Dialog("uploadfile-error", {
                caption: {
                    title: "Ошибка файла",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        },

        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            pb.progressbar('value', progress);
        }
    });

    // disable/enable button for orders
    yes.change(function () {
        var that = $(this);
        if (that.is(":checked")) {
            btnSend.removeAttr('disabled');
        } else {
            btnSend.attr('disabled', 'disabled');
        }

    });


    // validation and handler
    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '');
    }, "Выберите значение из списка");

    function addRules(rulesObj) {
        for (var item in rulesObj) {
            $("input[name=" + item + "]").rules('add', rulesObj[item]);
        }
    }

    function removeRules(rulesObj) {
        for (var item in rulesObj) {
            $("input[name=" + item + "]").rules('remove');
        }
    }

    var registerRules = {
        login: {
            required: true,
            minlength: 6
        },

        email: {
            required: true,
            email: true
        },

        password: {
            required: true,
            minlength: 6
        },

        confirmpassword: {
            equalTo: "#password"
        }
    };

    $("form.order").validate({
        ignore: "",
        rules: {
            idfile: {
                required: true
            },

            subject: {
                selectcheck: true
            },

            typeWork: {
                selectcheck: true
            },

            datatimeorder: {
                required: true
            },

            cost: {
                digits: true,
                max: 100000,
                min: 30
            },
            notify: {
                required: true
            }
        },

        messages: {
            idfile: {
                required: "Выберите файл"
            },

            subject: {
                selectcheck: "Выберите предмет"
            },

            typeWork: {
                selectcheck: "Выберите тип работы"
            },

            datatimeorder: {
                required: "Укажите срок решения"
            },

            cost: {
                max: "Максимальная сумма 100 000 рублей",
                min: "Минимальная сумма 30 рублей",
                digits: "Введите число"
            },

            notify: {
                required: "Выберите одно из значений"
            },
            // register
            login: {
                required: "Введите логин",
                minlength: "Минимальная длинна логина 6 символов",
            },

            email: {
                required: "Введите email",
                email: "Не верный формат email"
            },

            password: {
                required: "Введите пароль",
                minlength: "Минимальная длинна пароля 6 символов"
            },

            confirmpassword: {
                equalTo: "Пароли не совпадают"
            }
        },

        invalidHandler: function (event, validator) {
            $.each(validator.errorList, function (index) {
                var wrap = $(this.element).parents(".input-control").addClass("error");
                wrap.attr("original-title", this.message);
            });
        },

        unhighlight: function (element, errorClass, validClass) {
            var el = $(element).parents(".input-control");
            el.removeClass(errorClass);
            el.removeAttr("original-title");
            el.children("label.error").remove();
        },


        submitHandler: function (form) {
            debugger;
            $.Loader({ overlay: true });
            // task
            var idFile = $("input[name='idfile']").val();
            var subject = $("select[name='subject']").val();
            var typeWork = $("select[name='typeWork']").val();
            var dateOfExecution = $("input[name='datatimeorder']").val();
            var cost = $("input[name='cost']").val();
            var comment = $("textarea[name='comment']").val();
            var notify = $("input[name='notify']:checked").val();
            
            if (notify == EMAIL) {
                // user
                var email = $("input[name='email']").val();
                var login = $("input[name='login']").val();
                var password = $("input[name='password']").val();
                var confirmpassword = $("input[name='password']").val();

                var quickRequest = $.ajax({
                    url: "/api/auth/quickregister",
                    type: "POST",
                    data: {
                        User: {
                            Login: login,
                            Email: email,
                            Password: password,
                            ConfirmPassword: confirmpassword
                        },
                        Task: {
                            FileId: idFile,
                            DateOfExecution: RESHIM.COMMON.toFormatDateString(dateOfExecution, "/"),
                            Cost: cost,
                            SubjectId: subject,
                            TypeWorkId: typeWork,
                            Comment: comment
                        }
                    }
                });

                quickRequest.done(function (user) {
                    $.Loader.close();
                    
                    var infoPlace = $.InfoPlace({
                        text: "Уважаемый пользователь, <b>" + user.User.Login + "</b>. Спасибо за регистрацию! После закрытия окна, Вы окажетесь на странице управления своим заказом.",
                        image: RESHIM.ICO.Checkmark2ICO(),
                        type: 'success'
                    });


                    $.Dialog("registration-done", {
                        caption: {
                            title: "Успешное добавление заказа",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml(),
                        sysBtnCloseClick: function (event) {
                            window.location = "/task?status=&isMyTask=on";
                        }
                    });

                    $.Dialog.close("registration");
                });

                quickRequest.error(function (jqXHR, textStatus, errorThrown) {
                    $.Loader.close();

                    var errors = "";
                    $.each(jqXHR.responseJSON, function (index, value) {
                        errors += value.Message + "<br/>";
                    });

                    var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

                    $.Dialog("registration-error", {
                        caption: {
                            title: "Ошибка регистрации",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml()
                    });
                });
            } else if (notify == VK) {
                debugger;
                var request = $.ajax({
                    url: "/api/task/user/validationorder",
                    type: "POST",
                    data: {
                        FileId: idFile,
                        DateOfExecution: RESHIM.COMMON.toFormatDateString(dateOfExecution, "/"),
                        Cost: cost,
                        TypeWorkId: typeWork,
                        SubjectId: subject,
                        Comment: comment
                    }
                });

                request.done(function (data) {
                    $.Loader.close();
                    $.cookie.raw = true;
                    $.cookie('task', $(form).serializeJSON(), { expires: 1, path: '/' });
                    RESHIM.COMMON.showAuthConsumerPopup('/Auth/RegisterFromService?providerName=Vkontakte');
                });

                request.error(function(jqXHR, textStatus, errorThrown) {
                    $.Loader.close();

                    var errors = "";
                    $.each(jqXHR.responseJSON, function(index, value) {
                        errors += value.Message + "<br/>";
                    });

                    var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

                    $.Dialog("validationorder-error", {
                        caption: {
                            title: "Ошибка заказа",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml()
                    });
                });

            } else {
                var request = $.ajax({
                    url: "/api/task/user/addorder",
                    type: "POST",
                    data: {
                        FileId: idFile,
                        DateOfExecution: RESHIM.COMMON.toFormatDateString(dateOfExecution, "/"),
                        Cost: cost,
                        TypeWorkId: typeWork,
                        SubjectId: subject,
                        Comment: comment
                    }
                });

                request.done(function (data) {
                    $.Loader.close();
                    
                    var infoPlace = $.InfoPlace({
                        text: "Ваш заказ добавлен под номером " + data.LastId,
                        image: RESHIM.ICO.Checkmark2ICO(),
                        type: 'success'
                    });

                    $.Dialog("addorder-done", {
                        caption: {
                            title: "Ваш заказ добавлен",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml(),
                        sysBtnCloseClick: function (event) {
                            window.location = "/task";
                        }
                    });

                });

                request.error(function (jqXHR, textStatus, errorThrown) {
                    $.Loader.close();


                    var errors = "";
                    $.each(jqXHR.responseJSON, function (index, value) {
                        errors += value.Message + "<br/>";
                    });

                    var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

                    $.Dialog("addorder-error", {
                        caption: {
                            title: "Ошибка добавления заказа",
                            sysButtons: {
                                btnClose: true
                            }
                        },
                        overlay: true,
                        padding: 10,
                        width: WIDTH_POPUN,
                        content: infoPlace.toHtml()
                    });

                });
            }
        }
    });


    notifies.click(function (e) {
        var that = $(this);
        var notifyValue = that.val();
        if (notifyValue == EMAIL) {
            $(".quick-reg").fadeIn(250);
            addRules(registerRules);
        } else if (notifyValue == VK) {
            $(".quick-reg").fadeOut(250);
            removeRules(registerRules);
        }

    });

    $("form.order").find(".input-control").tipsy({ gravity: 's' });

});