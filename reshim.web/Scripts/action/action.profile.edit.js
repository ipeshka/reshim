﻿$(document).ready(function () {
    $("form[class='profile']").validate({
        submitHandler: function (form) {
            
            var icq = $("input[name='icq']").val();
            var phone = $("input[name='phone']").val();
            var skype = $("input[name='skype']").val();
            var urlVk = $("input[name='url-vk']").val();
            var country = $("input[name='country']").val();
            var city = $("input[name='city']").val();
            var placeOfStudy = $("input[name='place-study']").val();
            var dateOfBirth = $("input[name='date-birth']").val();
            var comments = $("textarea[name='comments']").val();

            $.Loader({ overlay: true });
            var request = $.ajax({
                url: "/api/user/updateprofile",
                type: "POST",
                data: {
                    Icq: icq,
                    Phone: phone,
                    Skype: skype,
                    UrlVk: urlVk,
                    Country: country,
                    City: city,
                    PlaceOfStudy: placeOfStudy,
                    DateOfBirth: RESHIM.COMMON.toFormatDateString(dateOfBirth, "/"),
                    Comments: comments
                }
            });


            request.done(function (data) {
                $.Loader.close();
                
                var infoPlace = $.InfoPlace({
                    text: "Ваш профиль обновлен.",
                    image: RESHIM.ICO.Checkmark2ICO(),
                    type: 'success'
                });

                $.Dialog("edit-profile-done", {
                    caption: {
                        title: "Обновление профиля",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

            });

            request.error(function (jqXHR, textStatus, errorThrown) {
                $.Loader.close();

                var errors = "";
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });


                $.Dialog("edit-profile-error", {
                    caption: {
                        title: "Ошибка обновления профиля",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

            });


        }
    });

    $("form[class='profile']").find(".input-control").tipsy({ gravity: 's' });

});