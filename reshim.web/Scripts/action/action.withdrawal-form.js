﻿$(document).ready(function () {
    // validation and handler
    jQuery.validator.addMethod('selectcheck', function (value) {
        return (value != '');
    }, "Выберите значение из списка");


    $("form[class='cash-out']").validate({
        rules: {
            amount: {
                required: true,
                number: true,
                min: 100
            },

            purse: {
                required: true
            },

            typePayment: {
                selectcheck: true
            }
        },

        messages: {
            amount: {
                required: "Введите сумму",
                number: "Только число",
                min: "Минимальная сумма заявки 100 рублей"
            },

            purse: {
                required: "Введите реквизиты"
            },

            typePayment: {
                selectcheck: "Выберите тип платежа"
            }
        },

        invalidHandler: function (event, validator) {
            $.each(validator.errorList, function (index) {
                var wrap = $(this.element).parent(".input-control").addClass("error");
                wrap.attr("original-title", this.message);
            });
        },

        unhighlight: function (element, errorClass, validClass) {
            var el = $(element).parent(".input-control");
            el.removeClass(errorClass);
            el.removeAttr("original-title");
            el.children("label.error").remove();
        },

        submitHandler: function (form) {
            var amount = $("input[name='amount']").val();
            var purse = $("input[name='purse']").val();
            var comment = $("textarea[name='comment']").val();
            var typePayment = $("select[name='typePayment']").val();


            $.Loader({ overlay: true });

            var request = $.ajax({
                url: "/api/pay/withdrawal",
                type: "POST",
                data: { Amount: amount, TypePayment: typePayment, Purse: purse, Comment: comment }
            });

            request.done(function (data) {
                $.Loader.close();

                var infoPlace = $.InfoPlace({
                    text: "Заявка на вывод успешно сформирована",
                    image: RESHIM.ICO.Checkmark2ICO(),
                    type: 'success'
                });

                $("input[name='amount']").val("");
                $("input[name='purse']").val("");
                $("textarea[name='comment']").val("");
                
                $.Dialog("withdrawal-succes", {
                    caption: {
                        title: "Успешная заявка",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });
            });

            request.error(function (jqXHR, textStatus, errorThrown) {
                $.Loader.close();

                $(".info-place.error").remove();
                var errors = "";
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

                $(infoPlace.toHtml()).insertBefore($("form[class='cash-out']"));

            });

        }
    });

    $("form[class='cash-out']").find(".input-control").tipsy({ gravity: 's' });

});