﻿$("#FormRegistration").validate({
    rules: {
        reg_login: {
            required: true,
            minlength: 3
        },

        reg_email: {
            required: true,
            email: true
        },

        reg_password: {
            required: true,
            minlength: 6
        },

        reg_confirmpassword: {
            equalTo: "#reg_password"
        }

    },

    messages: {
        reg_login: {
            required: "Введите логин",
            minlength: "Минимальная длинна логина 3 символов"
        },

        reg_email: {
            required: "Введите email",
            email: "Не верный формат email"
        },

        reg_password: {
            required: "Введите пароль",
            minlength: "Минимальная длинна пароля 6 символов"
        },

        reg_confirmpassword: {
            equalTo: "Пароли не совпадают"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parent(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parent(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },

    submitHandler: function (form) {
        var login = $("input[name='reg_login']").val();
        var email = $("input[name='reg_email']").val();
        var password = $("input[name='reg_password']").val();
        var confirmpassword = $("input[name='reg_confirmpassword']").val();

        $.Loader({ overlay: true });

        var request = $.ajax({
            url: "/api/auth/register",
            type: "POST",
            data: { Login: login, Email: email, Password: password, ConfirmPassword: confirmpassword }
        });

        request.done(function (user) {
            $.Loader.close();
            
            var infoPlace = $.InfoPlace({
                text: "Уважаемый пользователь, <b>" + user.Login + "</b>. Спасибо за регистрацию! После закрытия окна, Вы автоматический будите авторизированы.",
                image: RESHIM.ICO.Checkmark2ICO(),
                type: 'success'
            });

            $.Dialog("registration-done", {
                caption: {
                    title: "Успешная регистрация",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml(),
                sysBtnCloseClick: function (event) {
                    window.location = "/task/order";
                }
            });

            $.Dialog.close("registration");
        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            $(".info-place.error").remove();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

            $("#FormRegistration").prepend(infoPlace.toHtml());
        });

    }
});

$("#FormRegistration").find(".input-control").tipsy({ gravity: 's' });

$("#LoginVk").click(function (e) {
    RESHIM.COMMON.showAuthConsumerPopup('/Auth/RegisterFromService?providerName=Vkontakte');
    e.preventDefault();
});

$('[data-role=tab-control]').tabcontrol();