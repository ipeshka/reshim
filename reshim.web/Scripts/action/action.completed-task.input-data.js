﻿$("#FormOrderCompletedTask").validate({
    rules: {
        email: {
            required: true,
            email: true
        }
    },

    messages: {
        email: {
            required: "Введите email",
            email: "Введите корректный email"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parents(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parents(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },


    submitHandler: function (form) {
        $.Loader({ overlay: true });
        var email = $("input[name='email']").val();
        var completedTaskId = $("input[name='completedTaskId']").val();

        var message = "Выберите систему для оплаты";
        $.Loader.close();
        $.Dialog("select-systems-payments", {
            caption: {
                title: "Выбор системы оплаты",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            width: WIDTH_POPUN + 50,
            padding: 10,
            content: _.template(TEMPLATES.find("#select-systems-payments").html(), { message: message, email: email, completedTaskId: completedTaskId })
        });
    }
});

