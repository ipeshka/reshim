﻿$(document).ready(function () {
    $("td.task").click(function (e) {
        var that = $(this);
        var id = that.data('taskId');
        e.preventDefault();
        $.Loader({ overlay: false });

        var request = $.ajax({
            url: "/api/task/detail/" + id,
            type: "GET"
        });

        request.done(function (task) {
            $.Loader.close();
            $.Dialog("detail-order", {
                caption: {
                    title: "Подробная информация о заказе №" + task.Id,
                    sysButtons: {
                        btnClose: false
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: _.template(TEMPLATES.find("#detail-order").wrapTemplate(), { task: task })
            });
        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();
            
            var infoPlace = $.InfoPlace({
                text: "На строне сервера произошла ошибка, обратитесь к администратору!",
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error'
            });

            $.Dialog("detail-order-error", {
                caption: {
                    title: "Серверная ошибка",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        });

    });

});