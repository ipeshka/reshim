﻿$("#FormLogin").validate({
    rules: {
        login_emailOrLogin: {
            required: true,
        },

        login_password: {
            required: true,
            minlength: 6
        }

    },

    messages: {
        login_emailOrLogin: {
            required: "Введите email или логин",
        },

        login_password: {
            required: "Введите пароль",
            minlength: "Минимальная длинна пароля 6 символов"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parent(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parent(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },

    submitHandler: function (form) {
        var emailOrLogin = $("input[name='login_emailOrLogin']").val();
        var password = $("input[name='login_password']").val();

        $.Loader({ overlay: true });

        var request = $.ajax({
            url: "/api/auth/login",
            type: "POST",
            data: { EmailOrLogin: emailOrLogin, Password: password }
        });

        request.done(function (role) {
            $.Loader.close();
            $.Dialog.close("login");

            if (role == "1") // admin
                window.location = "/admin";
            else if (role == "2") // employe
                window.location = "/employee/task";
            else if (role == "3") // user
                window.location = "/user/personalcabinet";

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            $(".info-place.error").remove();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({
                text: errors,
                image: RESHIM.ICO.CancelCircleICO(),
                type: 'error',
                imageClose: RESHIM.ICO.CancelCircleICO()
            });

            $("#FormLogin").prepend(infoPlace.toHtml());
        });
    }
});

$("#FormLogin").find(".input-control").tipsy({ gravity: 's' });

$("#LoginVk").click(function (e) {
    RESHIM.COMMON.showAuthConsumerPopup('/Auth/RegisterFromService?providerName=Vkontakte');
    e.preventDefault();
});

$("#ForgotPassword").click(function (e) {
    e.preventDefault();

    $.Dialog("forgot-password", {
        caption: {
            title: "Восстановление пароля",
            sysButtons: {
                btnClose: true
            }
        },
        overlay: true,
        padding: 10,
        width: WIDTH_POPUN,
        content: _.template(TEMPLATES.find("#forgotpassword").wrapTemplate())
    });
});

$('[data-role=tab-control]').tabcontrol();