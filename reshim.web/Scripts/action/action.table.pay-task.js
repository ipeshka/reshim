﻿$("#PayTask").click(function (e) {
    e.preventDefault();

    $.Loader({ overlay: true });

    var taskId = $("input[name='taskId']").val();

    var request = $.ajax({
        url: "/api/task/user/pay/" + taskId,
        type: "POST"
    });

    request.done(function (data) {
        // change view         var requestChangeView = $.ajax({
            url: "/task/partialdetailtask",
            type: "GET",
            data: { taskId: taskId }
        });

        requestChangeView.done(function (view) {
            $.Loader.close();
            $("#WrapperDetailTask").html(view);
        });

        var infoPlace = $.InfoPlace({
            text: "Вы успешно оплатили заказ! Теперь Вы можете скачать решение.",
            image: RESHIM.ICO.Checkmark2ICO(),
            type: 'success'
        });

        $.Dialog("pay-task-complite", {
            caption: {
                title: "Успешно оплатили",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml()
        });

        
    });

    request.error(function (jqXHR, textStatus, errorThrown) {
        var errors = "";
        $.each(jqXHR.responseJSON, function (index, value) {
            errors += value.Message + "<br/>";
        });

        var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error' });

        $.Dialog("pay-task-error", {
            caption: {
                title: "Ошибка оплаты заказа",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml()
        });


        $.Loader.close();
    });
});