/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.language = 'en';
    config.contentsCss = '/Content/site.css';
    config.uiColor = "#40484C";
    config.bodyClass = 'article';

    config.extraPlugins = 'widget,lineutils,mathjax,spoiler,textselection,syntaxhighlight';
    config.mathJaxLib = '//cdn.mathjax.org/mathjax/2.2-latest/MathJax.js?config=TeX-AMS_HTML';
    config.codeSnippet_theme = 'github';

    config.toolbarGroups = [
		{ name: 'clipboard', groups: ['clipboard', 'undo'] },
		{ name: 'editing', groups: ['selection', 'editing', 'find', 'spellchecker'] },
		{ name: 'links', groups: ['links'] },
		{ name: 'insert', groups: ['insert'] },
		{ name: 'forms', groups: ['forms'] },
		{ name: 'tools', groups: ['tools'] },
		{ name: 'document', groups: ['mode', 'document', 'doctools'] },
		{ name: 'others', groups: ['others'] },
		'/',
		{ name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
		{ name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
		{ name: 'styles', groups: ['styles'] },
		{ name: 'colors', groups: ['colors'] },
		{ name: 'about', groups: ['about'] }
    ];

    config.removeButtons = 'Underline,Subscript,Superscript,Cut,Copy,Paste,' +
        'PasteText,PasteFromWord,Undo,Redo,Scayt,About,Print,SelectAll,Find,Flash,IFrame,' +
        'Form,Checkbox,RadioButton,TextField,TextArea,SelectionField,Button,ImageButton,HiddenField,' +
        'Save,NewPage,Preview,Templates,SetLanguage,FontName';
};
