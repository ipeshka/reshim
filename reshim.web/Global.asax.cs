﻿using System;
using System.Configuration;
using System.Data.Entity;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using MassTransit;
using Moveax.Mvc.ErrorHandler;
using Newtonsoft.Json.Converters;
using reshim.data.infrastructure;
using reshim.web.App_Start;
using reshim.web.core.Auth;
using reshim.web.core.Model;
using reshim.web.Mappers;

namespace reshim.web
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            var dateTimeConverter = new IsoDateTimeConverter
            {
                DateTimeFormat = "dd/MM/yyyy"
            };
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Converters.Add(dateTimeConverter);
            Bootstrapper.Run();
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfiguration.Configure();
            ConfigurationManager.AppSettings["AbsolutePath"] = Context.Server.MapPath("~");
            Database.SetInitializer(new ProjectInitializer());
        }

        private static bool IsValidAuthCookie(HttpCookie authCookie)
        {
            return authCookie != null && !String.IsNullOrEmpty(authCookie.Value);
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += this.PostAuthenticateRequestHandler;
            base.Init();
        }

        private void PostAuthenticateRequestHandler(object sender, EventArgs e)
        {
            HttpCookie authCookie = this.Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (IsValidAuthCookie(authCookie))
            {
                var formsAuthentication = DependencyResolver.Current.GetService<IFormsAuthentication>();

                var ticket = formsAuthentication.Decrypt(authCookie.Value);
                var efmvcUser = new ReshimUser(ticket) { IsAuthenticated = true };
                string[] userRoles = { efmvcUser.RoleName };
                this.Context.User = new GenericPrincipal(efmvcUser, userRoles);
                formsAuthentication.SetAuthCookie(this.Context, ticket);
            }
        }

#if !DEBUG
        protected void Application_Error(object sender, System.EventArgs e)
        {
            var errorHandler = new MvcApplicationErrorHandler(application: this, exception: this.Server.GetLastError())
            {
                EnableHttpReturnCodes = true,
                PassThroughHttp401 = false
            };

            errorHandler.Execute();
        }
#endif
    }
}