﻿using System.Web.Mvc;
using reshim.model.Entities;

namespace reshim.web.Areas.Admin.Helpers
{
    public static class CustomHtmlHelperAdmin
    {
        public static MvcHtmlString ColorForTask(this HtmlHelper helper, StatusTask status)
        {
            if (status == StatusTask.New)
            {
                return new MvcHtmlString("label-default");
            }
            else if (status == StatusTask.Rating)
            {
                return new MvcHtmlString("label-primary");
            }
            else if (status == StatusTask.Agreement)
            {
                return new MvcHtmlString("label-info");
            }
            else if (status == StatusTask.CompletedNotPay)
            {
                return new MvcHtmlString("label-warning");
            }
            else if (status == StatusTask.Completed)
            {
                return new MvcHtmlString("label-success");
            }
            else
            {
                return new MvcHtmlString(string.Empty);
            }
        }

    }
}