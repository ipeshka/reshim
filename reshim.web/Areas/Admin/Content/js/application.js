$(function () {
    //===== русские сообщения для валидации  =====//
    jQuery.extend(jQuery.validator.messages, {
        required: "Это обязательное поле.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Минимальное количество символов {0}."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    //CKEDITOR.replace('instancename', {
    //    filebrowserBrowseUrl: '/Scripts/Filemanager-master/index.html'
    //});

    //CKEDITOR.replace('.editor');
    $('.editor').ckeditor({
        filebrowserBrowseUrl: '/Scripts/Filemanager-master/index.html'
    });

    /* Bootstrap Multiselects */
    $('.multi-select').multiselect({
        buttonClass: 'btn btn-default',
        onChange: function (element, checked) {
            $.uniform.update();
        },
        maxHeight: 400
    });


    /* # Select2 dropdowns */
    $(".select-search").select2({
        width: 400
    });


    //===== Form elements styling =====//
    $(".styled, .multiselect-container input").uniform({ radioClass: 'choice', selectAutoWidth: false });





















    /* # Form Related Plugins
    ================================================== */
    //===== Pluploader (multiple file uploader) =====//
    $(".multiple-uploader").pluploadQueue({
        runtimes: 'html5, html4',
        url: '../upload.php',
        chunk_size: '1mb',
        unique_names: true,
        filters: {
            max_file_size: '10mb',
            mime_types: [
				{ title: "Image files", extensions: "jpg,gif,png" },
				{ title: "Zip files", extensions: "zip" }
            ]
        },
        resize: { width: 320, height: 240, quality: 90 }
    });

    //===== Elastic textarea =====//
    $('.elastic').autosize();

    //===== Dual select boxes =====//
    $.configureBoxes();

    //===== Input limiter =====//
    $('.limited').inputlimiter({
        limit: 100,
        boxId: 'limit-text',
        boxAttach: false
    });

    //===== Tags Input =====//	
    $('.tags').tagsInput({ width: '100%' });
    $('.tags-autocomplete').tagsInput({
        width: '100%',
        autocomplete_url: 'tags_autocomplete.html'
    });


    /* # Form Validation
    ================================================== */
    //===== jGrowl notifications defaults =====//
    $.jGrowl.defaults.closer = false;
    $.jGrowl.defaults.easing = 'easeInOutCirc';


    //===== Time pickers =====//
    $('#defaultValueExample, #time').timepicker({ 'scrollDefaultNow': true });

    $('#durationExample').timepicker({
        'minTime': '2:00pm',
        'maxTime': '11:30pm',
        'showDuration': true
    });

    $('#onselectExample').timepicker();
    $('#onselectExample').on('changeTime', function () {
        $('#onselectTarget').text($(this).val());
    });

    $('#timeformatExample1, #timeformatExample3').timepicker({ 'timeFormat': 'H:i:s' });
    $('#timeformatExample2, #timeformatExample4').timepicker({ 'timeFormat': 'h:i A' });



    //===== Color picker =====//
    $('.color-picker').colorpicker();

    $('.color-picker-hex').colorpicker({
        format: 'hex'
    });


    //===== jQuery UI Datepicker =====//
    $(".datepicker").datepicker({
        showOtherMonths: true
    });

    $(".datepicker-inline").datepicker({ showOtherMonths: true });

    $(".datepicker-multiple").datepicker({
        showOtherMonths: true,
        numberOfMonths: 3
    });

    $(".datepicker-trigger").datepicker({
        showOn: "button",
        buttonImage: "images/interface/datepicker_trigger.png",
        buttonImageOnly: true,
        showOtherMonths: true
    });

    $(".from-date").datepicker({
        defaultDate: "+1w",
        numberOfMonths: 3,
        showOtherMonths: true,
        onClose: function (selectedDate) {
            $(".to-date").datepicker("option", "minDate", selectedDate);
        }
    });
    $(".to-date").datepicker({
        defaultDate: "+1w",
        numberOfMonths: 3,
        showOtherMonths: true,
        onClose: function (selectedDate) {
            $(".from-date").datepicker("option", "maxDate", selectedDate);
        }
    });

    $(".datepicker-restricted").datepicker({ minDate: -20, maxDate: "+1M +10D", showOtherMonths: true });



    //===== Jquery UI sliders =====//
    $("#default-slider").slider();

    $("#increments-slider").slider({
        value: 100,
        min: 0,
        max: 500,
        step: 50,
        slide: function (event, ui) {
            $("#donation-amount").val("$" + ui.value);
        }
    });
    $("#donation-amount").val("$" + $("#increments-slider").slider("value"));

    $("#range-slider, #range-slider1").slider({
        range: true,
        min: 0,
        max: 500,
        values: [75, 300],
        slide: function (event, ui) {
            $("#price-amount, #price-amount1").val("$" + ui.values[0] + " - $" + ui.values[1]);
        }
    });
    $("#price-amount, #price-amount1").val("$" + $("#range-slider, #range-slider1").slider("values", 0) +
      " - $" + $("#range-slider, #range-slider1").slider("values", 1));

    $("#slider-range-min, #slider-range-min1").slider({
        range: "min",
        value: 37,
        min: 1,
        max: 700,
        slide: function (event, ui) {
            $("#min-amount, #min-amount1").val("$" + ui.value);
        }
    });
    $("#min-amount, #min-amount1").val("$" + $("#slider-range-min, #slider-range-min1").slider("value"));

    $("#slider-range-max, #slider-range-max1").slider({
        range: "max",
        min: 1,
        max: 10,
        value: 2,
        slide: function (event, ui) {
            $("#max-amount, #max-amount1").val(ui.value);
        }
    });
    $("#max-amount, #max-amount1").val($("#slider-range-max, #slider-range-max1").slider("value"));



    //===== Code prettifier =====//
    window.prettyPrint && prettyPrint();



    //===== Fancy box (lightbox plugin) =====//
    $(".lightbox").fancybox({
        padding: 1
    });



    //===== Fullcalendar =====//
    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var calendar = $('.fullcalendar').fullCalendar({
        header: {
            left: 'prev,next,today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        selectable: true,
        selectHelper: true,
        select: function (start, end, allDay) {
            var title = prompt('Event Title:');
            if (title) {
                calendar.fullCalendar('renderEvent',
					{
					    title: title,
					    start: start,
					    end: end,
					    allDay: allDay
					},
					true // make the event "stick"
				);
            }
            calendar.fullCalendar('unselect');
        },
        editable: true,
        events: [
			{
			    title: 'All Day Event',
			    start: new Date(y, m, 1)
			},
			{
			    title: 'Long Event',
			    start: new Date(y, m, d - 5),
			    end: new Date(y, m, d - 2)
			},
			{
			    id: 999,
			    title: 'Repeating Event',
			    start: new Date(y, m, d - 3, 16, 0),
			    allDay: false
			},
			{
			    id: 999,
			    title: 'Repeating Event',
			    start: new Date(y, m, d + 4, 16, 0),
			    allDay: false
			},
			{
			    title: 'Meeting',
			    start: new Date(y, m, d, 10, 30),
			    allDay: false
			},
			{
			    title: 'Lunch',
			    start: new Date(y, m, d, 12, 0),
			    end: new Date(y, m, d, 14, 0),
			    allDay: false
			},
			{
			    title: 'Birthday Party',
			    start: new Date(y, m, d + 1, 19, 0),
			    end: new Date(y, m, d + 1, 22, 30),
			    allDay: false
			}
        ]
    });

    /* Render hidden calendar on tab show */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function () {
        $('.fullcalendar').fullCalendar('render');
    });


    /* # Bootstrap Plugins
    ================================================== */
    //===== Tooltip =====//
    $('.tip').tooltip();

    //===== Popover =====//
    $("[data-toggle=popover]").popover().click(function (e) {
        e.preventDefault();
    });

    //===== Loading button =====//
    $('.btn-loading').click(function () {
        var btn = $(this);
        btn.button('loading');
        setTimeout(function () {
            btn.button('reset');
        }, 3000);
    });

    //===== Add fadeIn animation to dropdown =====//
    $('.dropdown, .btn-group').on('show.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
    });

    //===== Add fadeOut animation to dropdown =====//
    $('.dropdown, .btn-group').on('hide.bs.dropdown', function (e) {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
    });


    /* # Interface Related Plugins
    ================================================== */
    //===== Collapsible navigation =====//
    $('.expand').collapsible({
        defaultOpen: 'second-level,third-level',
        cssOpen: 'level-opened',
        cssClose: 'level-closed',
        speed: 150
    });


    /* # Default Layout Options
    ================================================== */
    //===== Hiding sidebar =====//

    $('.sidebar-toggle').click(function () {
        $('.page-container').toggleClass('hidden-sidebar');
    });

});