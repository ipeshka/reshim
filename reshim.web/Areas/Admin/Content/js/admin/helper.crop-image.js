﻿// upload image
var jqXHRData;

$('#uploadImage').fileupload({
    url: '/api/file/',
    formData: { type: "Image" },
    dataType: 'json',
    add: function (e, data) {
        jqXHRData = data;
    },
    done: function (event, data) {
        if (data.result.IsUploaded) {
            $("#tmpUploadedImagePath").val(data.result.Path);
            destroyCrop();
            $("#uploadedImage").attr("src", data.result.Path + "?t=" + new Date().getTime());
            initCrop();
            $("#cropImageArea").fadeIn("slow");
        }
    },
    fail: function (event, data) {
        if (data.files[0].error) {
            alert(data.files[0].error);
        }
    }
});

$("#startUploadImage").on('click', function (e) {
    e.preventDefault();
    if (jqXHRData) {
        jqXHRData.submit();
    }
    return false;
});




// cropping image
var imageCropWidth = 0;
var imageCropHeight = 0;
var cropPointX = 0;
var cropPointY = 0;

$("#btnCropImage").on("click", function (e) {
    e.preventDefault();
    cropImage();
});

function initCrop() {
    $('#uploadedImage').Jcrop({
        onChange: setCoordsAndImgSize,
        minSize: [650, 250],
        aspectRatio: 10 / 5
    });

    $("#btnCropImage").fadeIn("slow");
}

function destroyCrop() {
    var jcropApi = $('#uploadedImage').data('Jcrop');

    if (jcropApi !== undefined) {
        jcropApi.destroy();
        $('#uploadedImage').attr('style', "").attr("src", "");
    }
}

function setCoordsAndImgSize(e) {
    imageCropWidth = e.w;
    imageCropHeight = e.h;

    cropPointX = e.x;
    cropPointY = e.y;
}

function cropImage() {

    if (imageCropWidth == 0 && imageCropHeight == 0) {
        alert("Выберите область обрезки.");
        return;
    }

    $.ajax({
        url: '/api/helper/cropimage',
        type: 'POST',
        data: {
            ImagePath: $("#tmpUploadedImagePath").val(),
            CropPointX: Math.round(cropPointX),
            CropPointY: Math.round(cropPointY),
            ImageCropWidth: Math.round(imageCropWidth),
            ImageCropHeight: Math.round(imageCropHeight)
        },
        success: function (data) {

            $("#croppedImagePath").val(data.Path);

            $("#croppedImage")
                .attr("src", data.Path + "?t=" + new Date().getTime())
                .show();
        },
        error: function (data) { }
    });
}
