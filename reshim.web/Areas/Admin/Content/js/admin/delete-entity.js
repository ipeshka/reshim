﻿$('#delete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var name = button.data('name');
    var text = button.data('text');
    var urlDelete = button.data('url-delete');
    var modal = $(this);
    modal.find('.modal-title').text(name);
    modal.find('.body-text').text(text);

    $('#FormDelete').ajaxForm({
        dataType: 'json',
        type: 'POST',
        success: processJson,
        url: urlDelete
    });
});

function processJson(data) {
    if (data.Success == false) {
        $('#delete').modal('hide');
        $('#errors').modal('show');
    } else {
        $('#delete').modal('hide');
        $("#" + data.LastId).fadeOut(500, function () { $(this).remove(); });
    }
}
