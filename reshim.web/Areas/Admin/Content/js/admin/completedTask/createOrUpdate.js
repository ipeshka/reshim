﻿$(window).load(function () {
    var MAX_SIZE_FILE = 15000000; // 15 Mb

    var file = $('#UploadSolution');

    file.attr("data-url", "/api/file/");

    // file uploader
    file.fileupload({
        dataType: 'json',
        formData: { type: 'CompletedTask' },
        add: function (e, data) {
            debugger;
            if (data.files[0].size > MAX_SIZE_FILE) {
                alert("Размер файла больше 15 метров");
                return;
            } else if (!(new RegExp(/(\.|\/)(gif|jpe?g|png|docx?|zip|rar)$/i).test(data.files[0].name.toLowerCase()))) {
                alert("Можно загружать только gif, jpg, png, doc, docx, rar, zip");
                return;
            } else {
                data.submit();
            }
        },

        done: function (e, data) {
            $("input[name='SolutionId']").val(data.result.LastId);
            alert("Файл загружен!");
        },

        error: function (e, data) {
            alert("Ошибка загрузки файла!");
        }
    });
});