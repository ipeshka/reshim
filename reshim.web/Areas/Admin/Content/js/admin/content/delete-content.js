﻿$('#delete').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    var id = button.data('id');
    var name = button.data('name');
    var text = button.data('text');
    var modal = $(this);
    modal.find('.modal-title').text(name);
    modal.find('.body-text').text(text);
    modal.find(".modal-body input[name='id']").val(id);
});


$('#FormDelete').ajaxForm({
    dataType: 'json',
    success: processJson
});

function processJson(data) {
    if (data.Result.Success == false) {
        $('#delete').modal('hide');
        $('#errors').modal('show');
    } else {
        $('#delete').modal('hide');
        location.reload();
    }
}
