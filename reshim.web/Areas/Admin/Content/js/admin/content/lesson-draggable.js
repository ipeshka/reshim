﻿$('.table tbody').sortable({
    cursor: "move",
    placeholder: 'ui-state-highlight',
    helper: fixWidthHelper,
    start: function(event, ui) {
        $(ui.item).css("background-color", "#40484C");
    },
    stop: function (event, ui) {
        var curPage = $("input[name='curPage']").val();
        var self = ui.item.parent().find("tr");

        var data = _.map(self, function (obj) {
            return obj.id;
        });

        $.ajax({
            url: '/api/helper/changepositioncontent/',
            type: 'POST',
            data: {
                Data: data,
                CurPage: curPage
            },
            success: function (data) {
                $(ui.item).css("background-color", "#ffffff");
                
                var curPosition = curPage == 1 ? curPage : (curPage - 1) * 11;
                var positions = ui.item.parent().find(".position");
                positions.each(function (index, element) {
                    $(element).text(curPosition);
                    curPosition++;
                });

            },
            error: function (data) { }
        });
    }
}).disableSelection();

function fixWidthHelper(e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
}