﻿using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.CompletedTask;
using reshim.domain.Queryies.CompletedTask;
using reshim.domain.Queryies.Content.Subject;
using reshim.model.Entities;
using reshim.model.view.ViewModels.ComplitedTask;
using reshim.web.core.Extensions;
using reshim.web.Helpers;

namespace reshim.web.Areas.Admin.Controllers
{
    [Access(new[] { UserRoles.Admin })]
    public class CompletedTaskController : BaseController
    {
        public CompletedTaskController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        public ActionResult Index(int? page)
        {
            var taskView = QueryService.ExecuteQuery(new GetComplitedTasksByCriteria() { Count = 10, Page = page - 1 ?? 0 });
            return View(taskView);
        }

        public ActionResult History(int taskId, int? page)
        {
            var historiesView = QueryService.ExecuteQuery(new GetHistoriesCompletedTaskByTaskId() { TaskId = taskId, Count = 10, Page = page ?? 0 });
            ViewBag.TaskId = taskId;
            return View(historiesView);
        }

        public ActionResult CreateOrUpdate(int? taskId)
        {
            if (taskId.HasValue)
            {
                var task = QueryService.ExecuteQuery(new GetCompletedTaskById() { Id = taskId.Value });
                var modelForm = Mapper.Map<CompletedTaskViewModel, CompletedTaskForm>(task);
                modelForm.Subjects = QueryService.ExecuteQuery(new GetAllSubjects());
                return View(modelForm);
            }
            else
            {
                var modelForm = new CompletedTaskForm { Subjects = QueryService.ExecuteQuery(new GetAllSubjects()) };
                return View(modelForm);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CreateOrUpdate(CompletedTaskForm model)
        {
            if (ModelState.IsValid)
            {
                if (!model.Id.HasValue)
                {
                    var command = Mapper.Map<CompletedTaskForm, CreateCompletedTaskCommand>(model);
                    var result = CommandBus.Submit(command);
                    if (!result.Success)
                    {
                        this.AddModelErrors(new List<ValidationResult>()
                        {
                            new ValidationResult("Ошибка добавления задачи!")
                        });
                        model.Subjects = QueryService.ExecuteQuery(new GetAllSubjects());
                        return View(model);
                    }
                    Success("Задача успешно добавлена");
                }
                else
                {
                    var command = Mapper.Map<CompletedTaskForm, UpdateCompletedTaskCommand>(model);
                    var result = CommandBus.Submit(command);
                    if (!result.Success)
                    {
                        this.AddModelErrors(new List<ValidationResult>()
                        {
                            new ValidationResult("Ошибка обновления задачи!")
                        });
                        model.Subjects = QueryService.ExecuteQuery(new GetAllSubjects());
                        return View(model);
                    }
                    Success("Задача успешно обновлена");
                }
                return RedirectToAction("Index");
            }

            model.Subjects = QueryService.ExecuteQuery(new GetAllSubjects());
            return View(model);
        }
    }
}