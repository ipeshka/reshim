﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Content;

namespace reshim.web.Areas.Admin.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IQueryService QueryService;
        protected readonly ICommandBus CommandBus;

        public BaseController(ICommandBus commandBus, IQueryService queryService)
        {
            CommandBus = commandBus;
            QueryService = queryService;
        }

        protected const int CountRecord = 10;
        public void Success(string message)
        {
            TempData["success"] = message;
        }

        public void Error(string message)
        {
            TempData["error"] = message;
        }

        [HttpPost]
        public JsonResult Delete(int id)
        {
            var result = CommandBus.Submit(new DeleteContentCommand() { Id = id });
            return new JsonResult() { Data = new { Id = id, Result = result }, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}
