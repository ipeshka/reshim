﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using AutoMapper;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Content;
using reshim.domain.Queryies.Content.News;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.domain.Queryies.Content.Subject;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.News;
using reshim.model.view.ViewModels.Content.SeoArticle;
using reshim.model.view.ViewModels.Content.Subject;
using reshim.model.view.ViewModels.Content.TypeWork;
using reshim.web.Helpers.Handlers;

namespace reshim.web.Areas.Admin.Controllers
{
    public class ContentController : BaseController
    {
        public ContentController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        #region typeWork
        public ActionResult TypeWorks(int? page)
        {
            int p = !page.HasValue ? 1 : page.Value;
            var typeWorks = QueryService.ExecuteQuery(new GetTypeWorks() { Count = CountRecord, Page = p });
            return View(typeWorks);
        }

        public ActionResult CreateOrUpdateTypeWork(int? id)
        {
            if (id.HasValue)
            {
                var typeWork = QueryService.ExecuteQuery(new GetTypeWorkById() { Id = id.Value });
                return View(new TypeWorkFormModel()
                {
                    Id = typeWork.Id,
                    Name = typeWork.Name,
                    ShortName = typeWork.ShortName,
                    TitleSeo = typeWork.TitleSeo,
                    DescriptionSeo = typeWork.DescriptionSeo,
                    KeywordsSeo = typeWork.KeywordsSeo,
                    Text = typeWork.Text,
                    Cost = typeWork.Cost,
                    Period = typeWork.Period,
                    RedirectFromUrl = typeWork.RedirectFromUrl
                });
            }

            return View(new TypeWorkFormModel());
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateTypeWork(TypeWorkFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<TypeWorkFormModel, CreateOrEditTypeWorkCommand>(model);

                var errors =  CommandBus.Validate(new CanCreateOrEditContent()
                {
                    Id = command.Id,
                    RedirectFromUrl = model.RedirectFromUrl
                }).ToList();

                if (!errors.Any())
                {
                    var result = CommandBus.Submit(command);
                    if (result.Success)
                    {
                        RouteUrlModule.ReInit();
                        Success(string.Format("Тип учёбной работы \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлен" : "добавлен"));
                        return RedirectToAction("TypeWorks");
                    }

                    Error(string.Format("Произошла ошибка добавления типа работы!"));
                }

                Error(string.Join("<br>", errors.Select(x => x.Message).ToArray()));
            }

            return View(model);
        }
        #endregion


        #region subjects
        public ActionResult Subjects(int? page)
        {
            int p = !page.HasValue ? 1 : page.Value;
            var subject = QueryService.ExecuteQuery(new GetSubjects() { Count = CountRecord, Page = p });
            return View(subject);
        }

        public ActionResult CreateOrUpdateSubject(int? id)
        {
            if (id.HasValue)
            {
                var subject = QueryService.ExecuteQuery(new GetSubjectById() { Id = id.Value });
                return View(new SubjectFormModel()
                {
                    Id = subject.Id,
                    Name = subject.Name,
                    Preview = subject.Preview,
                    Image = !string.IsNullOrEmpty(subject.Image) ? subject.Image : ""
                });
            }

            return View(new SubjectFormModel() { Image = "" });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateSubject(SubjectFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<SubjectFormModel, CreateOrEditSubjectCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Предмет \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлен" : "добавлен"));
                    return RedirectToAction("Subjects");
                }
                Error(string.Format("Произошла ошибка добавления предмета!"));
            }

            return View(model);
        }
        #endregion


        #region news
        public ActionResult News(int? page)
        {
            int p = !page.HasValue ? 1 : page.Value;
            var news = QueryService.ExecuteQuery(new GetNews() { Count = CountRecord, Page = p });
            return View(news);
        }


        public ActionResult CreateOrUpdateNews(int? id)
        {
            if (id.HasValue)
            {
                var news = QueryService.ExecuteQuery(new GetNewsById() { Id = id.Value });
                return View(new NewsFormModel()
                {
                    Id = news.Id,
                    Name = news.Name,
                    TitleSeo = news.TitleSeo,
                    KeywordsSeo = news.KeywordsSeo,
                    DescriptionSeo = news.DescriptionSeo,
                    Text = news.Text,
                    Preview = news.Preview,
                });
            }

            return View(new NewsFormModel());
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateNews(NewsFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<NewsFormModel, CreateOrEditNewsCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    RouteUrlModule.ReInit();
                    Success(string.Format("Новость \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("News");
                }
                Error(string.Format("Произошла ошибка добавления новости!"));

            }
            return View(model);
        }

        #endregion


        #region seo article
        public ActionResult SeoArticles(int? page)
        {
            int p = !page.HasValue ? 1 : page.Value;
            var seoArticlesView = QueryService.ExecuteQuery(new GetSeoArticles()
            {
                Count = CountRecord,
                Page = p
            });
            return View(seoArticlesView);
        }

        public ActionResult CreateOrUpdateSeoArticle(int? id)
        {
            var typeWorks = QueryService.ExecuteQuery(new GetAllTypeWorks());
            if (id.HasValue)
            {
                var article = QueryService.ExecuteQuery(new GetSeoArticleById() { Id = id.Value });
                return View(new SeoArticleFormModel()
                {
                    DescriptionSeo = article.DescriptionSeo,
                    Id = article.Id,
                    KeywordsSeo = article.KeywordsSeo,
                    Name = article.Name,
                    ShortName = article.ShortName,
                    TitleSeo = article.TitleSeo,
                    Text = article.Text,
                    TypeWorkId = article.TypeWorkId,
                    TypesWorks = typeWorks,
                    RedirectFromUrl = article.RedirectFromUrl
                });
            }

            return View(new SeoArticleFormModel()
            {
                TypesWorks = typeWorks
            });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateSeoArticle(SeoArticleFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<SeoArticleFormModel, CreateOrEditSeoArticleCommand>(model);

                var errors = CommandBus.Validate(new CanCreateOrEditContent()
                {
                    Id = command.Id,
                    RedirectFromUrl = model.RedirectFromUrl
                }).ToList();

                if (!errors.Any())
                {
                    var result = CommandBus.Submit(command);
                    if (result.Success)
                    {
                        RouteUrlModule.ReInit();
                        Success(string.Format("Сео статья \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                        return RedirectToAction("SeoArticles");
                    }
                    Error(string.Format("Произошла ошибка добавления сео статьи!"));
                }

                Error(string.Join("<br>", errors.Select(x => x.Message).ToArray()));

            }

            var typeWorks = QueryService.ExecuteQuery(new GetAllTypeWorks());
            model.TypesWorks = typeWorks;
            return View(model);
        }

        #endregion

    }
}
