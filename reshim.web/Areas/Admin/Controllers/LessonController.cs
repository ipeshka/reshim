﻿using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Content;
using reshim.domain.Queryies.Content.Lesson;
using reshim.domain.StrategiesFetch;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.web.Areas.Admin.Controllers
{
    public class LessonController : BaseController
    {
        public LessonController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        #region category
        public ActionResult Categories(int? page, int? parentId, int? categoryId)
        {
            int p = page ?? 1;
            if (!parentId.HasValue)
            {
                var categories = QueryService.ExecuteQuery(new GetTopCategoriesLessons()
                {
                    Count = CountRecord,
                    Page = p
                });
                return View(new CategoriesLessonsViewModel()
                {
                    Categories = categories,
                    CategoryId = categoryId
                });
            }
            else
            {
                var categories = QueryService.ExecuteQuery(new GetChildrenCategoriesLessons()
                {
                    Count = CountRecord,
                    Page = p,
                    ParentId = parentId.Value
                });

                var parentCategory = QueryService.ExecuteQuery(new GetCategoryLessonsById() { Id = parentId.Value }); 

                return View(new CategoriesLessonsViewModel()
                {
                    Categories = categories,
                    CategoryId = categoryId,
                    ParentId = parentId,
                    Parent = parentCategory
                });
            }
        }


        [ChildActionOnly]
        public ActionResult CreateOrUpdateCategoryPartial(int? categoryId, int? parentId)
        {
            if (categoryId.HasValue)
            {
                var category = QueryService.ExecuteQuery(new GetCategoryLessonsById()
                {
                    Id = categoryId.Value,
                    IncludeProperties = new CategoryLessonWithParentFetchStrategy()
                });
                return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateCategory.cshtml", new CategoryLessonsFormModel()
                {
                    Id = category.Id,
                    Name = category.Name,
                    ParentId = category.ParentId,
                    Parent = category.Parent
                });
            }

            if (parentId.HasValue)
            {
                var parentCategory = QueryService.ExecuteQuery(new GetCategoryLessonsById()
                {
                    Id = parentId.Value,
                });

                return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateCategory.cshtml", new CategoryLessonsFormModel()
                {
                    ParentId = parentId,
                    Parent = parentCategory
                });
            }

            return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateCategory.cshtml", new CategoryLessonsFormModel());

        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateCategory(CategoryLessonsFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<CategoryLessonsFormModel, CreateOrEditCategoryLessonCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Категория для уроков \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("Categories", new { parentId = command.ParentId });
                }
                Error(string.Format("Произошла ошибка добавления категории для статей!"));

            }

            return View(model);
        }
        #endregion

        #region name lesson
        public ActionResult NamesLessons(int? page, int? nameLessonId)
        {
            int p = page ?? 1;
            var namesLessons = QueryService.ExecuteQuery(new GetNamesLessons()
            {
                Count = CountRecord,
                Page = p
            });



            return View(new NamesLessonsViewModel()
            {
                NameLessonId = nameLessonId,
                NamesLessons = namesLessons
            });
        }


        [ChildActionOnly]
        public ActionResult CreateOrUpdateNameLessonPartial(int? nameLessonId)
        {
            var categories = QueryService.ExecuteQuery(new GetAllTopCategoriesLessons());

            var flatCategories = QueryService.ExecuteQuery(new GetFlatLessonCategories()
            {
                Categories = categories
            });

            if (nameLessonId.HasValue)
            {
                var nameLessons = QueryService.ExecuteQuery(new GetNameLessonsById() { Id = nameLessonId.Value });
                return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateNameLesson.cshtml", new NameLessonsFormModel()
                {
                    Id = nameLessons.Id,
                    Name = nameLessons.Name,
                    CategoryId = nameLessons.CategoryId,
                    Categories = flatCategories
                });
            }

            return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateNameLesson.cshtml", new NameLessonsFormModel()
            {
                Categories = flatCategories
            });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateNameLesson(NameLessonsFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<NameLessonsFormModel, CreateOrEditNameLessonCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Название цикла уроков \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("NamesLessons");
                }
                Error(string.Format("Произошла ошибка добавления цикла уроков!"));

            }

            return View(model);
        }
        #endregion

        #region chapters
        public ActionResult Chapters(int nameLessonId, int? page, int? chapterId)
        {
            int p = page ?? 1;
            var chapters = QueryService.ExecuteQuery(new GetChapters()
            {
                Count = CountRecord,
                Page = p,
                NameLessonId = nameLessonId
            });

            var nameLesson = QueryService.ExecuteQuery(new GetNameLessonsById(){ Id = nameLessonId });

            return View(new ChaptersViewModel()
            {
                ChapterId = chapterId,
                NameLessonId = nameLessonId,
                Chapters = chapters,
                NameLesson = nameLesson
            });
        }


        [ChildActionOnly]
        public ActionResult CreateOrUpdateChapterPartial(int nameLessonId, int? chapterId)
        {
            if (chapterId.HasValue)
            {
                var chapter = QueryService.ExecuteQuery(new GetChapterById() { Id = chapterId.Value });
                return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateChapter.cshtml", new ChapterFormModel()
                {
                    Id = chapter.Id,
                    Name = chapter.Name,
                    NameLessonId = chapter.NameLessonId
                });
            }

            return View("~/Areas/Admin/Views/Lesson/_CreateOrUpdateChapter.cshtml", new ChapterFormModel() { NameLessonId = nameLessonId });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateChapter(ChapterFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<ChapterFormModel, CreateOrEditChapterCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Название главы \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("Chapters", new { nameLessonId = model.NameLessonId });
                }
                Error(string.Format("Произошла ошибка добавления главы для уроков!"));

            }

            return View(model);
        }
        #endregion

        #region lessons

        public ActionResult Lessons(int? page, int? nameLessonId, int? chapterId)
        {
            int p = page ?? 1;
            var nameLessons = QueryService.ExecuteQuery(new GetAllNamesLessons()).ToList();
            if (!nameLessonId.HasValue)
            {
                nameLessonId = nameLessons.First().Id;
            }

            var chapters = QueryService.ExecuteQuery(new GetAllChaptersByNameLessonId() { NameLessonId = nameLessonId.Value });

            var lessons = QueryService.ExecuteQuery(new GetLessons()
            {
                Count = CountRecord,
                Page = p,
                NameLessonId = nameLessonId,
                ChapterId = chapterId
            });

            return View(new LessonsViewModel()
            {
                Lessons = lessons,
                NamesLessons = nameLessons,
                NameLessonId = nameLessonId,
                Chapters = chapters,
                ChapterId = chapterId
            });
        }


        public ActionResult CreateOrUpdateLesson(int? id)
        {
            var namesLessons = QueryService.ExecuteQuery(new GetAllNamesLessons()).ToList();
            var nameLessonsId = namesLessons.First().Id;

            var chapters = QueryService.ExecuteQuery(new GetAllChaptersByNameLessonId() { NameLessonId = nameLessonsId });

            if (id.HasValue)
            {
                var lesson = QueryService.ExecuteQuery(new GetLessonById() { Id = id.Value });
                return View(new LessonFormModel()
                {
                    DescriptionSeo = lesson.DescriptionSeo,
                    Id = lesson.Id,
                    KeywordsSeo = lesson.KeywordsSeo,
                    Name = lesson.Name,
                    TitleSeo = lesson.TitleSeo,
                    Text = lesson.Text,
                    Preview = lesson.Preview,
                    Image = !string.IsNullOrEmpty(lesson.Image) ? lesson.Image : "",
                    NamesLessons = namesLessons,
                    NameLessonId = lesson.NameLessonId,
                    Chapters = chapters,
                    ChapterId = lesson.ChapterId
                });
            }

            return View(new LessonFormModel()
            {
                Image = "",
                Chapters = chapters,
                NamesLessons = namesLessons,
            });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdate(LessonFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<LessonFormModel, CreateOrEditLessonCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Урок \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлен" : "добавлен"));
                    return RedirectToAction("Lessons", new { nameLessonId  = command.NameLessonId, chapterId = command.ChapterId });
                }
                Error(string.Format("Произошла ошибка добавления урока!"));

            }

            return View(model);
        }

        #endregion
    }
}
