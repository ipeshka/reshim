﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.Task;
using reshim.domain.StrategiesFetch;
using reshim.model.Entities;
using reshim.web.Helpers;

namespace reshim.web.Areas.Admin.Controllers
{
    [Access(new[] { UserRoles.Admin })]
    public class TaskController : BaseController
    {
        public TaskController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        public ActionResult DetailTask(int id)
        {
            var taskView = QueryService.ExecuteQuery(new GetTaskById()
            {
                Id = id,
                IncludeProperties = new TaskFetchStrategyNumber1()
            });
            return View(taskView);
        }
    }
}
