﻿using System.Web.Mvc;
using AutoMapper;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Content;
using reshim.domain.Queryies.Content.Article;
using reshim.domain.StrategiesFetch;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.web.Areas.Admin.Controllers
{
    public class ArticleController : BaseController
    {
        public ArticleController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        #region category
        public ActionResult Categories(int? page, int? parentId, int? categoryId)
        {
            int p = page ?? 1;
            if (!parentId.HasValue)
            {
                var categories = QueryService.ExecuteQuery(new GetTopArticleCategories()
                {
                    Count = CountRecord,
                    Page = p
                });

                return View(new CategoriesArticleViewModel()
                {
                    Categories = categories,
                    CategoryId = categoryId
                });
            }
            else
            {
                var categories = QueryService.ExecuteQuery(new GetChildrenArticleCategories()
                {
                    Count = CountRecord,
                    Page = p,
                    ParentId = parentId.Value
                });

                var parentCategory = QueryService.ExecuteQuery(new GetArticleCategoryById()
                {
                    Id = parentId.Value
                });

                return View(new CategoriesArticleViewModel()
                {
                    Categories = categories,
                    CategoryId = categoryId,
                    ParentId = parentId,
                    Parent = parentCategory
                });
            }
        }


        [ChildActionOnly]
        public ActionResult CreateOrUpdateCategoryPartial(int? categoryId, int? parentId)
        {
            if (categoryId.HasValue)
            {
                var category = QueryService.ExecuteQuery(new GetArticleCategoryById()
                {
                    Id = categoryId.Value,
                    IncludeProperties = new CategoryArticleWithParentFetchStrategy()
                });

                return View("~/Areas/Admin/Views/Article/_CreateOrUpdateCategory.cshtml", new CategoryArticleFormModel()
                {
                    Id = category.Id,
                    Name = category.Name,
                    ParentId = category.ParentId,
                    Parent = category.Parent
                });
            }

            if (parentId.HasValue)
            {
                var parentCategoryArticle = QueryService.ExecuteQuery(new GetArticleCategoryById()
                {
                    Id = parentId.Value,
                });

                return View("~/Areas/Admin/Views/Article/_CreateOrUpdateCategory.cshtml", new CategoryArticleFormModel()
                {
                    ParentId = parentId,
                    Parent = parentCategoryArticle
                });
            }


            return View("~/Areas/Admin/Views/Article/_CreateOrUpdateCategory.cshtml", new CategoryArticleFormModel());
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateCategory(CategoryArticleFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<CategoryArticleFormModel, CreateOrEditCategoryArticleCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Категория для статей \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("Categories", new { parentId = command.ParentId });
                }
                Error(string.Format("Произошла ошибка добавления категории для статей!"));

            }

            return View(model);
        }
        #endregion

        #region articles
        public ActionResult Articles(int? page)
        {
            int p = page ?? 1;
            var articlesView = QueryService.ExecuteQuery(new GetArticles()
            {
                Count = CountRecord,
                Page = p
            });
            return View(articlesView);
        }

        public ActionResult CreateOrUpdateArticle(int? id)
        {
            var categories = QueryService.ExecuteQuery(new GetAllTopArticleCategories());

            var flatCategories = QueryService.ExecuteQuery(new GetFlatArticleCategories()
            {
                Categories = categories
            });

            if (id.HasValue)
            {
                var article = QueryService.ExecuteQuery(new GetArticleById() { Id = id.Value });
                return View(new ArticleFormModel()
                {
                    Id = article.Id,
                    Name = article.Name,
                    TitleSeo = article.TitleSeo,
                    DescriptionSeo = article.DescriptionSeo,
                    KeywordsSeo = article.KeywordsSeo,
                    Text = article.Text,
                    Preview = article.Preview,
                    Image = !string.IsNullOrEmpty(article.Image) ? article.Image : "",
                    CategoryId = article.CategoryId,
                    Categories = flatCategories
                });
            }


            return View(new ArticleFormModel()
            {
                Image = "",
                Categories = flatCategories
            });
        }

        [ValidateInput(false)]
        [HttpPost]
        public ActionResult CreateOrUpdateArticle(ArticleFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<ArticleFormModel, CreateOrEditArticleCommand>(model);

                var result = CommandBus.Submit(command);
                if (result.Success)
                {
                    Success(string.Format("Статья \"{0}\" успешно {1}!", model.Name, model.Id.HasValue ? "обновлена" : "добавлена"));
                    return RedirectToAction("Articles");
                }
                Error(string.Format("Произошла ошибка добавления статьи!"));

            }

            var categories = QueryService.ExecuteQuery(new GetAllTopArticleCategories());

            var flatCategories = QueryService.ExecuteQuery(new GetFlatArticleCategories()
            {
                Categories = categories
            });

            model.Categories = flatCategories;
            return View(model);
        }
        #endregion

    }
}