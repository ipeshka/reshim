﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.Task;
using reshim.domain.StrategiesFetch;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content;
using reshim.web.Helpers;
using reshim.web.Helpers.Binders;

namespace reshim.web.Areas.Admin.Controllers
{
    [Access(new[] { UserRoles.Admin })]
    public class HomeController : BaseController
    {
        public HomeController(ICommandBus commandBus, IQueryService queryService)
            : base(commandBus, queryService)
        {
        }

        public ActionResult Index([ModelBinder(typeof(TaskWithCriteriaAdminBinder))]GetTasksByCriteria model)
        {
            model.Count = CountRecord;
            model.IncludeProperties = new TaskFetchStrategyNumber3();
            var taskView = QueryService.ExecuteQuery(model);
            return View(taskView);
        }

        [ChildActionOnly]
        public ActionResult StatisticsTasks()
        {
            var statisticsTasksView = QueryService.ExecuteQuery(new GetStatisticCountTask());
            return PartialView("~/Areas/Admin/Views/Shared/Partial/Task/_StatisticsTasks.cshtml", statisticsTasksView);
        }

        [ChildActionOnly]
        public ActionResult Sidebar()
        {
            var items = new List<MenuItemModel>
            {
                new MenuItemModel { Text = "Главная", Action = "Index", Controller = "Home", Selected = false },
                new MenuItemModel { Text = "Контент", Action = "", Controller = "", Selected = false, SubMenu = new List<MenuItemModel>() {
                    new MenuItemModel { Text = "Новости", Action = "News", Controller = "Content", Selected = false },
                    new MenuItemModel { Text = "Предметы", Action = "Subjects", Controller = "Content", Selected = false },
                    new MenuItemModel { Text = "Тип работы", Action = "TypeWorks", Controller = "Content", Selected = false },
                    new MenuItemModel { Text = "Сео статьи", Action = "SeoArticles", Controller = "Content", Selected = false }
                    }
                },
                new MenuItemModel { Text = "Статьи", Action = "", Controller = "", Selected = false, SubMenu = new List<MenuItemModel>() {
                    new MenuItemModel { Text = "Категории", Action = "Categories", Controller = "Article", Selected = false },
                    new MenuItemModel { Text = "Статьи", Action = "Articles", Controller = "Article", Selected = false }
                    }
                },
                new MenuItemModel { Text = "Уроки", Action = "", Controller = "", Selected = false, SubMenu = new List<MenuItemModel>() {
                    new MenuItemModel { Text = "Категории", Action = "Categories", Controller = "Lesson", Selected = false },
                    new MenuItemModel { Text = "Серии уроков и параграфы", Action = "NamesLessons", Controller = "Lesson", Selected = false },
                    new MenuItemModel { Text = "Уроки", Action = "Lessons", Controller = "Lesson", Selected = false }
                    }
                },
                new MenuItemModel { Text = "Банк задач", Action = "", Controller = "", Selected = false, SubMenu = new List<MenuItemModel>() {
                    new MenuItemModel { Text = "Решенные задачи", Action = "Index", Controller = "CompletedTask", Selected = false }
                    }
                }
            };

            string controller = ControllerContext.ParentActionViewContext.RouteData.Values["controller"].ToString();

            foreach (var item in items)
            {
                if (item.Controller.ToLower() == controller.ToLower()) item.Selected = true;

                foreach (var subMenu in item.SubMenu)
                {
                    if (subMenu.Controller.ToLower() == controller.ToLower())
                    {
                        subMenu.Selected = true;
                        item.Selected = true;
                    }
                }
            }

            return PartialView("~/Areas/Admin/Views/Shared/Partial/_Sidebar.cshtml", items);
        }

    }
}



