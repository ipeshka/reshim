﻿
// estimate task
$("form[class='estimate-task']").validate({
    rules: {
        value: {
            required: true,
            digits: true
        }
    },

    messages: {
        value: {
            required: "Введите стоимость",
            digits: "Введите числа"
        }
    },

    invalidHandler: function (event, validator) {
        $.each(validator.errorList, function (index) {
            var wrap = $(this.element).parent(".input-control").addClass("error");
            wrap.attr("original-title", this.message);
        });
    },

    unhighlight: function (element, errorClass, validClass) {
        var el = $(element).parent(".input-control");
        el.removeClass(errorClass);
        el.removeAttr("original-title");
        el.children("label.error").remove();
    },

    submitHandler: function (form) {

        var value = $("input[name='value']").val();
        var taskId = $("input[name='taskId']").val();
        var comment = $("textarea[name='comment']").val();
        $.Loader({ overlay: true });

        var request = $.ajax({
            url: "/api/task/employee/estimate",
            type: "POST",
            data: { TaskId: taskId, Value: value, Comment: comment }
        });

        request.done(function (data) {             // change view             var requestChangeView = $.ajax({
                url: "/employee/task/partialdetailtask",
                type: "GET",
                data: { taskId: taskId }
            });

            requestChangeView.done(function (view) {
                $.Loader.close();
                $("#WrapperDetailTask").html(view);
            });


            // popun
            var infoPlace = $.InfoPlace({
                text: "Вы успешно оценили заказ!",
                image: RESHIM.ICO.Checkmark2ICO(),
                type: 'success'
            });

            $.Dialog("estimate-task-complite", {
                caption: {
                    title: "Успешная оценка заказа",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

            

        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            $.Loader.close();

            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

            $.Dialog("estimate-task-error", {
                caption: {
                    title: "Ошибка оценки задачи",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });

        });
    }
});

$("form[class='estimate-task']").find(".input-control").tipsy({ gravity: 's' });
$('[data-role=infoPlace]').infoPlace();
