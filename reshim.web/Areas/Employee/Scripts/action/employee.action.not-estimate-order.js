﻿$("#Unsubscribe").click(function (e) {
    var taskId = $("input[name='taskId']").val();
    $.Loader({ overlay: true });

    var request = $.ajax({
        url: "/api/task/employee/unsubscribe/" + taskId,
        type: "POST"
    });

    request.done(function (data) {
    	// change view         var requestChangeView = $.ajax({
            url: "/employee/task/partialdetailtask",
        	type: "GET",
        	data: { taskId: taskId }
        });

        requestChangeView.done(function (view) {
            $.Loader.close();
            $("#WrapperDetailTask").html(view);
        });

        // popun
        var infoPlace = $.InfoPlace({
            text: "Вы успешно отказались от заявки!",
            image: RESHIM.ICO.Checkmark2ICO(),
            type: 'success'
        });

        $.Dialog("unsubscribe-task-complite", {
            caption: {
                title: "Вы успешно отказались от заявки.",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml()
        });

        
    });

    request.error(function (jqXHR, textStatus, errorThrown) {
        $.Loader.close();

        var errors = "";
        $.each(jqXHR.responseJSON, function (index, value) {
            errors += value.Message + "<br/>";
        });

        var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

        $.Dialog("unsubscribe-task-error", {
            caption: {
                title: "Ошибка отказа от заявки",
                sysButtons: {
                    btnClose: true
                }
            },
            overlay: true,
            padding: 10,
            width: WIDTH_POPUN,
            content: infoPlace.toHtml()
        });

    });
});

$('[data-role=infoPlace]').infoPlace();