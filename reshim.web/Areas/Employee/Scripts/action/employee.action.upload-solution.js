﻿$(document).ready(function () {
    var MAX_SIZE_FILE = 15000000; // 15 Mb

    var file = $('#FileUpload');
    var selectFile = $("#SelectFile");

    var pb = $('#ProgressUploadTask').progressbar();

    file.attr("data-url", "/api/file/");

    selectFile.click(function (e) {
        e.preventDefault();
        file.focus().click();
    });

    // file uploader
    file.fileupload({
        dataType: 'json',
        formData: { type: 'Solution' },
        add: function (e, data) {
            if (data.files[0].size > MAX_SIZE_FILE) {
                alert("Размер файла больше 15 метров");
                return;
            } else {
                data.submit();
            }
        },

        done: function (e, data) {
            $("input[name='namefile']").val(data.result.Name);
            $("input[name='namefile']").blur();
            $("input[name='idfile']").val(data.result.LastId);
        },

        error: function (e, data) {
            
            var errors = "";
            $.each(e.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });
            
            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });
            
            $.Dialog("uploadfile-error", {
                caption: {
                    title: "Ошибка файла",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        },

        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            pb.progressbar('value', progress);
        }
    });


    $("form[class='solution']").validate({
        ignore: "",
        rules: {
            idfile: {
                required: true
            }
        },

        messages: {
            idfile: {
                required: "Выберите файл"
            }
        },

        invalidHandler: function (event, validator) {
            $.each(validator.errorList, function (index) {
                var wrap = $(this.element).parent(".input-control").addClass("error");
                wrap.attr("original-title", this.message);
            });
        },

        unhighlight: function (element, errorClass, validClass) {
            var el = $(element).parent(".input-control");
            el.removeClass(errorClass);
            el.removeAttr("original-title");
            el.children("label.error").remove();
        },


        submitHandler: function (form) {
            var idFile = $("input[name='idfile']").val();
            var idTask = $("input[name='id']").val();

            $.Loader({ overlay: true });

            var request = $.ajax({
                url: "/api/task/employee/uploadsolution",
                type: "POST",
                data: { FileId: idFile, TaskId: idTask }
            });


            request.done(function (data) {
                // change view                 var requestChangeView = $.ajax({
                    url: "/employee/task/partialdetailtask",
                    type: "GET",
                    data: { taskId: idTask }
                });

                requestChangeView.done(function (view) {
                    $.Loader.close();
                    $("#WrapperDetailTask").html(view);
                });

                var infoPlace = $.InfoPlace({ text: "Ваше решение успешно загружено.", image: RESHIM.ICO.Checkmark2ICO(), type: 'success' });

                $.Dialog("upload-solution-done", {
                    caption: {
                        title: "Решение загружено",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

                
            });

            request.error(function (jqXHR, textStatus, errorThrown) {
               
                var errors = "";
                $.each(jqXHR.responseJSON, function (index, value) {
                    errors += value.Message + "<br/>";
                });

                var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });


                $.Dialog("upload-solution-error", {
                    caption: {
                        title: "Ошибка загрузки решения",
                        sysButtons: {
                            btnClose: true
                        }
                    },
                    overlay: true,
                    padding: 10,
                    width: WIDTH_POPUN,
                    content: infoPlace.toHtml()
                });

                $.Loader.close();
            });


        }
    });

    $("form[class='solution']").find(".input-control").tipsy({ gravity: 's' });

});