﻿$(document).ready(function () {
    $("td.task").click(function (e) {
        var that = $(this);
        var id = that.data('taskId');
        e.preventDefault();

        var request = $.ajax({
            url: "/api/task/detail/" + id,
            type: "GET"
        });

        request.done(function (task) {
            $.Dialog("detail-order", {
                caption: {
                    title: "Подробная информация о заказе №" + task.Id,
                    sysButtons: {
                        btnClose: false
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: _.template(TEMPLATES.find("#detail-order").wrapTemplate(), { task: task })
            });
        });

        request.error(function (jqXHR, textStatus, errorThrown) {
            
            var errors = "";
            $.each(jqXHR.responseJSON, function (index, value) {
                errors += value.Message + "<br/>";
            });

            var infoPlace = $.InfoPlace({ text: errors, image: RESHIM.ICO.CancelCircleICO(), type: 'error', imageClose: RESHIM.ICO.CancelCircleICO() });

            $.Dialog("detail-order-error", {
                caption: {
                    title: "Серверная ошибка",
                    sysButtons: {
                        btnClose: true
                    }
                },
                overlay: true,
                padding: 10,
                width: WIDTH_POPUN,
                content: infoPlace.toHtml()
            });
        });

    });

});