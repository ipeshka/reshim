﻿using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Queryies.User;
using reshim.web.Controllers;

namespace reshim.web.Areas.Employee.Controllers
{
    public class UserController : BaseController
    {
        private readonly IQueryService _queryService;

        public UserController(IQueryService queryService)
        {
            this._queryService = queryService;
        }

        public ActionResult PersonalCabinet()
        {
            var model = _queryService.ExecuteQuery(new GetStatisticsTaskUser()
            {
                UserId = User.UserId
            });
            return View(model);
        }

    }
}
