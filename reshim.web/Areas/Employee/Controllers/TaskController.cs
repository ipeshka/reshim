﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web.Mvc;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.domain.Queryies.Task;
using reshim.domain.StrategiesFetch;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;
using reshim.web.Controllers;
using reshim.web.Helpers;
using reshim.web.Helpers.Binders;

namespace reshim.web.Areas.Employee.Controllers
{
    [Access(new[] { UserRoles.Employee })]
    public class TaskController : BaseController
    {
        private readonly IQueryService _queryService;


        public TaskController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        public ActionResult Index([ModelBinder(typeof(TaskWithCriteriaBinder))]GetTasksByCriteria model)
        {
            model.Count = CountRecord;
            model.IncludeProperties = new TaskFetchStrategyNumber3();
            var taskView = _queryService.ExecuteQuery(model);
            return View(taskView);
        }


        [AllowAnonymous]
        public ActionResult Detail(int id)
        {
            return View(id);
        }

        #region partial

        private const string PathViews = "~/Areas/Employee/Views/Task/Partials/State/";

        private readonly Dictionary<StatusTask, string> _templatesIsEstimatedCurUser = new Dictionary<StatusTask, string>
        {
            { StatusTask.New, PathViews +  "New/_Index.cshtml"},
            { StatusTask.Rating, PathViews +  "Rating/_Estimated.cshtml"},
            { StatusTask.Agreement, PathViews +  "Agreement/_Estimated.cshtml"},
            { StatusTask.CompletedNotPay, PathViews +  "CompletedNotPay/_Estimated.cshtml"},
            { StatusTask.Completed, PathViews +  "Completed/_Estimated.cshtml"}
        };

        private readonly Dictionary<StatusTask, string> _templatesNotEstimate = new Dictionary<StatusTask, string>
        {
            { StatusTask.New, PathViews +  "New/_Index.cshtml"},
            { StatusTask.Rating, PathViews +  "Rating/_NotEstimate.cshtml"},
            { StatusTask.Agreement, PathViews +  "Agreement/_NotEstimate.cshtml"},
            { StatusTask.CompletedNotPay, PathViews +  "CompletedNotPay/_NotEstimate.cshtml"},
            { StatusTask.Completed, PathViews +  "Completed/_NotEstimate.cshtml"}
        };

        public ActionResult PartialDetailTask(int taskId)
        {
            var taskView = _queryService.ExecuteQuery(new GetTaskById()
            {
                Id = taskId,
                IncludeProperties = new TaskFetchStrategyNumber1()
            });
            taskView.Statuses = EnumEx.GetKeyValue<StatusTask, NameAttribute, NameAttribute, TypeAttribute>(typeof(IsDisplayAttribute));

            if (taskView.Status == StatusTask.New)
            {
                return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesNotEstimate[taskView.Status],
                        Task = taskView
                    });
            }
            else if (taskView.Status == StatusTask.Rating)
            {
                // if current user subscribe task
                if (taskView.Subscribers.Any(x => x.UserId == User.UserId))
                {
                    var offer = taskView.Subscribers.FirstOrDefault(s => s.UserId == User.UserId);
                    if (offer != null) offer.IsCurrentUser = true;
                    taskView.Subscribers = taskView.Subscribers.OrderByDescending(x => x.IsCurrentUser).ToList();
                    return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                        new RenderPartialTabsDetailTaskViewModel()
                        {
                            Layout = _templatesIsEstimatedCurUser[taskView.Status],
                            Task = taskView
                        });
                }

                // if current user not subscribe task
                return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesNotEstimate[taskView.Status],
                        Task = taskView
                    });
            }
            else if (taskView.Status == StatusTask.Agreement)
            {
                // if current user selected employee
                if (taskView.Decided.UserId == User.UserId)
                {
                    return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                        new RenderPartialTabsDetailTaskViewModel()
                        {
                            Layout = _templatesIsEstimatedCurUser[taskView.Status],
                            Task = taskView
                        });
                }

                // if current user not selected employee
                return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesNotEstimate[taskView.Status],
                        Task = taskView
                    });
            }
            else if (taskView.Status == StatusTask.CompletedNotPay)
            {
                // if current user selected employee
                if (taskView.Decided.UserId == User.UserId)
                {
                    return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                        new RenderPartialTabsDetailTaskViewModel()
                        {
                            Layout = _templatesIsEstimatedCurUser[taskView.Status],
                            Task = taskView
                        });
                }

                // if current user not selected employee
                return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesNotEstimate[taskView.Status],
                        Task = taskView
                    });
            }
            else
            {
                // if current user selected employee
                if (taskView.Decided.UserId == User.UserId)
                {
                    return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                        new RenderPartialTabsDetailTaskViewModel()
                        {
                            Layout = _templatesIsEstimatedCurUser[taskView.Status],
                            Task = taskView
                        });
                }

                // if current user not selected employee
                return PartialView("~/Areas/Employee/Views/Task/Partials/_PartialDetailTask.cshtml",
                    new RenderPartialTabsDetailTaskViewModel()
                    {
                        Layout = _templatesNotEstimate[taskView.Status],
                        Task = taskView
                    });
            }

        }

        #endregion
    }

}
