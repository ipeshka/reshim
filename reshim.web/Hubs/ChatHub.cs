﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.Chat;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Database;
using reshim.model.view.ViewModels.Chat;
using reshim.web.core.Extensions;
using reshim.web.Hubs.Model;
using Task = System.Threading.Tasks.Task;

namespace reshim.web.Hubs
{
    [HubName("chat")]
    [Authorize]
    public class ChatHub : Hub
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly ILifetimeScope _hubLifetimeScope;
        private readonly ISql _sql;
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public ChatHub(ILifetimeScope hubLifetimeScope)
        {
            _hubLifetimeScope = hubLifetimeScope.BeginLifetimeScope();

            _sql = _hubLifetimeScope.Resolve<ISql>();
            _commandBus = _hubLifetimeScope.Resolve<ICommandBus>();
            _queryService = _hubLifetimeScope.Resolve<IQueryService>();
        }

        public ResultSendMessageViewModel Send(int dialogId, string message)
        {
            var userContext = Context.User.GetReshimUser();

            var resultMessage = _commandBus.Submit(new CreateMessageCommand()
            {
                Content = message,
                DialogId = dialogId,
                SenderId = userContext.UserId
            });

            var messageView = _queryService.ExecuteQuery(new GetMessageById() { MessageId = resultMessage.LastId });
            var connetctions = _queryService.ExecuteQuery(new GetConectionsByUserId() { UserId = messageView.RecipientId });

            foreach (var con in connetctions)
            {
                Clients.Client(con).messageIn(messageView);
            }

            return new ResultSendMessageViewModel() { Success = true, Message = messageView };
        }

        public ResultReadMessageViewModel ReadMessage(int id)
        {
            // TODO: поместить в команду
            var userContext = Context.User.GetReshimUser();
            _sql.ExecuteCommand("UPDATE dbo.Messages SET IsNew = @p0 WHERE Id = @p1", false, id);
            var result = new ResultReadMessageViewModel()
            {
                Success = true,
                CountMessages = _sql.ExecuteQuery<int>("SELECT COUNT(*) FROM dbo.Messages WHERE IsNew = @p0 AND RecipientId = @p1", true, userContext.UserId).First()
            };

            return result;
        }

        public IEnumerable<MessageViewModel> GetNewMessages()
        {
            var userContext = Context.User.GetReshimUser();
            var messagesView = _queryService.ExecuteQuery(new GetNewMessagesByUserId() { UserId = userContext.UserId });
            return messagesView;
        }

        public override Task OnConnected()
        {
            // TODO: поместить в команду
            var userContext = Context.User.GetReshimUser();

            // триггером обновляеться поле LastUpdateLogin в таблице Users
            _sql.ExecuteCommand("INSERT INTO dbo.Connections (ConnectionId, Connected, UserAgent, UserId, Date) VALUES (@p0, @p1, @p2, @p3, @p4)",
                Context.ConnectionId, true, Context.Request.Headers["User-Agent"], userContext.UserId, DateTime.Now);

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            // TODO: поместить в команду
            // триггером обновляеться поле LastUpdateLogin в таблице Users
            _sql.ExecuteCommand("UPDATE dbo.Connections SET Connected = @p0 WHERE ConnectionId = @p1", false, Context.ConnectionId);
            Logger.Trace("OnDisconnected - {0}", Context.ConnectionId);
            return base.OnDisconnected(stopCalled);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && _hubLifetimeScope != null)
                _hubLifetimeScope.Dispose();

            base.Dispose(disposing);
        }
    }
}