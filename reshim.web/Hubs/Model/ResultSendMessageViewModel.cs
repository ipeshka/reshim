﻿using reshim.model.view.ViewModels.Chat;

namespace reshim.web.Hubs.Model
{
    public class ResultSendMessageViewModel
    {
        public MessageViewModel Message { get; set; }
        public bool Success { get; set; }
    }
}