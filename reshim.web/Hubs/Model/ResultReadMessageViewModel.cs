﻿namespace reshim.web.Hubs.Model
{
    public class ResultReadMessageViewModel
    {
        public bool Success { get; set; }
        public int CountMessages { get; set; }
    }
}