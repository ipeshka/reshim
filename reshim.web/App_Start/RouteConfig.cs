﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using LowercaseRoutesMVC;

namespace reshim.web.App_Start
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("elmah.axd");

            routes.MapRoute(
                "sitemap.xml",
                "sitemap.xml",
                new { controller = "Seo", action = "Sitemap" },
                new[] { "reshim.web.Controllers" }
            );

            // home
            var homeAction = new List<string>() { "Support", "RulesOrder", "Price", "Partnership", "Guaranty", "Faqs" };

            foreach (var act in homeAction)
            {
                routes.MapRouteLowercase(
                    "home-" + act,
                    act,
                    new { controller = "Home", action = act },
                    new[] { "reshim.web.Controllers" }
                );
            }

            routes.MapRouteLowercase(
                "News",
                "News/{alias}",
                new { controller = "content", action = "news", alias = UrlParameter.Optional },
                new[] { "reshim.web.Controllers" }
            );

            routes.MapRouteLowercase(
                "Default",
                "{controller}/{action}/{id}",
                new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                new[] { "reshim.web.Controllers" }
            );

            routes.MapRouteLowercase(
                "ProviderName",
                "{controller}/{action}/{providerName}",
                new { controller = "Account", action = "Login", providerName = UrlParameter.Optional },
                new[] { "reshim.web.Controllers" }
            );
        }

    }
}