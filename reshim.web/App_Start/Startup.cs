﻿using Hangfire;
using Microsoft.Owin;
using Owin;
using reshim.web.Helpers.Jobs;
using Startup = reshim.web.App_Start.Startup;

[assembly: OwinStartup(typeof(Startup))]
namespace reshim.web.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();

            // hangfire
            GlobalConfiguration.Configuration.UseSqlServerStorage("ReshimDataContex");
            app.UseHangfireDashboard();
            app.UseHangfireServer();
            RecurringJob.AddOrUpdate<ChangeStatusTasksJob>(x => x.Start(), Cron.Hourly);
        }
    }
}