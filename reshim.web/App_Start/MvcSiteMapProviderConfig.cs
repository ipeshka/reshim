using System.Web.Hosting;
using MvcSiteMapProvider.Loader;
using MvcSiteMapProvider.Xml;
using reshim.web.DI;

namespace reshim.web
{
    internal class MvcSiteMapProviderConfig
    {
        public static void Register(IDependencyInjectionContainer container)
        {
            MvcSiteMapProvider.SiteMaps.Loader = container.GetInstance<ISiteMapLoader>();

            var validator = container.GetInstance<ISiteMapXmlValidator>();
            validator.ValidateXml(HostingEnvironment.MapPath("~/Mvc.sitemap"));
        }
    }
}
