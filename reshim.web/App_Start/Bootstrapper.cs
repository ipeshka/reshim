﻿using System.Reflection;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using Microsoft.AspNet.SignalR;
using reshim.web.Hubs;
using reshim.web.DI.Autofac.Modules;
using reshim.web.DI.Autofac;
using reshim.web.Helpers.Jobs;
using Hangfire;
using MassTransit;
using reshim.crosscutting.inversionofcontrol.Modules;
using MvcModule = reshim.web.DI.Autofac.Modules.MvcModule;

namespace reshim.web.App_Start
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainerWebApi();
            SetAutofacContainerSignalR();
            SetAutofacContainer();
            SetAutofacContainerHangfire();
        }

        private static void SetAutofacContainerWebApi()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new ApiModule(Assembly.GetExecutingAssembly()));
            builder.RegisterModule(new BusModule(Assembly.Load("reshim.databus.model.consumers")));
            var container = builder.Build();
            var resolver = new AutofacWebApiDependencyResolver(container);
            System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = resolver;
        }

        private static void SetAutofacContainerHangfire()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ChangeStatusTasksJob>().PropertiesAutowired();
            builder.RegisterModule(new HangfireModule());
            var container = builder.Build();
            GlobalConfiguration.Configuration.UseAutofacActivator(container);
        }

        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterModule(new crosscutting.inversionofcontrol.Modules.MvcModule(Assembly.GetExecutingAssembly()));
            builder.RegisterModule(new BusModule(Assembly.Load("reshim.databus.model.consumers")));
            builder.RegisterModule(new MvcSiteMapProviderModule());
            builder.RegisterModule(new MvcModule());
            builder.RegisterFilterProvider();
            var container = builder.Build();
            container.Resolve<IBusControl>().Start();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            MvcSiteMapProviderConfig.Register(new AutofacDependencyInjectionContainer(container));
            
        }

        private static void SetAutofacContainerSignalR()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<ChatHub>().ExternallyOwned();
            builder.RegisterModule(new SignalrModule());
            var container = builder.Build();
            var resolver = new Autofac.Integration.SignalR.AutofacDependencyResolver(container);
            GlobalHost.DependencyResolver = resolver;
        }


    }
}