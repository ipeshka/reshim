﻿using System.Web.Optimization;

namespace reshim.web.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // библиотеки
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/scripts/jquery-2.1.1.min.js")
                .Include("~/scripts/jquery.cookie.js")
                .Include("~/scripts/jquery.ui.widget.js")
                .Include("~/scripts/underscore-min.js")
                .Include("~/scripts/jquery.validate.min.js")
                .Include("~/scripts/jquery.tipsy.js")
                .Include("~/scripts/jquery.serialize-object.min.js")
                .Include("~/scripts/jquery.iframe-transport.js")
                .Include("~/scripts/jquery.cascadingdropdown.js")
                .Include("~/scripts/date-format.js")
                .Include("~/scripts/slick/slick.js")
                .Include("~/scripts/select2.js")
                .Include("~/scripts/i18n/ru.js")
                .Include("~/scripts/jquery.fileupload.js")
                .Include("~/scripts/jquery.jrumble.1.3.min.js")
                .Include("~/scripts/handlebars-v3.0.3.js")
                .Include("~/scripts/jquery.playSound.js"));
            
            // глобальные функции
            bundles.Add(new ScriptBundle("~/bundles/scroll")
                .Include("~/scripts/jquery.mousewheel.js")
                .Include("~/scripts/jquery.mousewheel-3.0.6.js")
                .Include("~/scripts/jquery.mCustomScrollbar.js"));

            // глобальные функции
            bundles.Add(new ScriptBundle("~/bundles/global")
                .Include("~/scripts/common.js")
                .Include("~/scripts/customs.js")
                .Include("~/scripts/global.js")
                .Include("~/scripts/custom-elements/locale.js"));

            // глобальные функции администратора
            bundles.Add(new ScriptBundle("~/bundles/global-admin")
                .Include("~/scripts/customs.js")
                .Include("~/areas/admin/scripts/admin.js"));

            // самописные функции 
            bundles.Add(new ScriptBundle("~/bundles/custom-elements")
                .Include("~/scripts/custom-elements/table.js")
                .Include("~/scripts/custom-elements/context-menu.js")
                .Include("~/scripts/custom-elements/fileinput.js")
                .Include("~/scripts/custom-elements/notify.js")
                .Include("~/scripts/custom-elements/checkbox.js")
                .Include("~/scripts/custom-elements/radio.js")
                .Include("~/scripts/custom-elements/progressbar.js")
                .Include("~/scripts/custom-elements/dialog.js")
                .Include("~/scripts/custom-elements/tab-control.js")
                .Include("~/scripts/custom-elements/calendar.js")
                .Include("~/scripts/custom-elements/datepicker.js")
                .Include("~/scripts/custom-elements/loader.js")
                .Include("~/scripts/custom-elements/info-place.js"));

            // хелперы
            bundles.Add(new ScriptBundle("~/bundles/function-helpres")
                .Include("~/scripts/helpers/access-file-solution.js"));
            
            bundles.Add(new StyleBundle("~/bundles/css")
                .Include("~/content/price.css", new CssRewriteUrlTransform())
                .Include("~/content/tipsy.css", new CssRewriteUrlTransform())
                .Include("~/content/1140.css", new CssRewriteUrlTransform())
                .Include("~/content/jquery.mCustomScrollbar.css", new CssRewriteUrlTransform())
                .Include("~/content/custom-elements.css", new CssRewriteUrlTransform())
                .Include("~/content/select2.css", new CssRewriteUrlTransform())
                .Include("~/scripts/slick/slick.css", new CssRewriteUrlTransform())
                .Include("~/scripts/slick/slick-theme.css", new CssRewriteUrlTransform())
                .Include("~/content/site.css", new CssRewriteUrlTransform()));

            // codesnippet ckeditor
            bundles.Add(new ScriptBundle("~/bundles/ckeditor-plugins")
                .Include("~/scripts/syntaxhighlighter/scripts/shCore.js")
                .Include("~/scripts/syntaxhighlighter/scripts/shBrushCpp.js")
                .Include("~/scripts/syntaxhighlighter/scripts/shBrushCSharp.js")
                .Include("~/scripts/spoiler.js"));

            bundles.Add(new StyleBundle("~/bundles/ckeditor-css")
                .Include("~/content/spoiler.css", new CssRewriteUrlTransform())
                .Include("~/scripts/syntaxhighlighter/styles/shCore.css", new CssRewriteUrlTransform())
                .Include("~/scripts/syntaxhighlighter/styles/shThemeEclipse.css", new CssRewriteUrlTransform()));
        }
    }
}