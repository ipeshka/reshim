﻿using System.Net;
using System.Net.Http;
using System.Web;
using AutoMapper;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Profile;
using reshim.model.Entities;
using reshim.model.view.ViewModels.File;
using reshim.model.view.ViewModels.User;
using reshim.web.core.Extensions;
using reshim.web.Helpers;
using System.Web.Http;

namespace reshim.web.Api
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class UserController : BaseApi
    {
        private readonly ICommandBus _commandBus;

        public UserController(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        [HttpPost]
        [Route("updateprofile")]
        [AccessApi(new UserRoles[] { UserRoles.Employee, UserRoles.User, UserRoles.Admin })]
        public HttpResponseMessage UpdateProfile(ProfileViewModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<ProfileViewModel, EditProfileCommand>(model);
                command.UserId = User.UserId;

                var result = _commandBus.Submit(command);
                if (result.Success)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }

                return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Не удалось обновить профиль, попробуйте позже.") });
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }


        [HttpPost]
        [Route("updatephoto")]
        [AccessApi(new UserRoles[] { UserRoles.Employee, UserRoles.User, UserRoles.Admin })]
        public HttpResponseMessage UpdatePhoto()
        {
            if (ModelState.IsValid)
            {
                var context = HttpContext.Current;

                var fullPath = context.Request.Files[0].SaveFile("Profile");

                var commandUpdatePhoto = new UpdatePhotoCommand
                {
                    UserId = User.UserId,
                    Path = fullPath
                };

                var result = _commandBus.Submit(commandUpdatePhoto);

                if (result.Success)
                {
                    var returnFile = new CreateFileReturn() { Path = commandUpdatePhoto.Path };
                    return Request.CreateResponse(HttpStatusCode.OK, returnFile);
                }

            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("deletephoto")]
        [AccessApi(new UserRoles[] { UserRoles.Employee, UserRoles.User, UserRoles.Admin })]
        public HttpResponseMessage DeletePhoto()
        {
            var commandDeletePhoto = new DeletePhotoCommand
            {
                UserId = User.UserId
            };
            var result = _commandBus.Submit(commandDeletePhoto);
            if (result.Success) return Request.CreateResponse(HttpStatusCode.OK, result);
            return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Фотографию не удалось удалить.") });
        }
    }
}
