﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using reshim.domain.Commands.Content;
using reshim.domain.Queryies.Chat;
using reshim.domain.Queryies.Content.Lesson;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.web.Helpers;
using reshim.common;
using System.Configuration;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.CompletedTask;
using reshim.model.view.ViewModels.Chat;
using reshim.model.view.ViewModels.Crop;
using reshim.model.view.ViewModels.File;
using reshim.model.view.ViewModels.Content;
using File = System.IO.File;

namespace reshim.web.Api
{
    [RoutePrefix("api/helper")]
    public class HelperController : BaseApi
    {
        private readonly IQueryService _queryService;
        private readonly ICommandBus _commandBus;

        public HelperController(IQueryService queryService, ICommandBus commandBus)
        {
            _queryService = queryService;
            _commandBus = commandBus;
        }

        [HttpPost]
        [Route("cropimage")]
        public HttpResponseMessage CropImage(HelperCropImageFormModel model)
        {
            if (string.IsNullOrEmpty(model.ImagePath)
                || !model.CropPointX.HasValue
                || !model.CropPointY.HasValue
                || !model.ImageCropWidth.HasValue
                || !model.ImageCropHeight.HasValue)
            {
                return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Не верные входные параметры.") });
            }

            byte[] imageBytes = File.ReadAllBytes(HostingEnvironment.MapPath(model.ImagePath));
            byte[] croppedImage = ImageHelper.CropImage(imageBytes, model.CropPointX.Value, model.CropPointY.Value, model.ImageCropWidth.Value, model.ImageCropHeight.Value);

            string tempFolderName = HostingEnvironment.MapPath("~/" + ConfigurationManager.AppSettings["Image.Content"]);

            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(model.ImagePath);
            string fileName = Path.GetFileName(model.ImagePath).Replace(fileNameWithoutExtension, fileNameWithoutExtension + "_" + Guid.NewGuid());

            try
            {
                if (tempFolderName != null) 
                    FileHelper.SaveFile(croppedImage, Path.Combine(tempFolderName, fileName));
                else
                    return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Нет временной папки для хранения файла.") });

            }
            catch (Exception)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new[] { new ValidationResult("Не верные входные параметры.") });
            }

            string photoPath = string.Concat(ConfigurationManager.AppSettings["Image.Content"], fileName);
            var returnFile = new CreateFileReturn() { Path = photoPath };
            return Request.CreateResponse(HttpStatusCode.OK, returnFile);
        }

        [HttpPost]
        [Route("getmessages")]
        public HttpResponseMessage GetMessages(GetMessagesFormModel model)
        {
            var chatView = _queryService.ExecuteQuery(new GetMessagesByDialogId()
            {
                DialogId = model.DialogId,
                Page = model.Page,
                Count = model.Count
            });
            return Request.CreateResponse(HttpStatusCode.OK, chatView);
        }

        [HttpPost]
        [Route("gettypeworks")]
        public HttpResponseMessage GetTypeWorks()
        {
            var typeWorks = _queryService.ExecuteQuery(new GetAllTypeWorks());
            return Request.CreateResponse(HttpStatusCode.OK, typeWorks);
        }

        [HttpPost]
        [Route("getchapters/{nameLessonId:int}")]
        public HttpResponseMessage GetChapters(int nameLessonId)
        {
            var chapters = _queryService.ExecuteQuery(new GetAllChaptersByNameLessonId()
            {
                NameLessonId = nameLessonId
            });
            return Request.CreateResponse(HttpStatusCode.OK, chapters);
        }


        [HttpPost]
        [Route("addcountpreview/{id:int}/article")]
        public HttpResponseMessage AddCountPreviewArticle(int id)
        {
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            var result = _commandBus.Submit(new AddPreviewArticleCommand() { ArticleId = id, Ip = ipAddress });
            return Request.CreateResponse(HttpStatusCode.OK, result.Success);
        }

        [HttpPost]
        [Route("addcountpreview/{id:int}/lesson")]
        public HttpResponseMessage AddCountPreviewLesson(int id)
        {
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            var result = _commandBus.Submit(new AddPreviewLessonCommand() { LessonId = id, Ip = ipAddress });
            return Request.CreateResponse(HttpStatusCode.OK, result.Success);
        }

        [HttpPost]
        [Route("addcountpreview/{id:int}/completedtask")]
        public HttpResponseMessage AddCountPreviewCompletedtask(int id)
        {
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            var result = _commandBus.Submit(new AddPreviewCompletedTaskCommand() { TaskId = id, Ip = ipAddress });
            return Request.CreateResponse(HttpStatusCode.OK, result.Success);
        }


        [HttpPost]
        [Route("changepositioncontent")]
        public HttpResponseMessage ChangePositionContent(ChangePositionContentForm model)
        {
            var result = _commandBus.Submit(new UpdatePositionContentCommand() { Ids = model.Data, CurPage = model.CurPage });
            return Request.CreateResponse<bool>(HttpStatusCode.OK, result.Success);
        }
    }
}