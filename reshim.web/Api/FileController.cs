﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.File;
using reshim.model.Entities;
using reshim.model.view.ViewModels.File;
using reshim.web.Helpers;
using System.Configuration;
using System.Web.Hosting;


namespace reshim.web.Api
{
    public class FileController : BaseApi
    {
        private readonly ICommandBus _commandBus;

        public FileController(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public HttpResponseMessage Post()
        {
            var context = HttpContext.Current;

            if (context.Request.Form["type"].ToLower() != "image")
            {
                var typeFileValue = (TypeFile) Enum.Parse(typeof (TypeFile), context.Request.Form["type"]);

                var fullPath = context.Request.Files[0].SaveFile(typeFileValue.ToString());
                var command = new CreateFileCommand()
                {
                    Name = Path.GetFileName(fullPath),
                    Path = fullPath,
                    TypeFile = typeFileValue
                };

                var result = _commandBus.Submit(command);

                if (result.Success)
                {
                    var returnFile = new CreateFileReturn() { LastId = result.LastId, Name = command.Name };
                    return Request.CreateResponse(HttpStatusCode.OK, returnFile);
                }
            }
            else
            {
                var myFile = context.Request.Files["Image"];
                string tempFolderName = ConfigurationManager.AppSettings["Image.TempFolderName"];
                bool isUploaded = false;
                if (myFile != null && myFile.ContentLength != 0)
                {
                    string tempFolderPath = HostingEnvironment.MapPath("~/" + tempFolderName);
                    if (FileHelper.CreateFolderIfNeeded(tempFolderPath))
                    {
                        try
                        {
                            if (tempFolderPath != null)
                                myFile.SaveAs(Path.Combine(tempFolderPath, myFile.FileName));
                            isUploaded = true;
                        }
                        catch (Exception)
                        {
                            return Request.CreateResponse(HttpStatusCode.Found);
                        }
                    }
                }

                if (myFile != null)
                {
                    string filePath = string.Concat(tempFolderName, myFile.FileName);
                    var returnFile = new CreateFileReturn()
                    {
                        Name = myFile.FileName,
                        Path = filePath,
                        IsUploaded = isUploaded
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, returnFile);
                }

                return Request.CreateResponse(HttpStatusCode.Found);
            }


            return Request.CreateResponse(HttpStatusCode.Found);
        }

    }


}
