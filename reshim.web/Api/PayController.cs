﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using AutoMapper;
using MassTransit;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.common.interkassa;
using reshim.databus.model.contracts.Contracts;
using reshim.databus.model.contracts.Contracts.SystemMail;
using reshim.domain.Commands.Billing;
using reshim.domain.Commands.Security;
using reshim.domain.Queryies.CompletedTask;
using reshim.domain.Queryies.User;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;
using reshim.web.core.Extensions;
using reshim.web.Helpers.Binders;

namespace reshim.web.Api
{
    [RoutePrefix("api/pay")]
    public class PayController : BaseApi
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IBus _bus;

        public PayController(ICommandBus commandBus, IQueryService queryService, IBus bus)
        {
            _commandBus = commandBus;
            _queryService = queryService;
            _bus = bus;
        }

        [HttpPost]
        [Route("input")]
        public HttpResponseMessage Input([ModelBinder(typeof(CreateRequestInterkassaBinder))]CreateRequestInterkassaFormModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.HasCompletedTask)
                {
                    var command = Mapper.Map<CreateRequestInterkassaFormModel, SimplyUserRegisterCommand>(model);
                    IEnumerable<ValidationResult> errors = _commandBus.Validate(command).ToArray();
                    if (!errors.Any())
                    {
                        var result = _commandBus.Submit(command);
                        if (result.Success)
                        {
                            var user = _queryService.ExecuteQuery(new GetUserByLoginOrEmail() { LoginOrEmail = command.Email, HasEmail = true });
                            var completedtask = _queryService.ExecuteQuery(new GetCompletedTaskById() { Id = model.CompletedTaskId });
                            _commandBus.Submit(new UserLogonCommand() { LoginOrEmail = model.Email, HasEmail = true });
                            _bus.Publish(new Registration(user.Email, user.Login));

                            model.PaymentNumber = result.LastId.ToString(CultureInfo.InvariantCulture);
                            model.CheckoutId = InterkassaConfig.CheckoutId;
                            model.Action = InterkassaConfig.Action;
                            model.Amount = completedtask.Cost;
                            return Request.CreateResponse(HttpStatusCode.OK, model);
                        }

                        return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Не удалось произвести регистрацию.") });
                    }

                    return Request.CreateResponse(HttpStatusCode.Found, errors);
                }
                else
                {
                    var command = Mapper.Map<CreateRequestInterkassaFormModel, CreateRequestInterkassaCommand>(model);
                    command.UserId = User.UserId;
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        model.PaymentNumber = result.LastId.ToString(CultureInfo.InvariantCulture);
                        model.CheckoutId = InterkassaConfig.CheckoutId;
                        model.Action = InterkassaConfig.Action;

                        return Request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found, new[] { "Оплату не удалось произвести, попробуйте позже" });
                }
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("confirm")]
        public HttpResponseMessage Confirm([ModelBinder(typeof(ConfirmRequestInterkassaBinder))]ConfirmRequestInterkassaFormModel model)
        {
            var command = Mapper.Map<ConfirmRequestInterkassaFormModel, ConfirmRequestInterkassaCommand>(model);
            var result = _commandBus.Submit(command);

            if (result.Success)
            {
                if (command.HasCompletedTask)
                {
                    var historyCompletedTask = _queryService.ExecuteQuery(new GetHistoryCompletedTaskById() { Id = command.PaymentNumber });
                    var pathCompletedtask = HttpRuntime.AppDomainAppPath + historyCompletedTask.CompletedTask.Solution.Path;
                    _bus.Publish(new SendCompletedTask(
                        historyCompletedTask.User.Email,
                        historyCompletedTask.User.Login,
                        System.IO.File.ReadAllBytes(pathCompletedtask),
                        historyCompletedTask.CompletedTask.Solution.Name)
                    );
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.Found);
        }

        [HttpPost]
        [Authorize]
        [Route("withdrawal")]
        public HttpResponseMessage Withdrawal(WithdrawalFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<WithdrawalFormModel, AddWithdrawalCommand>(model);
                command.UserId = User.UserId;

                var errors = _commandBus.Validate(command).ToList();

                if (!errors.Any())
                {
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found,
                        new[] { "Создать заявку на вывод не удалось, попробуйте позже." });
                }

                return Request.CreateResponse<IEnumerable<ValidationResult>>(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpGet]
        [Route("payments")]
        public HttpResponseMessage PaymentSystems()
        {
            var systems = EnumEx.GetKeyValue<PaywayVia, NameAttribute, NameAttribute, TypeAttribute>().ToList();
            return Request.CreateResponse(HttpStatusCode.OK, systems);
        }

        [HttpPost]
        [Authorize]
        [Route("statuscompletedtask/{id}")]
        public HttpResponseMessage StatusCompletedTask(int id)
        {
            var recordHistory = _queryService.ExecuteQuery(new GetHistoryCompletedTaskById() { Id = id });
            return Request.CreateResponse(HttpStatusCode.OK, recordHistory);
        }
    }
}
