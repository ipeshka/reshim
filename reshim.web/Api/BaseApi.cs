﻿using System.Web;
using System.Web.Http;
using reshim.web.core.Extensions;
using reshim.web.core.Model;

namespace reshim.web.Api
{
    public class BaseApi : ApiController
    {
        public string SiteUrl {
            get
            {
                var request = HttpContext.Current.Request;
                return string.Format("{0}://{1}", request.Url.Scheme, request.Url.Authority);
            }
        }

        public new ReshimUser User
        {
            get { return base.User.GetReshimUser(); }
        }
    }
}