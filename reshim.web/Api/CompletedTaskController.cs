﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using reshim.data.commandProcessor.Dispatcher;
using reshim.domain.Commands.CompletedTask;
using reshim.domain.Queryies.CompletedTask;

namespace reshim.web.Api
{
    [RoutePrefix("api/completedtask")]
    public class CompletedTaskController : BaseApi
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;

        public CompletedTaskController(ICommandBus commandBus, IQueryService queryService)
        {
            _commandBus = commandBus;
            _queryService = queryService;
        }

        [HttpPost]
        [Route("get/{id:int}")]
        public HttpResponseMessage Get(int id)
        {
            var result = _queryService.ExecuteQuery(new GetCompletedTaskById() { Id = id });
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
    }

    [Authorize]
    [RoutePrefix("api/completedtask/admin")]
    public class CompletedTaskAdminController : BaseApi
    {
        private readonly ICommandBus _commandBus;

        public CompletedTaskAdminController(
            ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        [HttpPost]
        [Route("delete/{id:int}")]
        public HttpResponseMessage Delete(int id)
        {
            var result = _commandBus.Submit(new DeleteCompletedTaskCommand() { TaskId = id });
            if (result.Success) return Request.CreateResponse(HttpStatusCode.OK, result);
            return Request.CreateResponse(HttpStatusCode.Found, result);
        }
    }
}
