﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using MassTransit;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.databus.model.contracts.Contracts.Alerts;
using reshim.databus.model.contracts.Contracts.SystemMail;
using reshim.domain.Commands.Security;
using reshim.domain.Queryies.User;
using reshim.model.view.ViewModels.Auth;
using reshim.web.core.Extensions;

namespace reshim.web.Api
{
    [RoutePrefix("api/auth")]
    public class AuthController : BaseApi
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IBus _bus;

        public AuthController(ICommandBus commandBus, IQueryService queryService, IBus bus)
        {
            _commandBus = commandBus;
            _queryService = queryService;
            _bus = bus;
        }

        [HttpPost]
        [Route("register")]
        public HttpResponseMessage Register(RegisterFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<RegisterFormModel, UserRegisterCommand>(model);
                IEnumerable<ValidationResult> errors = _commandBus.Validate(command).ToArray();

                if (!errors.Any())
                {
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        _commandBus.Submit(new UserLogonCommand() { LoginOrEmail = model.Login, HasEmail = false });
                        _bus.Publish(new Registration(model.Email, model.Login));
                        return Request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Не удалось произвести регистрацию.") });
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("quickregister")]
        public HttpResponseMessage QuickRegister(QuickRegisterFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<QuickRegisterFormModel, UserQuickRegisterCommand>(model);
                List<ValidationResult> errors = _commandBus.Validate(command.User).ToList();
                errors.AddRange(_commandBus.Validate(command.Task).ToList());

                if (!errors.Any())
                {
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        _commandBus.Submit(new UserLogonCommand() { LoginOrEmail = model.User.Login, HasEmail = false });
                        _bus.Publish(new Registration(model.User.Email, model.User.Login));
                        _bus.Publish(new AlertNewTask(result.LastId, SiteUrl));
                        return Request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Не удалось произвести регистрацию.") });
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("login")]
        public HttpResponseMessage Login(LoginFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<LoginFormModel, UserLoginCommand>(model);
                IEnumerable<ValidationResult> errors = _commandBus.Validate(command).ToArray();

                if (!errors.Any())
                {
                    _commandBus.Submit(new UserLogonCommand() { LoginOrEmail = model.EmailOrLogin, HasEmail = command.HasEmail });
                    var curUser = _queryService.ExecuteQuery(new GetUserByLoginOrEmail() { LoginOrEmail = model.EmailOrLogin, HasEmail = command.HasEmail });
                    return Request.CreateResponse(HttpStatusCode.OK, curUser.Role);
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("forgotpassword")]
        public HttpResponseMessage ForgotPassword(ForgotPasswordFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<ForgotPasswordFormModel, UserForgotPasswordCommand>(model);
                IEnumerable<ValidationResult> errors = _commandBus.Validate(command).ToArray();

                if (!errors.Any())
                {
                    var user = _queryService.ExecuteQuery(new GetUserByLoginOrEmail() { HasEmail = command.HasEmail, LoginOrEmail = command.EmailOrLogin });
                    _bus.Publish(new RestoryPassword(user.Email, user.Login, SiteUrl));
                    return Request.CreateResponse(HttpStatusCode.OK, user);
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);

            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("updatepassword")]
        public HttpResponseMessage UpdatePassword(UpdatePasswordFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<UpdatePasswordFormModel, UpdatePasswordCommand>(model);
                IEnumerable<ValidationResult> errors = _commandBus.Validate(command).ToArray();

                if (!errors.Any())
                {
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        var user = _queryService.ExecuteQuery(new GetUserById() { Id = model.UserId });
                        _bus.Publish(new RestoryPasswordSuccess(user.Email, user.Login));
                        return Request.CreateResponse(HttpStatusCode.OK, model);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found, new[] { new ValidationResult("Пароль не удалось сменить.") });
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);

            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }
    }
}
