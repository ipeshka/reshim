﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using MassTransit;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.domain.Queryies.Task;
using reshim.domain.StrategiesFetch;
using reshim.data.infrastructure.State;
using reshim.databus.model.contracts.Contracts.Alerts;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;
using reshim.web.core.Extensions;
using reshim.web.Helpers;

namespace reshim.web.Api
{
    [Authorize]
    [RoutePrefix("api/task")]
    public class TaskController : BaseApi
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IEmployeeTaskContext _employeeTask;
        private readonly IUserTaskContext _userTask;
        private readonly IBus _bus;


        public TaskController(
            ICommandBus commandBus,
            IEmployeeTaskContext employeeTask,
            IUserTaskContext userTask,
            IQueryService queryService,
            IBus bus)
        {
            _commandBus = commandBus;
            _employeeTask = employeeTask;
            _userTask = userTask;
            _queryService = queryService;
            _bus = bus;
        }

        [HttpGet]
        [Route("detail/{id:int}")]
        [AllowAnonymous]
        public TaskViewModel Detail(int id)
        {
            var viewTask = _queryService.ExecuteQuery(new GetTaskById()
            {
                Id = id,
                IncludeProperties = new TaskFetchStrategyNumber2()
            });
            return viewTask;
        }


        #region action user
        [HttpPost]
        [Route("user/validationorder")]
        [AllowAnonymous]
        public HttpResponseMessage ValidationOrder(CreateTaskFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<CreateTaskFormModel, CreateTaskCommand>(model);
                command.CustomerId = User.UserId;

                var errors = _commandBus.Validate(command).ToList();

                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }


        [HttpPost]
        [Route("user/addorder")]
        public HttpResponseMessage AddOrder(CreateTaskFormModel model)
        {
            if (ModelState.IsValid)
            {
                var command = Mapper.Map<CreateTaskFormModel, CreateTaskCommand>(model);
                command.CustomerId = User.UserId;

                var errors = _commandBus.Validate(command).ToList();

                if (!errors.Any())
                {
                    var result = _commandBus.Submit(command);
                    if (result.Success)
                    {
                        _bus.Publish(new AlertNewTask(result.LastId, SiteUrl));
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }

                    return Request.CreateResponse(HttpStatusCode.Found,
                        new[] { new ValidationResult("Заказ не удалось добавить, попробуйте позже.") });
                }

                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("user/editorder")]
        public HttpResponseMessage EditOrder(EditTaskFormModel model)
        {
            if (ModelState.IsValid)
            {
                var errors = _userTask.Edit(
                    model.TaskId,
                    model.FileId ?? 0,
                    model.DateOfExecution,
                    model.Cost,
                    model.SubjectId,
                    model.TypeWorkId,
                    model.Comment).ToList();
                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK, model);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("user/delete/{id:int}")]
        public HttpResponseMessage Delete(int id)
        {
            if (ModelState.IsValid)
            {
                var errors = _userTask.Delete(id, User.UserId).ToList();
                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK, id);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("user/selectemployee")]
        public HttpResponseMessage SelectEmployee(SelectEmployeeFormModel model)
        {
            if (ModelState.IsValid)
            {
                var errors = _userTask.SelectEmployee(model.TaskId, model.SubscriberId).ToList();
                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK, model);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("user/pay/{taskId:int}")]
        public HttpResponseMessage Pay(int taskId)
        {
            if (ModelState.IsValid)
            {
                var errors = _userTask.Pay(taskId, User.UserId).ToList();
                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK, taskId);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        // получаем список подписчиков
        [HttpPost]
        [Route("user/getsubscribers/{taskId:int}")]
        public HttpResponseMessage Subscribers(int taskId)
        {
            var subscriversView = _queryService.ExecuteQuery(new GetSubscribersByTaskId() { Id = taskId });
            return Request.CreateResponse(HttpStatusCode.OK, subscriversView);
        }

        // оценка оплаты
        [HttpGet]
        [Route("user/getestimatepay/{id:int}")]
        public HttpResponseMessage EstimatePay(int id)
        {

            var subscribers = _queryService.ExecuteQuery(new GetSubscribersByTaskId() { Id = id });
            return Request.CreateResponse(HttpStatusCode.OK, subscribers);
        }
        #endregion


        #region action employee
        [HttpPost]
        [Route("employee/estimate")]
        [AccessApi(new[] { UserRoles.Employee })]
        public HttpResponseMessage Estimate(EstimateTaskFormModel model)
        {
            if (ModelState.IsValid)
            {
                var errors = _employeeTask.Estimate(model.TaskId, User.UserId, model.Value, model.Comment).ToList();
                if (!errors.Any())
                {
                    var subscribers = _queryService.ExecuteQuery(new GetSubscribersByTaskId { Id = model.TaskId });
                    return Request.CreateResponse(HttpStatusCode.OK, subscribers);
                }
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());
        }

        [HttpPost]
        [Route("employee/unsubscribe/{taskId:int}")]
        [AccessApi(new[] { UserRoles.Employee })]
        public HttpResponseMessage Unsubscribe(int taskId)
        {
            if (ModelState.IsValid)
            {
                var errors = _employeeTask.Unsubscribe(taskId, User.UserId).ToList();
                if (!errors.Any())
                {
                    var subscribers = _queryService.ExecuteQuery(new GetSubscribersByTaskId { Id = taskId });
                    return Request.CreateResponse(HttpStatusCode.OK, subscribers);
                }
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());

        }

        [HttpPost]
        [Route("employee/uploadsolution")]
        [AccessApi(new[] { UserRoles.Employee })]
        public HttpResponseMessage UploadSolution(UploadSolutionFormModel model)
        {
            if (ModelState.IsValid)
            {
                var errors = _employeeTask.UploadSolution(model.TaskId, model.FileId, User.UserId).ToList();
                if (!errors.Any()) return Request.CreateResponse(HttpStatusCode.OK, model);
                return Request.CreateResponse(HttpStatusCode.Found, errors);
            }

            return Request.CreateResponse(HttpStatusCode.Found, ModelState.GetError());

        }

        #endregion
    }

    [Authorize]
    [RoutePrefix("api/task/admin")]
    public class TaskAdminController : BaseApi
    {
        private readonly ICommandBus _commandBus;
        private readonly IQueryService _queryService;
        private readonly IEmployeeTaskContext _employeeTask;
        private readonly IUserTaskContext _userTask;


        public TaskAdminController(
            ICommandBus commandBus,
            IEmployeeTaskContext employeeTask,
            IUserTaskContext userTask,
            IQueryService queryService)
        {
            _commandBus = commandBus;
            _employeeTask = employeeTask;
            _userTask = userTask;
            _queryService = queryService;
        }

        [HttpPost]
        [Route("delete/{id:int}")]
        public HttpResponseMessage Delete(int id)
        {
            var result = _commandBus.Submit(new DeleteTaskCommand() { TaskId = id });
            if (result.Success) return Request.CreateResponse(HttpStatusCode.OK, result);
            return Request.CreateResponse(HttpStatusCode.Found, result);
        }

    }
}
