﻿using System.Configuration;

namespace reshim.common.interkassa
{
    public static class InterkassaConfig
    {
        public static string CheckoutId
        {
            get
            {
                return ConfigurationManager.AppSettings["InterkassaCheckoutId"];
            }
        }

        public static string Action
        {
            get
            {
                return ConfigurationManager.AppSettings["InterkassaAction"];
            }
        }
    }
}
