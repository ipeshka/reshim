﻿using System;
using System.Collections.Generic;
using reshim.common;
using reshim.data.infrastructure.Repositories;
using reshim.data.infrastructure.State;
using reshim.model.Entities;

namespace reshim.domain.state.State
{
    public class UserTaskContext : IUserTaskContext
    {
        private readonly UserTaskState _newState;
        private readonly UserTaskState _ratingState;
        private readonly UserTaskState _agreementState;
        private readonly UserTaskState _completedNotPayState;
        private readonly UserTaskState _completedState;

        private UserTaskState _curState;

        private readonly ITaskRepository _taskRepository;

        public UserTaskContext(
            ITaskRepository taskRepository,
            UserTaskState newState,
            UserTaskState ratingState,
            UserTaskState agreementState,
            UserTaskState completedNotPayState,
            UserTaskState completedState
            )
        {
            _newState = newState;
            _ratingState = ratingState;
            _agreementState = agreementState;
            _completedNotPayState = completedNotPayState;
            _completedState = completedState;
            _taskRepository = taskRepository;
        }

        public IEnumerable<ValidationResult> Edit(int taskId, int fileId, DateTime dateOfExecution, decimal? cost, int subjectId, int typeWork, string comment)
        {
            GetCurState(taskId);
            return _curState.Edit(taskId, fileId, dateOfExecution, cost, subjectId, typeWork, comment);
        }

        public IEnumerable<ValidationResult> Delete(int taskId, int userId)
        {
            GetCurState(taskId);
            return _curState.Delete(taskId, userId);
        }

        public IEnumerable<ValidationResult> SelectEmployee(int taskId, int subscriberId)
        {
            GetCurState(taskId);
            return _curState.SelectEmployee(taskId, subscriberId);
        }

        public IEnumerable<ValidationResult> Pay(int taskId, int userId)
        {
            GetCurState(taskId);
            return _curState.Pay(taskId, userId);
        }

        public IEnumerable<ValidationResult> Download(int taskId)
        {
            throw new NotImplementedException();
        }

        private void GetCurState(int taskId)
        {
            var task = _taskRepository.GetById(taskId);
            if (task.Status == StatusTask.New)
            {
                _curState = _newState;
            }
            else if (task.Status == StatusTask.Rating)
            {
                _curState = _ratingState;
            }
            else if (task.Status == StatusTask.Agreement)
            {
                _curState = _agreementState;
            }
            else if (task.Status == StatusTask.CompletedNotPay)
            {
                _curState = _completedNotPayState;
            }
            else if (task.Status == StatusTask.Completed)
            {
                _curState = _completedState;
            }
            else
                throw new Exception("Внутренне исключение (такого задания нет)");

        }


    }
}
