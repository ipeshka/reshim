﻿using System;
using System.Collections.Generic;
using reshim.common;
using reshim.data.infrastructure.Repositories;
using reshim.data.infrastructure.State;
using reshim.model.Entities;

namespace reshim.domain.state.State
{
    public class EmployeeTaskContext : IEmployeeTaskContext
    {
        private readonly EmployeeTaskState _newState;
        private readonly EmployeeTaskState _ratingState;
        private readonly EmployeeTaskState _agreementState;
        private readonly EmployeeTaskState _completedNotPayState;
        private readonly EmployeeTaskState _completedState;
        private EmployeeTaskState _curState;

        private readonly ITaskRepository _taskRepository;

        public EmployeeTaskContext(
            ITaskRepository taskRepository,
            EmployeeTaskState newState,
            EmployeeTaskState ratingState,
            EmployeeTaskState agreementState,
            EmployeeTaskState completedNotPayState,
            EmployeeTaskState completedState
            )
        {
            _newState = newState;
            _ratingState = ratingState;
            _agreementState = agreementState;
            _completedNotPayState = completedNotPayState;
            _completedState = completedState;
            _taskRepository = taskRepository;
        }

        public IEnumerable<ValidationResult> Estimate(int taskId, int userId, decimal value, string comment)
        {
            GetCurState(taskId);
            return _curState.Estimate(taskId, userId, value, comment);
        }

        public IEnumerable<ValidationResult> Unsubscribe(int taskId, int userId)
        {
            GetCurState(taskId);
            return _curState.Unsubscribe(taskId, userId);
        }

        public IEnumerable<ValidationResult> UploadSolution(int taskId, int fileId, int userId)
        {
            GetCurState(taskId);
            return _curState.UploadSolution(taskId, fileId, userId);
        }

        private void GetCurState(int taskId)
        {
            var task = _taskRepository.GetById(taskId);
            if (task.Status == StatusTask.New)
            {
                _curState = _newState;
            }
            else if (task.Status == StatusTask.Rating)
            {
                _curState = _ratingState;
            }
            else if (task.Status == StatusTask.Agreement)
            {
                _curState = _agreementState;
            }
            else if (task.Status == StatusTask.CompletedNotPay)
            {
                _curState = _completedNotPayState;
            }
            else if (task.Status == StatusTask.Completed)
            {
                _curState = _completedState;
            }
            else
                throw new Exception("Внутренне исключение (такого задания нет)");
            
        }
    }
}
