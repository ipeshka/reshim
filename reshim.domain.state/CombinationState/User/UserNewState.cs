﻿using System;
using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.State;


namespace reshim.domain.state.CombinationState.User
{
    public class UserNewState : UserTaskState
    {
        protected readonly ICommandBus _commandBus;

        public UserNewState(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public override IEnumerable<ValidationResult> Edit(int taskId, int fileId, DateTime dateOfExecution, decimal? cost, int subjectId, int typeWorkId,
            string comment)
        {
            var command = new EditTaskCommand()
            {
                TaskId = taskId,
                FileId = fileId,
                DateOfExecution = dateOfExecution,
                Cost = cost,
                SubjectId = subjectId,
                TypeWorkId = typeWorkId,
                Comment = comment
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Оценку заказу не удалось установить.") };
            }

            return errors;
        }

        public override IEnumerable<ValidationResult> Delete(int taskId, int userId)
        {
            var command = new DeleteTaskCommand()
            {
                TaskId = taskId,
                UserId = userId
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Не удалось удалить заказ.") };
            }

            return errors;
        }
    }
}
