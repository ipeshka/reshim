﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;

namespace reshim.domain.state.CombinationState.User
{
    public class UserRatingState : UserNewState
    {
        public UserRatingState(ICommandBus commandBus)
            : base(commandBus)
        {
        }

        public override IEnumerable<ValidationResult> SelectEmployee(int taskId, int subscriberId)
        {
            var command = new SelectEmployeeTaskCommand()
            {
                TaskId = taskId,
                SubscriberId = subscriberId
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Не удалось выбрать исполнителя.") };
            }

            return errors;
        }
    }
}
