﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.State;

namespace reshim.domain.state.CombinationState.User
{
    public class UserNotPayState : UserTaskState
    {
        protected readonly ICommandBus _commandBus;

        public UserNotPayState(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public override IEnumerable<ValidationResult> Pay(int taskId, int userId)
        {
            var command = new PaymentTaskCommand()
            {
                TaskId = taskId,
                UserId = userId
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Не .") };
            }

            return errors;
        }
    }
}
