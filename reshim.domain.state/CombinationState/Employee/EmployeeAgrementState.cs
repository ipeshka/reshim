﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.State;

namespace reshim.domain.state.CombinationState.Employee
{
    public class EmployeeAgrementState : EmployeeTaskState
    {
        private readonly ICommandBus _commandBus;

        public EmployeeAgrementState(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public override IEnumerable<ValidationResult> UploadSolution(int taskId, int fileId, int userId)
        {
            var command = new UploadSolutionCommand()
            {
                TaskId = taskId,
                FileId = fileId,
                DecidedId = userId
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Загрузить решение не удалось") };
            }

            return errors;
        }
    }
}
