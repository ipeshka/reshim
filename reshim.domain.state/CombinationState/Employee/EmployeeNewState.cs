﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Dispatcher;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.State;


namespace reshim.domain.state.CombinationState.Employee
{
    public class EmployeeNewState : EmployeeTaskState
    {
        private readonly ICommandBus _commandBus;

        public EmployeeNewState(ICommandBus commandBus)
        {
            _commandBus = commandBus;
        }

        public override IEnumerable<ValidationResult> Estimate(int taskId, int userId, decimal value, string comment)
        {
            var command = new EstimateTaskCommand()
            {
                TaskId = taskId,
                UserId = userId,
                Value = value,
                Comment = comment
            };

            var errors = _commandBus.Validate(command).ToList();

            if (!errors.Any())
            {
                var result = _commandBus.Submit(command);
                if (!result.Success) return new List<ValidationResult>() { new ValidationResult("Оценку заказу не удалось установить.") };
            }

            return errors;
        }

    }
}
