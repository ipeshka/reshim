﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using reshim.oauth2.Client;
using reshim.oauth2.Configuration;
using reshim.oauth2.Infrastructure;

namespace reshim.oauth2
{
    public class AuthorizationRoot
    {
        private readonly IRequestFactory _requestFactory;
        private readonly OAuth2ConfigurationSection _configurationSection;

        public AuthorizationRoot() :
            this(new ConfigurationManager(), "oauth2", new RequestFactory())
        {
        }

        public AuthorizationRoot(IConfigurationManager configurationManager, string configurationSectionName, IRequestFactory requestFactory)
        {
            _requestFactory = requestFactory;
            _configurationSection = configurationManager.GetConfigSection<OAuth2ConfigurationSection>(configurationSectionName);
        }


        public virtual IEnumerable<IClient> Clients
        {
            get
            {
                var types = Assembly.GetExecutingAssembly().GetTypes().Where(typeof(IClient).IsAssignableFrom).ToList();
                Func<ClientConfiguration, Type> getType = configuration => types.First(x => x.Name == configuration.ClientTypeName);

                IEnumerable<IClient> clients = _configurationSection.Services.AsEnumerable()
                    .Where(configuration => configuration.IsEnabled)
                    .Select(configuration => (IClient)Activator.CreateInstance(getType(configuration), _requestFactory, configuration));

                return clients;

            }
        }
    }
}
