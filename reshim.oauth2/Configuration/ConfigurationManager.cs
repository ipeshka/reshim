﻿namespace reshim.oauth2.Configuration
{
    public class ConfigurationManager : IConfigurationManager
    {
        public string GetAppSetting(string key)
        {
            return System.Configuration.ConfigurationManager.AppSettings[key];
        }

        public T GetConfigSection<T>(string name)
        {
            return (T)System.Configuration.ConfigurationManager.GetSection(name);
        }
    }
}
