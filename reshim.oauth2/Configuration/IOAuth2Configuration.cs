﻿namespace reshim.oauth2.Configuration
{
    public interface IOAuth2Configuration
    {
        IClientConfiguration this[string clientTypeName] { get; }
    }
}
