﻿namespace reshim.oauth2.Configuration
{
    public interface IConfigurationManager
    {
        string GetAppSetting(string key);
        T GetConfigSection<T>(string name);
    }
}
