﻿using System.Configuration;

namespace reshim.oauth2.Configuration
{

    public class OAuth2ConfigurationSection : ConfigurationSection, IOAuth2Configuration
    {
        private const string CollectionName = "services";

        public new IClientConfiguration this[string clientTypeName]
        {
            get { return Services[clientTypeName]; }
        }

        [ConfigurationProperty(CollectionName), ConfigurationCollection(typeof(ServiceCollection))]
        public ServiceCollection Services
        {
            get { return (ServiceCollection)base[CollectionName]; }
        }
    }
}
