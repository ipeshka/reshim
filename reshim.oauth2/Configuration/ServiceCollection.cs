﻿using System.Collections.Generic;
using System.Configuration;

namespace reshim.oauth2.Configuration
{
    public class ServiceCollection : ConfigurationElementCollection
    {
        public IEnumerable<ClientConfiguration> AsEnumerable()
        {
            for (var i = 0; i < Count; i++)
            {
                yield return this[i];
            }
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get { return ConfigurationElementCollectionType.AddRemoveClearMap; }
        }

        public new ClientConfiguration this[string clientTypeName]
        {
            get { return (ClientConfiguration)BaseGet(clientTypeName); }
        }


        public ClientConfiguration this[int index]
        {
            get { return (ClientConfiguration)BaseGet(index); }
        }


        protected override ConfigurationElement CreateNewElement()
        {
            return new ClientConfiguration();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((ClientConfiguration)element).ClientTypeName;
        }
    }
}
