﻿using System.Configuration;
using reshim.oauth2.Infrastructure;

namespace reshim.oauth2.Configuration
{
    public class ClientConfiguration : ConfigurationElement, IClientConfiguration
    {
        private const string ClientTypeNameKey = "clientType";
        private const string ClientIdKey = "clientId";
        private const string EnabledKey = "enabled";
        private const string ClientSecretKey = "clientSecret";
        private const string ClientPublicKey = "clientPublic";
        private const string ScopeKey = "scope";
        private const string RedirectUriKey = "redirectUri";

        [ConfigurationProperty(ClientTypeNameKey, IsRequired = true, IsKey = true)]
        public string ClientTypeName
        {
            get { return (string)this[ClientTypeNameKey]; }
        }

        [ConfigurationProperty(ClientIdKey, IsRequired = true)]
        public string ClientId
        {
            get { return (string)this[ClientIdKey]; }
        }

        [ConfigurationProperty(EnabledKey, DefaultValue = true)]
        public bool IsEnabled
        {
            get { return (bool)this[EnabledKey]; }
        }

        [ConfigurationProperty(ClientSecretKey, IsRequired = true)]
        public string ClientSecret
        {
            get { return (string)this[ClientSecretKey]; }
        }

        [ConfigurationProperty(ClientPublicKey)]
        public string ClientPublic
        {
            get { return (string)this[ClientPublicKey]; }
        }

        [ConfigurationProperty(ScopeKey)]
        public string Scope
        {
            get { return (string)this[ScopeKey]; }
        }

        [ConfigurationProperty(RedirectUriKey, IsRequired = true)]
        public string RedirectUri
        {
            get
            {
                return UriUtility.ToAbsolute((string)this[RedirectUriKey]);
            }
        }
    }
}
