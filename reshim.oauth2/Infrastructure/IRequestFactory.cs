﻿using RestSharp;

namespace reshim.oauth2.Infrastructure
{
    public interface IRequestFactory
    {
        IRestClient NewClient(string uri);
        IRestRequest NewRequest();
    }
}
