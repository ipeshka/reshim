﻿using RestSharp;

namespace reshim.oauth2.Infrastructure
{
    public class RequestFactory : IRequestFactory
    {
        public IRestClient NewClient(string uri)
        {
            return new RestClient(uri);
        }

        public IRestRequest NewRequest()
        {
            return new RestRequest();
        }
    }
}
