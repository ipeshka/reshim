(function($) {
    $.Dialog = function(params) {
		
        if(!$.Dialog.opened) {
            $.Dialog.opened = true;
        } else {
            return DIALOG;
        }

        $.Dialog.settings = params;

        params = $.extend({
			// заголовок
            caption: {
				icon: false,
				title: false,
				sysButtons: {
					btnClose: false
				}
			},
            content: '',
			// затемнение
            overlay: false,
            width: 'auto',
            height: 'auto',
			// позиционирование
            position: {
				left: false,
				top: false
			},
			// отступы контекта
            padding: false,
            overlayClickClose: true,
            onShow: function(_dialog) {},
            sysBtnCloseClick: function(event) {}
		}, params);

        var _overlay, _window, _caption, _content;

		// затемнение фона
        _overlay = $("<div/>").addClass("dialog-overlay");
        if (params.overlay) {
            _overlay.css({
                backgroundColor: 'rgba(0,0,0,.7)'
            });
        }

        _window = $("<div/>").addClass("window");
        _content = $("<div/>").addClass("content");
		
        _content.css({
            paddingTop: params.padding,
            paddingLeft: params.padding,
            paddingRight: params.padding,
            paddingBottom: params.padding
        });

		// формирование залоговка всплывашки
		if(params.caption.icon || params.caption.title || params.caption.sysButtons.btnClose) {
			_caption = $("<div/>").addClass("caption");
		
			if (params.caption.sysButtons) {
				if (params.caption.sysButtons.btnClose) {
					$("<button/>").addClass("btn-close").on('click', function(e){
						e.preventDefault();
						e.stopPropagation();
						$.Dialog.close();
						params.sysBtnCloseClick(e);
					}).appendTo(_caption);
				}
			}

			if(params.caption.title) $("<div/>").addClass("title").html(params.title).appendTo(_caption);
			if(params.caption.icon) $(params.icon).addClass("icon").appendTo(_caption);
			
			_caption.appendTo(_window);
		}
		
		// формирование попапчика
        _content.html(params.content);
        _content.appendTo(_window);
        _window.appendTo(_overlay);

		
		// формирование высоты и ширины
        if (params.width != 'auto') _window.css('min-width', params.width);
        if (params.height != 'auto') _window.css('min-height', params.height);


        _overlay.hide().appendTo('body').fadeIn('fast');

        DIALOG = _window;

		
		// позиционирование попапчика
		_window
			.css("position", "fixed")
			.css("z-index", parseInt(_overlay.css('z-index')) + 10);
		
		if(!params.position.top && !params.position.left) {
			_window
				.css("top", ($(window).height() - DIALOG.outerHeight()) / 2 )
				.css("left", ($(window).width() - DIALOG.outerWidth()) / 2);
		} else {
			_window
				.css("top", params.position.top)
				.css("left", params.position.left);
		}

        _window.on('click', function(e){
            e.stopPropagation();
        });

        if (params.overlayClickClose) {
            _overlay.on('click', function(e){
                e.preventDefault();
                $.Dialog.close();
            });
        }
        params.onShow(DIALOG);

        return DIALOG;
    }

    $.Dialog.content = function(newContent) {
        if(!$.Dialog.opened || DIALOG == undefined) {
            return false;
        }

        if(newContent) {
            DIALOG.children(".content").html(newContent);
            $.Dialog.autoResize();
            return true;
        } else {
            return DIALOG.children(".content").html();
        }
    }

    $.Dialog.title = function(newTitle) {
        if(!$.Dialog.opened || DIALOG == undefined) {
            return false;
        }

        var _title = DIALOG.children('.caption').children('.title');

        if(newTitle) {
            _title.html(newTitle);
        } else {
            _title.html();
        }

        return true;
    }

    $.Dialog.autoResize = function() {
        if(!$.Dialog.opened || DIALOG == undefined) {
            return false;
        }

        var _content = DIALOG.children(".content");

        var top = ($(window).height() - DIALOG.outerHeight()) / 2;
        var left = ($(window).width() - DIALOG.outerWidth()) / 2;

        DIALOG.css({
            width: _content.outerWidth(),
            height: _content.outerHeight(),
            top: top,
            left: left
        });

        return true;
    }

    $.Dialog.close = function() {
        if(!$.Dialog.opened || DIALOG == undefined) {
            return false;
        }

        $.Dialog.opened = false;
        var _overlay = DIALOG.parent(".dialog-overlay");
        _overlay.fadeOut(function(){
            $(this).remove();
        });

        return false;
    }
})(jQuery);
