$(document).ready(function(){

	var TEMPLATES = false;
	// инициализация шаблонов
	$.ajax({type: "GET", url: "./templates/auth.xml", dataType: "html"})
	.done(
		function (templates) {
			TEMPLATES = $(templates);
		}
	);

	$(".block-info a").hover(
		function() {
			$(this).css("background-color", "#066bc6");
			$(this).css("color", "#FFF");
			var overlay = $(this).prev().prev().find(".overlay");
			overlay.css("background-color", "#066bc6");
		}, function() {
			$(this).css("background-color", "#FFF");
			$(this).css("color", "#066bc6");
			
			var overlay = $(this).prev().prev().find(".overlay");
			overlay.css("background-color", "#5a5a5a");
		}
	);
	
	
	// popun login
	$(".auth .login").click(
		function() {
			var btnLogin = $(this);
		
			$.Dialog({
				position: {
					left: btnLogin.offset().left,
					top: btnLogin.offset().top + btnLogin.height() + 20,
				},
				overlay: false,
				padding: 10,
				content: _.template(TEMPLATES.find("#login").html()),
				onShow: function(_dialog){
					// position dialog
					_dialog.css('left', _dialog.position().left -  _dialog.width() + btnLogin.outerWidth());
				}
			});
		}
	);
	
	// popun reg
	$(".auth .reg").click(
		function() {
			var btnReg = $(this);
		
			$.Dialog({
				position: {
					left: btnReg.offset().left,
					top: btnReg.offset().top + btnReg.height() + 20,
				},
				overlay: false,
				padding: 10,
				content: _.template(TEMPLATES.find("#reg").html()),
				onShow: function(_dialog){
					// position dialog
					_dialog.css('left', _dialog.position().left -  _dialog.width() + btnReg.outerWidth());
				}
			});
		}
	);
});