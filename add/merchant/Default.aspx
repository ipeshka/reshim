﻿<%@ Page Language="C#" EnableViewState="false" EnableSessionState="false" AutoEventWireup="true"
    CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Форма запроса платежа</title>
</head>
<body>
    <form id="paymentForm" runat="server">
        <div>
            <h1>
                Форма запроса платежа</h1>
            <p>
                Эта форма передает запрос с веб-сайта продавца в сервис Web Merchant Interface через
                веб-браузер покупателя.</p>
            <asp:Button ID="GoButton" runat="server" Text="Далее..." PostBackUrl="https://merchant.webmoney.ru/lmi/payment.asp" OnClick="GoButton_Click" />
        </div>
        <asp:HiddenField ID="LMI_PAYEE_PURSE" runat="server" Value="<%$ AppSettings:LMI_PAYEE_PURSE %>" />
        <asp:HiddenField ID="LMI_PAYMENT_AMOUNT" runat="server" Value="<%$ AppSettings:LMI_PAYMENT_AMOUNT %>" />
        <asp:HiddenField ID="LMI_PAYMENT_NO" runat="server" />
        <asp:HiddenField ID="LMI_PAYMENT_DESC" runat="server" Value="<%$ AppSettings:LMI_PAYMENT_DESC %>" />
        <asp:HiddenField ID="LMI_SIM_MODE" runat="server" Value="<%$ AppSettings:LMI_SIM_MODE %>" Visible="False" />
        <asp:HiddenField ID="LMI_RESULT_URL" runat="server" Visible="False" />
        <asp:HiddenField ID="LMI_SUCCESS_URL" runat="server" Value="<%$ AppSettings:LMI_SUCCESS_URL %>" Visible="False" />
        <asp:HiddenField ID="LMI_SUCCESS_METHOD" runat="server" Value="<%$ AppSettings:LMI_SUCCESS_METHOD %>" Visible="False" />
        <asp:HiddenField ID="LMI_FAIL_URL" runat="server" Value="<%$ AppSettings:LMI_FAIL_URL %>" Visible="False" />
        <asp:HiddenField ID="LMI_FAIL_METHOD" runat="server" Value="<%$ AppSettings:LMI_FAIL_METHOD %>" Visible="False" />
        <asp:HiddenField ID="LMI_PAYMENT_CREDITDAYS" runat="server" Value="<%$ AppSettings:LMI_PAYMENT_CREDITDAYS %>" Visible="False" />
    </form>
</body>
</html>
