﻿public partial class Success : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        InfoLiteral.Text = Application["Result"] as string;
    }
}