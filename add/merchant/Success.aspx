﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Success.aspx.cs" Inherits="Success" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Форма выполненного платежа</title>
</head>
<body>
    <form id="successForm" runat="server">
        <div>
            <p>
                Платеж выполнен успешно. <asp:Literal ID="InfoLiteral" runat="server"></asp:Literal></p>
        </div>
    </form>
</body>
</html>
