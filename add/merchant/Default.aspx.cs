﻿using System;

public partial class _Default : System.Web.UI.Page 
{
    protected void GoButton_Click(object sender, EventArgs e)
    {
        int num = (int)Application["Number"];
        Application["Number"] = ++num;
        LMI_PAYMENT_NO.Value = num.ToString();
    }
}