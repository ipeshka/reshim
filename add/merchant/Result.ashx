﻿<%@ WebHandler Language="C#" Class="Result" %>

using System.Security.Cryptography;
using System.Text;
using System.Web;

public class Result : IHttpHandler
{
    public void ProcessRequest (HttpContext context)
    {
        string LMI_PREREQUEST = context.Request.Form["LMI_PREREQUEST"];

        if ("1" == LMI_PREREQUEST)
        {
            context.Response.ContentType = "text/plain";
            // Здесь нужно проверить отосланные сервером данные, если что-то неверно, то просто выйти (не посылать ответ YES)
            context.Response.Write("YES");
        }
        else
        {
            string LMI_HASH = context.Request.Form["LMI_HASH"];
            
            string s = context.Request.Form["LMI_PAYEE_PURSE"] +
                context.Request.Form["LMI_PAYMENT_AMOUNT"] +
                context.Request.Form["LMI_PAYMENT_NO"] +
                context.Request.Form["LMI_MODE"] +
                context.Request.Form["LMI_SYS_INVS_NO"] +
                context.Request.Form["LMI_SYS_TRANS_NO"] +
                context.Request.Form["LMI_SYS_TRANS_DATE"] +
                //context.Request.Form["LMI_SECRET_KEY"] +
                "Здесь нужно вписать секретный ключ (если соединение не SSL, и не отмечено <<Высылать Secret Key на Result URL, если Result URL обеспечивает секретность>>, то секретный ключ не передается)" +
                context.Request.Form["LMI_PAYER_PURSE"] +
                context.Request.Form["LMI_PAYER_WM"];
            
            string hash = GetHashString(s);

            if (string.Compare(LMI_HASH, hash, true) == 0)
            {
                // Здесь нужно передать покупателю товар, к примеру отправить на e-mail
            }
            else { }
        }
    }

    private static string GetHashString(string s)
    {
        byte[] baValue = Encoding.UTF8.GetBytes(s);

        MD5CryptoServiceProvider cryptoServiceProvider = new MD5CryptoServiceProvider();
        byte[] baHash = cryptoServiceProvider.ComputeHash(baValue);

        string result = string.Empty;

        foreach (byte b in baHash)
        {
            result += string.Format("{0:x2}", b);
        }

        return result;
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}