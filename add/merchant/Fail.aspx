﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Fail.aspx.cs" Inherits="Fail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Форма невыполненного платежа</title>
</head>
<body>
    <form id="failForm" runat="server">
        <div>
            <p>Платеж не выполнен.</p>
        </div>
    </form>
</body>
</html>
