﻿using System;
using reshim.common.Attributes;

namespace reshim.model.Entities
{
    public class Withdrawal
    {
        public int Id { get; set; }
        public DateTime Create { get; set; }
        public decimal Amount { get; set; }
        public string Purse { get; set; }
        public string Comment { get; set; }
        public WithdrawalState State { get; set; }
        public PaywayVia PaywayVia { get; set; }
        public int PaymentDataId { get; set; }
        public PaymentData PaymentData { get; set; }
    }

    public enum WithdrawalState
    {
        [NameAttribute("Новый")]
        [TypeAttribute("new")]
        New = 0,

        [NameAttribute("Успешно проведен")]
        [TypeAttribute("success")]
        Success,
    }
}
