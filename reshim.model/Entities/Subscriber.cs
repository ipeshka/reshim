﻿using System;

namespace reshim.model.Entities {
    public class Subscriber {
        public int SubscriberId { get; set; }
        public decimal Value { get; set; }
        public DateTime DateCreate { get; set; }
        public string Comment { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public int TaskId { get; set; }
        public Task Task { get; set; }
    }
}
