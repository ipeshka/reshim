﻿using System;
using reshim.common.Attributes;

namespace reshim.model.Entities
{
    public class Payment
    {
        public int Id { get; set; }
        public DateTime Create { get; set; }
        public decimal Amount { get; set; }

        public PaymentState State { get; set; }
        public Currency Currency { get; set; }
        public PaywayVia PaywayVia { get; set; }

        public int PaymentDataId { get; set; }
        public PaymentData PaymentData { get; set; }
    }

    public enum Currency
    {
        [NameAttribute("Российские рубли")]
        [TypeAttribute("RUB")]
        Rub
    }

    public enum PaywayVia
    {
        [NameAttribute("Webmoney Rub")]
        [CommissionAttribute("0.8%")]
        [TypeAttribute("webmoney_webmoney_merchant_wmr")]
        [EnableWithdrawalAttribute(true)]
        WebmoneyWmr = 0,

        [NameAttribute("Сбербанк ОнЛ@йн")]
        [CommissionAttribute("1%")]
        [TypeAttribute("sberonline_w1_merchant2_rub")]
        SberbankOnlineRub,

        [NameAttribute("Сбербанк")]
        [CommissionAttribute("1%")]
        [TypeAttribute("sbrf_rusbank_receipt_rub")]
        SberbankRub,

        [NameAttribute("Интернет банк \"Связной банк\"")]
        [CommissionAttribute("2%")]
        [TypeAttribute("svyaznoybank_w1_merchant2_rub")]
        SvyaznoyInternetBank,

        [NameAttribute("Тестовая система \"Банк игоряна\"")]
        [CommissionAttribute("2.5%")]
        [TypeAttribute("test_interkassa_test_xts")]
        TestInterkassaTestXts
    }



    public enum PaymentState
    {
        [NameAttribute("Новый")]
        [TypeAttribute("new")]
        New = 0,

        [NameAttribute("Ожидание оплаты")]
        [TypeAttribute("waitAccept")]
        WaitAccept,

        [NameAttribute("Обрабатывается")]
        [TypeAttribute("process")]
        Process,

        [NameAttribute("Успешно проведен")]
        [TypeAttribute("success")]
        Success,

        [NameAttribute("Отменён")]
        [TypeAttribute("canceled")]
        Сanceled,

        [NameAttribute("Не проведён")]
        [TypeAttribute("fail")]
        Fail
    }
}
