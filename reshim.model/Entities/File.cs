﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace reshim.model.Entities
{
    public class File
    {
        [Key]
        public int FileId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        [Required]
        public TypeFile TypeFile { get; set; }
    }

    public enum TypeFile
    {
        [Description("Condition")]
        Condition = 1,
        [Description("Solution")]
        Solution = 2,
        [Description("CompletedTask")]
        CompletedTask = 3
    }
}
