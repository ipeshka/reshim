﻿using System.Collections.Generic;

namespace reshim.model.Entities
{
    public class PaymentData {
        public int Id { get; set; }
        public decimal Ballance { get; set; }
        public User User { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public ICollection<Withdrawal> Withdrawals { get; set; }
        public ICollection<InternalPayment> InternalPayments { get; set; }
    }
}
