﻿using System;

namespace reshim.model.Entities
{
    public class Message {
        public int Id { get; set; }
        public string Content { get; set; }
        public bool IsNew { get; set; }
        public DateTime DateCreate { get; set; }

        public int DialogId { get; set; }
        public Dialog Dialog { get; set; }

        public int SenderId { get; set; }
        public User Sender { get; set; }
        public int RecipientId { get; set; }
        public User Recipient { get; set; }
    }
}
