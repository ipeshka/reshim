﻿
using System;

namespace reshim.model.Entities
{
    public class HistoryCompletedTask
    {
        public int Id { get; set; }
        public DateTime Create { get; set; }

        public PaymentState State { get; set; }
        public Currency Currency { get; set; }
        public PaywayVia PaywayVia { get; set; }

        public int UserId { get; set; }
        public User User { get; set; }
        public int CompletedTaskId { get; set; }
        public CompletedTask CompletedTask { get; set; }
    }
}
