﻿using System;
using System.ComponentModel.DataAnnotations;

namespace reshim.model.Entities {
    public class Profile {
        public Profile()
        {
            DateOfBirth = new DateTime(1990, 1, 1);
        }

        [Key]
        public int ProfileId { get; set; }
        public string Icq { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string UrlVk { get; set; }
        
        public string Country { get; set; }
        public string City { get; set; }
        public string PlaceOfStudy { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Comments { get; set; }

        public User User { get; set; }
    }
}
