﻿using System;
using System.Collections.Generic;
using reshim.common;
using reshim.common.Attributes;

namespace reshim.model.Entities {
    public class User {
        public User() {
            DateCreated = DateTime.Now;
        }

        public int UserId { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Photo { get; set; }
        public string PasswordHash { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime? LastLoginTime { get; set; }
        public bool Activated { get; set; }
        public int IsService { get; set; }
        public string IdService { get; set; }
        public UserRoles Role { get; set; }
        public ICollection<Task> OrderedTasks { get; set; }
        public ICollection<Task> ComplitedTasks { get; set; }
        public ICollection<CompletedTask> ComplitedUserTasks { get; set; }
        public ICollection<HistoryCompletedTask> HistoriesCompletedTasks { get; set; }
        public ICollection<Dialog> Dialogs1 { get; set; }
        public ICollection<Dialog> Dialogs2 { get; set; }
        public ICollection<Message> SenderMessages { get; set; }
        public ICollection<Message> RecipientMessages { get; set; }
        public ICollection<Connection> Connections { get; set; }
        public ICollection<Subscriber> Subscribers { get; set; }
        public string Password {
            set { PasswordHash = Md5Encrypt.Md5EncryptData(value); }
        }

        private string CreateRandomPassword(int passwordLength)
        {
            const string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            var chars = new char[passwordLength];
            var rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public string ResetPassword() {
            var newPass = CreateRandomPassword(7);
            Password = newPass;
            return newPass;
        }
        public string DisplayName {
            get { return FirstName; }
        }

        public bool ValidatePassword(string password) {
            var encoded = Md5Encrypt.Md5EncryptData(password);
            return this.PasswordHash.Equals(encoded);
        }

        public string ConfirmToken {
            get {
                return Md5Encrypt.Md5EncryptData(Email + Login);
            }
        }

        public bool IsPopunForInfoUser
        {
            get
            {
                return string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Login);
            }
        }

        public Profile Profile { get; set; }
        public PaymentData PaymentData { get; set; }

        public bool Equals(User x, User y)
        {
            return x.UserId == y.UserId;
        }
    }

    public enum UserRoles
    {
        [NameAttribute("Администратор")]
        Admin = 1,
        [NameAttribute("Исполнитель")]
        Employee = 2,
        [NameAttribute("Пользователь")]
        User = 3
    }
}
