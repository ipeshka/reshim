﻿using System.Collections.Generic;

namespace reshim.model.Entities
{
    public class CompletedTask
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public decimal Cost { get; set; }
        public string Comment { get; set; }

        public string Condition { get; set; }
        public int SolutionId { get; set; }
        public File Solution { get; set; }
        public int? SubjectId { get; set; }
        public Subject Subject { get; set; }
        public int? DecidedId { get; set; }
        public User Decided { get; set; }
        public ICollection<Tag> Tags { get; set; }

        public ICollection<HistoryCompletedTask> HistoriesCompletedTasks { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}
