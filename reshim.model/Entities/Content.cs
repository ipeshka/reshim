﻿using System;
using System.Collections.Generic;

namespace reshim.model.Entities
{
    public abstract class Content
    {
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime? DateUpdate { get; set; }
        public string RedirectFromUrl { get; set; }
        public int Position { get; set; }
    }

    #region Article
    public class CategoryArticle : Content
    {
        public int? ParentId { get; set; }
        public CategoryArticle Parent { get; set; }
        public ICollection<CategoryArticle> Children { get; set; }
        public ICollection<Article> Articles { get; set; }
    }

    public class Article : Content
    {
        public int? CategoryId { get; set; }
        public CategoryArticle Category { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }

    #endregion

    public class News : Content
    {
    }

    #region Lessons
    public class CategoryLesson : Content
    {
        public int? ParentId { get; set; }
        public CategoryLesson Parent { get; set; }
        public ICollection<CategoryLesson> Children { get; set; }

        public ICollection<NameLesson> NameLessons { get; set; }

        public ICollection<Lesson> Lessons { get; set; }
    }

    public class NameLesson : Content
    {
        public int CategoryId { get; set; }
        public CategoryLesson Category { get; set; }

        public ICollection<Chapter> Chapters { get; set; }
        public ICollection<Lesson> Lessons { get; set; }
    }


    public class Chapter : Content
    {
        public int NameLessonId { get; set; }
        public NameLesson NameLesson { get; set; }

        public ICollection<Lesson> Lessons { get; set; }
    }

    public class Lesson : Content
    {
        public int? ChapterId { get; set; }
        public Chapter Chapter { get; set; }
        public int NameLessonId { get; set; }
        public NameLesson NameLesson { get; set; }
        public int CategoryIdLesson { get; set; }
        public CategoryLesson CategoryLesson { get; set; }
        public ICollection<Review> Reviews { get; set; }

    }
    #endregion

    #region task
    public class Subject : Content
    {

    }

    public class TypeWork : Content
    {
        public string Cost { get; set; }
        public string Period { get; set; }
        public ICollection<SeoArticle> SeoArticles { get; set; }
    }

    public class SeoArticle : Content
    {
        public int TypeWorkId { get; set; }
        public TypeWork TypeWork { get; set; }
    }
    #endregion
}
