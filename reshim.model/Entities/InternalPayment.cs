﻿using System;

namespace reshim.model.Entities
{
    public class InternalPayment
    {
        public InternalPayment()
        {
            DatePayment = DateTime.Now;
        }

        public int Id { get; set; }
        public decimal Amount { get; set; }
        public DateTime DatePayment { get; private set; }

        public int TaskId { get; set; }
        public Task Task { get; set; }
        public int PaymentDataId { get; set; }
        public PaymentData PaymentData { get; set; }
    }
}
