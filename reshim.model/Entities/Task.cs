﻿using System;
using System.Collections.Generic;
using reshim.common.Attributes;

namespace reshim.model.Entities {
    public class Task
    {
        public Task()
        {
            DateCreated = DateTime.Now;
            DateOfExecution = DateTime.Now;
            DateComplited = DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime DateCreated { get; private set; }
        public DateTime DateOfExecution { get; set; }
        public DateTime DateComplited { get; set; }
        public decimal Cost { get; set; }
        public string Comment { get; set; }


        public int ConditionId { get; set; }
        public File Condition { get; set; } // условие
        public int? SolutionId { get; set; }
        public File Solution { get; set; } // решение

        public int CustomerId { get; set; }
        public User Customer { get; set; } // заказчик
        public int? DecidedId { get; set; }
        public User Decided { get; set; } // исполнитель
        public ICollection<Subscriber> Subscribers { get; set; } // подписчики на задание и стоимость
        public StatusTask Status { get; set; } // статус
        public int? SubjectId { get; set; }
        public Subject Subject { get; set; }

        public int? TypeWorkId { get; set; }
        public TypeWork TypeWork { get; set; }

        public ICollection<InternalPayment> InternalsPayments { get; set; }
    }

    public enum StatusTask
    {
        [NameAttribute("Новый")]
        [TypeAttribute("new")]
        New = 1,

        [NameAttribute("Оценен")]
        [TypeAttribute("rating")]
        Rating = 2,

        [NameAttribute("Согласен")]
        [TypeAttribute("agreement")]
        Agreement = 3,

        [NameAttribute("Выполнен")]
        [TypeAttribute("completedNotPay")]
        CompletedNotPay = 4,

        [NameAttribute("Оплачен")]
        [TypeAttribute("completed")]
        Completed = 5,

        [NameAttribute("Не актуальный")]
        [TypeAttribute("notRelevant")]
        [IsDisplayAttribute(false)]
        NotRelevant = 6
    }
}
