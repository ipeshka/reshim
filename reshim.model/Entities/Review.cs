﻿namespace reshim.model.Entities
{
    public class Review
    {
        public int Id { get; set; }

        public string Ip { get; set; }

        public Article Article { get; set; }
        public int? ArticleId { get; set; }

        public Lesson Lesson { get; set; }
        public int? LessonId { get; set; }

        public CompletedTask CompletedTask { get; set; }
        public int? CompletedTaskId { get; set; }
    }
}
