﻿using System.Collections.Generic;

namespace reshim.model.Entities
{
    public class Tag
    {
        public Tag()
        {
            CompletedTasks = new HashSet<CompletedTask>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<CompletedTask> CompletedTasks { get; set; }
    }
}
