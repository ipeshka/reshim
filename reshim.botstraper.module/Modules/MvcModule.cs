﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using reshim.data.commandProcessor.Command;
using reshim.data.commandProcessor.Dispatcher;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Auth;
using Module = Autofac.Module;

namespace reshim.crosscutting.inversionofcontrol.Modules
{
    public class MvcModule : Module
    {
        private readonly Assembly[] _assembliesToScan;

        public MvcModule(params Assembly[] assembliesToScan)
        {
            _assembliesToScan = assembliesToScan;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(_assembliesToScan);
            builder.RegisterType<DefaultCommandBus>().As<ICommandBus>().InstancePerRequest();
            builder.RegisterType<QueryService>().As<IQueryService>().InstancePerRequest();
            builder.RegisterType<DefaultFormsAuthentication>().As<IFormsAuthentication>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<ISql>().InstancePerRequest();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerRequest();

            var services = Assembly.Load("reshim.domain");
            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(ICommandHandler<>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IValidationHandler<>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IQueryHandler<,>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
