﻿using System.Reflection;
using Autofac;
using Autofac.Integration.WebApi;
using reshim.data.commandProcessor.Command;
using reshim.data.commandProcessor.Dispatcher;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.data.infrastructure.State;
using reshim.domain.state.CombinationState.Employee;
using reshim.domain.state.CombinationState.User;
using reshim.domain.state.State;
using reshim.web.core.Auth;
using Module = Autofac.Module;

namespace reshim.crosscutting.inversionofcontrol.Modules
{
    public class ApiModule : Module
    {
        private readonly Assembly[] _assembliesToScan;

        public ApiModule(params Assembly[] assembliesToScan)
        {
            _assembliesToScan = assembliesToScan;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(_assembliesToScan);
            builder.RegisterType<DefaultCommandBus>().As<ICommandBus>().InstancePerRequest();
            builder.RegisterType<QueryService>().As<IQueryService>().InstancePerRequest();
            builder.RegisterType<DefaultFormsAuthentication>().As<IFormsAuthentication>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerRequest();
            builder.RegisterType<UnitOfWork>().As<ISql>().InstancePerRequest();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerRequest();

            // pattern state
            builder.RegisterType<EmployeeNewState>().Named<EmployeeTaskState>("employee-new").InstancePerRequest();
            builder.RegisterType<EmployeeRatingState>().Named<EmployeeTaskState>("employee-rating").InstancePerRequest();
            builder.RegisterType<EmployeeAgrementState>().Named<EmployeeTaskState>("employee-agrement").InstancePerRequest();
            builder.RegisterType<EmployeeNotPayState>().Named<EmployeeTaskState>("employee-not-pay").InstancePerRequest();
            builder.RegisterType<EmployeeComplitedState>().Named<EmployeeTaskState>("employee-complited").InstancePerRequest();

            builder.Register(c => new EmployeeTaskContext(
                    c.Resolve<ITaskRepository>(),
                    c.ResolveNamed<EmployeeTaskState>("employee-new"),
                    c.ResolveNamed<EmployeeTaskState>("employee-rating"),
                    c.ResolveNamed<EmployeeTaskState>("employee-agrement"),
                    c.ResolveNamed<EmployeeTaskState>("employee-not-pay"),
                    c.ResolveNamed<EmployeeTaskState>("employee-complited")
                )).As<IEmployeeTaskContext>().InstancePerRequest();

            builder.RegisterType<UserNewState>().Named<UserTaskState>("user-new").InstancePerRequest();
            builder.RegisterType<UserRatingState>().Named<UserTaskState>("user-rating").InstancePerRequest();
            builder.RegisterType<UserAgrementState>().Named<UserTaskState>("user-agrement").InstancePerRequest();
            builder.RegisterType<UserNotPayState>().Named<UserTaskState>("user-not-pay").InstancePerRequest();
            builder.RegisterType<UserComplitedState>().Named<UserTaskState>("user-complited").InstancePerRequest();

            builder.Register(c => new UserTaskContext(
                    c.Resolve<ITaskRepository>(),
                    c.ResolveNamed<UserTaskState>("user-new"),
                    c.ResolveNamed<UserTaskState>("user-rating"),
                    c.ResolveNamed<UserTaskState>("user-agrement"),
                    c.ResolveNamed<UserTaskState>("user-not-pay"),
                    c.ResolveNamed<UserTaskState>("user-complited")
                )).As<IUserTaskContext>().InstancePerRequest();

            var services = Assembly.Load("reshim.domain");
            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(ICommandHandler<>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IValidationHandler<>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IQueryHandler<,>)).InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerRequest();

        }
    }
}
