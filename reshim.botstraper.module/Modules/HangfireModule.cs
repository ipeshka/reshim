﻿using System.Reflection;
using Autofac;
using Hangfire;
using reshim.data.commandProcessor.Command;
using reshim.data.commandProcessor.Dispatcher;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using Module = Autofac.Module;

namespace reshim.crosscutting.inversionofcontrol.Modules
{
    public class HangfireModule : Module
    {
        private readonly Assembly[] _assembliesToScan;

        public HangfireModule(params Assembly[] assembliesToScan)
        {
            _assembliesToScan = assembliesToScan;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultCommandBus>().As<ICommandBus>().InstancePerDependency();
            builder.RegisterType<QueryService>().As<IQueryService>().InstancePerDependency();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerBackgroundJob();
            builder.RegisterType<UnitOfWork>().As<ISql>().InstancePerBackgroundJob();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerBackgroundJob();

            var services = Assembly.Load("reshim.domain");
            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(ICommandHandler<>)).InstancePerDependency();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IValidationHandler<>)).InstancePerDependency();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IQueryHandler<,>)).InstancePerDependency();

            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerDependency();

        }
    }
}
