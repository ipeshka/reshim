﻿using System;
using System.Configuration;
using System.Reflection;
using Autofac;
using MassTransit;
using Module = Autofac.Module;

namespace reshim.crosscutting.inversionofcontrol.Modules
{
    public class BusModule : Module
    {
        private readonly Assembly[] _assembliesToScan;

        public BusModule(params Assembly[] assembliesToScan)
        {
            _assembliesToScan = assembliesToScan;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterConsumers(_assembliesToScan);

            builder.Register(c => Bus.Factory.CreateUsingRabbitMq(sbc =>
            {
                sbc.Host(new Uri(ConfigurationManager.AppSettings["RabbitMQHost"]), h =>
                {
                    h.Username(ConfigurationManager.AppSettings["RabbitMQUsername"]);
                    h.Password(ConfigurationManager.AppSettings["RabbitMQPassword"]);
                });
            })).As<IBusControl>().As<IBus>().SingleInstance();
        }
    }
}
