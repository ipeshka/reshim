﻿using System.Reflection;
using Autofac;
using reshim.data.commandProcessor.Command;
using reshim.data.commandProcessor.Dispatcher;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Auth;
using Module = Autofac.Module;

namespace reshim.crosscutting.inversionofcontrol.Modules
{
    public class SignalrModule : Module
    {
        private readonly Assembly[] _assembliesToScan;

        public SignalrModule(params Assembly[] assembliesToScan)
        {
            _assembliesToScan = assembliesToScan;
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DefaultCommandBus>().As<ICommandBus>().InstancePerLifetimeScope();
            builder.RegisterType<QueryService>().As<IQueryService>().InstancePerLifetimeScope();
            builder.RegisterType<DefaultFormsAuthentication>().As<IFormsAuthentication>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerLifetimeScope();
            builder.RegisterType<UnitOfWork>().As<ISql>().InstancePerLifetimeScope();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerLifetimeScope();

            var services = Assembly.Load("reshim.domain");
            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(ICommandHandler<>)).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IValidationHandler<>)).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(services)
                .AsClosedTypesOf(typeof(IQueryHandler<,>)).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(UserRepository).Assembly)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}
