﻿namespace reshim.databus.model.contracts.Contracts.Alerts
{
    public class AlertNewTask
    {
        public AlertNewTask(int taskId, string domain)
        {
            TaskId = taskId;
            Domain = domain;
        }

        public int TaskId { get; private set; }
        public string Domain { get; private set; }
    }
}
