﻿namespace reshim.databus.model.contracts.Contracts.SystemMail
{
    public class Registration
    {
        public Registration(string email, string login)
        {
            Email = email;
            Login = login;
        }

        public string Email { get; private set; }
        public string Login { get; private set; }
    }
}