﻿namespace reshim.databus.model.contracts.Contracts.SystemMail
{
    public class SendCompletedTask
    {
        public SendCompletedTask(string email, string login, byte[] file, string filename)
        {
            Email = email;
            Login = login;
            File = file;
            FileName = filename;
        }

        public string Email { get; private set; }
        public string Login { get; private set; }
        public byte[] File { get; private set; }
        public string FileName { get; private set; }
    }
}
