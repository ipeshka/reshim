﻿namespace reshim.databus.model.contracts.Contracts.SystemMail
{
    public class RestoryPassword
    {
        public RestoryPassword(string email, string login, string site)
        {
            Email = email;
            Login = login;
            Site = site;
        }
        public string Email { get; private set; }
        public string Login { get; private set; }
        public string Site { get; private set; }
    }

    public class RestoryPasswordSuccess
    {
        public RestoryPasswordSuccess(string email, string login)
        {
            Email = email;
            Login = login;
        }

        public string Email { get; private set; }
        public string Login { get; private set; }
    }
}
