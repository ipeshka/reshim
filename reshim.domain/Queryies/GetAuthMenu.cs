﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Home;

namespace reshim.domain.Queryies
{
    public class GetAuthMenu : IQuery<AuthMenuViewModel>
    {
        public int UserId { get; set; }
    }
}
