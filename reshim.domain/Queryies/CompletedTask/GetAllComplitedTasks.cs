﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.Queryies.CompletedTask
{
    public class GetAllComplitedTasks : IQuery<IEnumerable<CompletedTaskViewModel>>
    {
    }
}
