﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.Queryies.CompletedTask
{
    public class GetHistoriesCompletedTaskByTaskId : IQuery<IPagedList<HistoryCompletedTaskViewModel>>
    {
        public int TaskId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
