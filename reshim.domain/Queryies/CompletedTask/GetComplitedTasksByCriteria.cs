﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.Queryies.CompletedTask
{
    public class GetComplitedTasksByCriteria : IQuery<IPagedList<CompletedTaskViewModel>>
    {
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
