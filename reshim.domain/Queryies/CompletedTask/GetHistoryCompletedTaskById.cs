﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.Queryies.CompletedTask
{
    public class GetHistoryCompletedTaskById : IQuery<HistoryCompletedTaskViewModel>
    {
        public int Id { get; set; }
    }
}
