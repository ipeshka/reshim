﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.Queryies.CompletedTask
{
    public class GetHistoryCompletedTaskByUserId : IQuery<IPagedList<HistoryCompletedTaskViewModel>>
    {
        public int UserId { get; set; }
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
