﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.User
{
    public class GetUserByLoginOrEmail : IQuery<UserViewModel>
    {
        public string LoginOrEmail { get; set; }
        public bool HasEmail { get; set; }
    }
}
