﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.User
{
    public class GetUserByIdService : IQuery<UserViewModel>
    {
        public string IdService { get; set; }
    }
}
