﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.User
{
    public class GetUserById : IQuery<UserViewModel>, IStarategyFetch<model.Entities.User>
    {
        public int Id { get; set; }

        public IncludeStrategy<model.Entities.User> IncludeProperties { get; set; }
    }
}
