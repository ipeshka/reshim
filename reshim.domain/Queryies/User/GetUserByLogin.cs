﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.User
{
    public class GetUserByLogin : IQuery<UserViewModel>
    {
        public string Login { get; set; }
    }
}
