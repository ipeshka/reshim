﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.User
{
    public class GetStatisticsTaskUser : IQuery<StatisticsViewModel>
    {
        public int UserId { get; set; }
    }
}
