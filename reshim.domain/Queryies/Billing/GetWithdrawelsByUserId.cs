﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Billing;

namespace reshim.domain.Queryies.Billing
{
    /// <summary>
    /// получение списка выводов по идетификатору пользователя
    /// </summary>
    public class GetWithdrawelsByUserId : IQuery<IPagedList<WithdrawalViewModel>>
    {
        public int UserId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
