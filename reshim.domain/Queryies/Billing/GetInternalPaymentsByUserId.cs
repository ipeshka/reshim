﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Billing;

namespace reshim.domain.Queryies.Billing
{
    /// <summary>
    /// получение внутренних переводов по идентификатору пользователя
    /// </summary>
    public class GetInternalPaymentsByUserId : IQuery<IPagedList<InternalPaymentViewModel>>
    {
        public int UserId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
