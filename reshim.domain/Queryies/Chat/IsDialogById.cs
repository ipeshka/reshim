﻿using reshim.data.commandProcessor.Query;

namespace reshim.domain.Queryies.Chat
{
    public class IsDialogById : IQuery<bool>
    {
        public int UserId1 { get; set; }
        public int UserId2 { get; set; }
    }
}
