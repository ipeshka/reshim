﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.Queryies.Chat
{
    public class GetMessageById : IQuery<MessageViewModel>
    {
        public int MessageId { get; set; }
    }
}
