﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.Queryies.Chat
{
    public class GetDialoguesByUserId : IQuery<IEnumerable<DialogViewModel>>
    {
        public int UserId { get; set; }
    }
}
