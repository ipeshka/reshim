﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.Queryies.Chat
{
    public class GetNewMessagesByUserId : IQuery<IEnumerable<MessageViewModel>>
    {
        public int UserId { get; set; }
    }

}
