﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;

namespace reshim.domain.Queryies.Chat
{
    public class GetConectionsByUserId : IQuery<IEnumerable<string>>
    {
        public int UserId { get; set; }
    }
}
