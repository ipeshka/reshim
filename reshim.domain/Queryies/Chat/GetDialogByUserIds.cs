﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.Queryies.Chat
{
    public class GetDialogByUserIds : IQuery<DialogViewModel>
    {
        public int UserId1 { get; set; }
        public int UserId2 { get; set; }
    }
}
