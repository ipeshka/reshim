﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.Queryies.Chat
{
    public class GetMessagesByDialogId : IQuery<ChatViewModel>
    {
        public int DialogId { get; set; }
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
