﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.Queryies.Profile
{
    public class GetProfileByUserId : IQuery<ProfileViewModel>
    {
        public int Id { get; set; }
    }
}
