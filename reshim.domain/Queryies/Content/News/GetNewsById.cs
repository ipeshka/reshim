﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.Queryies.Content.News
{
    public class GetNewsById : IQuery<NewsViewModel>
    {
        public int Id { get; set; }
    }
}
