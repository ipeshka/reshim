﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.Queryies.Content.News
{
    public class GetNewsByAlias : IQuery<NewsViewModel>
    {
        public string Alias { get; set; }
    }
}
