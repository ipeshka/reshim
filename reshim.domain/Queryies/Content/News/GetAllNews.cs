﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.Queryies.Content.News
{
    public class GetAllNews : IQuery<IEnumerable<NewsViewModel>>
    {
    }
}
