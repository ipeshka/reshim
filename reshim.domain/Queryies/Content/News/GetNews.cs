﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.Queryies.Content.News
{
    public class GetNews : IQuery<IPagedList<NewsViewModel>>
    {
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
