﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetChapterById : IQuery<ChapterViewModel>
    {
        public int Id { get; set; }
    }
}
