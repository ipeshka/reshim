﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetAllLessons : IQuery<IEnumerable<LessonViewModel>>
    {
    }
}
