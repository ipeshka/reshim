﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetLessons : IQuery<IPagedList<LessonViewModel>>, IStarategyFetch<model.Entities.Lesson>
    {
        public int? NameLessonId { get; set; }
        public int? ChapterId { get; set; }

        public string NameLessonAlias { get; set; }
        public string ChapterAlias { get; set; }
        public string CategoryAlias { get; set; }

        public int Count { get; set; }
        public int Page { get; set; }

        public bool IsDesc { get; set; }

        public IncludeStrategy<model.Entities.Lesson> IncludeProperties { get; set; }
    }
}
