﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetLessonById : IQuery<LessonViewModel>
    {
        public int Id { get; set; }

    }
}
