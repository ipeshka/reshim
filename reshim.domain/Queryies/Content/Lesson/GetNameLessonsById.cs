﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetNameLessonsById : IQuery<NameLessonsViewModel>, IStarategyFetch<NameLesson>
    {
        public int Id { get; set; }

        public IncludeStrategy<NameLesson> IncludeProperties { get; set; }
    }
}
