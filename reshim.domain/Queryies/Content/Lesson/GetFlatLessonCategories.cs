﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetFlatLessonCategories : IQuery<IEnumerable<CategoryLessonsViewModel>>
    {
        public IEnumerable<CategoryLessonsViewModel> Categories { get; set; }
    }
}
