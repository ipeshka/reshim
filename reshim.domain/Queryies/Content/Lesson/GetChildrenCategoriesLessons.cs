﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.Queryies.Content.Lesson
{
    public class GetChildrenCategoriesLessons : IQuery<IPagedList<CategoryLessonsViewModel>>
    {
        public int ParentId { get; set; }
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
