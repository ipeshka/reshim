﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.Queryies.Content.SeoArticle
{
    public class GetSeoArticles : IQuery<IPagedList<SeoArticleViewModel>>
    {
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
