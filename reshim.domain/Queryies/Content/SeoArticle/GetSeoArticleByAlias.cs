﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.Queryies.Content.SeoArticle
{
    public class GetSeoArticleByAlias : IQuery<SeoArticleViewModel>
    {
        public string Alias { get; set; }
    }
}
