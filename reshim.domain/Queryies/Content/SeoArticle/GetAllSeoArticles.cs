﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.Queryies.Content.SeoArticle
{
    public class GetAllSeoArticles : IQuery<IEnumerable<SeoArticleViewModel>>
    {
    }
}
