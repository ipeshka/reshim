﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.Queryies.Content.SeoArticle
{
    public class GetSeoArticleById : IQuery<SeoArticleViewModel>
    {
        public int Id { get; set; }
    }
}
