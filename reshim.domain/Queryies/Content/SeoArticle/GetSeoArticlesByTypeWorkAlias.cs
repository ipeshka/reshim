﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.Queryies.Content.SeoArticle
{
    public class GetSeoArticlesByTypeWorkAlias : IQuery<IEnumerable<SeoArticleViewModel>>
    {
        public string AliasTypeWork { get; set; }
    }
}
