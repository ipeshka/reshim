﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.Queryies.Content.Subject
{
    public class GetSubjects : IQuery<IPagedList<SubjectViewModel>>
    {
        public int Page { get; set; }
        public int Count { get; set; }
    }
}
