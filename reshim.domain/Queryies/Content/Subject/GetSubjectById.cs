﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.Queryies.Content.Subject
{
    public class GetSubjectById : IQuery<SubjectViewModel>
    {
        public int Id { get; set; }
    }
}
