﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.Queryies.Content.Subject
{
    public class GetAllSubjects : IQuery<IEnumerable<SubjectViewModel>>
    {
    }
}
