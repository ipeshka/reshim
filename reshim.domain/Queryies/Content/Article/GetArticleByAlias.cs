﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetArticleByAlias : IQuery<ArticleViewModel>
    {
        public string Alias { get; set; }
    }
}
