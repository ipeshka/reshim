﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetArticleCategoryById : IQuery<CategoryArticleViewModel>, IStarategyFetch<model.Entities.CategoryArticle>
    {
        public int Id { get; set; }

        public IncludeStrategy<model.Entities.CategoryArticle> IncludeProperties { get; set; }
    }
}
