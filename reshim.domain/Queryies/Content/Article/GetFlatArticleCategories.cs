﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetFlatArticleCategories : IQuery<IEnumerable<CategoryArticleViewModel>>
    {
        public IEnumerable<CategoryArticleViewModel> Categories { get; set; }
    }
}
