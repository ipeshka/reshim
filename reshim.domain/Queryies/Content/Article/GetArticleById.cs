﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetArticleById : IQuery<ArticleViewModel>
    {
        public int Id { get; set; }
    }
}
