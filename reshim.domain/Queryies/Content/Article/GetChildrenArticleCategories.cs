﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetChildrenArticleCategories: IQuery<IPagedList<CategoryArticleViewModel>>
    {
        public int ParentId { get; set; }
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
