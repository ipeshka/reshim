﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetCategoryArticleByAlias : IQuery<CategoryArticleViewModel>
    {
        public string Alias { get; set; }
    }
}
