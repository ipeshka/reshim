﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.Queryies.Content.Article
{
    public class GetAllTopArticleCategories : IQuery<IEnumerable<CategoryArticleViewModel>>
    {
    }
}
