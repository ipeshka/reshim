﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.Queryies.Content.TypeWork
{
    public class GetTypeWorkByAlias : IQuery<TypeWorkViewModel>
    {
        public string Alias { get; set; }
    }
}
