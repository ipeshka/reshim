﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.Queryies.Content.TypeWork
{
    public class GetTypeWorks : IQuery<IPagedList<TypeWorkViewModel>>
    {
        public int Count { get; set; }
        public int Page { get; set; }
    }
}
