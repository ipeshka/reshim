﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.Queryies.Content.TypeWork
{
    public class GetTypeWorkById : IQuery<TypeWorkViewModel>
    {
        public int Id { get; set; }
    }
}
