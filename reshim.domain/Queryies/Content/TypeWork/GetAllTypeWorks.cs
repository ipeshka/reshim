﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.Queryies.Content.TypeWork
{
    public class GetAllTypeWorks : IQuery<IEnumerable<TypeWorkViewModel>>
    {
    }
}
