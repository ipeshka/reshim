﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.Queryies.Task
{
    public class GetTasksByCriteria : IQuery<TasksViewModelWithCriteria>, IStarategyFetch<model.Entities.Task>
    {
        public int Page { get; set; }
        public string Status { get; set; }
        public bool IsMyTask { get; set; }
        public int UserId { get; set; }
        public int Count { get; set; }
        public IncludeStrategy<model.Entities.Task> IncludeProperties { get; set; }
    }
}
