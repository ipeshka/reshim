﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.Queryies.Task
{
    public class GetLastTask : IQuery<IEnumerable<TaskViewModel>>
    {
        public int CountLast { get; set; }
        public StatusTask Status { get; set; }
    }
}
