﻿using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.Queryies.Task
{
    public class CheckOrderConfirmationTask : IQuery<OrderConfirmationTaskReturnedModel>
    {
        public int Id { get; set; }
    }
}
