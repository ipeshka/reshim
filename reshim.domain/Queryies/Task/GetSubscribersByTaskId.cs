﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.Queryies.Task
{
    public class GetSubscribersByTaskId : IQuery<IEnumerable<SubscriberViewModel>>
    {
        public int Id { get; set; }
    }
}
