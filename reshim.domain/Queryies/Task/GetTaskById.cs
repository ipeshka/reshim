﻿using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.IncludeStrategy;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.Queryies.Task
{
    public class GetTaskById : IQuery<TaskViewModel>, IStarategyFetch<model.Entities.Task>
    {
        public int Id { get; set; }

        public IncludeStrategy<model.Entities.Task> IncludeProperties { get; set; }
    }
}
