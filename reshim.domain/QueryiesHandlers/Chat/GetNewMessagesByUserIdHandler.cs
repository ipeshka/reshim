﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetNewMessagesByUserIdHandler : IQueryHandler<GetNewMessagesByUserId, IEnumerable<MessageViewModel>>
    {
        private readonly IMessageRepository _messageRepository;

        public GetNewMessagesByUserIdHandler(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public IEnumerable<MessageViewModel> Execute(GetNewMessagesByUserId query)
        {
            var messages = _messageRepository.AllIncluding(x => x.Recipient, x => x.Sender)
                .OrderByDescending(x => x.DateCreate)
                .Where(x => x.RecipientId == query.UserId && x.IsNew);

            return messages.Select(Mapper.Map<Message, MessageViewModel>).ToList();
        }
    }
}
