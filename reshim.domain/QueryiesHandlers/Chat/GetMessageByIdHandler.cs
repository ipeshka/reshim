﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetMessageByIdHandler : IQueryHandler<GetMessageById, MessageViewModel>
    {
        private readonly IMessageRepository _messageRepository;

        public GetMessageByIdHandler(IMessageRepository messageRepository)
        {
            _messageRepository = messageRepository;
        }

        public MessageViewModel Execute(GetMessageById query)
        {
            var curMessage = _messageRepository.AllIncluding(x => x.Sender)
                .First(x => x.Id == query.MessageId);

            return Mapper.Map<Message, MessageViewModel>(curMessage);
        }
    }
}
