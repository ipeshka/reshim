﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetConectionsByUserIdHandler : IQueryHandler<GetConectionsByUserId, IEnumerable<string>>
    {
        private readonly IConnectionRepository _connectionRepository;

        public GetConectionsByUserIdHandler(IConnectionRepository connectionRepository)
        {
            _connectionRepository = connectionRepository;
        }

        public IEnumerable<string> Execute(GetConectionsByUserId query)
        {
            var connections = _connectionRepository.GetMany(x => x.UserId == query.UserId && x.Connected)
                .Select(x => x.ConnectionId)
                .ToList();

            return connections;
        }
    }
}
