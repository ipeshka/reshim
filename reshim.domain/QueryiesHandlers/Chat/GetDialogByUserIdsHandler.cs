﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetDialogByUserIdsHandler : IQueryHandler<GetDialogByUserIds, DialogViewModel>
    {
        private readonly IDialogRepository _dialogRepository;

        public GetDialogByUserIdsHandler(IDialogRepository dialogRepository)
        {
            _dialogRepository = dialogRepository;
        }

        public DialogViewModel Execute(GetDialogByUserIds query)
        {
            var dialog = _dialogRepository.Get(x => (x.User1Id == query.UserId1 && x.User2Id == query.UserId2) ||
                (x.User1Id == query.UserId2 && x.User2Id == query.UserId1));

            return Mapper.Map<Dialog, DialogViewModel>(dialog);
        }
    }
}
