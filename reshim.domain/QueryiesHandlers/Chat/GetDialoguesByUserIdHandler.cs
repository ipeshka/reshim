﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetDialoguesByUserIdHandler : IQueryHandler<GetDialoguesByUserId, IEnumerable<DialogViewModel>>
    {
        private readonly IDialogRepository _dialogRepository;

        public GetDialoguesByUserIdHandler(IDialogRepository dialogRepository)
        {
            _dialogRepository = dialogRepository;
        }

        public IEnumerable<DialogViewModel> Execute(GetDialoguesByUserId query)
        {
            var contextDialog = _dialogRepository.AllIncluding(x => x.User1, x => x.User2, x => x.Messages);

            var dialogs = contextDialog
                .Where(x => (x.User1Id == query.UserId || x.User2Id == query.UserId) && x.Messages.Any())
                .OrderByDescending(x => x.LastUpdate).ToList();

            return dialogs.Select(Mapper.Map<Dialog, DialogViewModel>);
        }
    }
}
