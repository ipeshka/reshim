﻿using System.Linq;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Database;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class IsDialogByIdHandler : IQueryHandler<IsDialogById, bool>
    {
        private readonly ISql _sql;

        public IsDialogByIdHandler(ISql sql)
        {
            _sql = sql;
        }

        public bool Execute(IsDialogById query)
        {
            var count = _sql.ExecuteQuery<int>("SELECT COUNT(Id) FROM dbo.Dialogues WHERE (User1Id = @p0 AND User2Id = @p1) OR (User2Id = @p0 AND User1Id = @p1)", query.UserId1, query.UserId2).ToList();
            return count.Sum() > 0;
        }
    }
}
