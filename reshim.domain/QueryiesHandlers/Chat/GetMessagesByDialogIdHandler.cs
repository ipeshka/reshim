﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Chat;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Chat;

namespace reshim.domain.QueryiesHandlers.Chat
{
    public class GetMessagesByDialogIdHandler : IQueryHandler<GetMessagesByDialogId, ChatViewModel>
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IDialogRepository _dialogRepository;

        public GetMessagesByDialogIdHandler(IMessageRepository messageRepository, IDialogRepository dialogRepository)
        {
            _messageRepository = messageRepository;
            _dialogRepository = dialogRepository;
        }

        public ChatViewModel Execute(GetMessagesByDialogId query)
        {
            int p = query.Page - 1;
            var messages = _messageRepository.AllIncluding(x => x.Sender)
                .Where(x => x.DialogId == query.DialogId)
                .OrderByDescending(x => x.DateCreate)
                .Skip(p * query.Count)
                .Take(query.Count);

            var dialog = _dialogRepository.GetById(query.DialogId);

            return new ChatViewModel()
            {
                Messages = messages.Select(Mapper.Map<Message, MessageViewModel>).Reverse().ToList(),
                User1Id = dialog.User1Id,
                User2Id = dialog.User2Id,
                DialogId = dialog.Id
            };
        }
    }
}
