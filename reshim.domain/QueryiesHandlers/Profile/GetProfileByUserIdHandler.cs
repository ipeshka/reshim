﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Profile;
using reshim.model.view.ViewModels.User;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.QueryiesHandlers.Profile
{
    public class GetProfileByUserIdHandler : IQueryHandler<GetProfileByUserId, ProfileViewModel>
    {
        private readonly IProfileRepository _profileRepository;

        public GetProfileByUserIdHandler(IProfileRepository profileRepository)
        {
            _profileRepository = profileRepository;
        }

        public ProfileViewModel Execute(GetProfileByUserId query)
        {
            var profile = _profileRepository.GetById(query.Id);
            return Mapper.Map<model.Entities.Profile, ProfileViewModel>(profile);
        }
    }
}
