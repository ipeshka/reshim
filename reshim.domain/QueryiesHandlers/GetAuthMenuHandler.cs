﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Home;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.QueryiesHandlers
{
    public class GetAuthMenuHandler : IQueryHandler<GetAuthMenu, AuthMenuViewModel>
    {
        private readonly IMessageRepository _messageRepository;
        private readonly IUserRepository _userRepository;

        public GetAuthMenuHandler(IMessageRepository messageRepository, IUserRepository userRepository)
        {
            _messageRepository = messageRepository;
            _userRepository = userRepository;
        }

        public AuthMenuViewModel Execute(GetAuthMenu query)
        {
            var modelView = new AuthMenuViewModel();
            var countNewMessage = _messageRepository.Count(x => x.IsNew && x.RecipientId == query.UserId);
            modelView.User = Mapper.Map<model.Entities.User, UserViewModel>(_userRepository.AllIncluding(x => x.PaymentData, x => x.Profile).First(x => x.UserId == query.UserId));
            modelView.CountNewMessage = countNewMessage;
            return modelView;
        }
    }
}
