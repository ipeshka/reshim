﻿using System;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies;
using reshim.model.view.ViewModels.Home;
using reshim.model.view.ViewModels.User;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.QueryiesHandlers
{
    public class GetStatisticsSystemHandler : IQueryHandler<GetStatisticsSystem, StatisticsSystemViewModel>
    {
        private readonly IConnectionRepository _connectionRepository;
        private readonly IUserRepository _userRepository;
        private readonly ISql _sql;

        public GetStatisticsSystemHandler(IConnectionRepository connectionRepository, IUserRepository userRepository, ISql sql)
        {
            _connectionRepository = connectionRepository;
            _userRepository = userRepository;
            _sql = sql;
        }

        public StatisticsSystemViewModel Execute(GetStatisticsSystem query)
        {
            var modelView = new StatisticsSystemViewModel();
            const int diffMinute = 10;

            var connection =
                _connectionRepository.AllIncluding(x => x.User)
                    .Where(x => (x.Connected && DbFunctions.DiffMinutes(x.Date, DateTime.Now) < diffMinute) || DbFunctions.DiffMinutes(x.Date, DateTime.Now) < diffMinute).Distinct();

            modelView.OnlineUsers = connection.Select(x => x.User).Distinct().Where(x => !string.IsNullOrEmpty(x.Login)).Select(Mapper.Map<model.Entities.User, UserViewModel>).ToList();
            modelView.CountComplitedTasks = _sql.ExecuteQuery<int>("SELECT COUNT(*) FROM dbo.Tasks WHERE Status = @p0 OR Status = @p1", StatusTask.Completed, StatusTask.CompletedNotPay).First();
            modelView.CountTasks = _sql.ExecuteQuery<int>("SELECT COUNT(*) FROM dbo.Tasks").First();
            modelView.CountUsers = _sql.ExecuteQuery<int>("SELECT COUNT(*) FROM dbo.Users").First();
            modelView.OnlineUsers.AddRange(_userRepository.GetMany(x => x.UserId < 10).Select(Mapper.Map<model.Entities.User, UserViewModel>).ToList());
            modelView.LastRegisterUser = Mapper.Map<model.Entities.User, UserViewModel>(_userRepository.GetAll().Last(x => !string.IsNullOrEmpty(x.Login)));

            return modelView;
        }
    }
}
