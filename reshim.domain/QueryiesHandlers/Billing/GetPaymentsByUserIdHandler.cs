﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Billing;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.domain.QueryiesHandlers.Billing
{
    public class GetPaymentsByUserIdHandler : IQueryHandler<GetPaymentsByUserId, IPagedList<PaymentViewModel>>
    {
        private readonly IPaymentRepository _paymentRepository;

        public GetPaymentsByUserIdHandler(IPaymentRepository paymentRepository)
        {
            _paymentRepository = paymentRepository;
        }

        public IPagedList<PaymentViewModel> Execute(GetPaymentsByUserId query)
        {
            var count = _paymentRepository.Count(x => x.PaymentDataId == query.UserId);
            var payments = _paymentRepository.GetMany(x => x.PaymentDataId == query.UserId)
                .OrderByDescending(x => x.Create)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count);

            var paymentsView = payments.Select(Mapper.Map<Payment, PaymentViewModel>).ToList();
            return new PagedList<PaymentViewModel>(paymentsView, query.Page - 1, query.Count, count);
        }
    }
}
