﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Billing;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.domain.QueryiesHandlers.Billing
{
    public class GetWithdrawelsByUserIdHandler : IQueryHandler<GetWithdrawelsByUserId, IPagedList<WithdrawalViewModel>>
    {
        private readonly IWithdrawalRepository _withdrawalRepository;

        public GetWithdrawelsByUserIdHandler(IWithdrawalRepository withdrawalRepository)
        {
            _withdrawalRepository = withdrawalRepository;
        }

        public IPagedList<WithdrawalViewModel> Execute(GetWithdrawelsByUserId query)
        {
            var count = _withdrawalRepository.Count(x => x.PaymentDataId == query.UserId);
            var withdrawals = _withdrawalRepository.GetMany(x => x.PaymentDataId == query.UserId)
                .OrderByDescending(x => x.Create)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count);

            var withdrawalsView = withdrawals.Select(Mapper.Map<Withdrawal, WithdrawalViewModel>).ToList();
            return new PagedList<WithdrawalViewModel>(withdrawalsView, query.Page - 1, query.Count, count);
        }
    }
}
