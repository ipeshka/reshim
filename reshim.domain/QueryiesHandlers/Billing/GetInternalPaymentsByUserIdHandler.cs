﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Billing;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Billing;

namespace reshim.domain.QueryiesHandlers.Billing
{
    public class GetInternalPaymentsByUserIdHandler : IQueryHandler<GetInternalPaymentsByUserId, IPagedList<InternalPaymentViewModel>>
    {
        private readonly IInternalPaymentRepository _insidePaymentRepository;

        public GetInternalPaymentsByUserIdHandler(IInternalPaymentRepository insidePaymentRepository)
        {
            _insidePaymentRepository = insidePaymentRepository;
        }

        public IPagedList<InternalPaymentViewModel> Execute(GetInternalPaymentsByUserId query)
        {
            var count = _insidePaymentRepository.Count(x => x.PaymentDataId == query.UserId);
            var payments = _insidePaymentRepository.GetMany(x => x.PaymentDataId == query.UserId)
                .OrderByDescending(x => x.DatePayment)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count);

            var paymentsView = payments.Select(Mapper.Map<InternalPayment, InternalPaymentViewModel>).ToList();
            return new PagedList<InternalPaymentViewModel>(paymentsView, query.Page - 1, query.Count, count);
        }
    }
}
