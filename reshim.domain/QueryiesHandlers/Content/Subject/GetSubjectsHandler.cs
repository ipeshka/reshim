﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Subject;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.QueryiesHandlers.Content.Subject
{
    public class GetSubjectsHandler : IQueryHandler<GetSubjects, IPagedList<SubjectViewModel>>
    {
        private readonly ISubjectRepository _subjectRepository;

        public GetSubjectsHandler(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public IPagedList<SubjectViewModel> Execute(GetSubjects query)
        {
            var count = _subjectRepository.Count();
            var subjects = _subjectRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count).ToList();

            var subjectsView = subjects.Select(Mapper.Map<model.Entities.Subject, SubjectViewModel>);
            return new PagedList<SubjectViewModel>(subjectsView, query.Page - 1, query.Count, count);
        }
    }
}
