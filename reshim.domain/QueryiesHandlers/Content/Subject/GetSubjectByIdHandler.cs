﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Subject;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.QueryiesHandlers.Content.Subject
{
    public class GetSubjectByIdHandler : IQueryHandler<GetSubjectById, SubjectViewModel>
    {
        private readonly ISubjectRepository _subjectRepository;

        public GetSubjectByIdHandler(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public SubjectViewModel Execute(GetSubjectById query)
        {
            var subject = _subjectRepository.GetById(query.Id);
            return Mapper.Map<model.Entities.Subject, SubjectViewModel>(subject);
        }
    }
}
