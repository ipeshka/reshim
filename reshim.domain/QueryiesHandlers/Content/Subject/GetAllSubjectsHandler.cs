﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Subject;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Subject;

namespace reshim.domain.QueryiesHandlers.Content.Subject
{
    public class GetAllSubjectsHandler : IQueryHandler<GetAllSubjects, IEnumerable<SubjectViewModel>>
    {
        private readonly ISubjectRepository _subjectRepository;

        public GetAllSubjectsHandler(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public IEnumerable<SubjectViewModel> Execute(GetAllSubjects query)
        {
            var subjects = _subjectRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate)
                .ToList();

            var viewSubjects = subjects.Select(Mapper.Map<model.Entities.Subject, SubjectViewModel>);
            return viewSubjects;

        }
    }
}
