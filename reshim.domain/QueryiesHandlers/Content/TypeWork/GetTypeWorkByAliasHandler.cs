﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.QueryiesHandlers.Content.TypeWork
{
    public class GetTypeWorkByAliasHandler : IQueryHandler<GetTypeWorkByAlias, TypeWorkViewModel>
    {
        private readonly ITypeWorkRepository _typeWorkRepository;

        public GetTypeWorkByAliasHandler(ITypeWorkRepository typeWorkRepository)
        {
            _typeWorkRepository = typeWorkRepository;
        }

        public TypeWorkViewModel Execute(GetTypeWorkByAlias query)
        {
            var typeWork = _typeWorkRepository.Get(x=>x.Alias == query.Alias);
            return Mapper.Map<model.Entities.TypeWork, TypeWorkViewModel>(typeWork);
        }
    }
}
