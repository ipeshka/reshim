﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.QueryiesHandlers.Content.TypeWork
{
    public class GetTypeWorkByIdHandler : IQueryHandler<GetTypeWorkById, TypeWorkViewModel>
    {
        private readonly ITypeWorkRepository _typeWorkRepository;

        public GetTypeWorkByIdHandler(ITypeWorkRepository typeWorkRepository)
        {
            _typeWorkRepository = typeWorkRepository;
        }

        public TypeWorkViewModel Execute(GetTypeWorkById query)
        {
            var typeWork = _typeWorkRepository.GetById(query.Id);
            return Mapper.Map<model.Entities.TypeWork, TypeWorkViewModel>(typeWork);
        }
    }
}
