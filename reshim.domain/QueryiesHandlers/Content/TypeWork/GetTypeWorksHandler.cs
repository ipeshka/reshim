﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.QueryiesHandlers.Content.TypeWork
{
    public class GetTypeWorksHandler : IQueryHandler<GetTypeWorks, IPagedList<TypeWorkViewModel>>
    {
        private readonly ITypeWorkRepository _typeWorkRepository;

        public GetTypeWorksHandler(ITypeWorkRepository typeWorkRepository)
        {
            _typeWorkRepository = typeWorkRepository;
        }

        public IPagedList<TypeWorkViewModel> Execute(GetTypeWorks query)
        {
            var count = _typeWorkRepository.Count();
            var typeWorks = _typeWorkRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count).ToList();

            var viewTypeWorks = typeWorks.Select(Mapper.Map<model.Entities.TypeWork, TypeWorkViewModel>);

            return new PagedList<TypeWorkViewModel>(viewTypeWorks, query.Page - 1, query.Count, count);
        }
    }
}
