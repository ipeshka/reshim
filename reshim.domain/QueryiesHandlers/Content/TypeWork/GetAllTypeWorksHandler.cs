﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.TypeWork;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.TypeWork;

namespace reshim.domain.QueryiesHandlers.Content.TypeWork
{
    public class GetAllTypeWorksHandler : IQueryHandler<GetAllTypeWorks, IEnumerable<TypeWorkViewModel>>
    {
        private readonly ITypeWorkRepository _typeWorkRepository;

        public GetAllTypeWorksHandler(ITypeWorkRepository typeWorkRepository)
        {
            _typeWorkRepository = typeWorkRepository;
        }

        public IEnumerable<TypeWorkViewModel> Execute(GetAllTypeWorks query)
        {
            var typeWorks = _typeWorkRepository.GetAll();
            return typeWorks.Select(Mapper.Map<model.Entities.TypeWork, TypeWorkViewModel>);
        }
    }
}
