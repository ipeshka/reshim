﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.News;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.QueryiesHandlers.Content.News
{
    public class GetAllNewsHandler: IQueryHandler<GetAllNews, IEnumerable<NewsViewModel>>
    {
        private readonly INewsRepository _newsRepository;

        public GetAllNewsHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public IEnumerable<NewsViewModel> Execute(GetAllNews query)
        {
            var news = _newsRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate).ToList();

            return news.Select(Mapper.Map<model.Entities.News, NewsViewModel>);
        }
    }
}
