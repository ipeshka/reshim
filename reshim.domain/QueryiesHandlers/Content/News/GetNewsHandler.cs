﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.News;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.QueryiesHandlers.Content.News
{
    public class GetNewsHandler : IQueryHandler<GetNews, IPagedList<NewsViewModel>>
    {
        private readonly INewsRepository _newsRepository;

        public GetNewsHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public IPagedList<NewsViewModel> Execute(GetNews query)
        {
            var count = _newsRepository.Count();
            var news = _newsRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var newsView = news.Select(Mapper.Map<model.Entities.News, NewsViewModel>);
            return new PagedList<NewsViewModel>(newsView, query.Page - 1, query.Count, count);
        }
    }
}
