﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.News;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.QueryiesHandlers.Content.News
{
    public class GetNewsByIdHandler : IQueryHandler<GetNewsById, NewsViewModel>
    {
        private readonly INewsRepository _newsRepository;

        public GetNewsByIdHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public NewsViewModel Execute(GetNewsById query)
        {
            var news = _newsRepository.GetById(query.Id);
            return Mapper.Map<model.Entities.News, NewsViewModel>(news);
        }
    }
}
