﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.News;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.QueryiesHandlers.Content.News
{
    public class GetNewsByAliasHandler : IQueryHandler<GetNewsByAlias, NewsViewModel>
    {
        private readonly INewsRepository _newsRepository;

        public GetNewsByAliasHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public NewsViewModel Execute(GetNewsByAlias query)
        {
            var news = _newsRepository.Get(x => x.Alias == query.Alias);
            return Mapper.Map<model.Entities.News, NewsViewModel>(news);
        }
    }
}
