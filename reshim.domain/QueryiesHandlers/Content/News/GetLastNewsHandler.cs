﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.News;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.News;

namespace reshim.domain.QueryiesHandlers.Content.News
{
    public class GetLastNewsHandler : IQueryHandler<GetLastNews, IEnumerable<NewsViewModel>>
    {
        private readonly INewsRepository _newsRepository;

        public GetLastNewsHandler(INewsRepository newsRepository)
        {
            _newsRepository = newsRepository;
        }

        public IEnumerable<NewsViewModel> Execute(GetLastNews query)
        {
            var news = _newsRepository.GetDataContext()
                .OrderByDescending(x => x.DateCreate)
                .Take(query.LastCount)
                .ToList();

            return news.Select(Mapper.Map<model.Entities.News, NewsViewModel>);
        }
    }
}
