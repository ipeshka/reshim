﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.QueryiesHandlers.Content.SeoArticle
{
    public class GetSeoArticleByAliasHandler : IQueryHandler<GetSeoArticleByAlias, SeoArticleViewModel>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;

        public GetSeoArticleByAliasHandler(ISeoArticleRepository seoArticleRepository)
        {
            _seoArticleRepository = seoArticleRepository;
        }

        public SeoArticleViewModel Execute(GetSeoArticleByAlias query)
        {
            var seoArticle = _seoArticleRepository.AllIncluding(x => x.TypeWork).FirstOrDefault(x => x.Alias == query.Alias);
            return Mapper.Map<model.Entities.SeoArticle, SeoArticleViewModel>(seoArticle);
        }
    }
}
