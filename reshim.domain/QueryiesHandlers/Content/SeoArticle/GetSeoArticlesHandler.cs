﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.QueryiesHandlers.Content.SeoArticle
{
    public class GetSeoArticlesHandler : IQueryHandler<GetSeoArticles, IPagedList<SeoArticleViewModel>>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;

        public GetSeoArticlesHandler(ISeoArticleRepository seoArticleRepository)
        {
            _seoArticleRepository = seoArticleRepository;
        }

        public IPagedList<SeoArticleViewModel> Execute(GetSeoArticles query)
        {
            IQueryable<model.Entities.SeoArticle> request = _seoArticleRepository.AllIncluding(x => x.TypeWork);

            var count = _seoArticleRepository.Count();
            var typeWorks = request
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count).ToList();

            var typeWorksView = typeWorks.Select(Mapper.Map<model.Entities.SeoArticle, SeoArticleViewModel>);
            return new PagedList<SeoArticleViewModel>(typeWorksView, query.Page - 1, query.Count, count);
        }
    }
}
