﻿using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.QueryiesHandlers.Content.SeoArticle
{
    public class GetSeoArticleByIdHandler : IQueryHandler<GetSeoArticleById, SeoArticleViewModel>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;

        public GetSeoArticleByIdHandler(ISeoArticleRepository seoArticleRepository)
        {
            _seoArticleRepository = seoArticleRepository;
        }

        public SeoArticleViewModel Execute(GetSeoArticleById query)
        {
            var seoArticle = _seoArticleRepository.GetById(query.Id);
            return Mapper.Map<model.Entities.SeoArticle, SeoArticleViewModel>(seoArticle);
        }
    }
}
