﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.QueryiesHandlers.Content.SeoArticle
{
    public class GetSeoArticlesByTypeWorkAliasHandler : IQueryHandler<GetSeoArticlesByTypeWorkAlias, IEnumerable<SeoArticleViewModel>>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;

        public GetSeoArticlesByTypeWorkAliasHandler(ISeoArticleRepository seoArticleRepository)
        {
            _seoArticleRepository = seoArticleRepository;
        }

        public IEnumerable<SeoArticleViewModel> Execute(GetSeoArticlesByTypeWorkAlias query)
        {
            IQueryable<model.Entities.SeoArticle> request = _seoArticleRepository.AllIncluding(x => x.TypeWork);

            var seoArticles = request
                .Where(x => x.TypeWork.Alias == query.AliasTypeWork)
                .ToList();

            return seoArticles.Select(Mapper.Map<model.Entities.SeoArticle, SeoArticleViewModel>);
        }
    }
}
