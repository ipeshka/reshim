﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.SeoArticle;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.SeoArticle;

namespace reshim.domain.QueryiesHandlers.Content.SeoArticle
{
    public class GetAllSeoArticlesHandler : IQueryHandler<GetAllSeoArticles, IEnumerable<SeoArticleViewModel>>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;

        public GetAllSeoArticlesHandler(ISeoArticleRepository seoArticleRepository)
        {
            _seoArticleRepository = seoArticleRepository;
        }

        public IEnumerable<SeoArticleViewModel> Execute(GetAllSeoArticles query)
        {
            IQueryable<model.Entities.SeoArticle> request = _seoArticleRepository.GetDataContext();;

            var seoArticles = request
                .OrderByDescending(x => x.DateCreate).ToList();

            var seoArticlesView = seoArticles.Select(Mapper.Map<model.Entities.SeoArticle, SeoArticleViewModel>).ToList();
            return seoArticlesView;
        }
    }
}
