﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetArticleCategoryByIdHandler : IQueryHandler<GetArticleCategoryById, CategoryArticleViewModel>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;

        public GetArticleCategoryByIdHandler(ICategoryArticleRepository categoryArticleRepository)
        {
            _categoryArticleRepository = categoryArticleRepository;
        }

        public CategoryArticleViewModel Execute(GetArticleCategoryById query)
        {
            IQueryable<CategoryArticle> request = query.IncludeProperties != null ? 
                query.IncludeProperties.ApplyTo(_categoryArticleRepository.GetDataContext()) : 
                _categoryArticleRepository.GetDataContext();

            var category = request.FirstOrDefault(x => x.Id == query.Id);
            return Mapper.Map<CategoryArticle, CategoryArticleViewModel>(category);
        }
    }
}
