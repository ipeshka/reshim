﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetCategoryArticleByAliasHandler : IQueryHandler<GetCategoryArticleByAlias, CategoryArticleViewModel>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;

        public GetCategoryArticleByAliasHandler(ICategoryArticleRepository categoryArticleRepository)
        {
            _categoryArticleRepository = categoryArticleRepository;
        }

        public CategoryArticleViewModel Execute(GetCategoryArticleByAlias query)
        {
            IQueryable<CategoryArticle> request = _categoryArticleRepository.GetDataContext();
            var category = request.FirstOrDefault(x => x.Alias == query.Alias);
            return Mapper.Map<CategoryArticle, CategoryArticleViewModel>(category);
        }
    }
}
