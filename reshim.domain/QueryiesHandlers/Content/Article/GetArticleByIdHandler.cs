﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetArticleByIdHandler : IQueryHandler<GetArticleById, ArticleViewModel>
    {
        private readonly IArticleRepository _articleRepository;

        public GetArticleByIdHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public ArticleViewModel Execute(GetArticleById query)
        {
            IQueryable<model.Entities.Article> request = _articleRepository.GetDataContext();

            var article = request.FirstOrDefault(x => x.Id == query.Id);

            var articleView = Mapper.Map<model.Entities.Article, ArticleViewModel>(article);
            return articleView;
        }
    }
}
