﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetArticleByAliasHandler : IQueryHandler<GetArticleByAlias, ArticleViewModel>
    {
        private readonly IArticleRepository _articleRepository;

        public GetArticleByAliasHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public ArticleViewModel Execute(GetArticleByAlias query)
        {
            IQueryable<model.Entities.Article> request = _articleRepository.AllIncluding(x => x.Reviews);

            var article = request
                    .FirstOrDefault(x => x.Alias.ToLower() == query.Alias.ToLower());

            var articleView = Mapper.Map<model.Entities.Article, ArticleViewModel>(article);
            return articleView;
        }
    }
}
