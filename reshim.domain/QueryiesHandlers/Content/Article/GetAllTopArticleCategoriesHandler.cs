﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.domain.Queryies.Content.Article;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetAllTopArticleCategoriesHandler : IQueryHandler<GetAllTopArticleCategories, IEnumerable<CategoryArticleViewModel>>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;

        public GetAllTopArticleCategoriesHandler(ICategoryArticleRepository categoryArticleRepository)
        {
            _categoryArticleRepository = categoryArticleRepository;
        }

        public IEnumerable<CategoryArticleViewModel> Execute(GetAllTopArticleCategories query)
        {
            var request = _categoryArticleRepository.AllIncluding(x => x.Children);

            var categories = request
                .Where(x => x.ParentId == null)
                .ToList();

            var categoryView = categories.Select(Mapper.Map<CategoryArticle, CategoryArticleViewModel>);
            return categoryView;
        }
    }
}
