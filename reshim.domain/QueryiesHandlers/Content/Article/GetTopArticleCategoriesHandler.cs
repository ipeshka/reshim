﻿using System;
using System.Linq;
using System.Linq.Expressions;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetTopArticleCategoriesHandler : IQueryHandler<GetTopArticleCategories, IPagedList<CategoryArticleViewModel>>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;

        public GetTopArticleCategoriesHandler(ICategoryArticleRepository categoryArticleRepository)
        {
            _categoryArticleRepository = categoryArticleRepository;
        }

        public IPagedList<CategoryArticleViewModel> Execute(GetTopArticleCategories query)
        {
            Expression<Func<CategoryArticle, bool>> predicateTopCategories = x => x.ParentId == null;
            IQueryable<CategoryArticle> request = _categoryArticleRepository.GetDataContext();

            var categories = request
                .OrderByDescending(x => x.DateCreate)
                .Where(predicateTopCategories)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var allCount = _categoryArticleRepository.Count(predicateTopCategories);

            var categoryView = categories.Select(Mapper.Map<CategoryArticle, CategoryArticleViewModel>);

            return new PagedList<CategoryArticleViewModel>(categoryView, query.Page - 1, query.Count, allCount);
        }
    }
}
