﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetAllArticlesHandler : IQueryHandler<GetAllArticles, IEnumerable<ArticleViewModel>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetAllArticlesHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IEnumerable<ArticleViewModel> Execute(GetAllArticles query)
        {
            var request = _articleRepository.GetAll();
            return request.Select(Mapper.Map<model.Entities.Article, ArticleViewModel>);
        }
    }
}
