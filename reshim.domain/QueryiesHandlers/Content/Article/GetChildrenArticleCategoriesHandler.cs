﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetChildrenArticleCategoriesHandler : IQueryHandler<GetChildrenArticleCategories, IPagedList<CategoryArticleViewModel>>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;

        public GetChildrenArticleCategoriesHandler(ICategoryArticleRepository categoryArticleRepository)
        {
            _categoryArticleRepository = categoryArticleRepository;
        }

        public IPagedList<CategoryArticleViewModel> Execute(GetChildrenArticleCategories query)
        {
            var count = _categoryArticleRepository.Count(x => x.ParentId == query.ParentId);

            IQueryable<CategoryArticle> request = _categoryArticleRepository.GetDataContext();

            var categories = request
                .Where(x => x.ParentId == query.ParentId)
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var categoryView = categories.Select(Mapper.Map<CategoryArticle, CategoryArticleViewModel>);
            return new PagedList<CategoryArticleViewModel>(categoryView, query.Page - 1, query.Count, count);
        }
    }
}
