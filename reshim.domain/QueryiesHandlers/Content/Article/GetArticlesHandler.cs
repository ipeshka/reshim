﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetArticlesHandler : IQueryHandler<GetArticles, IPagedList<ArticleViewModel>>
    {
        private readonly IArticleRepository _articleRepository;

        public GetArticlesHandler(IArticleRepository articleRepository)
        {
            _articleRepository = articleRepository;
        }

        public IPagedList<ArticleViewModel> Execute(GetArticles query)
        {
            var request = _articleRepository.AllIncluding(x => x.Category);
            var alias = !string.IsNullOrEmpty(query.CategoryAlias) ? query.CategoryAlias.ToLower() : null;

            if (!string.IsNullOrEmpty(query.CategoryAlias))
            {
                request = request.Where(x => x.Category.Alias == alias);
            }

            request = request.OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count);

            var articlesView = request.ToList().Select(Mapper.Map<model.Entities.Article, ArticleViewModel>);


            int count;
            if (!string.IsNullOrEmpty(query.CategoryAlias))
            {
                count = _articleRepository.AllIncluding(x => x.Category)
                    .Count(x => x.Category.Alias == alias);
            }
            else
            {
                count = _articleRepository.Count();
            }

            return new PagedList<ArticleViewModel>(articlesView, query.Page - 1, query.Count, count);
        }
    }
}
