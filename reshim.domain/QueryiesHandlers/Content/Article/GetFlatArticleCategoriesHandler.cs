﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Article;
using reshim.model.view.ViewModels.Content.Article;

namespace reshim.domain.QueryiesHandlers.Content.Article
{
    public class GetFlatArticleCategoriesHandler : IQueryHandler<GetFlatArticleCategories, IEnumerable<CategoryArticleViewModel>>
    {

        public IEnumerable<CategoryArticleViewModel> Execute(GetFlatArticleCategories query)
        {
            var returnedCategories = new List<CategoryArticleViewModel>();

            foreach (var cat in query.Categories)
            {
                returnedCategories.Add(cat);

                if (cat.Children != null && cat.Children.Any())
                {
                    var childrens = cat.Children.Select(x =>
                    {
                        x.Name = string.Format(" - {0}", x.Name);
                        return x;
                    });

                    returnedCategories.AddRange(childrens);
                }
            }

            return returnedCategories;
        }
    }
}
