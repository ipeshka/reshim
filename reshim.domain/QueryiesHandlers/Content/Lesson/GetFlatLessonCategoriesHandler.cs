﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetFlatLessonCategoriesHandler : IQueryHandler<GetFlatLessonCategories, IEnumerable<CategoryLessonsViewModel>>
    {

        public IEnumerable<CategoryLessonsViewModel> Execute(GetFlatLessonCategories query)
        {
            var returnedCategories = new List<CategoryLessonsViewModel>();

            foreach (var cat in query.Categories)
            {
                returnedCategories.Add(cat);

                if (cat.Children != null && cat.Children.Any())
                {
                    var childrens = cat.Children.Select(x =>
                    {
                        x.Name = string.Format(" - {0}", x.Name);
                        return x;
                    });

                    returnedCategories.AddRange(childrens);
                }
            }

            return returnedCategories;
        }
    }
}
