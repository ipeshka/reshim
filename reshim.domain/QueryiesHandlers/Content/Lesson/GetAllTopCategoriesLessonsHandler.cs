﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetAllTopCategoriesLessonsHandler : IQueryHandler<GetAllTopCategoriesLessons, IEnumerable<CategoryLessonsViewModel>>
    {
        private readonly ICategoryLessonRepository _categoryLessonRepository;

        public GetAllTopCategoriesLessonsHandler(ICategoryLessonRepository categoryLessonRepository)
        {
            _categoryLessonRepository = categoryLessonRepository;
        }

        public IEnumerable<CategoryLessonsViewModel> Execute(GetAllTopCategoriesLessons query)
        {
            IQueryable<CategoryLesson> request = _categoryLessonRepository.AllIncluding(x => x.Children);

            var categories = request
                .Where(x => x.ParentId == null)
                .OrderByDescending(x => x.DateCreate)
                .ToList();

            return categories.Select(Mapper.Map<CategoryLesson, CategoryLessonsViewModel>);
        }
    }
}
