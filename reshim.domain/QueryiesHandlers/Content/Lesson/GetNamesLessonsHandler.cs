﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.commandProcessor.Query;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetNamesLessonsHandler : IQueryHandler<GetNamesLessons, IPagedList<NameLessonsViewModel>>
    {
        private readonly INameLessonRepository _nameLessonRepository;

        public GetNamesLessonsHandler(INameLessonRepository nameLessonRepository)
        {
            _nameLessonRepository = nameLessonRepository;
        }

        public IPagedList<NameLessonsViewModel> Execute(GetNamesLessons query)
        {
            IQueryable<NameLesson> request = _nameLessonRepository.AllIncluding(x => x.Category);

            var namesLessons = request
                .OrderByDescending(x => x.DateCreate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var count = _nameLessonRepository.Count();

            var namesLessonsView = namesLessons.Select(Mapper.Map<NameLesson, NameLessonsViewModel>);

            return new PagedList<NameLessonsViewModel>(namesLessonsView, query.Page - 1, query.Count, count);
        }
    }
}
