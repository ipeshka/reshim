﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetTopCategoriesLessonsHandler : IQueryHandler<GetTopCategoriesLessons, IPagedList<CategoryLessonsViewModel>>
    {
        private readonly ICategoryLessonRepository _categoryLessonRepository;

        public GetTopCategoriesLessonsHandler(ICategoryLessonRepository categoryLessonRepository)
        {
            _categoryLessonRepository = categoryLessonRepository;
        }

        public IPagedList<CategoryLessonsViewModel> Execute(GetTopCategoriesLessons query)
        {
            Expression<Func<CategoryLesson, bool>> predicateTopCategories = x => x.ParentId == null;

            IQueryable<CategoryLesson> request = _categoryLessonRepository.GetDataContext();

            var categories = request
                .OrderByDescending(x => x.DateCreate)
                .Where(predicateTopCategories)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var count = _categoryLessonRepository.Count(predicateTopCategories);

            var categoryView = categories.Select(Mapper.Map<CategoryLesson, CategoryLessonsViewModel>);

            return new PagedList<CategoryLessonsViewModel>(categoryView, query.Page - 1, query.Count, count);
        }
    }
}
