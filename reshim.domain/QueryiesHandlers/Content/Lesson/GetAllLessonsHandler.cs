﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetAllLessonsHandler : IQueryHandler<GetAllLessons, IEnumerable<LessonViewModel>>
    {
        private readonly ILessonRepository _lessonRepository;

        public GetAllLessonsHandler(ILessonRepository lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }

        public IEnumerable<LessonViewModel> Execute(GetAllLessons query)
        {
            var request = _lessonRepository.GetAll();
            return request.Select(Mapper.Map<model.Entities.Lesson, LessonViewModel>);
        }
    }
}
