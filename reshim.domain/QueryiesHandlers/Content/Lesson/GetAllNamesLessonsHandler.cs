﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetAllNamesLessonsHandler : IQueryHandler<GetAllNamesLessons, IEnumerable<NameLessonsViewModel>>
    {
        private readonly INameLessonRepository _nameLessonRepository;

        public GetAllNamesLessonsHandler(INameLessonRepository nameLessonRepository)
        {
            _nameLessonRepository = nameLessonRepository;
        }

        public IEnumerable<NameLessonsViewModel> Execute(GetAllNamesLessons query)
        {
            IQueryable<NameLesson> request = _nameLessonRepository.GetDataContext();
            var namesLessonsView = request.OrderByDescending(x => x.DateCreate).ToList().Select(Mapper.Map<NameLesson, NameLessonsViewModel>);
            return namesLessonsView;
        }
    }
}
