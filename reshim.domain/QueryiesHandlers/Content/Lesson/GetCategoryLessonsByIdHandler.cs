﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetCategoryLessonsByIdHandler : IQueryHandler<GetCategoryLessonsById, CategoryLessonsViewModel>
    {
        private readonly ICategoryLessonRepository _categoryLessonRepository;

        public GetCategoryLessonsByIdHandler(ICategoryLessonRepository categoryLessonRepository)
        {
            _categoryLessonRepository = categoryLessonRepository;
        }

        public CategoryLessonsViewModel Execute(GetCategoryLessonsById query)
        {
            IQueryable<CategoryLesson> request = query.IncludeProperties != null ?
                query.IncludeProperties.ApplyTo(_categoryLessonRepository.GetDataContext()) :
                _categoryLessonRepository.GetDataContext();

            var category = request.FirstOrDefault(x => x.Id == query.Id);

            var categoryView = Mapper.Map<CategoryLesson, CategoryLessonsViewModel>(category);
            return categoryView;
        }
    }
}
