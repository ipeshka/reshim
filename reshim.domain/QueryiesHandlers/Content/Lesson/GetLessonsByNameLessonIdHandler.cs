﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetLessonsByNameLessonIdHandler : IQueryHandler<GetLessonsByNameLessonId, IEnumerable<LessonViewModel>>
    {
        private readonly ILessonRepository _lessonRepository;

        public GetLessonsByNameLessonIdHandler(ILessonRepository lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }

        public IEnumerable<LessonViewModel> Execute(GetLessonsByNameLessonId query)
        {
            IQueryable<model.Entities.Lesson> request = _lessonRepository.GetDataContext();

            request = request.Where(x => x.NameLessonId == query.NameLessonId).OrderBy(x => x.Position);

            var lessonsView = request.ToList().Select(Mapper.Map<model.Entities.Lesson, LessonViewModel>);

            return lessonsView;
        }

    }
}
