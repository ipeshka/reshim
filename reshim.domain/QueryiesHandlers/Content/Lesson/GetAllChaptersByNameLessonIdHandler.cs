﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetAllChaptersByNameLessonIdHandler : IQueryHandler<GetAllChaptersByNameLessonId, IEnumerable<ChapterViewModel>>
    {
        private readonly IChapterRepository _chapterRepository;

        public GetAllChaptersByNameLessonIdHandler(IChapterRepository chapterRepository)
        {
            _chapterRepository = chapterRepository;
        }

        public IEnumerable<ChapterViewModel> Execute(GetAllChaptersByNameLessonId query)
        {
            IQueryable<Chapter> request = _chapterRepository.GetDataContext();

            var chapters = request
                .OrderBy(x => x.Position)
                .Where(x => x.NameLessonId == query.NameLessonId)
                .ToList();

            return chapters.Select(Mapper.Map<Chapter, ChapterViewModel>);
        }
    }
}
