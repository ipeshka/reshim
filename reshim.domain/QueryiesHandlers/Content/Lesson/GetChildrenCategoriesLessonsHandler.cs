﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetChildrenCategoriesLessonsHandler : IQueryHandler<GetChildrenCategoriesLessons, IPagedList<CategoryLessonsViewModel>>
    {
        private readonly ICategoryLessonRepository _categoryLessonRepository;

        public GetChildrenCategoriesLessonsHandler(ICategoryLessonRepository categoryLessonRepository)
        {
            _categoryLessonRepository = categoryLessonRepository;
        }

        public IPagedList<CategoryLessonsViewModel> Execute(GetChildrenCategoriesLessons query)
        {
            var count = _categoryLessonRepository.Count(x => x.ParentId == query.ParentId);

            IQueryable<CategoryLesson> request = _categoryLessonRepository.GetDataContext();

            var categories = request
                .OrderByDescending(x => x.DateCreate)
                .Where(x => x.ParentId == query.ParentId)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var categoryView = categories.Select(Mapper.Map<CategoryLesson, CategoryLessonsViewModel>);

            return new PagedList<CategoryLessonsViewModel>(categoryView, query.Page - 1, query.Count, count);
        }
    }
}
