﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetLessonsHandler : IQueryHandler<GetLessons, IPagedList<LessonViewModel>>
    {
        private readonly ILessonRepository _lessonRepository;

        public GetLessonsHandler(ILessonRepository lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }

        public IPagedList<LessonViewModel> Execute(GetLessons query)
        {
            IQueryable<model.Entities.Lesson> request = query.IncludeProperties != null ?
                query.IncludeProperties.ApplyTo(_lessonRepository.GetDataContext()) :
                _lessonRepository.GetDataContext();

            Expression<Func<model.Entities.Lesson, bool>> expressionPredicate = x =>
                (!query.NameLessonId.HasValue ? true : x.NameLessonId == query.NameLessonId)
                && (!query.ChapterId.HasValue ? true : x.ChapterId == query.ChapterId)
                && (string.IsNullOrEmpty(query.CategoryAlias) ? true : x.CategoryLesson.Alias == query.CategoryAlias);

            int count = _lessonRepository.Count(expressionPredicate);
            request = query.IsDesc ? request.OrderByDescending(x => x.Position) : request.OrderBy(x => x.Position);

            request = request.Where(expressionPredicate)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count);

            var lessonsView = request.ToList().Select(Mapper.Map<model.Entities.Lesson, LessonViewModel>);

            return new PagedList<LessonViewModel>(lessonsView, query.Page - 1, query.Count, count);
        }

    }
}
