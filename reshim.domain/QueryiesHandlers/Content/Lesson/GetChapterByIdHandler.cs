﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetChapterByIdHandler : IQueryHandler<GetChapterById, ChapterViewModel>
    {
        private readonly IChapterRepository _chapterRepository;

        public GetChapterByIdHandler(IChapterRepository chapterRepository)
        {
            _chapterRepository = chapterRepository;
        }

        public ChapterViewModel Execute(GetChapterById query)
        {
            IQueryable<Chapter> request = _chapterRepository.GetDataContext();
            var chapter = request.FirstOrDefault(x => x.Id == query.Id);
            var chapterView = Mapper.Map<Chapter, ChapterViewModel>(chapter);
            return chapterView;
        }
    }
}
