﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetLessonByAliasHandler : IQueryHandler<GetLessonByAlias, LessonViewModel>
    {
        private readonly ILessonRepository _lessonRepository;

        public GetLessonByAliasHandler(ILessonRepository lessonRepository)
        {
            _lessonRepository = lessonRepository;
        }

        public LessonViewModel Execute(GetLessonByAlias query)
        {
            IQueryable<model.Entities.Lesson> request = _lessonRepository.AllIncluding(x => x.Reviews, x => x.NameLesson);

            var lesson = request
                .FirstOrDefault(x => x.Alias.ToLower() == query.Alias.ToLower());

            return Mapper.Map<model.Entities.Lesson, LessonViewModel>(lesson);
        }
    }
}
