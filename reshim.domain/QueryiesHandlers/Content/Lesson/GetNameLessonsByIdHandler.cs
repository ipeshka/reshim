﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetNameLessonsByIdHandler : IQueryHandler<GetNameLessonsById, NameLessonsViewModel>
    {
        private readonly INameLessonRepository _nameLessonRepository;

        public GetNameLessonsByIdHandler(INameLessonRepository nameLessonRepository)
        {
            _nameLessonRepository = nameLessonRepository;
        }

        public NameLessonsViewModel Execute(GetNameLessonsById query)
        {
            IQueryable<NameLesson> request = query.IncludeProperties == null 
                ? _nameLessonRepository.GetDataContext()
                : query.IncludeProperties.ApplyTo(_nameLessonRepository.GetDataContext());

            var nameLessons = request.FirstOrDefault(x => x.Id == query.Id);

            var nameLessonsView = Mapper.Map<NameLesson, NameLessonsViewModel>(nameLessons);
            return nameLessonsView;
        }
    }
}
