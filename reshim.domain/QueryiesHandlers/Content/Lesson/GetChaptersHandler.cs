﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Content.Lesson;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Content.Lesson;
using reshim.data.infrastructure.Pagination;

namespace reshim.domain.QueryiesHandlers.Content.Lesson
{
    public class GetChaptersHandler : IQueryHandler<GetChapters, IPagedList<ChapterViewModel>>
    {
        private readonly IChapterRepository _chapterRepository;

        public GetChaptersHandler(IChapterRepository chapterRepository)
        {
            _chapterRepository = chapterRepository;
        }

        public IPagedList<ChapterViewModel> Execute(GetChapters query)
        {
            Expression<Func<Chapter, bool>> filter = x => x.Id > 0; 
            if (query.NameLessonId.HasValue)
            {
                filter = x => x.NameLessonId == query.NameLessonId;
            }

            var count = _chapterRepository.Count(filter);

            IQueryable<Chapter> request = _chapterRepository.GetDataContext();

            var chapters = request
                .OrderBy(x => x.Position)
                .Where(filter)
                .Skip((query.Page - 1) * query.Count)
                .Take(query.Count)
                .ToList();

            var chaptersView = chapters.Select(Mapper.Map<Chapter, ChapterViewModel>);

            return new PagedList<ChapterViewModel>(chaptersView, query.Page - 1, query.Count, count);
        }
    }
}
