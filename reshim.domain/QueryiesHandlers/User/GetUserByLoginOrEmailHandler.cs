﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.User;
using reshim.model.view.ViewModels.User;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.QueryiesHandlers.User
{
    public class GetUserByLoginOrEmailHandler : IQueryHandler<GetUserByLoginOrEmail, UserViewModel>
    {
        private readonly IUserRepository _userRepository;

        public GetUserByLoginOrEmailHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserViewModel Execute(GetUserByLoginOrEmail query)
        {
            IQueryable<model.Entities.User> request = _userRepository.GetDataContext();

            var usr = query.HasEmail ?
                request.FirstOrDefault(x => x.Email == query.LoginOrEmail)
                : request.FirstOrDefault(x => x.Login == query.LoginOrEmail);
            return Mapper.Map<model.Entities.User, UserViewModel>(usr);
        }
    }
}
