﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.User;
using reshim.model.view.ViewModels.User;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.QueryiesHandlers.User
{
    public class GetUserByIdHandler : IQueryHandler<GetUserById, UserViewModel>
    {
        private readonly IUserRepository _userRepository;

        public GetUserByIdHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserViewModel Execute(GetUserById query)
        {
            IQueryable<model.Entities.User> request = query.IncludeProperties != null ?
                query.IncludeProperties.ApplyTo(_userRepository.GetDataContext()) :
                _userRepository.GetDataContext();


            var usr = request.FirstOrDefault(x => x.UserId == query.Id);

            return Mapper.Map<model.Entities.User, UserViewModel>(usr);
        }
    }
}
