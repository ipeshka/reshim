﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.User;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.QueryiesHandlers.User
{
    public class GetUserByLoginHandler : IQueryHandler<GetUserByLogin, UserViewModel>
    {
        private readonly IUserRepository _userRepository;

        public GetUserByLoginHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserViewModel Execute(GetUserByLogin query)
        {
            IQueryable<model.Entities.User> request = _userRepository.GetDataContext();

            var usr = request.FirstOrDefault(x => x.Login == query.Login);
            return Mapper.Map<model.Entities.User, UserViewModel>(usr);
        }
    }
}
