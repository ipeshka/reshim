﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.User;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.QueryiesHandlers.User
{

    public class GetUserByIdServiceHandler : IQueryHandler<GetUserByIdService, UserViewModel>
    {
        private readonly IUserRepository _userRepository;

        public GetUserByIdServiceHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public UserViewModel Execute(GetUserByIdService query)
        {
            IQueryable<model.Entities.User> request = _userRepository.GetDataContext();
            var usr = request.FirstOrDefault(x => x.IdService == query.IdService);
            return Mapper.Map<model.Entities.User, UserViewModel>(usr);
        }
    }
}
