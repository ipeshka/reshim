﻿using System.Linq;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.User;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.User;

namespace reshim.domain.QueryiesHandlers.User
{
    public class GetStatisticsTaskUserHandler : IQueryHandler<GetStatisticsTaskUser, StatisticsViewModel>
    {
        private readonly ITaskRepository _taskRepository;

        public GetStatisticsTaskUserHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public StatisticsViewModel Execute(GetStatisticsTaskUser query)
        {
            var solved = _taskRepository.GetMany(t => t.DecidedId == query.UserId).Count();
            var order = _taskRepository.GetMany(t => t.CustomerId == query.UserId).Count();
            return new StatisticsViewModel() { CountOrderTask = order, CountSolvedTask = solved };
        }
    }
}
