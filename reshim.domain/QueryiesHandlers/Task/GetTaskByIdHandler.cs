﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Task;
using reshim.model.view.ViewModels.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.QueryiesHandlers.Task
{
    public class GetTaskByIdHandler : IQueryHandler<GetTaskById, TaskViewModel>
    {
        private readonly ITaskRepository _taskRepository;

        public GetTaskByIdHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public TaskViewModel Execute(GetTaskById query)
        {
            IQueryable<model.Entities.Task> request = query.IncludeProperties != null ?
                query.IncludeProperties.ApplyTo(_taskRepository.GetDataContext()) :
                _taskRepository.GetDataContext();

            var task = request.FirstOrDefault(x => x.Id == query.Id);
            return Mapper.Map<model.Entities.Task, TaskViewModel>(task);
        }
    }
}
