﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Task;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.QueryiesHandlers.Task
{
    public class GetLastTaskHandler : IQueryHandler<GetLastTask, IEnumerable<TaskViewModel>>
    {
        private readonly ITaskRepository _taskRepository;

        public GetLastTaskHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public IEnumerable<TaskViewModel> Execute(GetLastTask query)
        {
            IQueryable<model.Entities.Task> request = _taskRepository.GetDataContext();

            var task = request.OrderByDescending(x => x.Id)
                .Where(x => x.Status == query.Status)
                .Take(query.CountLast).ToList();

            return task.Select(Mapper.Map<model.Entities.Task, TaskViewModel>);
        }
    }
}
