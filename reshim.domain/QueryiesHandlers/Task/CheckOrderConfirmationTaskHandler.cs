﻿using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Task;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.QueryiesHandlers.Task
{
    public class CheckOrderConfirmationTaskHandler : IQueryHandler<CheckOrderConfirmationTask, OrderConfirmationTaskReturnedModel>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IPaymentDataRepository _paymentDataRepository;

        public CheckOrderConfirmationTaskHandler(ITaskRepository taskRepository, IPaymentDataRepository paymentDataRepository)
        {
            _taskRepository = taskRepository;
            _paymentDataRepository = paymentDataRepository;
        }

        public OrderConfirmationTaskReturnedModel Execute(CheckOrderConfirmationTask query)
        {
            var task = _taskRepository.GetById(query.Id);
            var customerPaymentData = _paymentDataRepository.GetById(task.CustomerId);
            return new OrderConfirmationTaskReturnedModel()
            {
                Ballance = customerPaymentData.Ballance,
                Amount = task.Cost / 2,
                IsValid = customerPaymentData.Ballance >= task.Cost / 2
            };
        }
    }
}
