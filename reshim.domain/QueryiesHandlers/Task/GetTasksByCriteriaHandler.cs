﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.common.core.Pagination;
using reshim.domain.Queryies.Task;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.QueryiesHandlers.Task
{

    public class GetTasksByCriteriaHandler : IQueryHandler<GetTasksByCriteria, TasksViewModelWithCriteria>
    {
        private readonly ITaskRepository _taskRepository;

        public GetTasksByCriteriaHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public TasksViewModelWithCriteria Execute(GetTasksByCriteria query)
        {
            var request = query.IncludeProperties != null ? 
                query.IncludeProperties.ApplyTo(_taskRepository.GetDataContext()) :
                _taskRepository.GetDataContext();

            var modelView = new TasksViewModelWithCriteria { SelectedMyTask = query.IsMyTask };
            var curStatus = StatusTask.New;
            if (!string.IsNullOrEmpty(query.Status))
            {
                curStatus = EnumEx.GetEnumFromTypeAttribute<StatusTask>(query.Status);
                modelView.SelectedStatus = curStatus.GetAttributeValue(typeof(TypeAttribute), string.Empty);
            }

            Expression<Func<model.Entities.Task, bool>> filteringFunc = x =>
                (string.IsNullOrEmpty(query.Status) ? x.Status != StatusTask.NotRelevant : x.Status == curStatus)
                &&
                (!modelView.SelectedMyTask || x.CustomerId == query.UserId);

            request = request.OrderByDescending(x => x.Id)
                .Where(filteringFunc)
                .Skip(query.Page * query.Count)
                .Take(query.Count);

            int count = _taskRepository.Count(filteringFunc);
            var taskView = request.ToList().Select(Mapper.Map<model.Entities.Task, TaskViewModel>);

            modelView.Tasks = new PagedList<TaskViewModel>(taskView, query.Page, query.Count, count);
            modelView.Status = EnumEx.GetKeyValue<StatusTask, NameAttribute, NameAttribute, TypeAttribute>(typeof(IsDisplayAttribute));

            return modelView;
        }
    }
}
