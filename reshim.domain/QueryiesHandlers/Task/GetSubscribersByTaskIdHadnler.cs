﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.Task;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.QueryiesHandlers.Task
{
    public class GetSubscribersByTaskIdHadnler : IQueryHandler<GetSubscribersByTaskId, IEnumerable<SubscriberViewModel>>
    {
        private readonly ISubscriberRepository _subscriberRepository;

        public GetSubscribersByTaskIdHadnler(ISubscriberRepository subscriberRepository)
        {
            _subscriberRepository = subscriberRepository;
        }

        public IEnumerable<SubscriberViewModel> Execute(GetSubscribersByTaskId query)
        {
            IQueryable<Subscriber> request = _subscriberRepository.GetDataContext();

            var subscribers = request.Where(x => x.TaskId == query.Id);
            return subscribers.Select(Mapper.Map<Subscriber, SubscriberViewModel>).ToList();
        }
    }
}
