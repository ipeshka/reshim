﻿using System;
using System.Collections.Generic;
using reshim.data.commandProcessor.Query;
using reshim.common.Attributes;
using reshim.common.core.Extensions;
using reshim.domain.Queryies.Task;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.Task;

namespace reshim.domain.QueryiesHandlers.Task
{
    public class GetStatisticCountTaskHandler : IQueryHandler<GetStatisticCountTask, CountTaskViewModel>
    {
        private readonly ITaskRepository _taskRepository;

        public GetStatisticCountTaskHandler(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public CountTaskViewModel Execute(GetStatisticCountTask query)
        {
            var modelView = new CountTaskViewModel() { StatusTask = new List<Tuple<string, string, int>>() };

            var statusTask = EnumEx.GetKeyValue<StatusTask, NameAttribute, NameAttribute, TypeAttribute>();
            foreach (var st in statusTask)
            {
                var curStatus = EnumEx.GetEnumFromTypeAttribute<StatusTask>(st.Value);
                var count = _taskRepository.Count(x => x.Status == curStatus);
                modelView.StatusTask.Add(new Tuple<string, string, int>(st.Key, st.Value, count));
            }

            return modelView;
        }
    }
}
