﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetCompletedTaskByIdHandler : IQueryHandler<GetCompletedTaskById, CompletedTaskViewModel>
    {
        private readonly IComplitedTaskRepository _complitedTaskRepository;

        public GetCompletedTaskByIdHandler(IComplitedTaskRepository complitedTaskRepository)
        {
            _complitedTaskRepository = complitedTaskRepository;
        }

        public CompletedTaskViewModel Execute(GetCompletedTaskById query)
        {
            var request = _complitedTaskRepository.AllIncluding(x => x.Tags, x => x.Decided, x => x.Solution, x => x.Subject, x => x.HistoriesCompletedTasks, x=>x.Reviews);
            var task = request.FirstOrDefault(x => x.Id == query.Id);
            return Mapper.Map<model.Entities.CompletedTask, CompletedTaskViewModel>(task);
        }
    }
}
