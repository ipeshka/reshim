﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetHistoryCompletedTaskByUserIdHandler : IQueryHandler<GetHistoryCompletedTaskByUserId, IPagedList<HistoryCompletedTaskViewModel>>
    {
        private readonly IHistoryComplitedTaskRepository _historyComplitedTaskRepository;

        public GetHistoryCompletedTaskByUserIdHandler(IHistoryComplitedTaskRepository historyComplitedTaskRepository)
        {
            _historyComplitedTaskRepository = historyComplitedTaskRepository;
        }

        public IPagedList<HistoryCompletedTaskViewModel> Execute(GetHistoryCompletedTaskByUserId query)
        {
            var request = _historyComplitedTaskRepository.AllIncluding(x => x.CompletedTask, x=>x.CompletedTask.Solution);
            var histories = request.Where(x => x.UserId == query.UserId).OrderByDescending(x => x.Create)
                .Skip(query.Page * query.Count)
                .Take(query.Count).ToList();

            var count = request.Count(x => x.UserId == query.UserId);

            var viewHistoryCompletedTask = histories.Select(Mapper.Map<HistoryCompletedTask, HistoryCompletedTaskViewModel>).ToList();
            return new PagedList<HistoryCompletedTaskViewModel>(viewHistoryCompletedTask, query.Page, query.Count, count);
        }
    }
}
