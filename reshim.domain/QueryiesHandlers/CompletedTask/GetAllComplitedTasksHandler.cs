﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetAllComplitedTasksHandler : IQueryHandler<GetAllComplitedTasks, IEnumerable<CompletedTaskViewModel>>
    {
        private readonly IComplitedTaskRepository _complitedTaskRepository;

        public GetAllComplitedTasksHandler(IComplitedTaskRepository complitedTaskRepository)
        {
            _complitedTaskRepository = complitedTaskRepository;
        }

        public IEnumerable<CompletedTaskViewModel> Execute(GetAllComplitedTasks query)
        {
            var complitedTasks = _complitedTaskRepository.GetAll().ToList();
            var tasksView = complitedTasks.Select(Mapper.Map<model.Entities.CompletedTask, CompletedTaskViewModel>);
            return tasksView;
        }
    }
}
