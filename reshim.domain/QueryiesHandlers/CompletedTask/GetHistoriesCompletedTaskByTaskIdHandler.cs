﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetHistoriesCompletedTaskByTaskIdHandler : IQueryHandler<GetHistoriesCompletedTaskByTaskId, IPagedList<HistoryCompletedTaskViewModel>>
    {
        private readonly IHistoryComplitedTaskRepository _historyComplitedTaskRepository;

        public GetHistoriesCompletedTaskByTaskIdHandler(IHistoryComplitedTaskRepository historyComplitedTaskRepository)
        {
            _historyComplitedTaskRepository = historyComplitedTaskRepository;
        }

        public IPagedList<HistoryCompletedTaskViewModel> Execute(GetHistoriesCompletedTaskByTaskId query)
        {
            var request = _historyComplitedTaskRepository.AllIncluding(x => x.User);

            request = request.OrderByDescending(x => x.Id)
                .Skip(query.Page * query.Count)
                .Take(query.Count);

            int count = _historyComplitedTaskRepository.Count();
            var taskView = request.ToList().Select(Mapper.Map<model.Entities.HistoryCompletedTask, HistoryCompletedTaskViewModel>);

            return new PagedList<HistoryCompletedTaskViewModel>(taskView, query.Page, query.Count, count);
        }
    }
}
