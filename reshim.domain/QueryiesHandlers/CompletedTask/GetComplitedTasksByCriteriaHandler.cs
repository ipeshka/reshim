﻿using System.Linq;
using AutoMapper;
using reshim.common.core.Pagination;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Pagination;
using reshim.data.infrastructure.Repositories;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetComplitedTasksByCriteriaHandler : IQueryHandler<GetComplitedTasksByCriteria, IPagedList<CompletedTaskViewModel>>
    {
        private readonly IComplitedTaskRepository _complitedTaskRepository;

        public GetComplitedTasksByCriteriaHandler(IComplitedTaskRepository complitedTaskRepository)
        {
            _complitedTaskRepository = complitedTaskRepository;
        }

        public IPagedList<CompletedTaskViewModel> Execute(GetComplitedTasksByCriteria query)
        {
            var request = _complitedTaskRepository.AllIncluding(x => x.Tags, x => x.Decided, x => x.Solution, x => x.Subject, x => x.HistoriesCompletedTasks);

            request = request.OrderByDescending(x => x.Id)
                .Skip(query.Page * query.Count)
                .Take(query.Count);

            int count = _complitedTaskRepository.Count();
            var taskView = request.ToList().Select(Mapper.Map<model.Entities.CompletedTask, CompletedTaskViewModel>);

            return new PagedList<CompletedTaskViewModel>(taskView, query.Page, query.Count, count);
        }
    }
}
