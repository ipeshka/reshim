﻿using System.Linq;
using AutoMapper;
using reshim.data.commandProcessor.Query;
using reshim.domain.Queryies.CompletedTask;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.ComplitedTask;

namespace reshim.domain.QueryiesHandlers.CompletedTask
{
    public class GetHistoryCompletedTaskByIdHandler : IQueryHandler<GetHistoryCompletedTaskById, HistoryCompletedTaskViewModel>
    {
        private readonly IHistoryComplitedTaskRepository _historyComplitedTaskRepository;

        public GetHistoryCompletedTaskByIdHandler(IHistoryComplitedTaskRepository historyComplitedTaskRepository)
        {
            _historyComplitedTaskRepository = historyComplitedTaskRepository;
        }

        public HistoryCompletedTaskViewModel Execute(GetHistoryCompletedTaskById query)
        {
            var request = _historyComplitedTaskRepository.AllIncluding(x => x.CompletedTask, x => x.User, x => x.CompletedTask.Solution).FirstOrDefault(x => x.Id == query.Id);
            return Mapper.Map<HistoryCompletedTask, HistoryCompletedTaskViewModel>(request);
        }
    }
}
