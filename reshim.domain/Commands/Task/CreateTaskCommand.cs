﻿using System;
using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task {
    public class CreateTaskCommand : ICommand {
        public int FileId { get; set; }
        public DateTime DateOfExecution { get; set; }
        public decimal? Cost { get; set; }
        public int SubjectId { get; set; }
        public int TypeWorkId { get; set; }
        public int CustomerId { get; set; }
        public string Comment { get; set; }
    }
}
