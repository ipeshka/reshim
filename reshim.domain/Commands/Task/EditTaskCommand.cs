﻿using System;
using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task
{
    public class EditTaskCommand : ICommand
    {
        public int TaskId { get; set; }
        public int FileId { get; set; }
        public DateTime DateOfExecution { get; set; }
        public decimal? Cost { get; set; }
        public int SubjectId { get; set; }
        public int TypeWorkId { get; set; }
        public string Comment { get; set; }
    }
}
