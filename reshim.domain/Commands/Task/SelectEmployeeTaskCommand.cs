﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task
{
    public class SelectEmployeeTaskCommand : ICommand
    {
        public int TaskId { get; set; }
        public int SubscriberId { get; set; }
    }
}
