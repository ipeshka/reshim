﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task
{
    public class DeleteTaskCommand : ICommand
    {
        public int TaskId { get; set; }
        public int UserId { get; set; }
    }
}
