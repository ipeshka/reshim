﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task
{
    public class UploadSolutionCommand : ICommand
    {
        public int TaskId { get; set; }
        public int FileId { get; set; }
        public int DecidedId { get; set; }
    }
}
