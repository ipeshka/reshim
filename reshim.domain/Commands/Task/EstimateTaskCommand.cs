﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Task
{
    public class EstimateTaskCommand : ICommand
    {
        public int TaskId { get; set; }
        public int UserId { get; set; }
        public decimal Value { get; set; }
        public string Comment { get; set; }
    }
}
