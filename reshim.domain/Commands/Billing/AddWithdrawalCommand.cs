﻿using reshim.data.commandProcessor.Command;
using reshim.model.Entities;

namespace reshim.domain.Commands.Billing
{
    /// <summary>
    /// добавление нового вывода денег
    /// </summary>
    public class AddWithdrawalCommand : ICommand
    {
        public int UserId { get; set; }
        public decimal Amount { get; set; }
        public PaywayVia TypePayment { get; set; }
        public string Purse { get; set; }
        public string Comment { get; set; }
    }
}
