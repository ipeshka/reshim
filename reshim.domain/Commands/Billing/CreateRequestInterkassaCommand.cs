﻿using reshim.data.commandProcessor.Command;
using reshim.model.Entities;

namespace reshim.domain.Commands.Billing
{
    /// <summary>
    /// создаёт начальный запрос для интеркассы
    /// </summary>
    public class CreateRequestInterkassaCommand : ICommand
    {
        public int UserId { get; set; }
        public string CheckoutId { get; set; }
        public string PaymentNumber { get; set; }
        public decimal Amount { get; set; }
        public PaywayVia PaywayVia { get; set; }
    }
}
