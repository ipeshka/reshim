﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.model.Entities;

namespace reshim.domain.Commands.Billing
{
    /// <summary>
    /// запрос от интерксассы
    /// </summary>
    public class ConfirmRequestInterkassaCommand : ICommand
    {
        public string CheckoutId { get; set; }
        public int PaymentNumber { get; set; }
        public decimal Amount { get; set; }
        public PaywayVia PaywayVia { get; set; }
        public Currency Currency { get; set; }
        public PaymentState State { get; set; }
        public bool HasCompletedTask { get; set; }

        public override string ToString()
        {
            return String.Format("CheckoutId - {0}, PaymentNumber - {1}, Amount - {2}," +
                                 "PaywayVia - {3}, Currency - {4}, State - {5}," +
                                 "HasCompletedTask - {6}", CheckoutId, PaymentNumber, Amount, PaywayVia, Currency, State, HasCompletedTask);
        }
    }
}
