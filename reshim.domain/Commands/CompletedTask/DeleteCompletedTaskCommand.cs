﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.CompletedTask
{
    public class DeleteCompletedTaskCommand : ICommand
    {
        public int TaskId { get; set; }
    }
}
