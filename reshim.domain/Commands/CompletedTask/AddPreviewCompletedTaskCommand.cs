﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.CompletedTask
{
    public class AddPreviewCompletedTaskCommand : ICommand
    {
        public int TaskId { get; set; }
        public string Ip { get; set; }
    }
}
