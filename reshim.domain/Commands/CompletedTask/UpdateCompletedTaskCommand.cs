﻿namespace reshim.domain.Commands.CompletedTask
{
    public class UpdateCompletedTaskCommand : CreateCompletedTaskCommand
    {
        public int Id { get; set; }
    }
}
