﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.CompletedTask
{
    public class CreateCompletedTaskCommand : ICommand
    {
        public int FileId { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public decimal Cost { get; set; }
        public int SubjectId { get; set; }
        public string Comment { get; set; }
        public string Condition { get; set; }
    }
}
