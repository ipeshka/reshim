﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Profile
{
    public class UpdatePhotoCommand : ICommand
    {
        public int UserId { get; set; }
        public string Path { get; set; }
    }
}
