﻿using System;
using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Profile
{
    public class EditProfileCommand : ICommand
    {
        public int UserId { get; set; }
        public string Icq { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string UrlVk { get; set; }
        public string Photo { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string PlaceOfStudy { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Comments { get; set; }
    }
}
