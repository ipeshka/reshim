﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Profile
{
    public class DeletePhotoCommand : ICommand
    {
        public int UserId { get; set; }
    }
}
