﻿using reshim.data.commandProcessor.Command;
using reshim.model.Entities;

namespace reshim.domain.Commands.File {
    public class CreateFileCommand : ICommand {
        public string Name { get; set; }
        public string Path { get; set; }
        public TypeFile TypeFile { get; set; }
    }
}
