﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class UserLoginCommand: ICommand {
        public string EmailOrLogin { get; set; }
        public string Password { get; set; }
        public bool HasEmail { get; set; }
    }
}
