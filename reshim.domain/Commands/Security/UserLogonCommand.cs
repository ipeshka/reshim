﻿
using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class UserLogonCommand : ICommand
    {
        public string LoginOrEmail { get; set; }
        public bool HasEmail { get; set; }
    }
}
