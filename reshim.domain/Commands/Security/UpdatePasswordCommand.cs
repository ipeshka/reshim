﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class UpdatePasswordCommand : ICommand
    {
        public int UserId { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
