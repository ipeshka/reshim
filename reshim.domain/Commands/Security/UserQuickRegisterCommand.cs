﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;

namespace reshim.domain.Commands.Security
{
    public class UserQuickRegisterCommand : ICommand
    {
        public UserRegisterCommand User { get; set; }
        public CreateTaskCommand Task { get; set; }
    }
}
