﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class CanUpdatePasswordValidation : ICommand
    {
        public string Token { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
    }
}
