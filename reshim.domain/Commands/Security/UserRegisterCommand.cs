﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class UserRegisterCommand : ICommand {
        public string Login { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
