﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Security
{
    public class UserForgotPasswordCommand : ICommand
    {
        public string EmailOrLogin { get; set; }
        public bool HasEmail { get; set; }
    }
}
