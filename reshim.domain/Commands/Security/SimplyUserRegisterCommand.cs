﻿using reshim.data.commandProcessor.Command;
using reshim.model.Entities;

namespace reshim.domain.Commands.Security
{
    public class SimplyUserRegisterCommand : ICommand
    {
        public string Email { get; set; }
        public int CompletedTaskId { get; set; }
        public PaywayVia PaywayVia { get; set; }
    }
}
