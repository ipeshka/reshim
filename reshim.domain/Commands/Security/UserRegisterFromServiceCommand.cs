﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Profile;
using reshim.domain.Commands.Task;

namespace reshim.domain.Commands.Security
{
    public class UserRegisterFromServiceCommand : ICommand
    {
        public string IdService { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Service { get; set; }
        public string UrlVk { get; set; }
        public UpdatePhotoCommand UpdatePhoto { get; set; }
        public CreateTaskCommand CreateTask { get; set; }
    }
}
