﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    /// <summary>
    /// валидатор, можно ли добавить новое содержимое
    /// </summary>
    public class CanCreateOrEditContent : ICommand
    {
        public int Id { get; set; }
        public string RedirectFromUrl { get; set; }
    }
}
