﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    /// <summary>
    /// добавить новый просмотр к уроку
    /// </summary>
    public class AddPreviewLessonCommand : ICommand
    {
        public int LessonId { get; set; }
        public string Ip { get; set; }
    }
}
