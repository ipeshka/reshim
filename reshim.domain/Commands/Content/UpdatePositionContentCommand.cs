﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    /// <summary>
    /// меняет позиции
    /// </summary>
    public class UpdatePositionContentCommand : ICommand
    {
        public int[] Ids { get; set; }
        public int CurPage { get; set; }
    }
}
