﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    public class CreateOrEditTypeWorkCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string RedirectFromUrl { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Text { get; set; }
        public string Cost { get; set; }
        public string Period { get; set; }
    }

    public class CreateOrEditSubjectCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Preview { get; set; }
        public string Image { get; set; }
    }

    public class CreateOrEditNewsCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string Text { get; set; }
        public string Preview { get; set; }
    }

    public class CreateOrEditSeoArticleCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string RedirectFromUrl { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Text { get; set; }
        public int TypeWorkId { get; set; }
    }

    public class CreateOrEditCategoryArticleCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }

    public class CreateOrEditArticleCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Text { get; set; }
        public string Preview { get; set; }
        public string Image { get; set; }
        public int CategoryId { get; set; }
    }

    public class CreateOrEditCategoryLessonCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int? ParentId { get; set; }
    }

    public class CreateOrEditNameLessonCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
    }

    public class CreateOrEditChapterCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NameLessonId { get; set; }
    }

    public class CreateOrEditLessonCommand : ICommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleSeo { get; set; }
        public string DescriptionSeo { get; set; }
        public string KeywordsSeo { get; set; }
        public string Preview { get; set; }
        public string Text { get; set; }
        public string Image { get; set; }
        public int NameLessonId { get; set; }
        public int? ChapterId { get; set; }

    }

}
