﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    public class DeleteContentCommand : ICommand
    {
        public int Id { get; set; }
    }
}
