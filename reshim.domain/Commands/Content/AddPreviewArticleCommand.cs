﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Content
{
    /// <summary>
    /// добавить новый просмотр к статьи
    /// </summary>
    public class AddPreviewArticleCommand : ICommand
    {
        public int ArticleId { get; set; }
        public string Ip { get; set; }
    }
}
