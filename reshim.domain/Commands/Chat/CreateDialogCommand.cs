﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Chat
{
    /// <summary>
    /// создаёт новый диалог между пользователями
    /// </summary>
    public class CreateDialogCommand : ICommand
    {
        public int RecipientId { get; set; }
        public int SenderId { get; set; }
    }
}
