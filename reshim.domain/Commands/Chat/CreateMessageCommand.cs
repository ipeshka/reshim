﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Chat
{
    /// <summary>
    /// создаёт новое сообщение
    /// </summary>
    public class CreateMessageCommand : ICommand
    {
        public int SenderId { get; set; }
        public string Content { get; set; }
        public int DialogId { get; set; }
    }
}
