﻿using reshim.data.commandProcessor.Command;

namespace reshim.domain.Commands.Chat
{
    /// <summary>
    /// прочитать все сообщения
    /// </summary>
    public class ReadAllMessagesCommand : ICommand
    {
        public int DialogId { get; set; }
        public int UserId { get; set; }
    }
}
