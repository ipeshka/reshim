﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class CategoryArticleWithParentFetchStrategy : IncludeStrategy<model.Entities.CategoryArticle> 
    {
        public CategoryArticleWithParentFetchStrategy() 
        { 
            Include(b => b.Parent);
        } 
    }
}
