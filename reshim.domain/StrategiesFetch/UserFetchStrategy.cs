﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class UserFetchStrategy : IncludeStrategy<model.Entities.User> 
    {
        public UserFetchStrategy()
        {
            Include(b => b.Profile);
        } 
    }
}
