﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class LessonFetchStrategyNumber1 : IncludeStrategy<model.Entities.Lesson>
    {
        public LessonFetchStrategyNumber1()
        {
            Include(b => b.Chapter);
            Include(b => b.NameLesson);
            Include(b => b.CategoryLesson);
            Include(b => b.Reviews);
        }
    }
}
