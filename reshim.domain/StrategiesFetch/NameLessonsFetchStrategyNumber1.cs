﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class NameLessonsFetchStrategyNumber1 : IncludeStrategy<model.Entities.NameLesson>
    {
        public NameLessonsFetchStrategyNumber1()
        {
            Include(b => b.Chapters).Include(b => b.Lessons);
            Include(b => b.Lessons);
        }
    }
}
