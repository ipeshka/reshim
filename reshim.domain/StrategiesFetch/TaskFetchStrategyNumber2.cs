﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class TaskFetchStrategyNumber2 : IncludeStrategy<model.Entities.Task> 
    {
        public TaskFetchStrategyNumber2() 
        {
            Include(b => b.Customer);
            Include(b => b.TypeWork); 
            Include(b => b.Subject);
            Include(b => b.Subscribers).Include(x => x.User);
        } 
    }
}
