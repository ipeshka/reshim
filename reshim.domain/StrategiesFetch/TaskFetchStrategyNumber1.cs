﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class TaskFetchStrategyNumber1 : IncludeStrategy<model.Entities.Task> 
    {
        public TaskFetchStrategyNumber1() 
        {
            Include(b => b.Customer);
            Include(b => b.Decided); 
            Include(b => b.Condition); 
            Include(b => b.Solution); 
            Include(b => b.TypeWork); 
            Include(b => b.Subject);
            Include(b => b.Subscribers).Include(x => x.User);
        } 
    }
}
