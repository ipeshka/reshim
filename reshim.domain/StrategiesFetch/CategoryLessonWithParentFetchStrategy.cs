﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class CategoryLessonWithParentFetchStrategy : IncludeStrategy<model.Entities.CategoryLesson> 
    {
        public CategoryLessonWithParentFetchStrategy() 
        { 
            Include(b => b.Parent);
        } 
    }
}
