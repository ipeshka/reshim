﻿using reshim.data.infrastructure.IncludeStrategy;

namespace reshim.domain.StrategiesFetch
{
    public class TaskFetchStrategyNumber3 : IncludeStrategy<model.Entities.Task> 
    {
        public TaskFetchStrategyNumber3() 
        { 
            Include(b => b.Condition); 
            Include(b => b.Solution); 
            Include(b => b.Customer); 
            Include(b => b.Decided); 
            Include(b => b.TypeWork); 
            Include(b => b.Subject); 
        } 
    }
}
