﻿using System.Data.Entity;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.CompletedTask;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.CompletedTask
{
    public class DeleteCompletedTaskHandler : ICommandHandler<DeleteCompletedTaskCommand>
    {
        private readonly IComplitedTaskRepository _taskRepository;
        private readonly IHistoryComplitedTaskRepository _historyComplitedTaskRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IUnitOfWork _unitOfWork;


        public DeleteCompletedTaskHandler(IComplitedTaskRepository taskRepository, IHistoryComplitedTaskRepository historyComplitedTaskRepository, IReviewRepository reviewRepository, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _historyComplitedTaskRepository = historyComplitedTaskRepository;
            _reviewRepository = reviewRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(DeleteCompletedTaskCommand command)
        {
            var task = _taskRepository.GetDataContext().Include(x => x.HistoriesCompletedTasks).Include(x => x.Solution).Include(x => x.Reviews).FirstOrDefault(x => x.Id == command.TaskId);

            if(task == null) return new CommandResult(false);

            var idsDelete = task.HistoriesCompletedTasks.Select(x => x.Id).ToList();
            _historyComplitedTaskRepository.Delete(x => idsDelete.Any(i => i == x.Id));

            var idsReviewsDelete = task.Reviews.Select(x => x.Id).ToList();
            _reviewRepository.Delete(x => idsReviewsDelete.Any(i => i == x.Id));

            string pathFile = task.Solution.Path.ToFullPath();
            _taskRepository.Delete(task);
            _unitOfWork.Commit();

            if (System.IO.File.Exists(pathFile))
                System.IO.File.Delete(pathFile);

            return new CommandResult(true, task.Id);
        }
    }
}
