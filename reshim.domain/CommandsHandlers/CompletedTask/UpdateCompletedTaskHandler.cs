﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.CompletedTask;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.CompletedTask
{
    public class UpdateCompletedTaskHandler : ICommandHandler<UpdateCompletedTaskCommand>
    {
        private readonly IComplitedTaskRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileRepository _fileRepository;

        public UpdateCompletedTaskHandler(
            IComplitedTaskRepository taskRepository,
            IFileRepository fileRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
            _fileRepository = fileRepository;
        }

        public ICommandResult Execute(UpdateCompletedTaskCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var task = _taskRepository.GetById(command.Id);
                    var currentSolutionId = task.SolutionId;
                    task.Name = command.Name;
                    task.TitleSeo = command.TitleSeo;
                    task.DescriptionSeo = command.DescriptionSeo;
                    task.KeywordsSeo = command.KeywordsSeo;
                    task.Condition = command.Condition;
                    task.SolutionId = command.FileId;
                    task.Cost = command.Cost;
                    task.SubjectId = command.SubjectId;
                    task.Comment = command.Comment;
                    _taskRepository.Update(task);
                    _unitOfWork.Commit();

                    if (currentSolutionId != command.FileId)
                    {
                        // delete old solution
                        var oldSolution = _fileRepository.GetById(currentSolutionId);
                        var path = oldSolution.Path.ToFullPath();
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                        _fileRepository.Delete(oldSolution);

                        // rename file
                        var file = _fileRepository.GetById(command.FileId);
                        _fileRepository.Update(file.RenameFile("reshim.net_completed_task_" + task.Id + Path.GetExtension(file.Name)));
                        _unitOfWork.Commit();
                    }

                    scope.Complete();
                    return new CommandResult(true, task.Id);
                }
                catch
                {
                    return new CommandResult(false);
                }
            }
        }
    }
}
