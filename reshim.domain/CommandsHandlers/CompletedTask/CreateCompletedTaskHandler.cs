﻿using System.IO;
using System.Transactions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.CompletedTask;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.CompletedTask
{
    public class CreateCompletedTaskHandler : ICommandHandler<CreateCompletedTaskCommand>
    {
        private readonly IComplitedTaskRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileRepository _fileRepository;

        public CreateCompletedTaskHandler(
            IComplitedTaskRepository taskRepository,
            IFileRepository fileRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
            _fileRepository = fileRepository;
        }

        public ICommandResult Execute(CreateCompletedTaskCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var task = new model.Entities.CompletedTask()
                    {
                        Name = command.Name,
                        TitleSeo = command.TitleSeo,
                        DescriptionSeo = command.DescriptionSeo,
                        KeywordsSeo = command.KeywordsSeo,
                        Condition = command.Condition,
                        SolutionId = command.FileId,
                        Cost = command.Cost,
                        SubjectId = command.SubjectId,
                        Comment = command.Comment
                    };
                    _taskRepository.Add(task);
                    _unitOfWork.Commit();

                    var file = _fileRepository.GetById(command.FileId);
                    _fileRepository.Update(file.RenameFile("reshim.net_completed_task_" + task.Id + Path.GetExtension(file.Name)));
                    _unitOfWork.Commit();

                    scope.Complete();
                    return new CommandResult(true, task.Id);
                }
                catch
                {
                    return new CommandResult(false);
                }
            }
        }
    }
}
