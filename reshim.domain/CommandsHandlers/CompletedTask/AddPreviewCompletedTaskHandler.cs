﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.CompletedTask;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.CompletedTask
{
    public class AddPreviewCompletedTaskHandler : ICommandHandler<AddPreviewCompletedTaskCommand>
    {
        private readonly IComplitedTaskRepository _complitedTaskRepository;
        private readonly IUnitOfWork _unitOfWork;


        public AddPreviewCompletedTaskHandler(IComplitedTaskRepository complitedTaskRepository, IUnitOfWork unitOfWork)
        {
            _complitedTaskRepository = complitedTaskRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(AddPreviewCompletedTaskCommand command)
        {
            var task = _complitedTaskRepository.AllIncluding(x => x.Reviews)
                .FirstOrDefault(x => x.Id == command.TaskId);

            if (task != null && task.Reviews.FirstOrDefault(x => x.Ip == command.Ip) == null)
            {
                task.Reviews.Add(new Review() { Ip = command.Ip });
            }

            _complitedTaskRepository.Update(task);

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
