﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.File;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.File {
    public class CreateFileHandler : ICommandHandler<CreateFileCommand>
    {
        private readonly IFileRepository _fileRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateFileHandler(IFileRepository fileRepository, IUnitOfWork unitOfWork) {
            _fileRepository = fileRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateFileCommand command)
        {
            var file = new model.Entities.File() { Name = command.Name, Path = command.Path, TypeFile = command.TypeFile };
            _fileRepository.Add(file);
            _unitOfWork.Commit();
            return new CommandResult(true, file.FileId);
        }
    }
}
