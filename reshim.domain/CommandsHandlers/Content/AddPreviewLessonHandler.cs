﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Content
{
    public class AddPreviewLessonHandler : ICommandHandler<AddPreviewLessonCommand>
    {
        private readonly ILessonRepository _lessonRepository;
        private readonly IUnitOfWork _unitOfWork;


        public AddPreviewLessonHandler(ILessonRepository lessonRepository, IUnitOfWork unitOfWork)
        {
            _lessonRepository = lessonRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(AddPreviewLessonCommand command)
        {
            var lesson = _lessonRepository.AllIncluding(x => x.Reviews)
                .FirstOrDefault(x => x.Id == command.LessonId);

            if (lesson != null && lesson.Reviews.FirstOrDefault(x => x.Ip == command.Ip) == null)
            {
                lesson.Reviews.Add(new Review() { Ip = command.Ip });
            }

            _lessonRepository.Update(lesson);

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
