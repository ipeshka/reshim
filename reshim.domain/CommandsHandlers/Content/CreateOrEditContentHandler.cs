﻿using System;
using System.Linq;
using reshim.common.core.Extensions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.Content
{
    public class CreateOrEditTypeWorkHandler : ICommandHandler<CreateOrEditTypeWorkCommand>
    {
        private readonly ITypeWorkRepository _typeWorkRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditTypeWorkHandler(ITypeWorkRepository typeWorkRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _typeWorkRepository = typeWorkRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditTypeWorkCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias || x.RedirectFromUrl == alias);

                var typeWork = new TypeWork()
                {
                    Name = command.Name,
                    ShortName = command.ShortName,
                    TitleSeo = command.TitleSeo,
                    DescriptionSeo = command.DescriptionSeo,
                    KeywordsSeo = command.KeywordsSeo,
                    Text = command.Text,
                    Cost = command.Cost,
                    Period = command.Period,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    RedirectFromUrl = command.RedirectFromUrl.Trim('/')
                };

                _typeWorkRepository.Add(typeWork);
            }
            else // update
            {
                var typeWork = _typeWorkRepository.GetById(command.Id);
                typeWork.Name = command.Name;
                typeWork.ShortName = command.ShortName;
                typeWork.TitleSeo = command.TitleSeo;
                typeWork.DescriptionSeo = command.DescriptionSeo;
                typeWork.KeywordsSeo = command.KeywordsSeo;
                typeWork.Text = command.Text;
                typeWork.Cost = command.Cost;
                typeWork.Period = command.Period;
                typeWork.DateUpdate = DateTime.Now;
                typeWork.RedirectFromUrl = string.IsNullOrEmpty(command.RedirectFromUrl) ? command.RedirectFromUrl : command.RedirectFromUrl.Trim('/');

                _typeWorkRepository.Update(typeWork);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditSubjectHandler : ICommandHandler<CreateOrEditSubjectCommand>
    {
        private readonly ISubjectRepository _subjectRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditSubjectHandler(ISubjectRepository subjectRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _subjectRepository = subjectRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditSubjectCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var subject = new Subject()
                {
                    Name = command.Name,
                    Preview = command.Preview,
                    Image = command.Image,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias
                };

                _subjectRepository.Add(subject);
            }
            else // update
            {
                var subject = _subjectRepository.GetById(command.Id);
                subject.Name = command.Name;
                subject.Preview = command.Preview;
                subject.Image = command.Image;
                subject.DateUpdate = DateTime.Now;

                _subjectRepository.Update(subject);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditNewsHandler : ICommandHandler<CreateOrEditNewsCommand>
    {
        private readonly INewsRepository _newsRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditNewsHandler(INewsRepository newsRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _newsRepository = newsRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditNewsCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var news = new News()
                {
                    Name = command.Name,
                    TitleSeo = command.TitleSeo,
                    DescriptionSeo = command.DescriptionSeo,
                    KeywordsSeo = command.KeywordsSeo,
                    Preview = command.Preview,
                    Text = command.Text,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias
                };

                _newsRepository.Add(news);
            }
            else // update
            {
                var news = _newsRepository.GetById(command.Id);
                news.Name = command.Name;
                news.TitleSeo = command.TitleSeo;
                news.DescriptionSeo = command.DescriptionSeo;
                news.KeywordsSeo = command.KeywordsSeo;
                news.Preview = command.Preview;
                news.Text = command.Text;
                news.DateUpdate = DateTime.Now;

                _newsRepository.Update(news);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditSeoArticleHandler : ICommandHandler<CreateOrEditSeoArticleCommand>
    {
        private readonly ISeoArticleRepository _seoArticleRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditSeoArticleHandler(ISeoArticleRepository seoArticleRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _seoArticleRepository = seoArticleRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditSeoArticleCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias || x.RedirectFromUrl == alias);

                var seoArticle = new SeoArticle()
                {
                    Name = command.Name,
                    ShortName = command.ShortName,
                    TitleSeo = command.TitleSeo,
                    DescriptionSeo = command.DescriptionSeo,
                    KeywordsSeo = command.KeywordsSeo,
                    Text = command.Text,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    TypeWorkId = command.TypeWorkId,
                    RedirectFromUrl = string.IsNullOrEmpty(command.RedirectFromUrl) ? command.RedirectFromUrl : command.RedirectFromUrl.Trim('/')
                };

                _seoArticleRepository.Add(seoArticle);
            }
            else // update
            {
                var seoArticle = _seoArticleRepository.GetById(command.Id);
                seoArticle.Name = command.Name;
                seoArticle.ShortName = command.ShortName;
                seoArticle.TitleSeo = command.TitleSeo;
                seoArticle.DescriptionSeo = command.DescriptionSeo;
                seoArticle.KeywordsSeo = command.KeywordsSeo;
                seoArticle.Text = command.Text;
                seoArticle.DateUpdate = DateTime.Now;
                seoArticle.TypeWorkId = command.TypeWorkId;
                seoArticle.RedirectFromUrl = !string.IsNullOrEmpty(command.RedirectFromUrl) ? command.RedirectFromUrl.Trim('/') : command.RedirectFromUrl;

                _seoArticleRepository.Update(seoArticle);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditCategoryArticleHandler : ICommandHandler<CreateOrEditCategoryArticleCommand>
    {
        private readonly ICategoryArticleRepository _categoryArticleRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditCategoryArticleHandler(ICategoryArticleRepository categoryArticleRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _categoryArticleRepository = categoryArticleRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditCategoryArticleCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var category = new CategoryArticle()
                {
                    Name = command.Name,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    ParentId = command.ParentId
                };

                _categoryArticleRepository.Add(category);
            }
            else // update
            {
                var category = _categoryArticleRepository.GetById(command.Id);
                category.Name = command.Name;
                category.DateUpdate = DateTime.Now;
                category.ParentId = command.ParentId;

                _categoryArticleRepository.Update(category);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditArticleHandler : ICommandHandler<CreateOrEditArticleCommand>
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditArticleHandler(IArticleRepository articleRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _articleRepository = articleRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditArticleCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var obj = new Article()
                {
                    Name = command.Name,
                    TitleSeo = command.TitleSeo,
                    DescriptionSeo = command.DescriptionSeo,
                    KeywordsSeo = command.KeywordsSeo,
                    Text = command.Text,
                    Preview = command.Preview,
                    Image = command.Image,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    CategoryId = command.CategoryId
                };

                _articleRepository.Add(obj);
            }
            else // update
            {
                var obj = _articleRepository.GetById(command.Id);
                obj.Name = command.Name;
                obj.TitleSeo = command.TitleSeo;
                obj.DescriptionSeo = command.DescriptionSeo;
                obj.KeywordsSeo = command.KeywordsSeo;
                obj.Text = command.Text;
                obj.Preview = command.Preview;
                obj.Image = command.Image;
                obj.DateUpdate = DateTime.Now;
                obj.CategoryId = command.CategoryId;

                _articleRepository.Update(obj);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditCategoryLessonHandler : ICommandHandler<CreateOrEditCategoryLessonCommand>
    {
        private readonly ICategoryLessonRepository _categoryLessonRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditCategoryLessonHandler(ICategoryLessonRepository categoryLessonRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _categoryLessonRepository = categoryLessonRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditCategoryLessonCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var category = new CategoryLesson()
                {
                    Name = command.Name,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    ParentId = command.ParentId
                };

                _categoryLessonRepository.Add(category);
            }
            else // update
            {
                var category = _categoryLessonRepository.GetById(command.Id);
                category.Name = command.Name;
                category.DateUpdate = DateTime.Now;
                category.ParentId = command.ParentId;

                _categoryLessonRepository.Update(category);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditNameLessonHandler : ICommandHandler<CreateOrEditNameLessonCommand>
    {
        private readonly INameLessonRepository _nameLessonRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditNameLessonHandler(INameLessonRepository nameLessonRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _nameLessonRepository = nameLessonRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditNameLessonCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                var name = new NameLesson()
                {
                    Name = command.Name,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    CategoryId = command.CategoryId
                };

                _nameLessonRepository.Add(name);
            }
            else // update
            {
                var name = _nameLessonRepository.GetById(command.Id);
                name.Name = command.Name;
                name.DateUpdate = DateTime.Now;
                name.CategoryId = command.CategoryId;

                _nameLessonRepository.Update(name);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditChapterHandler : ICommandHandler<CreateOrEditChapterCommand>
    {
        private readonly IChapterRepository _chapterRepository;
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditChapterHandler(IChapterRepository chapterRepository, IContentRepository contentRepository, IUnitOfWork unitOfWork)
        {
            _chapterRepository = chapterRepository;
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditChapterCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Get(x => x.Alias == alias);

                int maxPosition = _chapterRepository.GetDataContext().Select(x => x.Position).DefaultIfEmpty(0).Max(x => x);

                var chapter = new Chapter()
                {
                    Name = command.Name,
                    DateCreate = DateTime.Now,
                    Alias = isContent != null ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    Position = maxPosition + 1,
                    NameLessonId = command.NameLessonId
                };

                _chapterRepository.Add(chapter);
            }
            else // update
            {
                var chapter = _chapterRepository.GetById(command.Id);
                chapter.Name = command.Name;
                chapter.DateUpdate = DateTime.Now;
                chapter.NameLessonId = command.NameLessonId;

                _chapterRepository.Update(chapter);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }

    public class CreateOrEditLessonHandler : ICommandHandler<CreateOrEditLessonCommand>
    {
        private readonly ILessonRepository _lessonRepository;
        private readonly IContentRepository _contentRepository;
        private readonly INameLessonRepository _nameLessonRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateOrEditLessonHandler(ILessonRepository lessonRepository, IContentRepository contentRepository, INameLessonRepository nameLessonRepository, IUnitOfWork unitOfWork)
        {
            _lessonRepository = lessonRepository;
            _contentRepository = contentRepository;
            _nameLessonRepository = nameLessonRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateOrEditLessonCommand command)
        {
            if (command.Id == 0) // create
            {
                var alias = command.Name.ToTranslit().ToSeoUrl();
                var isContent = _contentRepository.Count(x => x.Alias == alias);
                var nameLesson = _nameLessonRepository.GetById(command.NameLessonId);
                int maxPosition = _lessonRepository.GetDataContext().Max(x => x.Position);

                var lesson = new Lesson()
                {
                    Name = command.Name,
                    TitleSeo = command.TitleSeo,
                    DescriptionSeo = command.DescriptionSeo,
                    KeywordsSeo = command.KeywordsSeo,
                    Text = command.Text,
                    Preview = command.Preview,
                    Image = command.Image,
                    DateCreate = DateTime.Now,
                    Alias = isContent > 0 ? string.Format("{0}-{1}", alias, new Random().Next()) : alias,
                    ChapterId = command.ChapterId,
                    NameLessonId = command.NameLessonId,
                    CategoryIdLesson = nameLesson.CategoryId,
                    Position = maxPosition + 1
                };

                _lessonRepository.Add(lesson);
            }
            else // update
            {
                var lesson = _lessonRepository.GetById(command.Id);
                var nameLesson = _nameLessonRepository.GetById(command.NameLessonId);

                lesson.Name = command.Name;
                lesson.TitleSeo = command.TitleSeo;
                lesson.DescriptionSeo = command.DescriptionSeo;
                lesson.KeywordsSeo = command.KeywordsSeo;
                lesson.Text = command.Text;
                lesson.Preview = command.Preview;
                lesson.Image = command.Image;
                lesson.DateUpdate = DateTime.Now;
                lesson.NameLessonId = command.NameLessonId;
                lesson.ChapterId = command.ChapterId;
                lesson.CategoryIdLesson = nameLesson.CategoryId;
                _lessonRepository.Update(lesson);
            }

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
