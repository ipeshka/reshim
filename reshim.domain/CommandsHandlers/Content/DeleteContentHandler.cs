﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Content
{
    public class DeleteContentHandler : ICommandHandler<DeleteContentCommand>
    {
        private readonly IContentRepository _contentRepository;
        private readonly IReviewRepository _reviewRepository;
        private readonly IUnitOfWork _unitOfWork;

        public DeleteContentHandler(IContentRepository contentRepository, IReviewRepository reviewRepository, IUnitOfWork unitOfWork)
        {
            _contentRepository = contentRepository;
            _reviewRepository = reviewRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(DeleteContentCommand command)
        {
            var content = _contentRepository.GetById(command.Id);
            _contentRepository.Delete(content);

            _reviewRepository.Delete(x => x.ArticleId == content.Id || x.LessonId == content.Id);

            try
            {
                _unitOfWork.Commit();
            }
            catch (Exception e)
            {
                return new CommandResult(false);
            }
            
            return new CommandResult(true);
        }
    }
}
