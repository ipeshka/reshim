﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Content
{
    public class UpdatePositionContentHandler : ICommandHandler<UpdatePositionContentCommand>
    {
        private readonly IContentRepository _contentRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISql _sql;


        public UpdatePositionContentHandler(IContentRepository contentRepository, ISql sql, IUnitOfWork unitOfWork)
        {
            _contentRepository = contentRepository;
            _unitOfWork = unitOfWork;
            _sql = sql;
        }

        public ICommandResult Execute(UpdatePositionContentCommand command)
        {
            int i = command.CurPage == 1 ? 1 : (command.CurPage - 1) * 11;
            foreach (var id in command.Ids)
            {
                var content = _contentRepository.GetById(id);
                content.Position = i++;
                _contentRepository.Update(content);             
            }


            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
