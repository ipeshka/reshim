﻿using System;
using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Content
{
    public class CanCreateOrEditTypeWork : IValidationHandler<CanCreateOrEditContent>
    {
        private readonly IContentRepository _contentRepository;

        public CanCreateOrEditTypeWork(IContentRepository contentRepository)
        {
            _contentRepository = contentRepository;
        }

        public IEnumerable<ValidationResult> Validate(CanCreateOrEditContent command)
        {
            if (!string.IsNullOrEmpty(command.RedirectFromUrl))
            {
                var redirect = command.RedirectFromUrl.Trim('/');
                var content = _contentRepository.Get(
                    x => x.Id != command.Id &&
                    x.RedirectFromUrl.Equals(redirect, StringComparison.CurrentCultureIgnoreCase) ||
                    x.Alias.Equals(redirect, StringComparison.CurrentCultureIgnoreCase));

                if (content != null) yield return new ValidationResult("", string.Format("С данной страницы редирект/алиас происходит на страницу \"{0}\"", content.Name));
            }
        }
    }
}
