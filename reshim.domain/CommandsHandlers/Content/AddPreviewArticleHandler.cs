﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Content;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Content
{
    public class AddPreviewArticleHandler : ICommandHandler<AddPreviewArticleCommand>
    {
        private readonly IArticleRepository _articleRepository;
        private readonly IUnitOfWork _unitOfWork;


        public AddPreviewArticleHandler(IArticleRepository articleRepository, IUnitOfWork unitOfWork)
        {
            _articleRepository = articleRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(AddPreviewArticleCommand command)
        {
            var article = _articleRepository.AllIncluding(x => x.Reviews)
                .FirstOrDefault(x => x.Id == command.ArticleId);

            if (article != null && article.Reviews.FirstOrDefault(x => x.Ip == command.Ip) == null)
            {
                article.Reviews.Add(new Review() { Ip = command.Ip });
            }

            _articleRepository.Update(article);

            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
