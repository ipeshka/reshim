﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanPaymentTask : IValidationHandler<PaymentTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IPaymentDataRepository _paymentDataRepository;

        public CanPaymentTask(ITaskRepository taskRepository, IPaymentDataRepository paymentDataRepository)
        {
            _taskRepository = taskRepository;
            _paymentDataRepository = paymentDataRepository;
        }

        public IEnumerable<ValidationResult> Validate(PaymentTaskCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);
            var paymentCustomer = _paymentDataRepository.GetById(task.CustomerId);

            if (paymentCustomer.Ballance < task.Cost / 2) yield return new ValidationResult("", string.Format("Не хватает суммы для оплаты ({0} рублей).", task.Cost / 2 - paymentCustomer.Ballance));
            if (task.CustomerId != command.UserId) yield return new ValidationResult("", "Вы не можете оплатить данный заказ.");
        }
    }
}
