﻿using System;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.Task
{
    public class EditTaskHandler : ICommandHandler<EditTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EditTaskHandler(
            ITaskRepository taskRepository,
            IUnitOfWork unitOfWork)
        {
            this._taskRepository = taskRepository;
            this._unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(EditTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Condition).First(x => x.Id == command.TaskId);

            task.DateOfExecution = command.DateOfExecution;
            task.Cost = command.Cost ?? Decimal.Zero;
            task.Comment = command.Comment;

            var tmpPath = string.Empty;
            if (command.FileId != 0)
            {
                tmpPath = task.Condition.Path.ToFullPath();
                task.ConditionId = command.FileId;
            }

            task.SubjectId = command.SubjectId;
            task.TypeWorkId = command.TypeWorkId;
            _taskRepository.Update(task);
            _unitOfWork.Commit();

            if (System.IO.File.Exists(tmpPath))
                System.IO.File.Delete(tmpPath);

            return new CommandResult(true, task.Id);
        }
    }
}
