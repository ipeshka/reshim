﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanEditTask : IValidationHandler<EditTaskCommand>
    {
        public IEnumerable<ValidationResult> Validate(EditTaskCommand command)
        {
            return new List<ValidationResult>() {};
        }
    }
}
