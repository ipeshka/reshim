﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.Task
{
    public class DeleteTaskHandler : ICommandHandler<DeleteTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ISubscriberRepository _subscriberRepository;
        private readonly IUnitOfWork _unitOfWork;


        public DeleteTaskHandler(ITaskRepository taskRepository, ISubscriberRepository subscriberRepository,
            IUnitOfWork unitOfWork)
        {
            this._subscriberRepository = subscriberRepository;
            this._taskRepository = taskRepository;
            this._unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(DeleteTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Condition, x => x.Solution).First(x => x.Id == command.TaskId);
            var subscribers = _subscriberRepository.GetMany(x => x.TaskId == command.TaskId).ToArray();
            for (int i = 0; i < subscribers.Length; i++)
            {
                var subs = subscribers[i];
                _subscriberRepository.Delete(subs);
            }

            string tmpPathCondition = task.Condition.Path.ToFullPath();
            string tmpPathSolution = "";
            if (task.Solution != null)
            {
                tmpPathSolution = task.Solution.Path.ToFullPath();
            }


            _taskRepository.Delete(task);
            _unitOfWork.Commit();

            if (System.IO.File.Exists(tmpPathCondition))
                System.IO.File.Delete(tmpPathCondition);

            if (System.IO.File.Exists(tmpPathSolution))
                System.IO.File.Delete(tmpPathSolution);


            return new CommandResult(true, task.Id);
        }
    }
}
