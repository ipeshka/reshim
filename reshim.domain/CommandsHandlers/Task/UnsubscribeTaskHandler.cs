﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class UnsubscribeTaskHandler : ICommandHandler<UnsubscribeTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ISubscriberRepository _subscriberRepository;
        private readonly IUnitOfWork _unitOfWork;


        public UnsubscribeTaskHandler(
            ITaskRepository taskRepository, 
            ISubscriberRepository subscriberRepository,
            IUnitOfWork unitOfWork)
        {
            this._taskRepository = taskRepository;
            this._subscriberRepository = subscriberRepository;
            this._unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(UnsubscribeTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Subscribers).First(x => x.Id == command.TaskId);
            var subscriber = task.Subscribers.FirstOrDefault(x => x.UserId == command.UserId);
            _subscriberRepository.Delete(subscriber);
            if (task.Subscribers.Count == 0) task.Status = StatusTask.New;
            _taskRepository.Update(task);
            _unitOfWork.Commit();
            return new CommandResult(true, task.Id);
        }
    }
}
