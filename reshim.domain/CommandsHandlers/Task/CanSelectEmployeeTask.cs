﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanSelectEmployeeTask : IValidationHandler<SelectEmployeeTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ISubscriberRepository _subscriberRepository;
        private readonly IPaymentDataRepository _paymentDataRepository;

        public CanSelectEmployeeTask(ITaskRepository taskRepository, ISubscriberRepository subscriberRepository, IPaymentDataRepository paymentDataRepository)
        {
            _taskRepository = taskRepository;
            _subscriberRepository = subscriberRepository;
            _paymentDataRepository = paymentDataRepository;
        }
        public IEnumerable<ValidationResult> Validate(SelectEmployeeTaskCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);
            var subscriber = _subscriberRepository.GetById(command.SubscriberId);
            var paymentCustomer = _paymentDataRepository.GetById(task.CustomerId);

            if (paymentCustomer.Ballance < subscriber.Value / 2) yield return new ValidationResult("", string.Format("Для предоплаты, на Вашем счёте не хватает {0} рублей. Пополните балланс.", subscriber.Value / 2 - paymentCustomer.Ballance));

            if (task.Decided != null) yield return new ValidationResult("", "Исполнитель уже выбран.");
        }
    }
}
