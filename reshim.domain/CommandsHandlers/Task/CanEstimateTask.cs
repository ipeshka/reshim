﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanEstimateTask : IValidationHandler<EstimateTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        public CanEstimateTask(ITaskRepository taskRepository)
        {
            this._taskRepository = taskRepository;
        }
        public IEnumerable<ValidationResult> Validate(EstimateTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Subscribers).First(x => x.Id == command.TaskId);
            if (task.Subscribers.FirstOrDefault(x => x.UserId == command.UserId) != null) yield return new ValidationResult("", "Вы уже оценили данную задачу.");
        }
    }
}
