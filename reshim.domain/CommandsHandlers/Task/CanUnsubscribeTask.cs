﻿using System.Collections.Generic;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanUnsubscribeTask : IValidationHandler<UnsubscribeTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        public CanUnsubscribeTask(ITaskRepository taskRepository)
        {
            this._taskRepository = taskRepository;
        }

        public IEnumerable<ValidationResult> Validate(UnsubscribeTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Subscribers).First(x => x.Id == command.TaskId);
            if (task.Subscribers.All(x => x.UserId != command.UserId)) yield return new ValidationResult("", "Вас нет в списке подписок к заданию.");
        }
    }
}
