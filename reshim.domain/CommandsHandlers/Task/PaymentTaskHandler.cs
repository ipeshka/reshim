﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class PaymentTaskHandler : ICommandHandler<PaymentTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IPaymentDataRepository _paymentDataRepository;
        private readonly IInternalPaymentRepository _insidePaymentRepository;
        private readonly IUnitOfWork _unitOfWork;


        public PaymentTaskHandler(
            ITaskRepository taskRepository,
            IPaymentDataRepository paymentDataRepository,
            IInternalPaymentRepository insidePaymentRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _paymentDataRepository = paymentDataRepository;
            _insidePaymentRepository = insidePaymentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(PaymentTaskCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);

            var customerPaymentData = _paymentDataRepository.GetById(task.CustomerId);
            var decidedPaymentData = _paymentDataRepository.GetById(task.DecidedId.Value);

            // снимает с заказчика вторую половину суммы
            decimal amount = task.Cost / 2;
            customerPaymentData.Ballance -= amount;
            _insidePaymentRepository.Add(new InternalPayment() { TaskId = task.Id, Amount = -amount, PaymentDataId = customerPaymentData.Id});

            // исполнителю перечисляем всю сумму
            decidedPaymentData.Ballance += task.Cost;
            _insidePaymentRepository.Add(new InternalPayment() { TaskId = task.Id, Amount = task.Cost, PaymentDataId = decidedPaymentData.Id });
            
            task.Status = StatusTask.Completed;
            _taskRepository.Update(task);
            _unitOfWork.Commit();

            return new CommandResult(true, task.Id);
        }
    }
}
