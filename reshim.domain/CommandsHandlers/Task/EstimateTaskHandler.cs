﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class EstimateTaskHandler : ICommandHandler<EstimateTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly ISubscriberRepository _subscriberRepository;
        private readonly IUnitOfWork _unitOfWork;


        public EstimateTaskHandler(
            ITaskRepository taskRepository, 
            ISubscriberRepository subscriberRepository,
            IUnitOfWork unitOfWork)
        {
            _subscriberRepository = subscriberRepository;
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(EstimateTaskCommand command)
        {
            var subscriber = new Subscriber()
            {
                Value = command.Value,
                UserId = command.UserId,
                TaskId = command.TaskId,
                Comment = command.Comment,
                DateCreate = DateTime.Now
            };

            var task = _taskRepository.GetById(command.TaskId);
            task.Status = StatusTask.Rating;
            _subscriberRepository.Add(subscriber);
            _taskRepository.Update(task);
            _unitOfWork.Commit();
            return new CommandResult(true, task.Id);
        }
    }
}
