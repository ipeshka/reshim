﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Transactions;
using MassTransit;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CreateTaskHandler : ICommandHandler<CreateTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFileRepository _fileRepository;

        public CreateTaskHandler(
            IBus bus,
            ITaskRepository taskRepository,
            IFileRepository fileRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
            _fileRepository = fileRepository;
        }

        public ICommandResult Execute(CreateTaskCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var task = new model.Entities.Task()
                    {
                        CustomerId = command.CustomerId,
                        ConditionId = command.FileId,
                        DateOfExecution = command.DateOfExecution,
                        Cost = command.Cost ?? Decimal.Zero,
                        Status = StatusTask.New,
                        SubjectId = command.SubjectId,
                        TypeWorkId = command.TypeWorkId,
                        Comment = command.Comment,
                        Subscribers = new Collection<Subscriber>(),
                        InternalsPayments = new Collection<InternalPayment>()
                    };
                    _taskRepository.Add(task);
                    _unitOfWork.Commit();

                    var file = _fileRepository.GetById(command.FileId);
                    _fileRepository.Update(file.RenameFile("reshim.net_task_" + task.Id + Path.GetExtension(file.Name)));
                    _unitOfWork.Commit();

                    scope.Complete();

                    return new CommandResult(true, task.Id);
                }
                catch
                {
                    return new CommandResult(false);
                }
            }
        }
    }
}
