﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanUploadSolution : IValidationHandler<UploadSolutionCommand>
    {
        private readonly ITaskRepository _taskRepository;
        public CanUploadSolution(ITaskRepository taskRepository)
        {
            this._taskRepository = taskRepository;
        }
        public IEnumerable<ValidationResult> Validate(UploadSolutionCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);
            if (!task.DecidedId.HasValue) yield return new ValidationResult("", "У задачи нет исполнителя.");
            if (task.DecidedId.HasValue && task.DecidedId != command.DecidedId) yield return new ValidationResult("", "У вас нет прав на загрузке решения.");
            if (task.SolutionId.HasValue) yield return new ValidationResult("", "Решение уже загружено");
        }
    }
}
