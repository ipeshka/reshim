﻿using System;
using System.Linq;
using System.Transactions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class ChangeStatusTaskHandler : ICommandHandler<ChangeStatusTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IUnitOfWork _unitOfWork;

        public ChangeStatusTaskHandler(
            ITaskRepository taskRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(ChangeStatusTaskCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var currentDate = DateTime.Now.Date;
                    var notrelevateTasks =_taskRepository.GetMany(x => (x.Status == StatusTask.New || x.Status == StatusTask.Rating) && x.DateOfExecution < currentDate).ToList();
                    foreach (var task in notrelevateTasks)
                    {
                        task.Status = StatusTask.NotRelevant;
                        _taskRepository.Update(task);
                    }

                    _unitOfWork.Commit();
                    scope.Complete();
                    
                    return new CommandResult(true);
                }
                catch(Exception e)
                {
                    return new CommandResult(false);
                }
            }
        }
    }
}
