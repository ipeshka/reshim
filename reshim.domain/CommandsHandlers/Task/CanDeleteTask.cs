﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanDeleteTask : IValidationHandler<DeleteTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        public CanDeleteTask(ITaskRepository taskRepository)
        {
            this._taskRepository = taskRepository;
        }
        public IEnumerable<ValidationResult> Validate(DeleteTaskCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);
            if (task.CustomerId != command.UserId) yield return new ValidationResult("", "Вы не можете удалить данную задачу.");
        }
    }
}
