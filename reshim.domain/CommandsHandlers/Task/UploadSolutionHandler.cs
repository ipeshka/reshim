﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class UploadSolutionHandler : ICommandHandler<UploadSolutionCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITaskRepository _taskRepository;

        public UploadSolutionHandler(
            ITaskRepository taskRepository,
            IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            this._taskRepository = taskRepository;
        }

        public ICommandResult Execute(UploadSolutionCommand command)
        {
            var task = _taskRepository.GetById(command.TaskId);
            task.SolutionId = command.FileId;
            task.DateComplited = DateTime.Now;
            task.Status = StatusTask.CompletedNotPay;
            _taskRepository.Update(task);
            _unitOfWork.Commit();
            return new CommandResult(true, task.Id);
        }
    }
}
