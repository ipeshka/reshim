﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Task;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Task
{
    public class SelectEmployeeTaskHandler : ICommandHandler<SelectEmployeeTaskCommand>
    {
        private readonly ITaskRepository _taskRepository;
        private readonly IPaymentDataRepository _paymentDataRepository;
        private readonly IInternalPaymentRepository _insidePaymentRepository;
        private readonly IUnitOfWork _unitOfWork;


        public SelectEmployeeTaskHandler(
            ITaskRepository taskRepository,
            IPaymentDataRepository paymentDataRepository,
            IInternalPaymentRepository insidePaymentRepository,
            IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _paymentDataRepository = paymentDataRepository;
            _insidePaymentRepository = insidePaymentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(SelectEmployeeTaskCommand command)
        {
            var task = _taskRepository.AllIncluding(x => x.Subscribers).First(x => x.Id == command.TaskId);
            var subscriber = task.Subscribers.FirstOrDefault(x => x.SubscriberId == command.SubscriberId);

            if (subscriber != null)
            {
                var customerPaymentData = _paymentDataRepository.GetById(task.CustomerId);
                // снимает с заказчика половину суммы
                decimal amount = subscriber.Value / 2;
                customerPaymentData.Ballance -= amount;
                _insidePaymentRepository.Add(new InternalPayment() { TaskId = task.Id, Amount = -amount, PaymentDataId = customerPaymentData.Id });

                task.DecidedId = subscriber.UserId;
                task.Cost = subscriber.Value;
                task.Status = StatusTask.Agreement;
                _taskRepository.Update(task);
                _unitOfWork.Commit();
                return new CommandResult(true, task.Id);
            }

            return new CommandResult(false, task.Id);
        }
    }
}
