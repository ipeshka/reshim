﻿using System;
using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Task;

namespace reshim.domain.CommandsHandlers.Task
{
    public class CanCreateTask : IValidationHandler<CreateTaskCommand>
    {
        public IEnumerable<ValidationResult> Validate(CreateTaskCommand command)
        {
            if (command.DateOfExecution.Date < DateTime.Now.Date) yield return new ValidationResult("", "Выбрана уже прошедшая дата.");
        }
    }
}
