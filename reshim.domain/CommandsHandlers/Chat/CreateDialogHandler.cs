﻿using System;
using System.Data.SqlClient;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Chat;
using reshim.data.infrastructure.Database;

namespace reshim.domain.CommandsHandlers.Chat
{
    public class CreateDialogHandler : ICommandHandler<CreateDialogCommand>
    {
        private readonly ISql _sql;

        public CreateDialogHandler(ISql sql)
        {
            _sql = sql;
        }

        public ICommandResult Execute(CreateDialogCommand command)
        {
            const string sql = "INSERT INTO dbo.Dialogues (User1Id, User2Id, LastUpdate) VALUES (@user1Id, @user2Id, @lastUpdate) SELECT SCOPE_IDENTITY()";

            var result = _sql.ExecuteQuery<decimal>(sql,
                new SqlParameter("@user1Id", command.RecipientId),
                new SqlParameter("@lastUpdate", DateTime.Now),
                new SqlParameter("@user2Id", command.SenderId)
                ).FirstOrDefault();
            return new CommandResult(true, (int)result);
        }
    }
}
