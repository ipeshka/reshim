﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Chat;
using reshim.data.infrastructure.Database;

namespace reshim.domain.CommandsHandlers.Chat
{
    public class ReadAllMessagesHandler : ICommandHandler<ReadAllMessagesCommand>
    {
        private readonly ISql _sql;

        public ReadAllMessagesHandler(ISql sql)
        {
            _sql = sql;
        }

        public ICommandResult Execute(ReadAllMessagesCommand command)
        {
            _sql.ExecuteCommand("UPDATE dbo.Messages SET IsNew = @p0 WHERE DialogId = @p1 AND RecipientId = @p2", false, command.DialogId, command.UserId);
            return new CommandResult(true);
        }
    }
}
