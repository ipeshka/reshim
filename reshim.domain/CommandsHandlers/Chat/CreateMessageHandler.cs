﻿using System;
using System.Data.SqlClient;
using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Chat;
using reshim.data.infrastructure.Database;

namespace reshim.domain.CommandsHandlers.Chat
{
    public class CreateMessageHandler : ICommandHandler<CreateMessageCommand>
    {
        public class PairUsers
        {
            public int User1Id { get; set; }
            public int User2Id { get; set; }
        }


        private readonly ISql _sql;

        public CreateMessageHandler(ISql sql)
        {
            _sql = sql;
        }

        public ICommandResult Execute(CreateMessageCommand command)
        {
            var curDialog = _sql.ExecuteQuery<PairUsers>("SELECT User1Id, User2Id FROM dbo.Dialogues WHERE Id = @id", new SqlParameter("@id", command.DialogId)).First();

            const string sql = "INSERT INTO dbo.Messages (Content, IsNew, DateCreate, DialogId, RecipientId, SenderId) " +
                               "VALUES (@content, @isNew, @dateCreate, @dialogId, @recipientId, @senderId) SELECT SCOPE_IDENTITY()";

            var result = _sql.ExecuteQuery<decimal>(sql,
                new SqlParameter("@content", command.Content),
                new SqlParameter("@isNew", true),
                new SqlParameter("@dateCreate", DateTime.Now),
                new SqlParameter("@dialogId", command.DialogId),
                new SqlParameter("@recipientId", command.SenderId == curDialog.User1Id ? curDialog.User2Id : curDialog.User1Id),
                new SqlParameter("@senderId", command.SenderId)
                ).FirstOrDefault();

            // тригерром обновляем последнюю дату обновления диалога
            return new CommandResult(true, (int)result);
        }
    }
}
