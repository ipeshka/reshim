﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Profile;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.web.core.Extensions;

namespace reshim.domain.CommandsHandlers.Profile
{
    public class UpdatePhotoHandler : ICommandHandler<UpdatePhotoCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePhotoHandler(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(UpdatePhotoCommand command) {
            var user = _userRepository.GetById(command.UserId);
            var tmpPath = user.Photo.ToFullPath();
            user.Photo = command.Path;
            _userRepository.Update(user);
            _unitOfWork.Commit();

            if (System.IO.File.Exists(tmpPath))
                System.IO.File.Delete(tmpPath);

            return new CommandResult(true);
        }
    }
}
