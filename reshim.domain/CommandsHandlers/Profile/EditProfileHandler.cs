﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Profile;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Profile
{
    public class EditProfileHandler : ICommandHandler<EditProfileCommand>
    {
        private readonly IProfileRepository _profileRepository;
        private readonly IUnitOfWork _unitOfWork;

        public EditProfileHandler(IProfileRepository profileRepository, IUnitOfWork unitOfWork)
        {
            _profileRepository = profileRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(EditProfileCommand command)
        {
            var profile = _profileRepository.GetById(command.UserId);
            profile.City = command.City;
            profile.Comments = command.Comments;
            profile.Country = command.Country;
            profile.DateOfBirth = command.DateOfBirth;
            profile.Icq = command.Icq;
            profile.Phone = command.Phone;
            profile.PlaceOfStudy = command.PlaceOfStudy;
            profile.Skype = command.Skype;
            profile.UrlVk = command.UrlVk;

            _profileRepository.Update(profile);
            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
