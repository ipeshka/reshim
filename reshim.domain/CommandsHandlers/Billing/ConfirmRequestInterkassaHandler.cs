﻿using System.Linq;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Billing;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Billing
{
    public class ConfirmRequestInterkassaHandler : ICommandHandler<ConfirmRequestInterkassaCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPaymentRepository _paymentRepository;
        private readonly IHistoryComplitedTaskRepository _historyComplitedTaskRepository;

        public ConfirmRequestInterkassaHandler(
            IPaymentRepository paymentRepository,
            IHistoryComplitedTaskRepository historyComplitedTaskRepository,
            IUnitOfWork unitOfWork)
        {
            _paymentRepository = paymentRepository;
            _historyComplitedTaskRepository = historyComplitedTaskRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(ConfirmRequestInterkassaCommand command)
        {
            if (!command.HasCompletedTask)
            {
                var payment = _paymentRepository.AllIncluding(x => x.PaymentData).First(x => x.Id == command.PaymentNumber);
                payment.Amount = command.Amount;
                payment.Currency = command.Currency;
                payment.State = command.State;
                payment.PaywayVia = command.PaywayVia;

                if (payment.State == PaymentState.Success) payment.PaymentData.Ballance += payment.Amount;

                _paymentRepository.Update(payment);
            }
            else
            {
                var history = _historyComplitedTaskRepository.GetById(command.PaymentNumber);
                history.Currency = command.Currency;
                history.State = command.State;
                history.PaywayVia = command.PaywayVia;
                _historyComplitedTaskRepository.Update(history);
            }

            _unitOfWork.Commit();

            return new CommandResult(true);
        }
    }
}
