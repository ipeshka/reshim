﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Billing;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Billing
{
    public class CanAddWithdrawal : IValidationHandler<AddWithdrawalCommand>
    {
        private readonly IPaymentDataRepository _paymentDataRepository;

        public CanAddWithdrawal(IPaymentDataRepository paymentDataRepository)
        {
            _paymentDataRepository = paymentDataRepository;
        }

        public IEnumerable<ValidationResult> Validate(AddWithdrawalCommand command)
        {
            var paymentData = _paymentDataRepository.GetById(command.UserId);
            if (paymentData == null)
                yield return new ValidationResult("", "Пользователя не сущесвует.");

            if (paymentData != null && paymentData.Ballance < command.Amount) yield return new ValidationResult("", "Не достаточно суммы на счету.");
        }
    }
}
