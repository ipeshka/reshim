﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Billing;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Billing
{
    public class AddWithdrawalHandler : ICommandHandler<AddWithdrawalCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWithdrawalRepository _withdrawalRepository;

        public AddWithdrawalHandler(
            IWithdrawalRepository withdrawalRepository,
            IUnitOfWork unitOfWork)
        {
            _withdrawalRepository = withdrawalRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(AddWithdrawalCommand command)
        {
            var withdrawal = new Withdrawal()
            {
                Amount = command.Amount,
                Create = DateTime.Now,
                State = WithdrawalState.New,
                PaywayVia = command.TypePayment,
                Comment = command.Comment,
                Purse = command.Purse,
                PaymentDataId = command.UserId
            };

            _withdrawalRepository.Add(withdrawal);
            _unitOfWork.Commit();
            return new CommandResult(true, withdrawal.Id);
        }
    }
}
