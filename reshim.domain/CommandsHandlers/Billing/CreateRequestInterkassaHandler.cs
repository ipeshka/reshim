﻿using System;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Billing;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Billing
{
    public class CreateRequestInterkassaHandler : ICommandHandler<CreateRequestInterkassaCommand>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPaymentRepository _paymentRepository;

        public CreateRequestInterkassaHandler(
            IPaymentRepository paymentRepository,
            IUnitOfWork unitOfWork)
        {
            _paymentRepository = paymentRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(CreateRequestInterkassaCommand command)
        {
            var invoice = new Payment()
            {
                Amount = command.Amount,
                Create = DateTime.Now,
                State = PaymentState.New,
                PaywayVia = command.PaywayVia,
                PaymentDataId = command.UserId
            };

            _paymentRepository.Add(invoice);
            _unitOfWork.Commit();
            return new CommandResult(true, invoice.Id);
        }
    }
}
