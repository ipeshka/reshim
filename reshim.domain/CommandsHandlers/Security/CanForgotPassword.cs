﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class CanForgotPassword : IValidationHandler<UserForgotPasswordCommand>
    {
        private readonly IUserRepository _userRepository;

        public CanForgotPassword(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<ValidationResult> Validate(UserForgotPasswordCommand command)
        {
            Expression<Func<User, bool>> expEmail = x => x.Email == command.EmailOrLogin;
            Expression<Func<User, bool>> expLogin = x => x.Login == command.EmailOrLogin;
            var user = _userRepository.Get(command.HasEmail ? expEmail : expLogin);

            if (user != null)
            {
                if (!user.Activated) yield return new ValidationResult("", "Ваша учётная запись не активна.");
                if (user.IsService != 0)
                    yield return
                        new ValidationResult("",
                            "Вы не можете восстановить пароль. Поскольку зарегистрированы через сервис (ВК).");
            }
            else
                yield return new ValidationResult("", "Пользователя с данным email не сущесвует.");
        }
    }
}
