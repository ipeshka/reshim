﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class CanLoginUser : IValidationHandler<UserLoginCommand>
    {
        private readonly IUserRepository _userRepository;
        public CanLoginUser(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        public IEnumerable<ValidationResult> Validate(UserLoginCommand command)
        {
            User user = command.HasEmail ? _userRepository.Get(x => x.Email == command.EmailOrLogin) : _userRepository.Get(x => x.Login == command.EmailOrLogin);

            if (user == null) yield return new ValidationResult("", "Пользователя не существует");
            else if (!user.ValidatePassword(command.Password)) yield return new ValidationResult("", "Не верный пароль");
            else if (!user.Activated) yield return new ValidationResult("", "Пользователь не активирован");

        }
    }
}
