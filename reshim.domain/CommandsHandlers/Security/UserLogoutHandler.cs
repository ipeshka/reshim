﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.web.core.Auth;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UserLogoutHandler : ICommandHandler<UserLogoutCommand>
    {
        private readonly IFormsAuthentication _formAuthentication;

        public UserLogoutHandler(IFormsAuthentication formAuthentication)
        {
            _formAuthentication = formAuthentication;
        }

        public ICommandResult Execute(UserLogoutCommand command)
        {
            _formAuthentication.Signout();
            return new CommandResult(true);
        }
    }
}
