﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Security
{
    public class CanUpdatePassword : IValidationHandler<UpdatePasswordCommand>
    {
        private readonly IUserRepository _userRepository;

        public CanUpdatePassword(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public IEnumerable<ValidationResult> Validate(UpdatePasswordCommand command)
        {
            var isUserActivated = _userRepository.Get(x => x.UserId == command.UserId);
            if (isUserActivated == null)
                yield return new ValidationResult("", "Пользователя не сущесвует");

            if (isUserActivated != null && !isUserActivated.Activated)
                yield return new ValidationResult("", "Пользователь не активирован");
        }
    }

    public class CanUpdatePasswordWhereConfirm : IValidationHandler<CanUpdatePasswordValidation>
    {
        public IEnumerable<ValidationResult> Validate(CanUpdatePasswordValidation command)
        {
            if (!Md5Encrypt.Md5EncryptData(command.Email + command.Login).Equals(command.Token))
                yield return new ValidationResult("", "Не верная ссылка подтверждения.");
        }
    }
}
