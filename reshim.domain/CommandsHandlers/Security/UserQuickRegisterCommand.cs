﻿using System;
using System.Collections.ObjectModel;
using System.Transactions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.domain.Commands.Task;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UserQuickRegisterHandler : ICommandHandler<UserQuickRegisterCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommandHandler<CreateTaskCommand> _createTask;

        public UserQuickRegisterHandler(ICommandHandler<CreateTaskCommand> createTask, IUserRepository userRepository, IFileRepository fileRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _createTask = createTask;
        }

        public ICommandResult Execute(UserQuickRegisterCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var user = new User
                    {
                        Email = command.User.Email,
                        Login = command.User.Login,
                        Password = command.User.Password,
                        Role = UserRoles.User,
                        DateCreated = DateTime.Now,
                        LastLoginTime = DateTime.Now,
                        Activated = true,
                        Profile = new model.Entities.Profile() { },
                        PaymentData = new PaymentData() { Payments = new Collection<Payment>(), Withdrawals = new Collection<Withdrawal>() },
                        OrderedTasks = new Collection<model.Entities.Task>() { },
                        ComplitedTasks = new Collection<model.Entities.Task>() { },
                        Connections = new Collection<Connection>()
                    };
                    _userRepository.Add(user);
                    _unitOfWork.Commit();

                    command.Task.CustomerId = user.UserId;
                    var result = _createTask.Execute(command.Task);
                    if (!result.Success) return new CommandResult(false);

                    scope.Complete();
                    return new CommandResult(true, result.LastId);
                }
                catch
                {
                    return new CommandResult(false);
                }
            }
        }
    }
}
