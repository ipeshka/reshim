﻿using System;
using System.Collections.ObjectModel;
using System.Transactions;
using reshim.common.core.Extensions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.domain.Commands.Task;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UserRegisterFromServiceHandler : ICommandHandler<UserRegisterFromServiceCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICommandHandler<CreateTaskCommand> _createTask;

        public UserRegisterFromServiceHandler(ICommandHandler<CreateTaskCommand> createTask, IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _createTask = createTask;
        }

        public ICommandResult Execute(UserRegisterFromServiceCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                try
                {
                    var user = new User
                    {
                        IdService = command.IdService,
                        IsService = command.Service,
                        FirstName = command.FirstName,
                        LastName = command.LastName,
                        Login = command.FirstName.ToTranslit() + command.LastName.ToTranslit(),
                        Role = UserRoles.User,
                        DateCreated = DateTime.Now,
                        LastLoginTime = DateTime.Now,
                        Activated = true,
                        Photo = command.UpdatePhoto.Path,
                        Profile = new model.Entities.Profile()
                        {
                            UrlVk = command.UrlVk
                        },
                        PaymentData = new PaymentData() { Payments = new Collection<Payment>(), Withdrawals = new Collection<Withdrawal>() },
                        OrderedTasks = new Collection<model.Entities.Task>(),
                        ComplitedTasks = new Collection<model.Entities.Task>(),
                        Connections = new Collection<Connection>()
                    };

                    _userRepository.Add(user);
                    _unitOfWork.Commit();

                    if (command.CreateTask != null)
                    {
                        command.CreateTask.CustomerId = user.UserId;
                        var result = _createTask.Execute(command.CreateTask);
                        if (result.Success)
                        {
                            scope.Complete();
                            return result;
                        }
                        return new CommandResult(false);
                    }

                    scope.Complete();
                    return new CommandResult(true);
                }
                catch
                {
                    return new CommandResult(false);
                }
            }

        }
    }
}
