﻿using System;
using System.Collections.ObjectModel;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UserRegisterHandler : ICommandHandler<UserRegisterCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        public UserRegisterHandler(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(UserRegisterCommand command)
        {
            var user = new User
            {
                Email = command.Email,
                Login = command.Login,
                Password = command.Password,
                Role = UserRoles.User,
                DateCreated = DateTime.Now,
                LastLoginTime = DateTime.Now,
                Activated = true,
                Profile = new model.Entities.Profile() { },
                PaymentData = new PaymentData() { Payments = new Collection<Payment>(), Withdrawals = new Collection<Withdrawal>()},
                OrderedTasks = new Collection<model.Entities.Task>(),
                ComplitedTasks = new Collection<model.Entities.Task>(),
                Connections = new Collection<Connection>()
            };

            _userRepository.Add(user);
            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
