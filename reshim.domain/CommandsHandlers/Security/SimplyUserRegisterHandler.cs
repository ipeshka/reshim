﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Transactions;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;

namespace reshim.domain.CommandsHandlers.Security
{
    public class SimplyUserRegisterHandler : ICommandHandler<SimplyUserRegisterCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ISql _sql;
        private readonly string _prefixLogin;

        public SimplyUserRegisterHandler(IUserRepository userRepository, IUnitOfWork unitOfWork, ISql sql)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _sql = sql;
            _prefixLogin = ConfigurationManager.AppSettings["DefaultPrefixUser"];
        }

        public ICommandResult Execute(SimplyUserRegisterCommand command)
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
            {
                var history = new HistoryCompletedTask()
                {
                    CompletedTaskId = command.CompletedTaskId,
                    Create = DateTime.Now,
                    State = PaymentState.New,
                    PaywayVia = command.PaywayVia
                };

                var user = new User
                {
                    Email = command.Email,
                    Role = UserRoles.User,
                    DateCreated = DateTime.Now,
                    LastLoginTime = DateTime.Now,
                    Activated = true,
                    Profile = new model.Entities.Profile() { },
                    PaymentData =
                    new PaymentData()
                    {
                        Payments = new Collection<Payment>(),
                        Withdrawals = new Collection<Withdrawal>()
                    },
                    OrderedTasks = new Collection<model.Entities.Task>(),
                    ComplitedTasks = new Collection<model.Entities.Task>(),
                    Connections = new Collection<Connection>(),
                    HistoriesCompletedTasks = new Collection<HistoryCompletedTask>() { }
                };

                try
                {
                    var login = _sql.ExecuteQuery<string>(string.Format("SELECT CONVERT(varchar(128), '{0}'+ CONVERT(varchar(128), (SELECT MAX(UserId) + 1 FROM Users)))", _prefixLogin)).FirstOrDefault();
                    user.Login = login;
                    user.ResetPassword();
                    _userRepository.Add(user);
                    _unitOfWork.Commit();

                    user.HistoriesCompletedTasks.Add(history);
                    _unitOfWork.Commit();

                    scope.Complete();
                }
                catch
                {
                    return new CommandResult(false);
                }

                return new CommandResult(true, history.Id);
            }
        }
    }
}
