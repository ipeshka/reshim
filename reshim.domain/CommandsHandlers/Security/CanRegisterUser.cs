﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Security
{
    public class CanRegisterUser : IValidationHandler<UserRegisterCommand>
    {
        private readonly IUserRepository _userRepository;
        public CanRegisterUser(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        public IEnumerable<ValidationResult> Validate(UserRegisterCommand command)
        {
            var isUserExists = _userRepository.Get(c => c.Email == command.Email);

            if (isUserExists != null)
            {
                yield return new ValidationResult("", "Email уже существует в нашей система");
            }

            isUserExists = _userRepository.Get(c => c.Login == command.Login);

            if (isUserExists != null)
            {
                yield return new ValidationResult("", "Логин уже сущесвует в нашей системе");
            }
        }
    }

    public class CanSimplyRegisterUser : IValidationHandler<SimplyUserRegisterCommand>
    {
        private readonly IUserRepository _userRepository;
        public CanSimplyRegisterUser(IUserRepository userRepository)
        {
            this._userRepository = userRepository;
        }
        public IEnumerable<ValidationResult> Validate(SimplyUserRegisterCommand command)
        {
            var isUserExists = _userRepository.Get(c => c.Email == command.Email);

            if (isUserExists != null)
            {
                yield return new ValidationResult("", "Email уже существует в нашей система");
            }
        }
    }
}
