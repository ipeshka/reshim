﻿using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Database;
using reshim.data.infrastructure.Repositories;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UpdatePasswordHandler : ICommandHandler<UpdatePasswordCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public UpdatePasswordHandler(IUserRepository userRepository, IUnitOfWork unitOfWork)
        {
            this._userRepository = userRepository;
            this._unitOfWork = unitOfWork;
        }

        public ICommandResult Execute(UpdatePasswordCommand command)
        {
            var user = _userRepository.Get(x => x.UserId == command.UserId);
            user.Password = command.Password;
            _userRepository.Update(user);
            _unitOfWork.Commit();
            return new CommandResult(true);
        }
    }
}
