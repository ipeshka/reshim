﻿using System.Web;
using AutoMapper;
using reshim.data.commandProcessor.Command;
using reshim.domain.Commands.Security;
using reshim.data.infrastructure.Repositories;
using reshim.model.Entities;
using reshim.model.view.ViewModels.User;
using reshim.web.core.Auth;

namespace reshim.domain.CommandsHandlers.Security
{
    public class UserLogonHandler : ICommandHandler<UserLogonCommand>
    {
        private readonly IUserRepository _userRepository;
        private readonly IFormsAuthentication _formAuthentication;

        public UserLogonHandler(IUserRepository userRepository, IFormsAuthentication formAuthentication)
        {
            _userRepository = userRepository;
            _formAuthentication = formAuthentication;
        }

        public ICommandResult Execute(UserLogonCommand command)
        {
            var usr = command.HasEmail ? _userRepository.Get(x => x.Email == command.LoginOrEmail) 
                : _userRepository.Get(x => x.Login == command.LoginOrEmail);

            _formAuthentication.SetAuthCookie(HttpContext.Current,
                UserAuthenticationTicketBuilder.CreateAuthenticationTicket(Mapper.Map<User, UserViewModel>(usr)));
            return new CommandResult(true);
        }
    }
}
