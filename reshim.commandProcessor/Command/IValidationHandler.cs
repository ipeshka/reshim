﻿using System.Collections.Generic;
using reshim.common;

namespace reshim.data.commandProcessor.Command {
    public interface IValidationHandler<in TCommand> where TCommand : ICommand {
        IEnumerable<ValidationResult> Validate(TCommand command);
    }
}
