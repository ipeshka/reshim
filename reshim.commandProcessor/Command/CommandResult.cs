﻿namespace reshim.data.commandProcessor.Command {
    public class CommandResult : ICommandResult {
        public CommandResult(bool success) {
            Success = success;
        }

        public CommandResult(bool success, int lastId)
        {
            Success = success;
            LastId = lastId;
        }

        public bool Success { get; protected set; }
        public int LastId { get; protected set; }
    }
}
