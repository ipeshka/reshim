﻿namespace reshim.data.commandProcessor.Command {
    public interface ICommandResult {
        bool Success { get; }
        int LastId { get; }
    }
}
