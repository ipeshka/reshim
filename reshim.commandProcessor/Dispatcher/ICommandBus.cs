﻿using System.Collections.Generic;
using reshim.data.commandProcessor.Command;
using reshim.common;

namespace reshim.data.commandProcessor.Dispatcher {
    public interface ICommandBus {
        ICommandResult Submit<TCommand>(TCommand command) where TCommand : ICommand;
        IEnumerable<ValidationResult> Validate<TCommand>(TCommand command) where TCommand : ICommand;
    }
}
