﻿using System.Collections.Generic;
using Autofac;
using reshim.data.commandProcessor.Command;
using reshim.common;

namespace reshim.data.commandProcessor.Dispatcher
{
    public class DefaultCommandBus : ICommandBus
    {
        private readonly IComponentContext _container;

        public DefaultCommandBus(IComponentContext container)
        {
            _container = container;
        }

        public ICommandResult Submit<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = _container.Resolve<ICommandHandler<TCommand>>();
            if (!((handler != null) && handler is ICommandHandler<TCommand>))
            {
                throw new CommandHandlerNotFoundException(typeof(TCommand));
            }
            return handler.Execute(command);
        }

        public IEnumerable<ValidationResult> Validate<TCommand>(TCommand command) where TCommand : ICommand
        {
            var handler = _container.Resolve<IValidationHandler<TCommand>>();
            if (!((handler != null) && handler is IValidationHandler<TCommand>))
            {
                throw new ValidationHandlerNotFoundException(typeof(TCommand));
            }
            return handler.Validate(command);
        }
    }
}
