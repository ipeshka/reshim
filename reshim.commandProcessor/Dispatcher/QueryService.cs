﻿using Autofac;
using reshim.data.commandProcessor.Query;

namespace reshim.data.commandProcessor.Dispatcher
{
    public class QueryService : IQueryService
    {
        private readonly IComponentContext _container;

        public QueryService(IComponentContext container)
        {
            _container = container;
        }

        public TResult ExecuteQuery<TResult>(IQuery<TResult> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));
            var handler = _container.Resolve(handlerType);
            return (TResult)((dynamic)handler).Execute((dynamic)query);
        }
    }
}
