﻿using reshim.data.commandProcessor.Query;

namespace reshim.data.commandProcessor.Dispatcher
{
    public interface IQueryService
    {
        TResult ExecuteQuery<TResult>(IQuery<TResult> query);
    }
}
