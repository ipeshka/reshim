﻿using reshim.databus.handler.email.Services;
using Topshelf;

namespace reshim.databus.handler.email
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<EmailService>(s =>
                {
                    s.ConstructUsing(name => new EmailService());
                    s.WhenStarted(ts => ts.Start());
                    s.WhenStopped(ts => ts.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Reshim service consumer emails");
                x.SetDisplayName("Reshim.Consumers.Email");
                x.SetServiceName("Reshim.Consumers.Email");
            });
        }
    }
}
