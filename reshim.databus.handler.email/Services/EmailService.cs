﻿using System;
using System.Configuration;
using MassTransit;
using reshim.databus.model.consumers.Consumer.Alerts;
using reshim.databus.model.consumers.Consumer.SystemMail;

namespace reshim.databus.handler.email.Services
{
    class EmailService
    {
        private readonly IBusControl _busControl;

        public EmailService()
        {
            var uriString = ConfigurationManager.AppSettings["uri"];
            var user = ConfigurationManager.AppSettings["user"];
            var password = ConfigurationManager.AppSettings["password"];

            _busControl = Bus.Factory.CreateUsingRabbitMq(x =>
            {
                var host = x.Host(new Uri(uriString), h =>
                {
                    h.Username(user);
                    h.Password(password);
                });
                x.ReceiveEndpoint(host, "email", e =>
                {
                    e.Consumer<RegistrationConsumer>();
                    e.Consumer<RestoryPasswordConsumer>();
                    e.Consumer<CompletedTaskConsumer>();
                    e.Consumer<AlertsOfTasksConsumer>();
                });
            });
        }

        public void Start()
        {
            _busControl.Start();
        }

        public void Stop()
        {
            _busControl.Stop();
        }
    }
}
