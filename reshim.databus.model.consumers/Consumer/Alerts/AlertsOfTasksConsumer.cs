﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Model;
using MassTransit;
using reshim.databus.model.contracts.Contracts.Alerts;
using RazorEngine;

namespace reshim.databus.model.consumers.Consumer.Alerts
{
    public class AlertsOfTasksConsumer : IConsumer<AlertNewTask>
    {
        public Task Consume(ConsumeContext<AlertNewTask> context)
        {
            var rootPath = Environment.CurrentDirectory;
            var mandrillApi = ConfigurationManager.AppSettings["mandrillApi"];
            var sendFrom = ConfigurationManager.AppSettings["sendFrom"];
            var sendToAlerts = ConfigurationManager.AppSettings["sendToAlerts"];
            var toEmails = sendToAlerts.Split(new[] { "," }, StringSplitOptions.None).Select(x => new MandrillMailAddress(x));

            var template = File.ReadAllText(rootPath + @"\EmailTemplates\Alerts\NewTask.html");
            string html = Razor.Parse(template, string.Format("{0}/task/detail/{1}", context.Message.Domain, context.Message.TaskId));

            var api = new MandrillApi(mandrillApi);
            var message = new MandrillMessage
            {
                To = toEmails.ToList(),
                FromEmail = sendFrom,
                Subject = "Заказ готовой задачи на сайте reshim.net",
                Html = html
            };
            api.Messages.Send(message);
            return Task.FromResult(0);
        }
    }
}
