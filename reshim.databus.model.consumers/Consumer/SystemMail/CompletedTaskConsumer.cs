﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Model;
using MassTransit;
using reshim.databus.model.contracts.Contracts.SystemMail;
using RazorEngine;

namespace reshim.databus.model.consumers.Consumer.SystemMail
{
    public class CompletedTaskConsumer : IConsumer<SendCompletedTask>
    {
        public Task Consume(ConsumeContext<SendCompletedTask> context)
        {
            var rootPath = Environment.CurrentDirectory;
            var mandrillApi = ConfigurationManager.AppSettings["mandrillApi"];
            var sendFrom = ConfigurationManager.AppSettings["sendFrom"];

            var template = File.ReadAllText(rootPath + @"\EmailTemplates\SendCompletedTask.html");
            string html = Razor.Parse(template);

            var api = new MandrillApi(mandrillApi);
            var message = new MandrillMessage(sendFrom, context.Message.Email, "Заказ готовой задачи на сайте reshim.net", html);
            var attachment = new MandrillAttachment
            {
                Name = context.Message.FileName,
                Content = context.Message.File
            };
            message.Attachments.Add(attachment);
            api.Messages.Send(message);
            return Task.FromResult(0);
        }
    }
}
