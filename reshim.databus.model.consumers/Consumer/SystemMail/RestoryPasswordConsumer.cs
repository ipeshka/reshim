﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Model;
using MassTransit;
using reshim.common;
using reshim.databus.model.contracts.Contracts;
using reshim.databus.model.contracts.Contracts.SystemMail;
using reshim.model.view.ViewModels.Mail;
using RazorEngine;

namespace reshim.databus.model.consumers.Consumer.SystemMail
{
    public class RestoryPasswordConsumer : IConsumer<RestoryPassword>, IConsumer<RestoryPasswordSuccess>
    {
        private const string PathForgot = @"{0}\auth\forgotpassword?token={1}&login={2}";

        public Task Consume(ConsumeContext<RestoryPassword> context)
        {
            var rootPath = Environment.CurrentDirectory;
            var mandrillApi = ConfigurationManager.AppSettings["mandrillApi"];
            var sendFrom = ConfigurationManager.AppSettings["sendFrom"];

            var template = File.ReadAllText(rootPath + @"\EmailTemplates\RestoryPassword.html");
            var model = new ForgotPasswordViewModel()
            {
                Login = context.Message.Login,
                Link = string.Format(PathForgot, context.Message.Site, Md5Encrypt.Md5EncryptData(context.Message.Email + context.Message.Login), context.Message.Login)
            };

            string html = Razor.Parse(template, model);

            var api = new MandrillApi(mandrillApi);
            var message = new MandrillMessage(sendFrom, context.Message.Email, "Восстановление пароля на сайте reshim.net пользователя " + context.Message.Login, html);
            api.Messages.Send(message);
            return Task.FromResult(0);
        }

        public Task Consume(ConsumeContext<RestoryPasswordSuccess> context)
        {
            var rootPath = Environment.CurrentDirectory;
            var mandrillApi = ConfigurationManager.AppSettings["mandrillApi"];
            var sendFrom = ConfigurationManager.AppSettings["sendFrom"];

            var template = File.ReadAllText(rootPath + @"\EmailTemplates\RestoryPasswordSuccess.html");
            string html = Razor.Parse(template, context.Message.Login);

            var api = new MandrillApi(mandrillApi);
            var message = new MandrillMessage(sendFrom, context.Message.Email, "Успешное восстановление пароля на сайте reshim.net полязователя " + context.Message.Login, html);
            api.Messages.Send(message);
            return Task.FromResult(0);
        }
    }
}
