﻿using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Mandrill;
using Mandrill.Model;
using MassTransit;
using reshim.databus.model.contracts.Contracts;
using reshim.databus.model.contracts.Contracts.SystemMail;
using reshim.model.view.ViewModels.Mail;
using RazorEngine;

namespace reshim.databus.model.consumers.Consumer.SystemMail
{
    public class RegistrationConsumer : IConsumer<Registration>
    {
        public Task Consume(ConsumeContext<Registration> context)
        {
            var rootPath = Environment.CurrentDirectory;
            var mandrillApi = ConfigurationManager.AppSettings["mandrillApi"];
            var sendFrom = ConfigurationManager.AppSettings["sendFrom"];

            var template = File.ReadAllText(rootPath+ @"\EmailTemplates\ConfirmRegistration.html");
            var model = new ConfirmRegistrationViewModel() { Login = context.Message.Login };
            string html = Razor.Parse(template, model);

            var api = new MandrillApi(mandrillApi);
            var message = new MandrillMessage(sendFrom, context.Message.Email, "Регистрации на сайте reshim.net пользователя " + context.Message.Login, html);
            api.Messages.Send(message);
            return Task.FromResult(0);
        }
    }
}
